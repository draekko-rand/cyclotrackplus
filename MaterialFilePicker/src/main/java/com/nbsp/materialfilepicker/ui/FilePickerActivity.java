package com.nbsp.materialfilepicker.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.draekko.iconcontextmenu.IIconContextItemSelectedListener;
import com.draekko.iconcontextmenu.IconContextMenu;
import com.nbsp.materialfilepicker.R;
import com.nbsp.materialfilepicker.filter.FileFilter;
import com.nbsp.materialfilepicker.filter.PatternFilter;
import com.nbsp.materialfilepicker.utils.FileUtils;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class FilePickerActivity extends AppCompatActivity implements DirectoryFragment.FileClickListener {
    private static final String TAG = "FilePickerActivity";

    public static final String ARG_START_FILE = "arg_start_path";
    public static final String ARG_START_FILE_1 = "arg_start_path1";
    public static final String ARG_START_FILE_2 = "arg_start_path2";
    public static final String ARG_CURRENT_FILE = "arg_current_path";

    public static final String ARG_FILTER = "arg_filter";
    public static final String ARG_CLOSEABLE = "arg_closeable";
    public static final String ARG_TITLE = "arg_title";

    public static final String STATE_START_FILE = "state_start_path";
    public static final String STATE_START_FILE_1 = "state_start_path_1";
    public static final String STATE_START_FILE_2 = "state_start_path_2";
    public static final String RESULT_FILE_PATH = "result_file_path";
    private static final String STATE_CURRENT_FILE = "state_current_path";
    private static final int HANDLE_CLICK_DELAY = 150;

    private Toolbar mToolbar;

    private File mStart = null;
    private File mStart2 = null;
    private File mCurrent = null;

    private CharSequence mTitle;

    private Boolean mCloseable = true;

    private FileFilter mFilter;

    private Context currentContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentContext = FilePickerActivity.this;

        setContentView(R.layout.activity_file_picker);

        //  deepcode ignore JavaSameEvalBinaryExpressiontrue: <Validation within the life cycle of the activity.>
        if (mStart == null) {
            String mStorage = ContextCompat.getExternalFilesDirs(getApplicationContext(), null)[0].getPath();

            if (mStorage.contains("/Android/data")) {
                int index = mStorage.indexOf("/Android/data");
                mStorage = mStorage.substring(0, index);
            }

            mStart = new File(mStorage);
            mCurrent = mStart;
        }

        initArguments(savedInstanceState);
        initViews();
        initToolbar();

        if (savedInstanceState == null) {
            initBackStackState();
        }
    }

    private void initArguments(Bundle savedInstanceState) {
        if (getIntent().hasExtra(ARG_FILTER)) {
            Serializable filter = getIntent().getSerializableExtra(ARG_FILTER);

            if (filter instanceof Pattern) {
                mFilter = new PatternFilter((Pattern) filter, false);
            } else {
                mFilter = (FileFilter) filter;
            }
        }

        if (savedInstanceState != null) {
            mStart = (File) savedInstanceState.getSerializable(STATE_START_FILE);
            mStart2 = (File) savedInstanceState.getSerializable(STATE_START_FILE_2);
            if (mStart2 != null) {
                mStart = (File) savedInstanceState.getSerializable(STATE_START_FILE_1);
            }
            mCurrent = (File) savedInstanceState.getSerializable(STATE_CURRENT_FILE);
            updateTitle();
        } else {
            if (getIntent().hasExtra(ARG_START_FILE)) {
                mStart = (File) getIntent().getSerializableExtra(ARG_START_FILE);
                mCurrent = mStart;
            }
            if (getIntent().hasExtra(ARG_START_FILE_2)) {
                mStart2 = (File) getIntent().getSerializableExtra(ARG_START_FILE_2);
            }
            if (mStart2 != null && getIntent().hasExtra(ARG_START_FILE_1)) {
                mStart = (File) getIntent().getSerializableExtra(ARG_START_FILE_1);
                mCurrent = mStart;
            }

            if (getIntent().hasExtra(ARG_CURRENT_FILE)) {
                File currentFile = (File) getIntent().getSerializableExtra(ARG_CURRENT_FILE);

                if (FileUtils.isParent(currentFile, mStart)) {
                    mCurrent = currentFile;
                }
            }
        }

        if (getIntent().hasExtra(ARG_TITLE)) {
            mTitle = getIntent().getCharSequenceExtra(ARG_TITLE);
        }

        if (getIntent().hasExtra(ARG_CLOSEABLE)) {
            mCloseable = getIntent().getBooleanExtra(ARG_CLOSEABLE, true);
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);

        // Truncate start of path
        try {
            Field f;
            if (TextUtils.isEmpty(mTitle)) {
                f = mToolbar.getClass().getDeclaredField("mTitleTextView");
            } else {
                f = mToolbar.getClass().getDeclaredField("mSubtitleTextView");
            }

            f.setAccessible(true);
            TextView textView = (TextView) f.get(mToolbar);
            textView.setEllipsize(TextUtils.TruncateAt.START);
        } catch (Exception ignored) {
        }

        if (!TextUtils.isEmpty(mTitle)) {
            setTitle(mTitle);
        }
        updateTitle();
    }

    private void initViews() {
        mToolbar = findViewById(R.id.toolbar);
    }

    private void initBackStackState() {
        final List<File> path = new ArrayList<>();

        File current = mCurrent;

        while (current != null) {
            path.add(current);

            if (current.equals(mStart)) {
                break;
            }

            current = FileUtils.getParentOrNull(current);
        }

        Collections.reverse(path);

        for (File file : path) {
            addFragmentToBackStack(file);
        }
    }

    private TextView textview;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mStart != null && mStart2 != null) {
                IconContextMenu iconContextMenu = new IconContextMenu(
                        currentContext,
                        R.menu.storage_path_section_menu,
                        IconContextMenu.THEME_DARK,
                        IconContextMenu.SANS);

                iconContextMenu.setOnIconContextItemSelectedListener(new IIconContextItemSelectedListener() {
                    @Override
                    public void onIconContextItemSelected(MenuItem c_item, Object c_info) {
                        int id = c_item.getItemId();
                        if (id == R.id.action_select_internal) {
                            mCurrent = mStart;
                            updateTitle();
                            addFragmentToBackStack(mCurrent);
                        }
                        if (id == R.id.action_select_external) {
                            mCurrent = mStart2;
                            updateTitle();
                            addFragmentToBackStack(mCurrent);
                        }
                    }
                });

                iconContextMenu.show();
            }
        }
    };

    private void updateTitle() {
        if (getSupportActionBar() != null) {
            //final String titlePath = mCurrent.getAbsolutePath();
            // Show only path name
            final String titlePath = mCurrent.getName();

            if (mCurrent.getPath().equals(mStart.getPath()) ||
                    (mStart2 != null && mCurrent.getPath().equals(mStart2.getPath()))) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                if (textview == null) {
                    try {
                        Field titleField = Toolbar.class.getDeclaredField("mTitleTextView");
                        titleField.setAccessible(true);
                        if (mToolbar != null) {
                            textview = (TextView)titleField.get(mToolbar);
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (NoSuchFieldException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (textview != null) {
                    textview.setOnClickListener(clickListener);
                }
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                if (textview == null) {
                    try {
                        Field titleField = Toolbar.class.getDeclaredField("mTitleTextView");
                        titleField.setAccessible(true);
                        if (mToolbar != null) {
                            textview = (TextView)titleField.get(mToolbar);
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (NoSuchFieldException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (textview != null) {
                    textview.setOnClickListener(null);
                }
            }

            if (TextUtils.isEmpty(mTitle)) {
                getSupportActionBar().setTitle(titlePath);
            } else {
                getSupportActionBar().setSubtitle(titlePath);
            }
        }
    }

    private void addFragmentToBackStack(File file) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, DirectoryFragment.getInstance(file, mFilter))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        menu.findItem(R.id.action_close).setVisible(mCloseable);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (menuItem.getItemId() == R.id.action_close) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            mCurrent = FileUtils.getParentOrNull(mCurrent);
            updateTitle();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_CURRENT_FILE, mCurrent);
        outState.putSerializable(STATE_START_FILE, mStart);
    }

    @Override
    public void onFileClicked(final File clickedFile) {
        new Handler().postDelayed(() -> handleFileClicked(clickedFile), HANDLE_CLICK_DELAY);
    }

    private void handleFileClicked(final File clickedFile) {
        if (isFinishing()) {
            return;
        }
        if (clickedFile.isDirectory()) {
            mCurrent = clickedFile;
            // If the user wanna go to the emulated directory, he will be taken to the
            // corresponding user emulated folder.
            if (mCurrent.getAbsolutePath().equals("/storage/emulated")) {
                mCurrent = Environment.getExternalStorageDirectory();
            }
            addFragmentToBackStack(mCurrent);
            updateTitle();
        } else {
            setResultAndFinish(clickedFile);
        }
    }

    private void setResultAndFinish(File file) {
        Intent data = new Intent();
        data.putExtra(RESULT_FILE_PATH, file.getPath());
        setResult(RESULT_OK, data);
        finish();
    }
}
