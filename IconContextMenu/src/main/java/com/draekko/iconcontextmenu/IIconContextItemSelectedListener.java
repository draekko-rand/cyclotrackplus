/**************************************************************************

  @copyright © 2013-2021 Benoit Touchette, all rights reserved.

 *************************************************************************/

package com.draekko.iconcontextmenu;

import android.view.MenuItem;

public interface IIconContextItemSelectedListener {
    void onIconContextItemSelected(MenuItem c_item, Object c_info);
}
