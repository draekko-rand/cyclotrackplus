/*************************************************************************

 @copyright © 2013-2021 Benoit Touchette, all rights reserved.

 *************************************************************************/

package com.draekko.iconcontextmenu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;

public class IconContextMenu {

    public final static int THEME_DARK = 0x1;
    public final static int THEME_LIGHT = 0x2;

    public final static int SANS = 0x1;
    public final static int SERIF = 0x2;

    private Typeface typeFaceRegular;
    private Typeface typeFaceBold;
    private final AlertDialog dialog;
    private final Menu menu;
    private IIconContextItemSelectedListener iconContextItemSelectedListener;
    private Object info;
    private boolean darkTheme;
    private int themeid;
    private int typeface;
    final IconContextMenuAdapter adapter;

    /**
     * @param c_context
     * @param c_menuId
     * @param c_theme
     */
    public IconContextMenu(Context c_context, int c_menuId, int c_theme, int c_typeface) {
        this(c_context, newMenu(c_context, c_menuId), c_theme, c_typeface);
    }

    /**
     * @param c_context
     * @param c_menuId
     * @param c_theme
     */
    public IconContextMenu(Context c_context, int c_menuId, int c_theme, int c_typeface, boolean c_tint) {
        this(c_context, newMenu(c_context, c_menuId), c_theme, c_typeface, c_tint);
    }


    /**
     * @param c_context
     * @param c_menuId
     * @return
     */
    public static Menu newMenu(Context c_context, int c_menuId) {
        Menu menu = new MenuBuilder(c_context);
        new MenuInflater(c_context).inflate(c_menuId, menu);
        return menu;
    }


    /**
     * @param c_context
     * @param c_menu
     * @param c_theme
     * @param c_typeface
     */
    public IconContextMenu(Context c_context, Menu c_menu, int c_theme, int c_typeface) {
        this(c_context, c_menu, c_theme, c_typeface, true);
    }

    /**
     * @param c_context
     * @param c_menu
     * @param c_theme
     * @param c_typeface
     * @param c_tint
     */
    public IconContextMenu(Context c_context, Menu c_menu, int c_theme, int c_typeface, boolean c_tint) {
        this(c_context, c_menu, c_theme, c_typeface, c_tint, false);
    }

    /**
     * @param c_context
     * @param c_menu
     * @param c_theme
     * @param c_typeface
     * @param c_tint
     */
    public IconContextMenu(Context c_context, Menu c_menu, int c_theme, int c_typeface, boolean c_tint, boolean c_isbutton) {
        menu = c_menu;
        typeface = c_typeface;
        themeid = R.style.Theme_IconContextMenu_Light;
        darkTheme = (c_theme == THEME_DARK);
        if (darkTheme) {
            themeid = R.style.Theme_IconContextMenu_Dark;
        }

        if (typeface == 2) {
            typeFaceBold = Typeface.SERIF;
            typeFaceRegular = Typeface.SERIF;
        } else {
            typeFaceBold = Typeface.SANS_SERIF;
            typeFaceRegular = Typeface.SANS_SERIF;
        }

        adapter = new IconContextMenuAdapter(c_context, menu, c_theme, typeface, c_tint, c_isbutton);
        final ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(c_context, themeid);
        dialog = new AlertDialog.Builder(contextThemeWrapper).setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (iconContextItemSelectedListener != null) {
                    iconContextItemSelectedListener.onIconContextItemSelected(adapter.getItem(which), info);
                }
            }
        }).create();
        dialog.setInverseBackgroundForced(true);
    }

    /**
     *
     * @param c_context
     * @param c_menu
     * @param c_positiveButtonText
     * @param c_negativeButtonText
     * @param c_theme
     * @param c_typeface
     */
    public IconContextMenu(final Context c_context,
                           final Menu c_menu,
                           final String c_positiveButtonText,
                           final String c_negativeButtonText,
                           final int c_theme,
                           final int c_typeface) {
        this(c_context, c_menu, c_positiveButtonText, c_negativeButtonText, c_theme, c_typeface, true);
    }

    /**
     *
     * @param c_context
     * @param c_menu
     * @param c_positiveButtonText
     * @param c_negativeButtonText
     * @param c_theme
     * @param c_typeface
     * @param c_tint
     */
    public IconContextMenu(final Context c_context,
                           final Menu c_menu,
                           final String c_positiveButtonText,
                           final String c_negativeButtonText,
                           final int c_theme,
                           final int c_typeface,
                           final boolean c_tint) {
        this(c_context, c_menu, c_positiveButtonText, c_negativeButtonText, c_theme, c_typeface, c_tint, false);
    }

    /**
     *
     * @param c_context
     * @param c_menu
     * @param c_positiveButtonText
     * @param c_negativeButtonText
     * @param c_theme
     * @param c_typeface
     * @param c_tint
     * @param c_isbutton
     */
    public IconContextMenu(final Context c_context,
                           final Menu c_menu,
                           final String c_positiveButtonText,
                           final String c_negativeButtonText,
                           final int c_theme,
                           final int c_typeface,
                           final boolean c_tint,
                           final boolean c_isbutton) {
        menu = c_menu;
        themeid = R.style.Theme_IconContextMenu_Light;
        typeface = c_typeface;
        darkTheme = (c_theme == THEME_DARK);
        if (darkTheme) {
            themeid = R.style.Theme_IconContextMenu_Dark;
        }

        if (typeface == 2) {
            typeFaceBold = Typeface.SERIF;
            typeFaceRegular = Typeface.SERIF;
        } else {
            typeFaceBold = Typeface.SANS_SERIF;
            typeFaceRegular = Typeface.SANS_SERIF;
        }

        adapter = new IconContextMenuAdapter(c_context, menu, c_theme, c_typeface, c_tint, c_isbutton);
        final ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(c_context, themeid);
        dialog = new AlertDialog.
                Builder(contextThemeWrapper).
                setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (iconContextItemSelectedListener != null) {
                            iconContextItemSelectedListener.onIconContextItemSelected(adapter.getItem(which), info);
                        }
                    }
                }).
                setPositiveButton(c_positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).
                setNegativeButton(c_negativeButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).
                create();
        dialog.setInverseBackgroundForced(true);
    }

    /**
     *
     * @return
     */
    public boolean isTintOn() {
        return adapter.isTintOn();
    }

    /**
     *
     * @param tintOn
     */
    public void setTintOn(boolean tintOn) {
        adapter.setTintOn(tintOn);
    }

    /**
     *
     * @param c_typeFaceRegular
     */
    public void setTypeFaceRegular(Typeface c_typeFaceRegular) {
        typeFaceRegular = c_typeFaceRegular;
    }

    /**
     *
     * @param c_typeFaceBold
     */
    public void setTypeFaceBold(Typeface c_typeFaceBold) {
        typeFaceRegular = c_typeFaceBold;
    }

    /**
     *
     * @param c_info
     */
	public void setInfo(Object c_info) {
		info = c_info;
	}

    /**
     *
     * @return
     */
	public Object getInfo() {
		return info;
	}

    /**
     *
     * @return
     */
	public Menu getMenu() {
		return menu;
	}

    /**
     *
     * @param c_iconContextItemSelectedListener
     */
    public void setOnIconContextItemSelectedListener(IIconContextItemSelectedListener c_iconContextItemSelectedListener) {
        iconContextItemSelectedListener = c_iconContextItemSelectedListener;
    }

    /**
     *
     * @param onCancelListener
     */
    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
    	dialog.setOnCancelListener(onCancelListener);
    }

    /**
     *
     * @param onDismissListener
     */
    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
    	dialog.setOnDismissListener(onDismissListener);
    }

    /**
     *
     * @param title
     */
    public void setTitle(CharSequence title) {
    	dialog.setTitle(title);
    }

    /**
     *
     * @param titleId
     */
    public void setTitle(int titleId) {
    	dialog.setTitle(titleId);
    }

    /**
     *
     */
    public void setFastScroll() {
        dialog.getListView().setFastScrollEnabled(true);
        dialog.getListView().setFastScrollAlwaysVisible(true);
    }

    /**
     *
     */
    public void show() {
    	dialog.show();
    }

    /**
     *
     */
    public void dismiss() {
    	dialog.dismiss();
    }

    /**
     *
     */
    public void cancel() {
    	dialog.cancel();
    }

    /**
     *
     * @return
     */
    public AlertDialog getDialog() {
    	return dialog;
    }
}