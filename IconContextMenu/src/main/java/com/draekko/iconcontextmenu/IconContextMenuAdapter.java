/*************************************************************************

 @copyright © 2013-2021 Benoit Touchette, all rights reserved.

 *************************************************************************/

package com.draekko.iconcontextmenu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import java.security.InvalidParameterException;

public class IconContextMenuAdapter extends BaseAdapter {

    private Typeface typeFaceRegular;
    private Typeface typeFaceBold;
    private static Context context;
    private Menu menu;
    private int themeid;
    private int theme;
    private int typeface;
    private boolean tint = true;
    private boolean isbutton = false;

    /**
     * IconContextMenuAdapter
     * @param context
     * @param menu
     * @param c_theme
     * @param c_typeface
     */
    public IconContextMenuAdapter(Context context, Menu menu, int c_theme, int c_typeface, boolean c_tint) {
        this(context, menu, c_theme, c_typeface, c_tint, false);
    }

    /**
     * IconContextMenuAdapter
     * @param context
     * @param menu
     * @param c_theme
     * @param c_typeface
     */
    public IconContextMenuAdapter(Context context, Menu menu, int c_theme, int c_typeface, boolean c_tint, boolean c_button) {
        this.context = context;
        this.menu = menu;
        this.typeface = c_typeface;
        this.tint = c_tint;
        this.isbutton = c_button;
        themeid = R.drawable.selector_light;
        theme = c_theme;
        if (theme == 1) {
            themeid = R.drawable.selector_dark;
        }
        if (typeface == 2) {
            typeFaceBold = Typeface.SERIF;
            typeFaceRegular = Typeface.SERIF;
        } else {
            typeFaceBold = Typeface.SANS_SERIF;
            typeFaceRegular = Typeface.SANS_SERIF;
        }
    }

    /**
     *
     * @param c_typeFaceRegular
     */
    public void setTypeFaceRegular(Typeface c_typeFaceRegular) {
        typeFaceRegular = c_typeFaceRegular;
    }

    /**
     *
     * @param c_typeFaceBold
     */
    public void setTypeFaceBold(Typeface c_typeFaceBold) {
        typeFaceRegular = c_typeFaceBold;
    }

    /**
     *
     * @return
     */
	@Override
	public int getCount() {
		return menu.size();
	}

    /**
     *
     * @param position
     * @return
     */
	@Override
	public MenuItem getItem(int position) {
		return menu.getItem(position);
	}

    /**
     *
     * @param position
     * @return
     */
	@Override
	public long getItemId(int position) {
		return getItem(position).getItemId();
	}

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuItem item = getItem(position);
        Drawable drawable = item.getIcon();
        int id = R.layout.select_dialog_item;
        if (isbutton) {
            id = R.layout.select_dialog_item_button;
        }
        View rootView = View.inflate(context, id, null);
        TextView textView = (TextView) rootView.findViewById(R.id.text1);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageview);

        if (theme == 1) {
            textView.setTextColor(context.getColor(R.color.white));
        } else {
            textView.setTextColor(context.getColor(R.color.black));
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            rootView.setBackgroundDrawable(ContextCompat.getDrawable(context, themeid));
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            rootView.setBackground(ContextCompat.getDrawable(context, themeid));
        } else {
            rootView.setBackground(ContextCompat.getDrawable(context, themeid));
        }

        if (drawable != null) {
            int h = drawable.getIntrinsicHeight();
            int w = drawable.getIntrinsicWidth();
            if (h == -1 && w == -1) {
                h = getDpValue(context, 24);
                w = h;
                Bitmap temp = drawableToBitmap(drawable, w, h);
                drawable = new BitmapDrawable(context.getResources(), temp);
            } else if ((h <= 0) && (w <= 0)) {
                drawable = null;
            }
        } else {
            drawable = null;
        }

        if (drawable != null) {
            if (tint) {
                int color = R.color.black;
                if (theme == 1) {
                    color = R.color.white;
                }
                ColorStateList colorStateList = context.getColorStateList(color);
                imageView.setImageTintList(colorStateList);
            }
            imageView.setImageDrawable(drawable);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }

        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setTag(item);
        textView.setText(" " + item.getTitle());
        textView.setTypeface(typeFaceRegular);

        return rootView;
    }

    /**
     *
     * @param drawable
     * @param height
     * @param width
     * @return
     */
    public static Bitmap drawableToBitmap (Drawable drawable, int height, int width) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     *
     * @param c_context
     * @param c_height
     * @return
     */
    public static int getDpValue(Context c_context, int c_height) {
        if (c_context == null) throw new InvalidParameterException("c_context == null");
        DisplayMetrics displayMetrics = c_context.getResources().getDisplayMetrics();
        int type = TypedValue.COMPLEX_UNIT_DIP;
        return (int) TypedValue.applyDimension(type, c_height, displayMetrics);
    }

    /**
     *
     * @return
     */
    public boolean isTintOn() {
        return tint;
    }

    public void setTintOn(boolean tintOn) {
        tint = tintOn;
    }
}
