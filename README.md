Cyclometer
===
Android bicycle computer and stats tracking app.

--- 

* Renamed project to cyclometer.
* Modded for offline maps.
* Uses Mapsforge compatible maps
* Removed Google Firebase analytics data catcher.
* Removed Google Maps code in favor of offline code via Mapsforge.
* Removed deprecated Google Fit services
* Added gpx track file data export.
* Added map, tcx & gpx tracks support for progress tracking while doing rides.
* Fixed cvs data file export.
* Partially refactored code.
* Added manual orientation for landscape modes during rides.
* Added mapsforge map theme support.
* Added guesstimating power out based on various factors.
* Added BT support for reading power meters (untested since i don't have a power meter yet).
* Added TCX file export.

---

* Tested with OpenAndroMaps for offline map usage (https://www.openandromaps.org/en).
* Tested with BT BLE Tuya BTH01 with pivx firmware (BTHome compatible) to record temperature & relative humidity.
* Tested with Coospo BK467 cadence and speed BT BLE sensors to record speed and cadence.

---

Forked from Cyclotrack @ https://github.com/kevinvanleer/cyclotrack

---

#### Donate:

* Bitcoin donations can be sent to bc1qech68pjafrvp0s409sdnrdu28elgqh08r4zx4s.

<img src="https://gitlab.com/draekko-rand/cyclometer/-/raw/main/images/bitcoin.png" data-canonical-src="https://gitlab.com/draekko-rand/cyclometer/-/raw/main/images/bitcoin.png" />

* Litecoin donations can be sent to lsawedfqdgrgasz3kizvtoecajsqfkb24h

<img src="https://gitlab.com/draekko-rand/cyclometer/-/raw/main/images/litecoin.jpg" data-canonical-src="https://gitlab.com/draekko-rand/cyclometer/-/raw/main/images/litecoin.jpg" />

