package com.draekko.cyclometer

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.draekko.cyclometer.data.Bike
import com.draekko.cyclometer.data.BikeDao
import com.draekko.cyclometer.data.CadenceSpeedMeasurement
import com.draekko.cyclometer.data.CadenceSpeedMeasurementDao
import com.draekko.cyclometer.data.Export
import com.draekko.cyclometer.data.ExportDao
import com.draekko.cyclometer.data.ExternalSensor
import com.draekko.cyclometer.data.ExternalSensorDao
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.data.HeartRateMeasurementDao
import com.draekko.cyclometer.data.Measurements
import com.draekko.cyclometer.data.MeasurementsDao
import com.draekko.cyclometer.data.OnboardSensors
import com.draekko.cyclometer.data.OnboardSensorsDao
import com.draekko.cyclometer.data.PowerMeterMeasurement
import com.draekko.cyclometer.data.PowerMeterMeasurementDao
import com.draekko.cyclometer.data.SensorTypeConverter
import com.draekko.cyclometer.data.Split
import com.draekko.cyclometer.data.SplitDao
import com.draekko.cyclometer.data.TempRelHumMeasurement
import com.draekko.cyclometer.data.TempRelHumMeasurementDao
import com.draekko.cyclometer.data.TimeState
import com.draekko.cyclometer.data.TimeStateDao
import com.draekko.cyclometer.data.TimeStateEnumConverter
import com.draekko.cyclometer.data.Trip
import com.draekko.cyclometer.data.TripDao
import com.draekko.cyclometer.data.UserSexEnumConverter
import com.draekko.cyclometer.data.Weather
import com.draekko.cyclometer.data.WeatherDao

@Database(
    entities = [
        Trip::class,
        Measurements::class,
        TimeState::class,
        Split::class,
        OnboardSensors::class,
        Bike::class,
        ExternalSensor::class,
        Weather::class,
        HeartRateMeasurement::class,
        CadenceSpeedMeasurement::class,
        TempRelHumMeasurement::class,
        PowerMeterMeasurement::class,
        Export::class
    ],
    version = 33
)
@TypeConverters(
    TimeStateEnumConverter::class,
    UserSexEnumConverter::class,
    SensorTypeConverter::class
)
abstract class TripsDatabase : RoomDatabase() {
    abstract fun tripDao(): TripDao
    abstract fun measurementsDao(): MeasurementsDao
    abstract fun timeStateDao(): TimeStateDao
    abstract fun splitDao(): SplitDao
    abstract fun onboardSensorsDao(): OnboardSensorsDao
    abstract fun bikeDao(): BikeDao
    abstract fun externalSensorsDao(): ExternalSensorDao
    abstract fun weatherDao(): WeatherDao
    abstract fun heartRateMeasurementDao(): HeartRateMeasurementDao
    abstract fun cadenceSpeedMeasurementDao(): CadenceSpeedMeasurementDao
    abstract fun tempRelHumMeasurementDao(): TempRelHumMeasurementDao
    abstract fun powerMeterMeasurementDao(): PowerMeterMeasurementDao
    abstract fun exportDao(): ExportDao
}
