package com.draekko.cyclometer.services

import android.annotation.SuppressLint
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothProfile
import android.os.Build
import android.util.Log
import com.draekko.cyclometer.data.ExternalSensorFeatures
import com.draekko.cyclometer.util.getIntValue
import java.util.UUID

fun getGattUuid(id: String): UUID {
    val gattUuidSuffix = "0000-1000-8000-00805f9b34fb"
    return UUID.fromString("$id-$gattUuidSuffix")
}

const val batteryGattServiceId = "180f"
const val batterLevelCharacteristicId = "2a19"

const val updateNotificationDescriptorId = "2902"

const val cadenceSpeedGattServiceId = "1816"
const val cscMeasurementCharacteristicId = "2a5b"
const val cscFeatureCharacteristicId = "2a5c"

const val heartRateServiceId = "180d"
const val hrmCharacteristicId = "2a37"

const val fitnessMachineServiceId = "1826"
const val fitnessMachineFeatureCharacteristicId = "2acc"
const val indoorBikeDataCharacteristicId = "2ad2"

const val envSensingServiceId = "181a"
const val temperatureCharacteristicId = "2a6e"
const val relativeHumidityCharacteristicId = "2a6f"
const val barometricPressureCharacteristicId = "2aa3"

const val envCyclingPowerServiceId = "1818"
const val controlPointCyclingPowerMeterCharacteristicId = "2a66"
const val measurementCyclingPowerMeterCharacteristicId = "2a63"

var hasTempRelHum = false

val powerMeterServiceUuid = getGattUuid(envCyclingPowerServiceId)
val controlPointPowerMeterCharacteristicUuid = getGattUuid(controlPointCyclingPowerMeterCharacteristicId)
val measurementPowerMeterCharacteristicUuid = getGattUuid(measurementCyclingPowerMeterCharacteristicId)

val stagesGen2ControlPointPowerMeterCharacteristicUuid = UUID.fromString("a026e005-0a7d-4ab3-97fa-f1500f9feb8b")

val stagesGen3PowerMeterServiceUuid = UUID.fromString("d445fe01-d139-9a5d-6707-1cc6a58b6303")
val stagesGen3ControlPointPowerMeterCharacteristicUuid = UUID.fromString("d445fe02-d139-9a5d-6707-1cc6a58b6303")


val characteristicUpdateNotificationDescriptorUuid =
    getGattUuid(updateNotificationDescriptorId)

val batteryServiceUuid = getGattUuid(batteryGattServiceId)
val batteryLevelCharUuid = getGattUuid(batterLevelCharacteristicId)

val heartRateServiceUuid = getGattUuid(heartRateServiceId)
val hrmCharacteristicUuid = getGattUuid(hrmCharacteristicId)

val cadenceSpeedServiceUuid = getGattUuid(cadenceSpeedGattServiceId)
val cscMeasurementCharacteristicUuid = getGattUuid(cscMeasurementCharacteristicId)
val cscFeatureCharacteristicUuid = getGattUuid(cscFeatureCharacteristicId)

val enviroSensingServiceUuid = getGattUuid(envSensingServiceId)
val temperatureCharacteristicUuid = getGattUuid(temperatureCharacteristicId)
val relativeHumidityCharacteristicUuid = getGattUuid(relativeHumidityCharacteristicId)
val barometricPressureCharacteristicUuid = getGattUuid(barometricPressureCharacteristicId)

fun readCharacteristic(gatt: BluetoothGatt, serviceUuid: UUID, characteristicUuid: UUID) {
    val logTag = "readCharacteristic"
    val characteristic = gatt
        .getService(serviceUuid)?.getCharacteristic(characteristicUuid)
    Log.v(logTag, "read characteristic ${characteristic?.uuid}")
    try {
        characteristic?.let { gatt.readCharacteristic(it) }
    } catch (e: SecurityException) {
        Log.w(logTag, "Bluetooth permissions have not been granted", e)
    }
}

fun readCharacteristic(gatt: BluetoothGatt, serviceUuid: UUID, characteristicUuid1: UUID, characteristicUuid2: UUID) {
    val logTag = "readCharacteristic"
    val characteristic1 = gatt.getService(serviceUuid)?.getCharacteristic(characteristicUuid1)
    val characteristic2 = gatt.getService(serviceUuid)?.getCharacteristic(characteristicUuid2)
    Log.v(logTag, "read characteristic ${characteristic1?.uuid}")
    try {
        characteristic1?.let {
            gatt.readCharacteristic(it)
        }
    } catch (e: SecurityException) {
        Log.w(logTag, "Bluetooth permissions have not been granted", e)
    }
    Log.v(logTag, "read characteristic ${characteristic1?.uuid}")
    try {
        characteristic2?.let {
            gatt.readCharacteristic(it)
        }
    } catch (e: SecurityException) {
        Log.w(logTag, "Bluetooth permissions have not been granted", e)
    }
}

fun readBatteryLevel(gatt: BluetoothGatt) {
    val logTag = "readBatteryLevel"
    val batteryLevelChar = gatt
        .getService(batteryServiceUuid)?.getCharacteristic(batteryLevelCharUuid)
    Log.v(logTag, "read battery level characteristic ${batteryLevelChar?.uuid}")
    try {
        batteryLevelChar?.let { gatt.readCharacteristic(it) }
    } catch (e: SecurityException) {
        Log.w(logTag, "Bluetooth permissions have not been granted", e)
    }
}

fun enableNotifications(
    gatt: BluetoothGatt,
    characteristic: BluetoothGattCharacteristic,
    enable: Boolean = true,
) {
    val descriptor =
        characteristic.getDescriptor(characteristicUpdateNotificationDescriptorUuid)
    val descriptorValue =
        //if (enable) {
            BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        //} else {
        //    byteArrayOf(0x00, 0x00)
        //}
    try {
        if (Build.VERSION.SDK_INT >= 33) {
            gatt.writeDescriptor(descriptor, descriptorValue)
        } else {
            @Suppress("DEPRECATION")
            gatt.writeDescriptor(descriptor.apply { value = descriptorValue })
        }
    } catch (e: SecurityException) {
        Log.w("BleUtils:enableNotifications", "Bluetooth permissions have not been granted", e)
    }
}


fun enableNotifications(
    gatt: BluetoothGatt,
    characteristic1: BluetoothGattCharacteristic,
    characteristic2: BluetoothGattCharacteristic,
    enable: Boolean = true,
) {
    val descriptor1 =
        characteristic1.getDescriptor(characteristicUpdateNotificationDescriptorUuid)
    val descriptor2 =
        characteristic2.getDescriptor(characteristicUpdateNotificationDescriptorUuid)
    val descriptorValue1 =
        //if (enable) {
        BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
    //} else {
    //    byteArrayOf(0x00, 0x00)
    //}
    val descriptorValue2 = descriptorValue1
    try {
        if (Build.VERSION.SDK_INT >= 33) {
            gatt.writeDescriptor(descriptor1, descriptorValue1)
            gatt.writeDescriptor(descriptor2, descriptorValue2)
        } else {
            @Suppress("DEPRECATION")
            gatt.writeDescriptor(descriptor1.apply { value = descriptorValue1 })
            @Suppress("DEPRECATION")
            gatt.writeDescriptor(descriptor2.apply { value = descriptorValue2 })
        }
    } catch (e: SecurityException) {
        Log.w("BleUtils:enableNotifications", "Bluetooth permissions have not been granted", e)
    }
}

fun enableNotifications(
    gatt: BluetoothGatt,
    serviceUuid: UUID,
    characteristicUuid: UUID,
    enable: Boolean = true
) {
    gatt
        .getService(serviceUuid)?.getCharacteristic(characteristicUuid)?.let {
            enableNotifications(gatt, it, enable)
        }
}

fun BluetoothGatt.hasCharacteristic(serviceUuid: UUID, charUuid: UUID): Boolean {
    val thisService = services.find { it.uuid == serviceUuid }
    return null != thisService?.characteristics?.find { it.uuid == charUuid }
}

fun BluetoothGatt.hasCharacteristic(serviceUuid: UUID, charUuid1: UUID, charUuid2: UUID): Boolean {
    val thisService = services.find { it.uuid == serviceUuid }
    return null != thisService?.characteristics?.find { it.uuid == charUuid1 } &&
            null != thisService?.characteristics?.find { it.uuid == charUuid2 }
}

fun BluetoothGatt.printGattTable() {
    val logTag = "printGattTable"
    if (services.isEmpty()) {
        Log.v(
            logTag,
            "No service and characteristic available, call discoverServices() first?"
        )
        return
    }
    var characteristicsTable = ""
    services.forEach { service ->
        val characteristicsTableRow = service.characteristics.joinToString(
            separator = "\n|--",
            prefix = "|--"
        ) { "${it.uuid} ${it.descriptors?.getOrNull(0)?.uuid}" }
        characteristicsTable += "\nService ${service.uuid}\nCharacteristics:\n$characteristicsTableRow"
    }
    Log.v(logTag, characteristicsTable)
}

fun getGattDiscoverServicesCallback(
    onGetFeatures: (gatt: BluetoothGatt, value: Int) -> Unit,
    onReadBatteryLevel: (gatt: BluetoothGatt, value: Byte) -> Unit
) =
    object : BluetoothGattCallback() {
        val logTag = "gattDiscoverServicesCallback"
        override fun onConnectionStateChange(
            gatt: BluetoothGatt,
            status: Int,
            newState: Int,
        ) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    Log.i(logTag, "Connected to GATT server ${gatt.device.address}.")
                    try {
                        Log.i(
                            logTag,
                            "Attempting to start service discovery: ${gatt.discoverServices()}"
                        )
                    } catch (e: SecurityException) {
                        Log.w(logTag, "Bluetooth permissions have not been granted", e)
                    }
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    Log.i(logTag, "Disconnected ${gatt.device.address} from GATT server.")
                }
            }
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int,
        ) {
            Log.d(logTag, "Write descriptor finished")
            super.onDescriptorWrite(gatt, descriptor, status)
            if (gatt != null && descriptor != null) {
                try {
                    gatt.setCharacteristicNotification(descriptor.characteristic, true)
                } catch (e: SecurityException) {
                    Log.w(logTag, "Bluetooth permissions have not been granted", e)
                }
                //TODO: Trigger next stage in pipeline here that would allow setup of
                // other notifications or reads like battery level below
                // Be careful you can cause an infinite loop!
                readBatteryLevel(gatt)
            }
        }

        // New services discovered
        @SuppressLint("MissingPermission")
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            with(gatt) {
                Log.d(logTag, "Discovered services status: $status")
                printGattTable()

                val bitmap =
                    hasCharacteristic(
                        heartRateServiceUuid,
                        hrmCharacteristicUuid
                    ).toInt() * ExternalSensorFeatures().HRM +
                            hasCharacteristic(
                                getGattUuid(fitnessMachineServiceId),
                                getGattUuid(indoorBikeDataCharacteristicId)
                            ).toInt() * ExternalSensorFeatures().FITNESS_MACHINE

                if (hasCharacteristic(
                        powerMeterServiceUuid,
                        measurementPowerMeterCharacteristicUuid
                    )) {
                    readCharacteristic(this,
                        powerMeterServiceUuid,
                        measurementPowerMeterCharacteristicUuid
                    )
                }

                if (hasCharacteristic(
                        stagesGen3PowerMeterServiceUuid,
                        stagesGen3ControlPointPowerMeterCharacteristicUuid
                    )) {
                    readCharacteristic(this,
                        stagesGen3PowerMeterServiceUuid,
                        stagesGen3ControlPointPowerMeterCharacteristicUuid
                    )
                }

                if (hasCharacteristic(
                        enviroSensingServiceUuid,
                        temperatureCharacteristicUuid,
                        relativeHumidityCharacteristicUuid
                    )) {
                    hasTempRelHum = true
                    readCharacteristic(this,
                        enviroSensingServiceUuid,
                        temperatureCharacteristicUuid
                    )
                    readCharacteristic(this,
                        enviroSensingServiceUuid,
                        relativeHumidityCharacteristicUuid
                    )
                    /*
                    readCharacteristic(this,
                        enviroSensingServiceUuid,
                        temperatureCharacteristicUuid,
                        relativeHumidityCharacteristicUuid)
                     */
                } else {
                    hasTempRelHum = false
                    if (hasCharacteristic(enviroSensingServiceUuid, temperatureCharacteristicUuid)) {
                        readCharacteristic(this,
                            enviroSensingServiceUuid,
                            temperatureCharacteristicUuid
                        )
                    }
                    if (hasCharacteristic(enviroSensingServiceUuid, relativeHumidityCharacteristicUuid)) {
                        readCharacteristic(this,
                            enviroSensingServiceUuid,
                            relativeHumidityCharacteristicUuid
                        )
                    }
                }

                if (hasCharacteristic(enviroSensingServiceUuid, barometricPressureCharacteristicUuid)) {
                    readCharacteristic(this,
                        enviroSensingServiceUuid,
                        barometricPressureCharacteristicUuid
                    )
                }

                when (hasCharacteristic(
                    cadenceSpeedServiceUuid,
                    cscMeasurementCharacteristicUuid
                )) {
                    true ->
                        readCharacteristic(
                            this, cadenceSpeedServiceUuid,
                            cscFeatureCharacteristicUuid
                        )
                    else -> {
                        onGetFeatures(this, bitmap)
                    }
                }
            }
        }

        @Suppress("DEPRECATION")
        @Deprecated("Deprecated in Java")
        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
        ) {
            Log.v(logTag, "DEPRECATED -- onCharacteristicChanged")
            enableNotifications(gatt, characteristic, false)
            when (characteristic.uuid) {
                temperatureCharacteristicUuid -> {
                    if (hasTempRelHum) {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                        Log.v(logTag, "TEMP_RELHUM FEATURE")
                    } else {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMPERATURE)
                        Log.v(logTag, "TEMPERATURE FEATURE")
                    }
                }
                relativeHumidityCharacteristicUuid -> {
                    if (hasTempRelHum) {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                        Log.v(logTag, "TEMP_RELHUM FEATURE")
                    } else {
                        onGetFeatures(gatt, ExternalSensorFeatures().RELATIVEHUMIDITY)
                        Log.v(logTag, "RELATIVEHUMIDITY FEATURE")
                    }
                }
                barometricPressureCharacteristicUuid -> {
                    onGetFeatures(gatt, ExternalSensorFeatures().AIRPRESSURE)
                    Log.v(logTag, "AIRPRESSURE FEATURE")
                }
                cscMeasurementCharacteristicUuid -> {
                    val speedId = 0x01
                    val cadenceId = 0x02
                    val sensorType = characteristic.value[0].toInt()
                    when {
                        (sensorType and speedId > 0) -> onGetFeatures(
                            gatt,
                            ExternalSensorFeatures().SPEED
                        )
                        (sensorType and cadenceId > 0) -> onGetFeatures(
                            gatt,
                            ExternalSensorFeatures().CADENCE
                        )
                    }
                }
            }
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            value: ByteArray
        ) {
            Log.v(logTag, "onCharacteristicChanged")
            enableNotifications(gatt, characteristic, false)
            when (characteristic.uuid) {
                temperatureCharacteristicUuid -> {
                    if (hasTempRelHum) {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                        Log.v(logTag, "TEMP_RELHUM FEATURE")
                    } else {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMPERATURE)
                        Log.v(logTag, "TEMPERATURE FEATURE")
                    }
                }
                relativeHumidityCharacteristicUuid -> {
                    if (hasTempRelHum) {
                        onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                        Log.v(logTag, "TEMP_RELHUM FEATURE")
                    } else {
                        onGetFeatures(gatt, ExternalSensorFeatures().RELATIVEHUMIDITY)
                        Log.v(logTag, "RELATIVEHUMIDITY FEATURE")
                    }
                }
                barometricPressureCharacteristicUuid -> {
                    onGetFeatures(gatt, ExternalSensorFeatures().AIRPRESSURE)
                    Log.v(logTag, "AIRPRESSURE FEATURE")
                }
                cscMeasurementCharacteristicUuid -> {
                    val speedId = 0x01
                    val cadenceId = 0x02
                    val sensorType = value[0].toInt()
                    when {
                        (sensorType and speedId > 0) -> onGetFeatures(
                            gatt,
                            ExternalSensorFeatures().SPEED
                        )
                        (sensorType and cadenceId > 0) -> onGetFeatures(
                            gatt,
                            ExternalSensorFeatures().CADENCE
                        )
                    }
                }
            }
        }

        @Deprecated("Deprecated in Java")
        @Suppress("DEPRECATION")
        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int,
        ) {
            Log.v(logTag, "DEPRECATED -- onCharacteristicRead")
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    when (characteristic.uuid) {
                        batteryLevelCharUuid -> {
                            onReadBatteryLevel(gatt, characteristic.value[0])
                        }
                        temperatureCharacteristicUuid -> {
                            if (hasTempRelHum) {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                                Log.v(logTag, "TEMP_RELHUM FEATURE")
                            } else {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMPERATURE)
                                Log.v(logTag, "TEMPERATURE FEATURE")
                            }
                        }
                        relativeHumidityCharacteristicUuid -> {
                            if (hasTempRelHum) {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                                Log.v(logTag, "TEMP_RELHUM FEATURE")
                            } else {
                                onGetFeatures(gatt, ExternalSensorFeatures().RELATIVEHUMIDITY)
                                Log.v(logTag, "RELATIVEHUMIDITY FEATURE")
                            }
                        }
                        barometricPressureCharacteristicUuid -> {
                            onGetFeatures(gatt, ExternalSensorFeatures().AIRPRESSURE)
                            Log.v(logTag, "AIRPRESSURE FEATURE")
                        }
                        cscFeatureCharacteristicUuid -> {
                            val speedId = 0x01
                            val cadenceId = 0x02
                            val sensorType =
                                characteristic.getIntValue(
                                    BluetoothGattCharacteristic.FORMAT_UINT8,
                                    0
                                )
                            when {
                                (sensorType and speedId > 0) -> onGetFeatures(
                                    gatt,
                                    ExternalSensorFeatures().SPEED
                                )
                                (sensorType and cadenceId > 0) -> onGetFeatures(
                                    gatt,
                                    ExternalSensorFeatures().CADENCE
                                )
                            }
                        }
                    }
                }
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            value: ByteArray,
            status: Int,
        ) {
            Log.v(logTag, "onCharacteristicRead")
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    when (characteristic.uuid) {
                        batteryLevelCharUuid -> {
                            onReadBatteryLevel(gatt, value[0])
                        }
                        temperatureCharacteristicUuid -> {
                            if (hasTempRelHum) {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                                Log.v(logTag, "TEMP_RELHUM FEATURE")
                            } else {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMPERATURE)
                                Log.v(logTag, "TEMPERATURE FEATURE")
                            }
                        }
                        relativeHumidityCharacteristicUuid -> {
                            if (hasTempRelHum) {
                                onGetFeatures(gatt, ExternalSensorFeatures().TEMP_RELHUM)
                                Log.v(logTag, "TEMP_RELHUM FEATURE")
                            } else {
                                onGetFeatures(gatt, ExternalSensorFeatures().RELATIVEHUMIDITY)
                                Log.v(logTag, "RELATIVEHUMIDITY FEATURE")
                            }
                        }
                        barometricPressureCharacteristicUuid -> {
                            onGetFeatures(gatt, ExternalSensorFeatures().AIRPRESSURE)
                            Log.v(logTag, "AIRPRESSURE FEATURE")
                        }
                        cscFeatureCharacteristicUuid -> {
                            val speedId = 0x01
                            val cadenceId = 0x02
                            val sensorType =
                                value.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0) ?: 0
                            when {
                                (sensorType and speedId > 0) -> onGetFeatures(
                                    gatt,
                                    ExternalSensorFeatures().SPEED
                                )
                                (sensorType and cadenceId > 0) -> onGetFeatures(
                                    gatt,
                                    ExternalSensorFeatures().CADENCE
                                )
                            }
                        }
                    }
                }
            }
        }
    }

fun Boolean.toInt() = if (this) 1 else 0
