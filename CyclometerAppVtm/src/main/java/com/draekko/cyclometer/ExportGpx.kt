package com.draekko.cyclometer

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.draekko.cyclometer.data.Measurements;
import com.draekko.cyclometer.data.CadenceSpeedMeasurement
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.util.getFileName
import org.joda.time.DateTime
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone

@SuppressLint("HardwareIds")
fun writeGpxFileOld(
    context: Context,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    val creator = context.getString(R.string.cyclometer)
    val gpxversion = "1.1"
    val metadata_link = "https://draekko.com"
    val metadata_link_text = creator
    val creator_version = BuildConfig.VERSION_NAME

    var minlat = 0.0
    var minlng = 0.0
    var maxlat = 0.0
    var maxlng = 0.0

    var datetimeFormat1 = SimpleDateFormat("yyyy/mm/dd HH:mm", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy/mm/dd HH:mm:ssZ", Locale.US)
    val date = Date(System.currentTimeMillis())
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date).replace(" ", "T").replace("/", "-")
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $startTime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $startTime"
    } catch (e: Exception) {
    }

    var sb = StringBuilder()
    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n")
    sb.append("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" " +
            "xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" "+
            "creator=\"$creator v$creator_version\"  version=\"$gpxversion\" " +
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
            "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " +
            "http://www.topografix.com/GPX/1/1/gpx.xsd\">\n")
    sb.append("  <metadata>\n")
    sb.append("    <name><![CDATA[$datetime]]></name>\n")
    sb.append("    <link href=\"$metadata_link\">\n")
    sb.append("      <text>$metadata_link_text</text>\n")
    sb.append("    </link>\n")
    sb.append("    <time>$datetimeZulu</time>\n")
    sb.append("    <bounds maxlat=\"$maxlat\" maxlon=\"$maxlng\" minlat=\"$minlat\" minlon=\"$minlng\"/>\n")
    sb.append("  </metadata>\n")
    sb.append("  <trk>\n")
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("    <name>$trackname</name>\n")
    } else {
        sb.append("    <name>Undefined</name>\n")
    }
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("    <desc>$trackdesc</desc>\n")
    }
    sb.append("    <type>Undefined</type>\n")
    sb.append("    <trkseg>\n")
    var handleSplitSegments = false
    try {
        var splitsize = exportData.splits!!.size
        if (splitsize > 0) {
            handleSplitSegments = true
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    try {
        var splitCount = 0
        var trksize = exportData.measurements!!.size
        if (trksize > 2) {
            for (measurement: Measurements in exportData.measurements!!) {
                val ptTime = measurement.time
                if (handleSplitSegments) {
                    var splitTime = exportData.splits!!.get(splitCount).timestamp
                    if (ptTime >= splitTime) {
                        sb.append("    </trkseg>\n")
                        sb.append("    <trkseg>\n")
                        splitCount++
                    }
                }
                val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val timedate = sdf.format(Date(ptTime)).replace(" ", "T").replace("/", "-") + "Z"
                val lat = measurement.latitude
                val lng = measurement.longitude
                val elevation = measurement.altitude
                sb.append("      <trkpt lat=\"$lat\" lon=\"$lng\">\n")
                sb.append("        <ele>$elevation</ele>\n")
                sb.append("        <time>$timedate</time>\n")
                /*
                var heartRate = findHeartRateForTime(ptTime, exportData.heartRateMeasurements)
                var cadence = findCadenceForTime(ptTime, exportData.cadenceMeasurements)
                var temperature = findTemperatureForTime()
                var power = findpowerForTime()
                if (heartRate >= 0.0f || cadence >= 0.0f || power >= 0.0f || temperature > -273.0f) {
                    sb.append("        <extensions>")
                    if (power >= 0.0f) {
                        sb.append("            <power>36</power>")
                    }
                    sb.append("            <gpxtpx:TrackPointExtension>")
                    if (temperature >= 0.0f) {
                        sb.append("                <gpxtpx:atemp>${temperature.toInt()}</gpxtpx:atemp>")
                    }
                    if (temperature >= 0.0f) {
                        sb.append("                <gpxtpx:atemp>${temperature.toInt()}</gpxtpx:atemp>")
                    }
                    if (heartRate >= 0.0f) {
                        sb.append("                <gpxtpx:hr>${heartRate.toInt()}</gpxtpx:hr>")
                    }
                    if (cadence >= 0.0f) {
                        sb.append("                <gpxtpx:cad>${cadence.toInt()}</gpxtpx:cad>")
                    }
                    sb.append("            </gpxtpx:TrackPointExtension>")
                    sb.append("        </extensions>")
                }
                 */
                sb.append("      </trkpt>\n")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    sb.append("    </trkseg>\n")
    sb.append("  </trk>\n")
    sb.append("</gpx>\n")

    if (BuildConfig.DEBUG) {
        Log.i("GPX", sb.toString())
    }

    outputFile(context, file, sb.toString())
}

fun findCadenceForTime(timeStamp: Long, cadenceMeasurements: Array<CadenceSpeedMeasurement>?): Float {
    if (cadenceMeasurements != null && cadenceMeasurements.size > 0) {
        for (ii in 0..(cadenceMeasurements.size-1)) {
            if (cadenceMeasurements.get(ii).timestamp >= timeStamp) {
                return cadenceMeasurements.get(ii).rpmOrSpeed!!.toFloat()
            }
        }
    }

    return -1.0f
}

fun findHeartRateForTime(timeStamp: Long, heartRateMeasurements: Array<HeartRateMeasurement>?): Float {
    if (heartRateMeasurements != null && heartRateMeasurements.size > 0) {
        for (ii in 0..(heartRateMeasurements.size-1)) {
            if (heartRateMeasurements.get(ii).timestamp >= timeStamp) {
                return heartRateMeasurements.get(ii).heartRate.toFloat()
            }
        }
    }

    return -1.0f
}

/*
@SuppressLint("HardwareIds")
fun writeGpxFileNew(
    context: Context,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    var creator = context.getString(R.string.cyclometer)
    var creator_version = BuildConfig.VERSION_NAME
    //val gpxversion = "1.1"
    //val metadata_link = "https://draekko.com"
    //val metadata_link_text = creator

    var gpx: Gpx.Builder
    gpx = Gpx.Builder()
    gpx.setCreator(creator)
    gpx.setVersion(creator_version)

    // Set Metadata
    var metadata = Metadata.Builder()

    var minlat = 180.0
    var minlng = 180.0
    var maxlat = -180.0
    var maxlng = -180.0

    var listsize = 0
    try {
        listsize = exportData.measurements!!.size
        if (listsize > 2) {
            for (m in 0..(listsize-1)) {
                var altitude = exportData.measurements[m].altitude
                var latitude = exportData.measurements[m].latitude
                var longitude = exportData.measurements[m].longitude
                if (latitude >= maxlat) maxlat = latitude
                if (longitude >= maxlng) maxlng = longitude
                if (latitude < maxlat) minlat = latitude
                if (longitude < maxlng) minlng = longitude
            }
        } else {
            minlat = 0.0
            minlng = 0.0
            maxlat = 0.0
            maxlng = 0.0
        }
    } catch (e: Exception) {
        minlat = 0.0
        minlng = 0.0
        maxlat = 0.0
        maxlng = 0.0
    }

    var bounds = Bounds.Builder()
    if (listsize > 2) {
        bounds.setMaxLat(maxlat)
        bounds.setMaxLon(maxlng)
        bounds.setMinLat(minlat)
        bounds.setMinLon(minlng)
    }

    var link = Link.Builder()
    link.setLinkHref("https://draekko.com")
    link.setLinkText("$creator $creator_version")

    var datetimeFormat1 = SimpleDateFormat("yyyy/mm/dd HH:mm", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy/mm/dd HH:mm:ssZ", Locale.US)
    val date = Date(System.currentTimeMillis())
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date).replace(" ", "T").replace("/", "-")
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $startTime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $startTime"
    } catch (e: Exception) {
    }

    var time: DateTime = DateTime.parse(datetimeZulu)

    //metadata.setAuthor()
    //metadata.setCopyright()
    //metadata.setKeywords()
    metadata.setDesc(trackdesc)
    if (listsize > 2) {
        metadata.setBounds(bounds.build())
    }
    metadata.setLink(link.build())
    metadata.setTime(time)
    metadata.setName(trackdesc)
    gpx.setMetadata(metadata.build())



    var trackSegs: MutableList<TrackSegment> = ArrayList<TrackSegment>()
    var track = Track.Builder()
    var trackType = "Biking"
    track.setTrackCmt(trackdesc)
    track.setTrackDesc(trackdesc)
    track.setTrackType(trackType)
    track.setTrackSrc("$creator $creator_version")

    var trackSegment = TrackSegment.Builder()
    trackSegment.setTrackPoints()

    try {
        var trkSegSize = exportData.splits!!.size

        listsize = exportData.measurements!!.size
        if (listsize > 2) {
            for (m in 0..(listsize-1)) {
                trackSegs.add()
                var altitude = exportData.measurements[m].altitude
                var latitude = exportData.measurements[m].latitude
                var longitude = exportData.measurements[m].longitude
                if (latitude >= maxlat) maxlat = latitude
                if (longitude >= maxlng) maxlng = longitude
                if (latitude < maxlat) minlat = latitude
                if (longitude < maxlng) minlng = longitude
            }
        } else {
            minlat = 0.0
            minlng = 0.0
            maxlat = 0.0
            maxlng = 0.0
        }
    } catch (e: Exception) {
        minlat = 0.0
        minlng = 0.0
        maxlat = 0.0
        maxlng = 0.0
    }

    track.setTrackSegments(trackSegs)
    tracking

    gpx.setTracks(tracking)

    if (BuildConfig.DEBUG) {
        //Log.i("GPX", sb.toString())
    }
}
*/

private fun outputFile(context:Context, file:File, data:String) {
    try {
        FileOutputStream(file).use {
            val output = data.toByteArray()
            it.write(output, 0, output.size)
            it.flush()
            it.close()
        }
        Toast.makeText(context, "Exported " +file.name, Toast.LENGTH_LONG).show()
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(context, "Failed to export " +file.name, Toast.LENGTH_LONG).show()
    }
}

fun exportRideToGpx(
    context: Context,
    destination: File,
    exportData: TripDetailsViewModel.ExportData,
) {

    val privateAppFile = File(
        File("/storage/emulated/0/cyclometer/data"),
        getFileName(destination)
    )

    writeGpxFileOld(
        context,
        DateTime(Date(exportData.summary!!.timestamp)),
        privateAppFile,
        exportData
    )
}
