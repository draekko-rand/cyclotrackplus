package com.draekko.cyclometer

import com.draekko.cyclometer.data.Measurements

data class TripProgress(
    val measurements: Measurements?,
    val accuracy: Float,
    val bearing: Float,
    val speed: Float,
    val maxSpeed: Float,
    val distance: Double,
    val slope: Double,
    val grade: Double,
    val duration: Double,
    val tracking: Boolean,
)
