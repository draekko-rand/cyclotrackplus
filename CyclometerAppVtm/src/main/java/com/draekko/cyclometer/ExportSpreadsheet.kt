package com.draekko.cyclometer

import android.content.Context
import android.util.Log
import android.widget.Toast
import org.dhatim.fastexcel.Workbook
import org.dhatim.fastexcel.Worksheet
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberProperties

inline fun <reified T : Any> getDataCsv(measurements: Array<T>): Array<String> {
    val rows = ArrayList<String>()

    rows.add(getDataHeaderRow<T>())
    val reflection: KClass<T> = T::class

    measurements.forEach { measurement ->
        var row = ""
        for (prop in reflection.declaredMemberProperties) {
            row += "${prop.call(measurement).toString()},"
        }
        rows.add(row)
    }
    return rows.toTypedArray()
}

inline fun <reified T : Any> getDataHeaderRow(): String {
    var row = ""
    val reflection: KClass<T> = T::class
    for (prop in reflection.declaredMemberProperties) {
        row += "${prop.name},"
    }
    return row
}

/*
fun exportRideToCsv(
    file: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    Log.d("EXPORT_SERVICE", "Exporting...")
    FileOutputStream(file).use {
        Log.d("EXPORT_SERVICE", "Opening...")
        ZipOutputStream(BufferedOutputStream(it)).use { stream ->
            Log.d("EXPORT_SERVICE", "Zipping...")

            stream.putNextEntry(
                ZipEntry(
                    "${
                        String.format(
                            "%06d",
                            exportData.summary!!.id
                        )
                    }_measurements.csv"
                )
            )
            getDataCsv(exportData.measurements!!).forEach { row ->
                stream.write("$row\n".toByteArray())
            }
            stream.closeEntry()

            stream.putNextEntry(
                ZipEntry(
                    "${
                        String.format(
                            "%06d",
                            exportData.summary.id
                        )
                    }_onboardSensors.csv"
                )
            )
            getDataCsv(exportData.onboardSensors!!).forEach { row ->
                stream.write("$row\n".toByteArray())
            }
            stream.closeEntry()

            stream.putNextEntry(
                ZipEntry(
                    "${
                        String.format(
                            "%06d",
                            exportData.summary.id
                        )
                    }_timeStates.csv"
                )
            )
            getDataCsv(exportData.timeStates!!).forEach { row ->
                stream.write("$row\n".toByteArray())
            }
            stream.closeEntry()

            stream.finish()
            stream.close()
        }
    }
}
 */

inline fun <reified T : Any> populateRow(
    sheet: Worksheet,
    rowIdx: Int,
    reflection: KClass<T>,
    data: T,
) {
    reflection.declaredMemberProperties.forEachIndexed { cellIdx, prop ->
        prop.get(data).toString().let { stringVal ->
            stringVal.toDoubleOrNull()?.let { doubleVal ->
                sheet.value(rowIdx, cellIdx, doubleVal)
            } ?: sheet.value(rowIdx, cellIdx, stringVal)
        }
    }
}

inline fun <reified T : Any> getDataHeaderRowXlsx(sheet: Worksheet) {
    val reflection: KClass<T> = T::class
    reflection.declaredMemberProperties.forEachIndexed { cellIdx, prop ->
        sheet.value(0, cellIdx, prop.name)
    }
}

inline fun <reified T : Any> addDataToSheet(sheet: Worksheet, data: Array<T>) {
    getDataHeaderRowXlsx<T>(sheet)
    val reflection: KClass<T> = T::class

    data.forEachIndexed { rowIdx, measurement ->
        populateRow(sheet, rowIdx + 1, reflection, measurement)
    }
    sheet.finish()
}

inline fun <reified T : Any> addDataToSheet(sheet: Worksheet, data: T) {
    val reflection: KClass<T> = T::class

    getDataHeaderRowXlsx<T>(sheet)
    populateRow(sheet, 1, reflection, data)
    sheet.finish()
}

fun exportRideToFastExcel(
    context: Context,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    try {
        FileOutputStream(file).use { stream ->
            exportRideToFastExcelStream(stream, exportData)
            stream.close()
        }
        Toast.makeText(context, "Exported " + file.name, Toast.LENGTH_LONG).show()
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(context, "Failed to export " + file.name, Toast.LENGTH_LONG).show()
    }
}

fun exportRideToFastExcelStream(
    stream: FileOutputStream,
    exportData: TripDetailsViewModel.ExportData
) {
    val workbook = Workbook(stream, "Cyclometer", "${BuildConfig.VERSION_CODE}.0")

    addDataToSheet(workbook.newWorksheet("summary"), exportData.summary!!)
    addDataToSheet(workbook.newWorksheet("measurements"), exportData.measurements!!)
    addDataToSheet(workbook.newWorksheet("timeStates"), exportData.timeStates!!)
    addDataToSheet(workbook.newWorksheet("splits"), exportData.splits!!)
    if (exportData.onboardSensors?.isNotEmpty() == true) {
        addDataToSheet(workbook.newWorksheet("onboardSensors"), exportData.onboardSensors)
    }
    if (exportData.weather?.isNotEmpty() == true) {
        addDataToSheet(workbook.newWorksheet("weather"), exportData.weather)
    }
    if (exportData.heartRateMeasurements?.isNotEmpty() == true) {
        addDataToSheet(workbook.newWorksheet("heartRate"), exportData.heartRateMeasurements)
    }
    if (exportData.cadenceMeasurements?.isNotEmpty() == true) {
        addDataToSheet(workbook.newWorksheet("cadence"), exportData.cadenceMeasurements)
    }
    if (exportData.speedMeasurements?.isNotEmpty() == true) {
        addDataToSheet(workbook.newWorksheet("speed"), exportData.speedMeasurements)
    }

    workbook.setCompressionLevel(9)
    workbook.finish()
    stream.close()
}

fun exportRideToXlsx(
    context:Context,
    file: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    exportRideToFastExcel(context, file, exportData)
}
