package com.draekko.cyclometer

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Transformation
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Space
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.MenuProvider
import androidx.core.view.doOnPreDraw
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.draekko.cyclometer.data.Biometrics
import com.draekko.cyclometer.data.CadenceSpeedMeasurement
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.data.Measurements
import com.draekko.cyclometer.data.PowerMeterMeasurement
import com.draekko.cyclometer.data.Split
import com.draekko.cyclometer.data.SyncStatusEnum
import com.draekko.cyclometer.data.TimeState
import com.draekko.cyclometer.data.Trip
import com.draekko.cyclometer.export.ExportTripWorker
import com.draekko.cyclometer.util.getCaloriesBurnedLabel
import com.draekko.cyclometer.util.getSpeedDataFromGps
import com.draekko.cyclometer.util.getSpeedDataFromSensor
import com.draekko.cyclometer.util.getTrendData
import com.draekko.cyclometer.util.useBleSpeedData
import com.draekko.cyclometer.widgets.AxisLabelOrientation
import com.draekko.cyclometer.widgets.AxisLabels
import com.draekko.cyclometer.widgets.BordersEnum
import com.draekko.cyclometer.widgets.Entry
import com.draekko.cyclometer.widgets.HeadingView
import com.draekko.cyclometer.widgets.LineGraph
import com.draekko.cyclometer.widgets.LineGraphAreaDataset
import com.draekko.cyclometer.widgets.LineGraphDataset
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.oscim.backend.CanvasAdapter
import org.oscim.backend.canvas.Bitmap
import org.oscim.backend.canvas.Color
import org.oscim.core.BoundingBox
import org.oscim.core.GeoPoint
import org.oscim.core.MapPosition
import org.oscim.core.MercatorProjection
import org.oscim.core.Tile
import org.oscim.layers.PathLayer
import org.oscim.layers.marker.ItemizedLayer
import org.oscim.layers.marker.MarkerInterface
import org.oscim.layers.marker.MarkerItem
import org.oscim.layers.marker.MarkerSymbol
import org.oscim.layers.tile.buildings.BuildingLayer
import org.oscim.layers.tile.vector.VectorTileLayer
import org.oscim.layers.tile.vector.labeling.LabelLayer
import org.oscim.renderer.GLViewport
import org.oscim.scalebar.DefaultMapScaleBar
import org.oscim.scalebar.MapScaleBar
import org.oscim.scalebar.MapScaleBarLayer
import org.oscim.theme.RenderTheme
import org.oscim.theme.VtmThemes
import org.oscim.theme.styles.LineStyle
import org.oscim.tiling.source.mapfile.MapFileTileSource
import org.oscim.utils.FastMath
import org.oscim.utils.IOUtils
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.roundToInt

fun Float.precision(digits: Int) = "%.${digits}f".format(this)

data class LineChartDataset(
    val entries: ArrayList<Pair<Float, Float>>,
    val trend: ArrayList<Pair<Float, Float>>,
    val hi: ArrayList<Pair<Float, Float>>,
    val lo: ArrayList<Pair<Float, Float>>,
)

@AndroidEntryPoint
class TripDetailsFragment : Fragment(),
        View.OnTouchListener,
        ItemizedLayer.OnItemGestureListener<MarkerInterface> {

    private val logTag = "TripDetailsFragment"

    private var startHeight: Int = 0
    private var startY: Float = 0.0f
    private val viewModel: TripDetailsViewModel by viewModels()
    private val args: TripDetailsFragmentArgs by navArgs()
    private lateinit var mapTheme: RenderTheme
    private lateinit var mapView: org.oscim.android.MapView
    private lateinit var titleNameView: TextView
    private lateinit var titleDateView: TextView
    private lateinit var notesView: TextView
    private lateinit var scrollView: ScrollView
    private lateinit var constraintLayout: ConstraintLayout
    private lateinit var maxGuide: View
    private lateinit var defaultGuide: View
    private lateinit var minGuide: View
    private lateinit var stravaSyncStatus: SyncStatusEnum
    private lateinit var currentContext: Context
    private lateinit var sharedPreferences: SharedPreferences

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
    }

    fun drawPath(measurements: Array<Measurements>) {
        Log.d("TRIP_SUMMARY_CARD", "DRAW PATH")

        if (measurements == null) {
            Log.e(logTag, "Measurements is null")
            return
        }
        if (measurements.isEmpty()) {
            Log.e(logTag, "Measurements is empty")
            return
        }
        if (measurements.size < 2) {
            Log.e(logTag, "Measurements size < 2")
            Toast.makeText(currentContext, "Cannot display track due to missing data", Toast.LENGTH_LONG).show()
            return
        }

        var maxLat = -180.0
        var maxLng = -180.0
        var minLat = 180.0
        var minLng = 180.0
        Log.i(logTag, "parsing measurements")
        for (measurement in measurements) {
            if (measurement.latitude > maxLat) {
                maxLat = measurement.latitude
            }
            if (measurement.longitude > maxLng) {
                maxLng = measurement.longitude
            }
            if (measurement.latitude < minLat) {
                minLat = measurement.latitude
            }
            if (measurement.longitude < minLng) {
                minLng = measurement.longitude
            }
        }
        var bbox = BoundingBox(minLat, minLng, maxLat, maxLng)

        var zoomLevel = 1 shl 17

        val dx = abs(
            MercatorProjection.longitudeToX(bbox.maxLongitude)
                    - MercatorProjection.longitudeToX(bbox.minLongitude)
        )

        val dy = abs(
            MercatorProjection.latitudeToY(bbox.minLatitude)
                    - MercatorProjection.latitudeToY(bbox.maxLatitude)
        )

        var map = mapView.map()

        var zx: Double = map.getWidth() / (dx * Tile.SIZE)
        val zy: Double = map.getHeight() / (dy * Tile.SIZE)
        val scale = Math.min(zx, zy)
        val scaleOffset = scale * 0.14

        /* SET UP MAP VIEWPORT SCALING AND CENTERING */
        zoomLevel = FastMath.log2(scale.toInt())
        val p = bbox.centerPoint
        val centerMapPosition = MapPosition()
            .setX(MercatorProjection.longitudeToX(p.getLongitude()))
            .setY(MercatorProjection.latitudeToY(p.getLatitude()))
            .setZoomLevel(zoomLevel)
            .setScale(scale - scaleOffset)
        map.viewport().setMapPosition(centerMapPosition)

        /* DISPLAY TRACK PATH */
        if (measurements.size >= 2) {
            val pts: ArrayList<GeoPoint> = ArrayList()

            var startAccuracy = measurements.get(0).accuracy
            var start = GeoPoint(
                measurements.get(0).latitude,
                measurements.get(0).longitude)

            var endAccuracy = measurements.get(measurements.size - 1).accuracy
            var end = GeoPoint(
                measurements.get(measurements.size - 1).latitude,
                measurements.get(measurements.size - 1).longitude)

            for (measurement: Measurements in measurements) {
                pts.add(GeoPoint(measurement.latitude, measurement.longitude))
            }

            /* line path */
            if (true) {
                val pathLayerBlue: PathLayer
                val cB: Int = Color.RED
                val styleB: LineStyle = LineStyle.builder()
                    .color(cB)
                    .strokeWidth(4.0f)
                    .cap(org.oscim.backend.canvas.Paint.Cap.ROUND)
                    .build()
                pathLayerBlue = PathLayer(map, styleB)
                pathLayerBlue.setPoints(pts)
                map.layers().add(pathLayerBlue)
            }

            /* stipple line path */
            if (false) {
                val pathLayerRed: PathLayer
                val cR: Int = Color.RED
                val styleR: LineStyle = LineStyle.builder()
                    .stippleColor(cR)
                    .stipple(24)
                    .stippleWidth(1.0f)
                    .strokeWidth(8.0f)
                    .strokeColor(cR)
                    .fixed(true)
                    .randomOffset(false)
                    .isOutline(true)
                    .build()
                pathLayerRed = PathLayer(map, styleR)
                pathLayerRed.setPoints(pts)
                map.layers().add(pathLayerRed)
            }

            /* start and stop markers */
            if (true) {
                var `is`: InputStream? = null
                var bitmapMarkerStart: Bitmap? = null
                var bitmapMarkerEnd: Bitmap? = null
                try {
                    `is` = resources.openRawResource(R.raw.marker_start)
                    bitmapMarkerStart = CanvasAdapter.decodeSvgBitmap(
                        `is`,
                        (48 * CanvasAdapter.getScale()).toInt(),
                        (48 * CanvasAdapter.getScale()).toInt(), 100
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    IOUtils.closeQuietly(`is`)
                }

                try {
                    `is` = resources.openRawResource(R.raw.marker_end)
                    bitmapMarkerEnd = CanvasAdapter.decodeSvgBitmap(
                        `is`,
                        (48 * CanvasAdapter.getScale()).toInt(),
                        (48 * CanvasAdapter.getScale()).toInt(), 100
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    IOUtils.closeQuietly(`is`)
                }

                var startSymbol = MarkerSymbol(bitmapMarkerStart, MarkerSymbol.HotspotPlace.CENTER, false)
                var endSymbol = MarkerSymbol(bitmapMarkerEnd, MarkerSymbol.HotspotPlace.CENTER, false)
                var startMarkerLayer = ItemizedLayer(map, ArrayList<MarkerInterface>(), startSymbol, this)
                var endMarkerLayer = ItemizedLayer(map, ArrayList<MarkerInterface>(), endSymbol, this)
                map.layers().add(startMarkerLayer);
                map.layers().add(endMarkerLayer);

                val ptsStart: ArrayList<MarkerInterface> = ArrayList()
                val ptsEnd: ArrayList<MarkerInterface> = ArrayList()
                var descriptionStart = "start marker"
                var descriptionEnd = "end marker"

                var startMarker = MarkerItem(
                    start.latitude.toString() + "/" + start.longitude.toString(),
                    descriptionStart,
                    GeoPoint(start.latitude, start.longitude))

                var endMarker = MarkerItem(
                    end.latitude.toString() + "/" + end.longitude.toString(),
                    descriptionEnd,
                    GeoPoint(end.latitude, end.longitude))

                ptsStart.add(startMarker)
                ptsEnd.add(endMarker)
                startMarkerLayer.addItems(ptsStart);
                endMarkerLayer.addItems(ptsEnd);
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_trip_details, container, false)
    }

    private fun configureSyncOptions(menu: Menu) {
        //NOTE: TEMPORARY OVERRIDE FOR STRAVA TESTING
        /*
        menu.findItem(R.id.details_menu_action_unsync).isVisible = false
        when (stravaSyncStatus) {
            SyncStatusEnum.SYNCED -> {
                menu.findItem(R.id.details_menu_action_sync).isVisible = false
            }
            SyncStatusEnum.NOT_SYNCED, SyncStatusEnum.REMOVED -> {
                menu.findItem(R.id.details_menu_action_sync).isVisible = true
            }
            else -> {
                menu.findItem(R.id.details_menu_action_sync).isVisible = false
            }
        }
         */
    }

    private fun deleteTrip() {
        Log.d(logTag, "trip overview has observers: ${viewModel.tripOverview.hasObservers()}")
        viewModel.tripOverview.removeObservers(viewLifecycleOwner)
        viewModel.locationMeasurements.removeObservers(viewLifecycleOwner)
        viewModel.splits.removeObservers(viewLifecycleOwner)
        viewModel.timeState.removeObservers(viewLifecycleOwner)
        viewModel.onboardSensors.removeObservers(viewLifecycleOwner)
        Log.d(logTag, "trip overview has observers: ${viewModel.tripOverview.hasObservers()}")
        WorkManager.getInstance(requireContext())
            .enqueue(
                OneTimeWorkRequestBuilder<RemoveTripWorker>()
                    .setInputData(workDataOf("tripIds" to arrayOf(viewModel.tripId)))
                    .build()
            )
        requireActivity().finish()
    }

    private fun showDeleteDialog() =
        activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton(
                    "DELETE"
                ) { _, _ ->
                    deleteTrip()
                }
                setNegativeButton(
                    "CANCEL"
                ) { _, _ ->
                    Log.d("TRIP_DELETE_DIALOG", "CLICKED CANCEL")
                }
                setTitle("Delete ride?")
                setMessage("You are about to remove this ride from your history. This change cannot be undone.")
            }

            builder.create()
        }?.show()

    private fun observeWeather(view: View) {
        val temperatureText = view.findViewById<TextView>(R.id.trip_details_temperature_value)
        val windText = view.findViewById<TextView>(R.id.trip_details_wind_value)
        val temperatureIcon = view.findViewById<ImageView>(R.id.trip_details_temperature_icon)
        val windIcon = view.findViewById<ImageView>(R.id.trip_details_wind_icon)
        viewModel.tripWeather.observe(viewLifecycleOwner) { weathers ->
            if (weathers.isNotEmpty()) {
                temperatureIcon.visibility = View.VISIBLE
                temperatureText.visibility = View.VISIBLE
                temperatureText.text = String(
                    "${
                        getUserTemperature(
                            requireContext(),
                            weathers.map { it.temperature }.average()
                        )
                    } ${getUserTemperatureUnit(requireContext())}".toCharArray()
                )

                windIcon.visibility = View.VISIBLE
                windText.visibility = View.VISIBLE
                weathers.getAverageWind().let { wind ->
                    windText.text = String(
                        "%.1f %s %s".format(
                            getUserSpeed(requireContext(), wind.first),
                            getUserSpeedUnitShort(requireContext()),
                            degreesToCardinal(wind.second.toFloat())
                        ).toCharArray()
                    )
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = ""

        constraintLayout = view.findViewById(R.id.TripDetailsFragment)
        maxGuide = view.findViewById(R.id.trip_details_max_map_guide)
        minGuide = view.findViewById(R.id.trip_details_min_map_guide)
        defaultGuide = view.findViewById(R.id.trip_details_default_map_guide)


        mapView = view.findViewById(R.id.trip_details_map_view)

        titleNameView = view.findViewById(R.id.trip_details_title_name)
        titleDateView = view.findViewById(R.id.trip_details_title_date)
        notesView = view.findViewById(R.id.trip_details_notes)
        val distanceHeadingView: HeadingView = view.findViewById(R.id.trip_details_distance)
        val durationHeadingView: HeadingView = view.findViewById(R.id.trip_details_time)
        val speedHeadingView: HeadingView = view.findViewById(R.id.trip_details_speed)
        val speedChartView: ImageView = view.findViewById(R.id.trip_details_speed_chart)
        val elevationChartView: ImageView = view.findViewById(R.id.trip_details_elevation_chart)
        val heartRateChartView: ImageView = view.findViewById(R.id.trip_details_heart_rate_chart)
        val heartRateHeadingView: HeadingView = view.findViewById(R.id.trip_details_heart_rate)
        val cadenceChartView: ImageView = view.findViewById(R.id.trip_details_cadence_chart)
        val cadenceHeadingView: HeadingView = view.findViewById(R.id.trip_details_cadence)
        val wattsChartView: ImageView = view.findViewById(R.id.trip_details_watts_chart)
        val wattsHeadingView: HeadingView = view.findViewById(R.id.trip_details_watts)

        val strokeStyle = Paint().apply {
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = 5F
            strokeCap = Paint.Cap.ROUND
            strokeJoin = Paint.Join.ROUND
            color = ResourcesCompat.getColor(
                resources,
                R.color.accentColor,
                null
            )
        }
        val trendStyle = Paint(strokeStyle).apply {
            color = ResourcesCompat.getColor(
                resources,
                R.color.secondaryGraphColor,
                null
            )
        }

        scrollView = view.findViewById(R.id.trip_details_scroll_view)

        heartRateHeadingView.visibility = View.GONE
        heartRateChartView.visibility = View.GONE
        cadenceHeadingView.visibility = View.GONE
        cadenceChartView.visibility = View.GONE
        wattsHeadingView.visibility = View.GONE
        wattsChartView.visibility = View.GONE
        notesView.visibility = View.GONE

        view.findViewById<HeadingView>(R.id.trip_details_elevation).value =
            getUserAltitudeUnitLong(requireContext())

        val elevationAlpha = 0.05
        try {
            val tripId = args.tripId
            if (args.tripId == -1L) throw IllegalArgumentException()
            Log.d(logTag, String.format("Displaying details for trip %d", tripId))
            viewModel.tripId = tripId

            addMenuProvider()

            observeWeather(view)
            viewModel.updateSplits()
            drawSplitsGrid()

            observeHeartRate(
                heartRateHeadingView,
                heartRateChartView,
                strokeStyle,
            )
            observeCadence(
                cadenceHeadingView,
                cadenceChartView,
                strokeStyle,
                trendStyle,
            )
            observeWatts(
                wattsHeadingView,
                wattsChartView,
                strokeStyle,
                trendStyle,
            )
            observeSpeed(speedChartView, strokeStyle, trendStyle)

            viewModel.tripOverview.observe(viewLifecycleOwner) { overview ->
                if (overview != null) {
                    distanceHeadingView.value =
                        String.format(
                            "%.2f %s",
                            getUserDistance(requireContext(), overview.distance ?: 0.0),
                            getUserDistanceUnitShort(requireContext())
                        )
                    durationHeadingView.value = formatDuration(overview.duration ?: 0.0)
                    speedHeadingView.value = String.format(
                        "%.1f %s (average)",
                        getUserSpeed(
                            requireContext(),
                            overview.distance ?: 0.0,
                            overview.duration ?: 1.0
                        ),
                        getUserSpeedUnitShort(requireContext())
                    )

                    titleNameView.text = overview.name
                    if (overview.notes != null) {
                        notesView.visibility = View.VISIBLE
                        notesView.text = overview.notes
                    }
                }
            }

            zipLiveData(viewModel.locationMeasurements, viewModel.timeState).observe(
                viewLifecycleOwner
            ) { pair ->
                viewLifecycleOwner.lifecycleScope.launch {
                    val measurements = pair.first
                    val timeStates = pair.second
                    val mapData = plotPath(measurements, timeStates)

                    drawPath(measurements)
                }
            }

            zipLiveData(viewModel.locationMeasurements, viewModel.timeState).observe(
                viewLifecycleOwner
            ) { observed ->
                val tripMeasurements = observed.first
                val timeStates = observed.second
                Log.d(logTag, "Observed change to measurements and time state")

                fun makeElevationDataset(
                    measurements: Array<Measurements>,
                    _totalDistance: Float,
                ): ArrayList<Entry> {
                    val entries = ArrayList<Entry>()
                    var totalDistance = _totalDistance
                    var lastMeasurements: Measurements? = null
                    var smoothed: Double = measurements[0].altitude
                    var smoothedLast = smoothed
                    measurements.forEach {
                        smoothed =
                            exponentialSmoothing(
                                elevationAlpha,
                                it.altitude,
                                smoothedLast
                            )
                        smoothedLast = smoothed

                        lastMeasurements?.let { last ->
                            totalDistance += getDistance(it, last)
                        }
                        lastMeasurements = it

                        entries.add(
                            Entry(
                                totalDistance,
                                getUserAltitude(
                                    requireContext(),
                                    smoothed
                                ).toFloat()
                            )
                        )
                    }
                    return entries
                }

                fun makeElevationLineChart(intervals: Array<LongRange>) {
                    val legs = getTripLegs(tripMeasurements, intervals)
                    val data = ArrayList<Entry>()

                    var totalDistance = 0f
                    legs.forEach { leg ->
                        if (leg.isNotEmpty()) {
                            makeElevationDataset(leg, totalDistance).let { dataset ->
                                data.addAll(dataset)
                                dataset.takeIf { it.isNotEmpty() }?.let {
                                    totalDistance = it.last().first
                                }
                            }
                        }
                    }

                    try {
                        val xMin = data.first().first
                        val xMax = data.last().first
                        val yMin = data.minBy { element -> element.second }.second
                        val yMax = data.maxBy { element -> element.second }.second
                        val yRangePadding = (yMax - yMin) * 0.2f
                        val yViewMin = max(yMin - yRangePadding, 0f)
                        val yViewMax = yMax + yRangePadding

                        elevationChartView.setImageDrawable(
                            LineGraph(
                                areas = listOf(
                                    LineGraphAreaDataset(
                                        points1 = data.toList(),
                                        points2 = listOf(
                                            Entry(xMin, yViewMin),
                                            Entry(xMax, yViewMin)
                                        ),
                                        xRange = Pair(xMin, xMax),
                                        yRange = Pair(yViewMin, yViewMax),
                                        xAxisWidth = xMax - xMin,
                                        yAxisHeight = yViewMax - yViewMin,
                                        paint = Paint(strokeStyle).apply {
                                            style = Paint.Style.FILL_AND_STROKE
                                            alpha = 50
                                        }
                                    )
                                ),
                                datasets = listOf(
                                    LineGraphDataset(
                                        points = data.toList(),
                                        xRange = Pair(xMin, xMax),
                                        yRange = Pair(yViewMin, yViewMax),
                                        xAxisWidth = xMax - xMin,
                                        yAxisHeight = yViewMax - yViewMin,
                                        paint = strokeStyle
                                    ),
                                ),
                                borders = BordersEnum.BOTTOM.value,
                                xLabels = getAxisLabelsDistanceX(xMin, xMax),
                                yLabels = AxisLabels(
                                    labels = listOf(
                                        Pair(
                                            yMin, "${yMin.roundToInt()} ${
                                                getUserAltitudeUnitShort(
                                                    requireContext()
                                                )
                                            }"
                                        ),
                                        Pair(
                                            yMax, "${yMax.roundToInt()} ${
                                                getUserAltitudeUnitShort(
                                                    requireContext()
                                                )
                                            }"
                                        )
                                    ),
                                    range = Pair(yViewMin, yViewMax),
                                    lines = true,
                                    background = (scrollView.background as ColorDrawable).color,
                                    orientation = AxisLabelOrientation.INSIDE
                                )
                            )
                        )
                    } catch (e: Exception) {
                        Log.e(logTag, "Could not draw elevation chart", e)
                    }
                }


                Log.d(
                    logTag,
                    "Recorded ${tripMeasurements.size} measurements for trip $tripId"
                )

                if (tripMeasurements.isEmpty()) return@observe
                if (timeStates.isNotEmpty()) titleDateView.text =
                    String.format(
                        "%s: %s - %s",
                        SimpleDateFormat(
                            "MMMM d",
                            Locale.US
                        ).format(Date(timeStates.first().timestamp)),
                        SimpleDateFormat(
                            "h:mm",
                            Locale.US
                        ).format(Date(timeStates.first().timestamp)),
                        SimpleDateFormat(
                            "h:mm",
                            Locale.US
                        ).format(Date(timeStates.last().timestamp))
                    )
                val intervals = getTripIntervals(timeStates, tripMeasurements)
                viewLifecycleOwner.lifecycleScope.launch {
                    val elevationChange = getElevationChange(tripMeasurements)
                    view.findViewById<HeadingView>(R.id.trip_details_elevation).value =
                        "+${
                            getUserAltitude(
                                requireContext(),
                                elevationChange.first
                            ).roundToInt()
                        }/${
                            getUserAltitude(
                                requireContext(),
                                elevationChange.second
                            ).roundToInt()
                        } ${
                            getUserAltitudeUnitLong(requireContext())
                        }"
                    makeElevationLineChart(intervals)

                    scrollView.setOnTouchListener(this@TripDetailsFragment)
                }
            }

            zipLiveData(viewModel.heartRateMeasurements, viewModel.tripOverview).observe(
                viewLifecycleOwner
            ) { pairs ->
                val measurements = pairs.first
                val overview = pairs.second
                Log.d(logTag, "Observed change to measurements and overview")
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.getCombinedBiometrics(overview.timestamp, requireContext())
                        .let { biometrics ->
                            Log.d(logTag, "biometrics: $biometrics")
                            if (biometrics.userWeight != null) {
                                Log.d(logTag, "Calculating calories burned")
                                getCaloriesBurned(biometrics, overview, measurements)
                            }
                        }
                }
            }
            /*
            if (FeatureFlags.devBuild) {
                viewModel.tripOverview.observe(viewLifecycleOwner) { trip ->
                    trip?.timestamp?.let { timestamp ->
                        getDatasets(
                            requireActivity(),
                            timestamp,
                            (timestamp + (trip.duration?.times(
                                1000
                            ) ?: 1).toLong())
                        )
                    }
                }
            }
            */
        } catch (e: Exception) {
            when (e) {
                is java.lang.IllegalArgumentException,
                is java.lang.reflect.InvocationTargetException -> {
                    Log.e(logTag, "Failed to parse navigation args", e)
                    AlertDialog.Builder(requireContext()).apply {
                        setTitle("Something went wrong!")
                        setMessage("There was a problem accessing the data for this ride. Please try again.")
                        setPositiveButton("OK") { _, _ -> }
                    }.create()
                }

                else -> throw e
            }
        }

        initMapView(currentContext)

        if (Build.VERSION.SDK_INT >= 30) {
            requireActivity().window.insetsController?.apply {
                hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
            }
        }
    }

    private fun getCaloriesBurned(
        biometrics: Biometrics,
        overview: Trip,
        measurements: Array<HeartRateMeasurement>,
    ) {
        Log.d(logTag, "User weight: ${overview.userWeight}")
        val caloriesHeadingView: HeadingView =
            requireView().findViewById(R.id.trip_details_calories)
        val avgHr = getAverageHeartRate(measurements)
        try {
            com.draekko.cyclometer.util.getCaloriesBurned(
                requireContext(),
                biometrics,
                overview,
                avgHr,
            ).let {
                caloriesHeadingView.label = getCaloriesBurnedLabel(
                    requireContext(),
                    biometrics,
                    overview,
                    avgHr,
                )
                caloriesHeadingView.visibility = View.VISIBLE
                caloriesHeadingView.value = it.toString()
            }

        } catch (e: NullPointerException) {
            caloriesHeadingView.visibility = View.GONE
            Log.e(tag, "Failed to calculate calories burned", e)
        }
    }

    private fun drawSplitsGrid(
    ) {
        val splitsHeadingView: HeadingView = requireView().findViewById(R.id.trip_details_splits)
        val splitsGridView: GridLayout = requireView().findViewById(R.id.trip_details_splits_grid)
        splitsHeadingView.value = ""

        fun makeSplitRow(
            idx: Int,
            split: Split,
            maxSpeed: Float
        ): Triple<TextView, LinearLayout, TextView> {
            val distanceView = TextView(activity).apply {
                text = String.format(
                    "%d %s",
                    idx + 1, getUserDistanceUnitShort(requireContext())
                )
                layoutParams = GridLayout.LayoutParams(
                    GridLayout.spec(
                        idx + 1,
                        GridLayout.CENTER
                    ),
                    GridLayout.spec(0)
                )
            }
            val speedText = TextView(activity).apply {
                maxLines = 1
                text = String.format(
                    "%.2f %s",
                    getUserSpeed(
                        requireContext(),
                        split.distance,
                        split.duration
                    ),
                    getUserSpeedUnitShort(requireContext())
                )
            }
            val splitPrIcon = ImageView(requireContext()).apply {
                val heightDip = 12f
                val heightPx = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    heightDip,
                    resources.displayMetrics
                ).toInt()
                layoutParams =
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        heightPx
                    ).apply { gravity = Gravity.CENTER }
                setImageResource(R.drawable.ic_trophy)
                setColorFilter(
                    android.graphics.Color.WHITE,
                    PorterDuff.Mode.SRC_ATOP
                )
                visibility = View.INVISIBLE
            }
            val speedBar = LinearLayout(activity).apply {
                background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.rounded_corner,
                    null
                )
                addView(speedText)
                addView(splitPrIcon)
            }
            val prIcon = ImageView(requireContext()).apply {
                val heightDip = 14f
                val heightPx = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    heightDip,
                    resources.displayMetrics
                ).toInt()
                layoutParams =
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        heightPx
                    ).apply { gravity = Gravity.CENTER }
                setImageResource(R.drawable.ic_trophy)
                setColorFilter(
                    requireContext().getColor(R.color.primaryDarkColor),
                    PorterDuff.Mode.SRC_ATOP
                )
                visibility = View.INVISIBLE
            }
            val timeView = TextView(activity).apply {
                text = formatDuration(split.totalDuration)
                layoutParams = GridLayout.LayoutParams(
                    GridLayout.spec(
                        idx + 1,
                        GridLayout.CENTER
                    ),
                    GridLayout.spec(2)
                )
            }

            val speedView = LinearLayout(activity).apply {
                layoutParams =
                    GridLayout.LayoutParams(
                        GridLayout.spec(idx + 1),
                        GridLayout.spec(1, 100f)
                    )

                doOnPreDraw {
                    speedBar.minimumWidth =
                        (measuredWidth * split.distance / split.duration / maxSpeed).toInt() - prIcon.measuredWidth
                }
                addView(speedBar)
                addView(Space(requireContext()).apply {
                    layoutParams = LinearLayout.LayoutParams(0, 0, 1f)
                })
                addView(prIcon)
            }

            viewLifecycleOwner.lifecycleScope.launch {
                val fastestTotalDistance = viewModel.getFastestDistance(
                    getUserDistance(requireContext(), split.totalDistance).roundToInt(),
                    getUserDistance(requireContext(), 1.0), 3
                )
                prIcon.visibility =
                    when (fastestTotalDistance.firstOrNull()?.tripId == split.tripId && fastestTotalDistance.size == 3) {
                        true -> View.VISIBLE
                        else -> View.INVISIBLE
                    }
                val fastestSplit = viewModel.getFastestSplit(
                    getUserDistance(requireContext(), split.totalDistance).roundToInt(),
                    getUserDistance(requireContext(), 1.0), 3
                )
                splitPrIcon.visibility =
                    when (fastestSplit.firstOrNull()?.tripId == split.tripId && fastestSplit.size == 3) {
                        true -> View.VISIBLE
                        else -> View.INVISIBLE
                    }
            }

            return Triple(distanceView, speedView, timeView)
        }

        fun makeSplitsGrid(splits: Array<Split>) {
            splitsGridView.removeAllViews()
            splitsGridView.visibility = View.VISIBLE
            splitsHeadingView.visibility = View.VISIBLE
            if (splits.isEmpty()) {
                splitsGridView.visibility = View.GONE
                splitsHeadingView.visibility = View.GONE
                return
            }

            var maxSpeed = 0.0f
            splits.forEach {
                val splitSpeed = it.distance / it.duration
                if (splitSpeed > maxSpeed) maxSpeed = splitSpeed.toFloat()
            }

            splits.forEachIndexed { idx, split ->
                val (distanceView, speedView, timeView) = makeSplitRow(idx, split, maxSpeed)
                splitsGridView.addView(distanceView)
                splitsGridView.addView(speedView)
                splitsGridView.addView(timeView)
            }
        }

        viewModel.splits.observe(viewLifecycleOwner) { splits ->
            makeSplitsGrid(splits)
        }
    }

    private fun addMenuProvider() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
                // Inflate the menu; this adds items to the action bar if it is present.
                inflater.inflate(R.menu.menu_details, menu)
                Log.d(logTag, "Options menu created")
                viewModel.tripOverview.observe(viewLifecycleOwner) {
                    stravaSyncStatus = it.stravaSyncStatus
                }
            }

            override fun onPrepareMenu(menu: Menu) {
                super.onPrepareMenu(menu)
                if (this@TripDetailsFragment::stravaSyncStatus.isInitialized
                ) configureSyncOptions(menu)
            }

            override fun onMenuItemSelected(item: MenuItem): Boolean {
                Log.d(logTag, "Options menu clicked")
                return when (item.itemId) {
                    R.id.details_menu_action_edit -> {
                        try {
                            findNavController()
                                .navigate(
                                    TripDetailsFragmentDirections.actionEditTrip(
                                        args.tripId,
                                        titleNameView.text.toString(),
                                        titleDateView.text.toString(),
                                        notesView.text.toString()
                                    )
                                )
                        } catch (e: IllegalArgumentException) {
                            Log.e(logTag, "Cannot edit trip ${args.tripId}. Does not exist.", e)
                        }
                        true
                    }

                    R.id.details_menu_action_delete -> {
                        Log.d(logTag, "Options menu clicked delete")
                        showDeleteDialog()
                        true
                    }

                    R.id.details_menu_action_export_csv -> {
                        if (requireActivity().checkSelfPermission(
                                "android.permission.WRITE_EXTERNAL_STORAGE"
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            ActivityCompat.requestPermissions(
                                requireActivity(),
                                arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
                                0
                            )
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                        }
                        WorkManager.getInstance(requireContext())
                            .enqueue(
                                OneTimeWorkRequestBuilder<ExportTripWorker>()
                                    .setInputData(
                                        workDataOf(
                                            "tripId" to viewModel.tripId,
                                            "fileType" to "csv"
                                        )
                                    )
                                    .build()
                            )
                        true
                    }

                    R.id.details_menu_action_export_gpx -> {
                        if (requireActivity().checkSelfPermission(
                                "android.permission.WRITE_EXTERNAL_STORAGE"
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            ActivityCompat.requestPermissions(
                                requireActivity(),
                                arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
                                0
                            )
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                        }
                        WorkManager.getInstance(requireContext())
                            .enqueue(
                                OneTimeWorkRequestBuilder<ExportTripWorker>()
                                    .setInputData(
                                        workDataOf(
                                            "tripId" to viewModel.tripId,
                                            "fileType" to "gpx"
                                        )
                                    )
                                    .build()
                            )
                        true
                    }

                    R.id.details_menu_action_export_tcx -> {
                        //Toast.makeText(currentContext, "TCX Export Coming Soon.", Toast.LENGTH_LONG).show()
                        if (true) {
                            if (requireActivity().checkSelfPermission(
                                    "android.permission.WRITE_EXTERNAL_STORAGE"
                                ) != PackageManager.PERMISSION_GRANTED
                            ) {
                                ActivityCompat.requestPermissions(
                                    requireActivity(),
                                    arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
                                    0
                                )
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                            }
                            WorkManager.getInstance(requireContext())
                                .enqueue(
                                    OneTimeWorkRequestBuilder<ExportTripWorker>()
                                        .setInputData(
                                            workDataOf(
                                                "tripId" to viewModel.tripId,
                                                "fileType" to "tcx"
                                            )
                                        )
                                        .build()
                                )
                        }
                        true
                    }

                    R.id.details_menu_action_export_xlsx -> {
                        if (requireActivity().checkSelfPermission(
                                "android.permission.WRITE_EXTERNAL_STORAGE"
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            ActivityCompat.requestPermissions(
                                requireActivity(),
                                arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
                                0
                            )
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                        }
                        WorkManager.getInstance(requireContext())
                            .enqueue(
                                OneTimeWorkRequestBuilder<ExportTripWorker>()
                                    .setInputData(
                                        workDataOf(
                                            "tripId" to viewModel.tripId,
                                            "fileType" to "xlsx"
                                        )
                                    )
                                    .build()
                            )
                        true
                    }

                    /*
                    R.id.details_menu_action_sync -> {
                        WorkManager.getInstance(requireContext())
                            .enqueue(
                                OneTimeWorkRequestBuilder<StravaCreateActivityWorker>()
                                    .setInputData(workDataOf("tripId" to viewModel.tripId)).build()
                            )
                        true
                    }

                    R.id.details_menu_action_unsync -> {
                        Log.i(logTag, "No function associated with this menu item")
                        true
                    }
                     */

                    R.id.details_menu_action_export_fit -> {
                        WorkManager.getInstance(requireContext())
                            .enqueue(
                                OneTimeWorkRequestBuilder<ExportTripWorker>()
                                    .setInputData(
                                        workDataOf(
                                            "tripId" to viewModel.tripId,
                                            "fileType" to "fit"
                                        )
                                    )
                                    .build()
                            )
                        true
                    }

                    else -> {
                        Log.w(logTag, "unimplemented menu item selected")
                        false
                    }
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::mapView.isInitialized) mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }

    private fun startTouchSequence(event: MotionEvent?): Boolean {
        startY = event?.rawY ?: 0f
        startHeight = scrollView.marginTop
        Log.v("TOUCH_SEQ", "Start sequence: $startY")
        return true
    }

    private fun adjustMap(event: MotionEvent?): Boolean {
        val newHeight =
            (startHeight + (event?.rawY ?: startY) - startY).toInt()

        if (scrollView.scrollY > 0 || (startHeight == minGuide.top && ((event?.rawY
                ?: startY) - startY < 0))
        ) {
            startY = event?.rawY ?: startY
            return false
        }

        Log.v(
            "TOUCH_SEQ_MOVE",
            "startHeight:$startHeight, rawY:${event?.rawY}, startY:$startY, newHeight:$newHeight"
        )
        val marginParams: ViewGroup.MarginLayoutParams =
            scrollView.layoutParams as ViewGroup.MarginLayoutParams
        marginParams.topMargin = newHeight
        scrollView.layoutParams = marginParams

        return true
    }

    private fun endTouchSequence(event: MotionEvent?): Boolean {
        val thresholdDip = 100f
        val thresholdPx = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            thresholdDip,
            resources.displayMetrics
        )

        val currentHeight = scrollView.marginTop

        val expand = when {
            scrollView.marginTop < defaultGuide.top -> defaultGuide.top
            else -> maxGuide.top
        }
        val collapse = when {
            scrollView.marginTop < defaultGuide.top -> minGuide.top
            else -> defaultGuide.top
        }
        val newHeight = when {
            (event?.rawY ?: startY) - startY > thresholdPx -> expand
            startY - (event?.rawY ?: startY) > thresholdPx -> collapse
            else -> startHeight
        }
        val interpolator = when (newHeight) {
            startY.toInt() -> AccelerateInterpolator()
            else -> DecelerateInterpolator()
        }

        val delta = newHeight - currentHeight
        Log.v(
            "TOUCH_SEQ_END",
            "startHeight:$startHeight, rawY:${event?.rawY}, startY:$startY, newHeight:$newHeight, currentHeight:$currentHeight, delta:$delta"
        )

        val marginAnimation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                super.applyTransformation(interpolatedTime, t)
                val newParams: ViewGroup.MarginLayoutParams =
                    scrollView.layoutParams as ViewGroup.MarginLayoutParams
                newParams.topMargin = (newHeight - (delta * (1 - interpolatedTime))).toInt()
                scrollView.layoutParams = newParams
            }
        }

        marginAnimation.duration = 100
        marginAnimation.interpolator = interpolator
        scrollView.startAnimation(marginAnimation)

        return true
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (scrollView.scrollY != 0) return false
        return when (event?.action) {
            MotionEvent.ACTION_UP -> {
                val retVal = endTouchSequence(event)
                if (event.rawY == startY) {
                    //Get rid of warning
                    //This seems to do nothing
                    v?.performClick()
                }
                retVal
            }

            MotionEvent.ACTION_DOWN -> startTouchSequence(event)
            MotionEvent.ACTION_MOVE -> adjustMap(event)
            else -> false
        }
    }

    private fun getAxisLabelsTimeX(
        xMin: Float,
        xMax: Float,
        precision: Float
    ) = AxisLabels(
        labels = listOf(
            (xMin + (xMax - xMin) * 0.25f).div(precision).toInt()
                .times(precision)
                .let {
                    Pair(
                        it,
                        formatDurationShort(it.toDouble())
                    )
                },
            (xMin + (xMax - xMin) * 0.5f).div(precision).toInt()
                .times(precision)
                .let {
                    Pair(
                        it,
                        formatDurationShort(it.toDouble())
                    )
                },
            (xMin + (xMax - xMin) * 0.75f).div(precision).toInt()
                .times(precision)
                .let {
                    Pair(
                        it,
                        formatDurationShort(it.toDouble())
                    )
                },
        ),
        range = Pair(xMin, xMax),
        ticks = true,
        orientation = AxisLabelOrientation.BOTTOM,
        background = (scrollView.background as ColorDrawable).color,
    )

    private fun getAxisLabelsDistanceX(
        xMin: Float,
        xMax: Float,
    ) = AxisLabels(
        labels = listOf(
            (xMin + (xMax - xMin) * 0.25f).roundToInt().toFloat()
                .let {
                    Pair(
                        it,
                        "${
                            getUserDistance(
                                requireContext(),
                                it.toDouble()
                            ).roundToInt()
                        } ${getUserDistanceUnitShort(requireContext())}"
                    )
                },
            (xMin + (xMax - xMin) * 0.5f).roundToInt().toFloat()
                .let {
                    Pair(
                        it,
                        "${
                            getUserDistance(
                                requireContext(),
                                it.toDouble()
                            ).roundToInt()
                        } ${getUserDistanceUnitShort(requireContext())}"
                    )
                },
            (xMin + (xMax - xMin) * 0.75f).roundToInt().toFloat()
                .let {
                    Pair(
                        it,
                        "${
                            getUserDistance(
                                requireContext(),
                                it.toDouble()
                            ).roundToInt()
                        } ${getUserDistanceUnitShort(requireContext())}"
                    )
                },
        ),
        range = Pair(xMin, xMax),
        ticks = true,
        orientation = AxisLabelOrientation.BOTTOM,
        background = (scrollView.background as ColorDrawable).color,
    )

    private fun observeSpeed(
        speedChartView: ImageView,
        strokeStyle: Paint,
        trendStyle: Paint,
    ) {
        viewModel.speedLiveData().observe(
            viewLifecycleOwner
        ) { observed ->
            fun makeSpeedDataset(
                getDataFunc: (ArrayList<Entry>, ArrayList<Entry>, ArrayList<Entry>, ArrayList<Entry>) -> Unit,
            ): LineChartDataset {
                val entries = ArrayList<Entry>()
                val trend = ArrayList<Entry>()
                val hi = ArrayList<Entry>()
                val lo = ArrayList<Entry>()

                getDataFunc(entries, trend, hi, lo)

                return LineChartDataset(entries = entries, trend = trend, hi = hi, lo = lo)
            }

            fun observeGpsSpeed(
                timeStates: Array<TimeState>,
                locationMeasurements: Array<Measurements>,
                avgSpeed: Float
            ): LineChartDataset {
                Log.d(logTag, "observeGpsSpeed")
                val intervals = getTripIntervals(timeStates, locationMeasurements)
                val legs = getTripLegs(locationMeasurements, intervals)

                val allData = LineChartDataset(
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                )
                legs.forEachIndexed { idx, leg ->
                    when (leg.isNotEmpty()) {
                        true -> makeSpeedDataset(
                            getSpeedDataFromGps(
                                requireContext(), leg,
                                intervals.sliceArray(IntRange(0, idx)),
                                avgSpeed
                            )
                        )

                        else -> LineChartDataset(
                            ArrayList(),
                            ArrayList(),
                            ArrayList(),
                            ArrayList(),
                        )
                    }.let {
                        allData.entries.addAll(it.entries)
                        allData.trend.addAll(it.trend)
                        allData.hi.addAll(it.hi)
                        allData.lo.addAll(it.lo)
                    }
                }
                return allData
            }

            fun observeBleSpeed(
                overview: Trip,
                timeStates: Array<TimeState>,
                speedMeasurements: Array<CadenceSpeedMeasurement>,
                avgSpeed: Float
            ): LineChartDataset {
                val tripId = overview.id

                val intervals = getTripIntervals(timeStates, speedMeasurements)
                val legs = getTripLegs(speedMeasurements, intervals)
                Log.d(
                    logTag,
                    "Auto circumference trip $tripId: ${overview.autoWheelCircumference}"
                )

                val effectiveCircumference =
                    getEffectiveCircumference(overview, legs.flatten().toTypedArray())
                Log.d(logTag, "Effective circumference trip $tripId: $effectiveCircumference")

                effectiveCircumference?.let { e ->
                    overview.autoWheelCircumference?.let { a ->
                        Log.d(
                            logTag,
                            "Auto circumference variance: ${(a / e - 1f)}"
                        )
                    }
                }
                Log.d(
                    logTag,
                    "User circumference trip $tripId: ${overview.userWheelCircumference}"
                )

                val allData = LineChartDataset(
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                )
                legs.forEachIndexed { idx, leg ->
                    when (leg.isNotEmpty()) {
                        true -> makeSpeedDataset(
                            getSpeedDataFromSensor(
                                requireContext(),
                                observed.summary!!,
                                effectiveCircumference,
                                leg,
                                intervals.sliceArray(IntRange(0, idx)),
                                avgSpeed
                            )
                        )

                        else -> LineChartDataset(
                            ArrayList(),
                            ArrayList(),
                            ArrayList(),
                            ArrayList(),
                        )
                    }.let {
                        allData.entries.addAll(it.entries)
                        allData.trend.addAll(it.trend)
                        allData.hi.addAll(it.hi)
                        allData.lo.addAll(it.lo)
                    }
                }
                return allData
            }

            fun makeSpeedLineChart(avgSpeed: Float) {
                val speedMeasurements = observed.speedMeasurements

                if (observed.summary == null ||
                    observed.timeStates.isEmpty() ||
                    (speedMeasurements.isEmpty() && observed.locationMeasurements.isEmpty())
                ) return

                val timeStates = observed.timeStates
                val overview = observed.summary

                when (useBleSpeedData(
                    observed.speedMeasurements,
                    observed.locationMeasurements
                )) {
                    true -> observeBleSpeed(
                        overview,
                        timeStates,
                        observed.speedMeasurements,
                        avgSpeed
                    )

                    else -> {
                        observeGpsSpeed(
                            timeStates,
                            observed.locationMeasurements,
                            avgSpeed
                        )
                    }
                }.takeIf { it.trend.size > 0 }?.let { lineData ->
                    speedChartView.setImageDrawable(
                        getSpeedCadenceLineGraph(
                            lineData,
                            avgSpeed,
                            { value -> "${value.precision(1)} ${getUserSpeedUnitShort(requireContext())}" },
                            strokeStyle,
                            trendStyle.apply { style = Paint.Style.FILL_AND_STROKE }
                        )
                    )
                }
            }

            if (observed != null) {
                val avgSpeed = getUserSpeed(
                    requireContext(),
                    observed.summary?.distance ?: 0.0,
                    observed.summary?.duration ?: 1.0
                )
                makeSpeedLineChart(avgSpeed)
            }
        }
    }

    private fun getSpeedCadenceLineGraph(
        lineData: LineChartDataset,
        average: Float,
        labelTemplate: (Float) -> String,
        strokeStyle: Paint,
        areaStyle: Paint
    ): LineGraph {
        val xMin = lineData.entries.first().first
        val xMax = lineData.entries.last().first
        val yMin = lineData.entries.minBy { element -> element.second }.second
        val yTrendMin = lineData.trend.minBy { element -> element.second }.second
        val yMax = lineData.entries.maxBy { element -> element.second }.second
        val dataMax = lineData.entries.maxBy { element -> element.second }.second
        val yRangePadding = (yMax - yMin) * 0.2f
        val yViewMin = max(yMin - yRangePadding, 0f)

        val dataMaxPadding = (dataMax - yMin) * 0.2f
        return LineGraph(
            datasets = listOf(
                LineGraphDataset(
                    points = lineData.trend.toList(),
                    xRange = Pair(xMin, xMax),
                    xAxisWidth = xMax - xMin,
                    yRange = Pair(yViewMin, dataMax + dataMaxPadding),
                    yAxisHeight = dataMax + dataMaxPadding - yViewMin,
                    paint = strokeStyle
                ),
            ),
            areas = listOf(
                LineGraphAreaDataset(
                    points1 = lineData.hi.toList(),
                    points2 = lineData.lo.toList(),
                    xRange = Pair(xMin, xMax),
                    xAxisWidth = xMax - xMin,
                    yRange = Pair(yViewMin, dataMax + dataMaxPadding),
                    yAxisHeight = dataMax + dataMaxPadding - yViewMin,
                    paint = areaStyle
                ),
            ),
            borders = BordersEnum.BOTTOM.value,
            xLabels = getAxisLabelsTimeX(xMin, xMax, 60f),
            yLabels = AxisLabels(
                labels = listOf(
                    Pair(
                        yTrendMin,
                        labelTemplate(yTrendMin)
                    ),
                    Pair(
                        dataMax,
                        labelTemplate(dataMax)
                    ),
                    Pair(
                        average,
                        labelTemplate(average)
                    )
                ),
                range = Pair(yViewMin, dataMax + dataMaxPadding),
                lines = true,
                background = (scrollView.background as ColorDrawable).color
            )
        )
    }

    private fun observeCadence(
        cadenceHeadingView: HeadingView,
        cadenceChartView: ImageView,
        strokeStyle: Paint,
        trendStyle: Paint,
    ) {
        zipLiveData(viewModel.cadenceMeasurements, viewModel.timeState).observe(
            viewLifecycleOwner
        ) { pair ->
            val measurements = pair.first
            val timeStates = pair.second
            val intervals = getTripIntervals(timeStates, measurements)
            val legs = getTripLegs(measurements, intervals)

            fun makeCadenceDataset(
                measurementsList: Array<CadenceSpeedMeasurement>,
                intervals: Array<LongRange>,
                avgCadence: Float,
            ): LineChartDataset {
                val entries = ArrayList<Pair<Float, Float>>()
                val hi = ArrayList<Pair<Float, Float>>()
                val lo = ArrayList<Pair<Float, Float>>()
                val trend = ArrayList<Pair<Float, Float>>()
                val intervalStart = intervals.last().first
                var trendLast: Float? = null
                var trendAlpha = 0.5f
                var hiLast: Float? = null
                var loLast: Float? = null

                val accumulatedTime = accumulateTime(intervals)
                var lastMeasurements: CadenceSpeedMeasurement? = null
                measurementsList.forEach { measurements ->
                    lastMeasurements
                        ?.let { last ->
                            if (validateCadence(measurements, last)) {
                                try {
                                    getRpm(
                                        rev = measurements.revolutions,
                                        revLast = last.revolutions,
                                        time = measurements.lastEvent,
                                        timeLast = last.lastEvent,
                                        delta = measurements.timestamp - last.timestamp
                                    )
                                        .takeIf { it.isFinite() }
                                        ?.let { rpm ->
                                            val timestamp =
                                                (accumulatedTime + (measurements.timestamp - intervalStart) / 1e3).toFloat()

                                            entries.add(
                                                Pair(
                                                    timestamp,
                                                    rpm
                                                )
                                            )
                                            getTrendData(
                                                rpm,
                                                trendAlpha,
                                                avgCadence,
                                                trendLast,
                                                hiLast,
                                                loLast
                                            ).let { (trendNew, hiNew, loNew) ->
                                                trend.add(
                                                    Pair(
                                                        timestamp,
                                                        trendNew
                                                    )
                                                )
                                                trendLast = trendNew
                                                hiNew?.let {
                                                    hi.add(
                                                        Pair(
                                                            timestamp,
                                                            it
                                                        )
                                                    )
                                                    hiLast = it
                                                }
                                                loNew?.let {
                                                    lo.add(
                                                        Pair(
                                                            timestamp,
                                                            it
                                                        )
                                                    )
                                                    loLast = it
                                                }
                                            }
                                            if (trendAlpha > 0.01f) trendAlpha -= 0.005f
                                            if (trendAlpha < 0.01f) trendAlpha =
                                                0.01f
                                        }

                                } catch (e: Exception) {
                                    Log.e(
                                        logTag,
                                        "Could not create rpm value for timestamp ${measurements.timestamp}"
                                    )
                                }
                            }
                        }
                    lastMeasurements = measurements
                }

                return LineChartDataset(entries = entries, trend = trend, hi = hi, lo = lo)
            }

            fun makeCadenceLineChart(
                intervals: Array<LongRange>,
                legs: Array<Array<CadenceSpeedMeasurement>>,
                avgCadence: Float
            ) {
                val allData = LineChartDataset(
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                )
                legs.forEachIndexed { idx, leg ->
                    makeCadenceDataset(
                        leg,
                        intervals.sliceArray(IntRange(0, idx)),
                        avgCadence
                    ).let {
                        allData.entries.addAll(it.entries)
                        allData.trend.addAll(it.trend)
                        allData.hi.addAll(it.hi)
                        allData.lo.addAll(it.lo)
                    }
                }
                allData.takeIf { it.trend.size > 0 }?.let { lineData ->
                    try {
                        cadenceChartView.setImageDrawable(
                            getSpeedCadenceLineGraph(
                                lineData,
                                avgCadence,
                                { value: Float -> "${value.roundToInt()} rpm" },
                                strokeStyle,
                                trendStyle.apply { style = Paint.Style.FILL_AND_STROKE }
                            )
                        )
                    } catch (e: Exception) {
                        Log.e(logTag, "Could not create cadence chart", e)
                    }
                }
            }

            val avgCadence = getAverageCadence(legs.flatten().toTypedArray())
            if (avgCadence != null) {
                cadenceHeadingView.visibility = View.VISIBLE
                cadenceChartView.visibility = View.VISIBLE
                cadenceHeadingView.value =
                    "${
                        avgCadence.takeIf { it.isFinite() }?.roundToInt() ?: 0
                    } rpm (average)"
                makeCadenceLineChart(intervals, legs, avgCadence)
            }
        }
    }

    private fun getWattsLineGraph(
        lineData: LineChartDataset,
        average: Float,
        labelTemplate: (Float) -> String,
        strokeStyle: Paint,
        areaStyle: Paint
    ): LineGraph {
        val xMin = lineData.entries.first().first
        val xMax = lineData.entries.last().first
        val yMin = lineData.entries.minBy { element -> element.second }.second
        val yTrendMin = lineData.trend.minBy { element -> element.second }.second
        val yMax = lineData.entries.maxBy { element -> element.second }.second
        val dataMax = lineData.entries.maxBy { element -> element.second }.second
        val yRangePadding = (yMax - yMin) * 0.2f
        val yViewMin = max(yMin - yRangePadding, 0f)

        val dataMaxPadding = (dataMax - yMin) * 0.2f
        return LineGraph(
            datasets = listOf(
                LineGraphDataset(
                    points = lineData.trend.toList(),
                    xRange = Pair(xMin, xMax),
                    xAxisWidth = xMax - xMin,
                    yRange = Pair(yViewMin, dataMax + dataMaxPadding),
                    yAxisHeight = dataMax + dataMaxPadding - yViewMin,
                    paint = strokeStyle
                ),
            ),
            areas = listOf(
                LineGraphAreaDataset(
                    points1 = lineData.hi.toList(),
                    points2 = lineData.lo.toList(),
                    xRange = Pair(xMin, xMax),
                    xAxisWidth = xMax - xMin,
                    yRange = Pair(yViewMin, dataMax + dataMaxPadding),
                    yAxisHeight = dataMax + dataMaxPadding - yViewMin,
                    paint = areaStyle
                ),
            ),
            borders = BordersEnum.BOTTOM.value,
            xLabels = getAxisLabelsTimeX(xMin, xMax, 60f),
            yLabels = AxisLabels(
                labels = listOf(
                    Pair(
                        yTrendMin,
                        labelTemplate(yTrendMin)
                    ),
                    Pair(
                        dataMax,
                        labelTemplate(dataMax)
                    ),
                    Pair(
                        average,
                        labelTemplate(average)
                    )
                ),
                range = Pair(yViewMin, dataMax + dataMaxPadding),
                lines = true,
                background = (scrollView.background as ColorDrawable).color
            )
        )
    }

    private fun observeWatts(
        wattsHeadingView: HeadingView,
        wattsChartView: ImageView,
        strokeStyle: Paint,
        trendStyle: Paint,
    ) {
        zipLiveData(viewModel.powerMeasurements, viewModel.timeState).observe(
            viewLifecycleOwner
        ) { pair ->
            val measurements = pair.first
            val timeStates = pair.second
            val intervals = getTripIntervals(timeStates, measurements)
            val legs = getTripLegs(measurements, intervals)

            fun makeWattsDataset(
                measurementsList: Array<PowerMeterMeasurement>,
                intervals: Array<LongRange>,
                avgWatts: Float,
            ): LineChartDataset {
                val entries = ArrayList<Pair<Float, Float>>()
                val hi = ArrayList<Pair<Float, Float>>()
                val lo = ArrayList<Pair<Float, Float>>()
                val trend = ArrayList<Pair<Float, Float>>()
                val intervalStart = intervals.last().first
                var trendLast: Float? = null
                var trendAlpha = 0.5f
                var hiLast: Float? = null
                var loLast: Float? = null

                val accumulatedTime = accumulateTime(intervals)
                var lastMeasurements: PowerMeterMeasurement? = null
                measurementsList.forEach { measurements ->
                    lastMeasurements
                        ?.let { last ->
                            if (validateWatts(measurements, last)) {
                                try {
                                    getWatts(
                                        watts = measurements.watts!!,
                                        wattsLast = last.watts!!,
                                        time = measurements.timestamp,
                                        timeLast = last.timestamp,
                                        delta = measurements.timestamp - last.timestamp
                                    )
                                        .takeIf { it.isFinite() }
                                        ?.let { watts ->
                                            val timestamp =
                                                (accumulatedTime + (measurements.timestamp - intervalStart) / 1e3).toFloat()

                                            entries.add(
                                                Pair(
                                                    timestamp,
                                                    watts
                                                )
                                            )
                                            getTrendData(
                                                watts,
                                                trendAlpha,
                                                avgWatts,
                                                trendLast,
                                                hiLast,
                                                loLast
                                            ).let { (trendNew, hiNew, loNew) ->
                                                trend.add(
                                                    Pair(
                                                        timestamp,
                                                        trendNew
                                                    )
                                                )
                                                trendLast = trendNew
                                                hiNew?.let {
                                                    hi.add(
                                                        Pair(
                                                            timestamp,
                                                            it
                                                        )
                                                    )
                                                    hiLast = it
                                                }
                                                loNew?.let {
                                                    lo.add(
                                                        Pair(
                                                            timestamp,
                                                            it
                                                        )
                                                    )
                                                    loLast = it
                                                }
                                            }
                                            if (trendAlpha > 0.01f) trendAlpha -= 0.005f
                                            if (trendAlpha < 0.01f) trendAlpha =
                                                0.01f
                                        }

                                } catch (e: Exception) {
                                    Log.e(
                                        logTag,
                                        "Could not create watts value for timestamp ${measurements.timestamp}"
                                    )
                                }
                            }
                        }
                    lastMeasurements = measurements
                }

                return LineChartDataset(entries = entries, trend = trend, hi = hi, lo = lo)
            }

            fun makeWattsLineChart(
                intervals: Array<LongRange>,
                legs: Array<Array<PowerMeterMeasurement>>,
                avgWatts: Float
            ) {
                val allData = LineChartDataset(
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                    ArrayList(),
                )
                legs.forEachIndexed { idx, leg ->
                    makeWattsDataset(
                        leg,
                        intervals.sliceArray(IntRange(0, idx)),
                        avgWatts
                    ).let {
                        allData.entries.addAll(it.entries)
                        allData.trend.addAll(it.trend)
                        allData.hi.addAll(it.hi)
                        allData.lo.addAll(it.lo)
                    }
                }
                allData.takeIf { it.trend.size > 0 }?.let { lineData ->
                    try {
                        wattsChartView.setImageDrawable(
                            getWattsLineGraph(
                                lineData,
                                avgWatts,
                                { value: Float -> "${value.roundToInt()} rpm" },
                                strokeStyle,
                                trendStyle.apply { style = Paint.Style.FILL_AND_STROKE }
                            )
                        )
                    } catch (e: Exception) {
                        Log.e(logTag, "Could not create watts chart", e)
                    }
                }
            }

            val avgWatts = getAverageWatts(legs.flatten().toTypedArray())
            if (avgWatts != null) {
                wattsHeadingView.visibility = View.VISIBLE
                wattsChartView.visibility = View.VISIBLE
                wattsHeadingView.value =
                    "${
                        avgWatts.takeIf { it.isFinite() }?.roundToInt() ?: 0
                    } watts (average)"
                makeWattsLineChart(intervals, legs, avgWatts)
            }
        }
    }

    private fun observeHeartRate(
        heartRateView: HeadingView,
        heartRateChartView: ImageView,
        strokeStyle: Paint,
    ) {
        zipLiveData(viewModel.heartRateMeasurements, viewModel.timeState).observe(
            viewLifecycleOwner
        ) { pair ->
            val hrmData = pair.first
            val timeStates = pair.second
            fun makeHeartRateDataset(
                measurements: Array<HeartRateMeasurement>,
                intervals: Array<LongRange>,
            ): ArrayList<Pair<Float, Float>> {
                val entries = ArrayList<Pair<Float, Float>>()
                val intervalStart = intervals.last().first

                val accumulatedTime = accumulateTime(intervals)

                measurements.forEach {
                    val timestamp =
                        (accumulatedTime + (it.timestamp - intervalStart) / 1e3).toFloat()
                    entries.add(Pair(timestamp, it.heartRate.toFloat()))
                }

                return entries
            }

            fun makeHeartRateLineChart(avgHeartRate: Short) {
                val intervals = getTripIntervals(timeStates, hrmData)
                val legs = getTripLegs(hrmData, intervals)
                val data = ArrayList<Pair<Float, Float>>()

                legs.forEachIndexed { idx, leg ->
                    data.addAll(
                        makeHeartRateDataset(
                            leg,
                            intervals.sliceArray(IntRange(0, idx))
                        )
                    )
                }

                try {
                    val xMin = data.first().first
                    val xMax = data.last().first
                    val yMin = data.minBy { element -> element.second }.second
                    val yMax = data.maxBy { element -> element.second }.second
                    val yRangePadding = (yMax - yMin) * 0.2f
                    val yViewMin = max(yMin - yRangePadding, 0f)
                    val yViewMax = yMax + yRangePadding

                    heartRateChartView.setImageDrawable(
                        LineGraph(
                            datasets = listOf(
                                LineGraphDataset(
                                    points = data.toList(),
                                    xRange = Pair(xMin, xMax),
                                    yRange = Pair(yViewMin, yViewMax),
                                    xAxisWidth = xMax - xMin,
                                    yAxisHeight = yViewMax - yViewMin,
                                    paint = strokeStyle
                                )
                            ),
                            borders = BordersEnum.BOTTOM.value,
                            yLabels = AxisLabels(
                                labels = listOf(
                                    Pair(yMin, "${yMin.roundToInt()} bpm"),
                                    Pair(yMax, "${yMax.roundToInt()} bpm"),
                                    Pair(avgHeartRate.toFloat(), "$avgHeartRate bpm")
                                ),
                                range = Pair(yViewMin, yViewMax),
                                lines = true,
                                background = (scrollView.background as ColorDrawable).color,
                            ),
                            xLabels = getAxisLabelsTimeX(xMin, xMax, 60f)
                        )
                    )
                } catch (e: Exception) {
                    Log.e(logTag, "Could not draw heart rate chart", e)
                }
            }

            val avgHeartRate = getAverageHeartRate(hrmData)
            if (avgHeartRate != null) {
                heartRateView.visibility = View.VISIBLE
                heartRateChartView.visibility = View.VISIBLE
                heartRateView.value = "$avgHeartRate bpm (average)"
                makeHeartRateLineChart(avgHeartRate)
            }
        }
    }

    private fun openMapFile(mapfile: File) {
        try {
            // Tile source
            var fis = FileInputStream(mapfile)
            val tileSource = MapFileTileSource()
            tileSource.setMapFileInputStream(fis)

            var map = mapView.map()
            // Vector layer
            val tileLayer: VectorTileLayer = map.setBaseMap(tileSource)

            // Building layer
            mapView.map().layers().add(BuildingLayer(map, tileLayer))

            // Label layer
            mapView.map().layers().add(LabelLayer(map, tileLayer))

            // Render theme
            var vtmTheme = VtmThemes.DEFAULT
            var maptheme = sharedPreferences.getString(
                currentContext.getString(R.string.preference_map_theme_key),
                "1")
            when (maptheme) {
                "1" -> {
                    vtmTheme = VtmThemes.DEFAULT
                }
                "2" -> {
                    vtmTheme = VtmThemes.NEWTRON
                }
                "3" -> {
                    vtmTheme = VtmThemes.TRONRENDER
                }
                "4" -> {
                    vtmTheme = VtmThemes.BIKER
                }
                "5" -> {
                    vtmTheme = VtmThemes.MAPZEN
                }
                "6" -> {
                    vtmTheme = VtmThemes.OSMARENDER
                }
                "7" -> {
                    vtmTheme = VtmThemes.OSMAGRAY
                }
                "8" -> {
                    vtmTheme = VtmThemes.MOTORIDER
                }
                "9" -> {
                    vtmTheme = VtmThemes.MOTORIDER_DARK
                }
            }

            mapTheme = map.setTheme(vtmTheme) as RenderTheme

            // Scale bar
            if (false) {
                val mapScaleBar: MapScaleBar = DefaultMapScaleBar(map)
                val mapScaleBarLayer = MapScaleBarLayer(map, mapScaleBar)
                mapScaleBarLayer.renderer.setPosition(GLViewport.Position.BOTTOM_LEFT)
                mapScaleBarLayer.renderer.setOffset(5 * CanvasAdapter.getScale(), 0f)
                map.layers().add(mapScaleBarLayer)
            }
        } catch (e: java.lang.Exception) {
            /*
             * In case of map file errors avoid crash, but developers should handle these cases!
             */
            e.printStackTrace()
        }
    }

    private fun initMapView(context: Context) {
        var mapFilename = sharedPreferences.getString(getString(R.string.preferences_map_filename_key), "")
        if (!mapFilename.isNullOrEmpty()) {
            var mapFile = File(mapFilename)
            if (mapFile.exists()) {
                openMapFile(mapFile)
            } else {
                Log.e(logTag, "FAILED to load map file " + mapFile.name)
                Toast.makeText(currentContext, "FAILED to load map file " + mapFile.name, Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(currentContext, "No map was selected ", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onItemSingleTapUp(index: Int, item: MarkerInterface?): Boolean {
        return true
    }

    override fun onItemLongPress(index: Int, item: MarkerInterface?): Boolean {
        return true
    }
}
