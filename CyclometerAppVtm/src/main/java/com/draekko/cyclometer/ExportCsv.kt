package com.draekko.cyclometer

import android.content.Context
import android.util.Log
import android.widget.Toast
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


fun exportRideToCsv(
    context: Context,
    file: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    try {
        Log.d("EXPORT_SERVICE", "Exporting...")
        FileOutputStream(file).use {
            Log.d("EXPORT_SERVICE", "Opening...")
            ZipOutputStream(BufferedOutputStream(it)).use { stream ->
                Log.d("EXPORT_SERVICE", "Zipping...")

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary!!.id
                            )
                        }_measurements.csv"
                    )
                )
                getDataCsv(exportData.measurements!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary.id
                            )
                        }_onboardSensors.csv"
                    )
                )
                getDataCsv(exportData.onboardSensors!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary.id
                            )
                        }_timeStates.csv"
                    )
                )
                getDataCsv(exportData.timeStates!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                stream.finish()
                stream.close()
            }
        }
        Toast.makeText(context, "Exported " + file.name, Toast.LENGTH_LONG).show()
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(context, "Failed to export " + file.name, Toast.LENGTH_LONG).show()
    }
}
