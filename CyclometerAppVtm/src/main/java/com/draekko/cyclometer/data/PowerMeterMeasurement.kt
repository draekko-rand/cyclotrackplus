package com.draekko.cyclometer.data

import androidx.annotation.Keep
import androidx.room.*
import com.draekko.cyclometer.util.SystemUtils

enum class PowerMeterSensorType(val value: Int) {
    ESTIMATION(1),
    CADENCE_ESTIMATION(2),
    POWER_METER(3),
    UNKNOWN(99),
}

class PowerSensorTypeConverter {
    @TypeConverter
    fun fromSensorType(value: SensorType): Int {
        return value.value
    }

    @TypeConverter
    fun toSensorType(value: Int): PowerMeterSensorType {
        return when (value) {
            1 -> PowerMeterSensorType.ESTIMATION
            2 -> PowerMeterSensorType.CADENCE_ESTIMATION
            3 -> PowerMeterSensorType.POWER_METER
            else -> {
                PowerMeterSensorType.UNKNOWN
            }
        }
    }
}


@Entity(
    foreignKeys = [ForeignKey(
        entity = Trip::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("tripId"),
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index(value = ["tripId"])]
)
@Keep
data class PowerMeterMeasurement(
    val tripId: Long,
    val timestamp: Long = SystemUtils.currentTimeMillis(),
    val watts: Int?,
    val rpm: Int?,
    val sensorType: PowerMeterSensorType,
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
)
