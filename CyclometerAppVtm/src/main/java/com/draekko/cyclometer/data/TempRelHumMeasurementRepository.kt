package com.draekko.cyclometer.data

import javax.inject.Inject

class TempRelHumMeasurementRepository @Inject constructor(private val tempRelHumMeasurementDao: TempRelHumMeasurementDao) {
    suspend fun save(new: TempRelHumMeasurement) = tempRelHumMeasurementDao.save(new)
    fun observeTripTemperature(tripId: Long) = tempRelHumMeasurementDao.subscribeTemperature(tripId)
    fun observeTripRelativeHumidity(tripId: Long) = tempRelHumMeasurementDao.subscribeRelativeHumidity(tripId)
    suspend fun getTemperatureMeasurements(tripId: Long) = tempRelHumMeasurementDao.loadTemperature(tripId)
    suspend fun getRelativeHumidityMeasurements(tripId: Long) = tempRelHumMeasurementDao.loadRelativeHumidity(tripId)
    suspend fun changeTrip(tripId: Long, newTripId: Long) = tempRelHumMeasurementDao.changeTrip(tripId, newTripId)

/*
    suspend fun getAutoTempRelHumTimeStates(
        tripId: Long,
        referenceTime: Long = System.currentTimeMillis(),
        temperatureThreshold: Float = 1f,
        pauseThreshold: Long = 10000,
        resumeThreshold: Long? = null,
    ) = tempRelHumMeasurementDao.getAutoTempRelHumTimeStates(
        tripId = tripId,
        referenceTime = referenceTime,
        temperatureThreshold = temperatureThreshold,
        pauseThreshold = pauseThreshold,
        resumeThreshold = resumeThreshold ?: pauseThreshold,
    )
 */
}
