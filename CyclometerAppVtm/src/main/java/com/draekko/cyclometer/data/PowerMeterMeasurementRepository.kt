package com.draekko.cyclometer.data

import javax.inject.Inject

class PowerMeterMeasurementRepository @Inject constructor(private val powerMeterMeasurementDao: PowerMeterMeasurementDao) {
    suspend fun save(new: PowerMeterMeasurement) = powerMeterMeasurementDao.save(new)

    fun observeTripEstimation(tripId: Long) = powerMeterMeasurementDao.subscribePowerMeterEstimation(tripId)
    fun observeTripCadenceEstimation(tripId: Long) = powerMeterMeasurementDao.subscribePowerMeterCadenceEstimation(tripId)
    fun observeTripPowerMeter(tripId: Long) = powerMeterMeasurementDao.subscribePowerMeter(tripId)

    suspend fun getEstimationMeasurements(tripId: Long) = powerMeterMeasurementDao.loadPowerMeterEstimation(tripId)
    suspend fun getCadenceEstimationMeasurements(tripId: Long) = powerMeterMeasurementDao.loadPowerMeterCadenceEstimation(tripId)
    suspend fun getPowerMeterMeasurements(tripId: Long) = powerMeterMeasurementDao.loadPowerMeter(tripId)

    suspend fun changeTrip(tripId: Long, newTripId: Long) = powerMeterMeasurementDao.changeTrip(tripId, newTripId)

    /*
    suspend fun getPowerMeterAutoTimeStates(
        tripId: Long,
        referenceTime: Long = System.currentTimeMillis(),
        wattsThreshold: Float = 50f,
        pauseThreshold: Long = 10000,
        resumeThreshold: Long? = null,
    ) = powerMeterMeasurementDao.getPowerMeterAutoTimeStates(
        tripId = tripId,
        referenceTime = referenceTime,
        wattsThreshold = wattsThreshold,
        pauseThreshold = pauseThreshold,
        resumeThreshold = resumeThreshold ?: pauseThreshold,
    )
     */
}
