package com.draekko.cyclometer.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

const val TEMPERATURE = 1
const val RELATIVE_HUMIDITY = 2


data class AutoTempRelHumTimeStates(
    val timeState: TimeStateEnum,
    val timestamp: Long,
    val triggered: Boolean,
    val sensorType: TempRelHumSensorType
)

@Dao
interface TempRelHumMeasurementDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(measurements: TempRelHumMeasurement): Long

    @Update
    fun update(measurements: TempRelHumMeasurement)

    @Query("SELECT * FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $TEMPERATURE ORDER BY timestamp ASC")
    suspend fun loadTemperature(tripId: Long): Array<TempRelHumMeasurement>

    @Query("SELECT * FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $TEMPERATURE ORDER BY timestamp ASC")
    fun subscribeTemperature(tripId: Long): LiveData<Array<TempRelHumMeasurement>>

    @Query("SELECT * FROM TempRelHumMeasurement WHERE timestamp = (SELECT max(timestamp) FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $TEMPERATURE) ORDER BY timestamp ASC")
    fun subscribeLatestTemperature(tripId: Long): LiveData<Array<TempRelHumMeasurement>>

    @Query("SELECT * FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $RELATIVE_HUMIDITY ORDER BY timestamp ASC")
    suspend fun loadRelativeHumidity(tripId: Long): Array<TempRelHumMeasurement>

    @Query("SELECT * FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $RELATIVE_HUMIDITY ORDER BY timestamp ASC")
    fun subscribeRelativeHumidity(tripId: Long): LiveData<Array<TempRelHumMeasurement>>

    @Query("SELECT * FROM TempRelHumMeasurement WHERE timestamp = (SELECT max(timestamp) FROM TempRelHumMeasurement WHERE tripId = :tripId and sensorType = $RELATIVE_HUMIDITY) ORDER BY timestamp ASC")
    fun subscribeLatestRelativeHumidity(tripId: Long): LiveData<Array<TempRelHumMeasurement>>

    @Query("UPDATE TempRelHumMeasurement SET tripId = :newTripId WHERE tripId = :tripId")
    suspend fun changeTrip(tripId: Long, newTripId: Long)

    /*
    @Query(
        """
        select 
            case 
                when csm.tempretature >= :temperatureThreshold then 2
                when csm.tempretature < :temperatureThreshold then 1
            end as timeState, 
            max(csm.timestamp) as timestamp,
            case
                when csm.tempretature >= :temperatureThreshold then :referenceTime - max(csm.timestamp) > :pauseThreshold
                when csm.tempretature < :temperatureThreshold then :referenceTime - max(csm.timestamp) > :resumeThreshold
            end as triggered,
            csm.sensorType
        from TempRelHumMeasurement as csm
        where csm.tripId = :tripId and csm.timestamp < :referenceTime and csm.sensorType = 1
        group by csm.tempretature < :temperatureThreshold
    """
    )
    suspend fun getAutoTempRelHumTimeStates(
        tripId: Long,
        referenceTime: Long = System.currentTimeMillis(),
        pauseThreshold: Long = 5000,
        resumeThreshold: Long = 5000,
        temperatureThreshold: Float = 1f
    ): Array<AutoTempRelHumTimeStates>
     */
}

