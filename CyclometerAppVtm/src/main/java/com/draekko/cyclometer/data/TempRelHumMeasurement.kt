package com.draekko.cyclometer.data

import androidx.annotation.Keep
import androidx.room.*
import com.draekko.cyclometer.util.SystemUtils

enum class TempRelHumSensorType(val value: Int) {
    TEMPERATURE(1),
    RELATIVE_HUMIDITY(2),
}

class TempRelHumSensorTypeConverter {
    @TypeConverter
    fun fromTempRelHumSensorType(value: TempRelHumSensorType): Int {
        return value.value
    }

    @TypeConverter
    fun toTempRelHumSensorType(value: Int): TempRelHumSensorType {
        return when (value) {
            1 -> TempRelHumSensorType.TEMPERATURE
            2 -> TempRelHumSensorType.RELATIVE_HUMIDITY
            else -> TempRelHumSensorType.TEMPERATURE
        }
    }
}

@Entity(
    foreignKeys = [ForeignKey(
        entity = Trip::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("tripId"),
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index(value = ["tripId"])]
)
@Keep
data class TempRelHumMeasurement(
    val tripId: Long,
    val timestamp: Long = SystemUtils.currentTimeMillis(),
    val temperature: Float,
    val relativeHumidity: Float,
    val sensorType: TempRelHumSensorType,
    val lastEvent: Int,
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
)
