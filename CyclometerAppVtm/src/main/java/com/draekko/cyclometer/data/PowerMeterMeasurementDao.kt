package com.draekko.cyclometer.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

const val ESTIMATION = 1
const val CADENCE_ESTIMATION = 2
const val POWER_METER = 3

data class PowerMeterAutoTimeState(
    val timeState: TimeStateEnum,
    val timestamp: Long,
    val triggered: Boolean,
    val sensorType: SensorType
)

@Dao
interface PowerMeterMeasurementDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(measurements: PowerMeterMeasurement): Long

    @Update
    fun update(measurements: PowerMeterMeasurement)

    @Query("UPDATE PowerMeterMeasurement SET tripId = :newTripId WHERE tripId = :tripId")
    suspend fun changeTrip(tripId: Long, newTripId: Long)


    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $ESTIMATION ORDER BY timestamp ASC")
    suspend fun loadPowerMeterEstimation(tripId: Long): Array<PowerMeterMeasurement>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $ESTIMATION ORDER BY timestamp ASC")
    fun subscribePowerMeterEstimation(tripId: Long): LiveData<Array<PowerMeterMeasurement>>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE timestamp = (SELECT max(timestamp) FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $ESTIMATION) ORDER BY timestamp ASC")
    fun subscribeLatestPowerMeterEstimation(tripId: Long): LiveData<Array<PowerMeterMeasurement>>


    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $CADENCE_ESTIMATION ORDER BY timestamp ASC")
    suspend fun loadPowerMeterCadenceEstimation(tripId: Long): Array<PowerMeterMeasurement>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $CADENCE_ESTIMATION ORDER BY timestamp ASC")
    fun subscribePowerMeterCadenceEstimation(tripId: Long): LiveData<Array<PowerMeterMeasurement>>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE timestamp = (SELECT max(timestamp) FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $CADENCE_ESTIMATION) ORDER BY timestamp ASC")
    fun subscribeLatestPowerMeterCadenceEstimation(tripId: Long): LiveData<Array<PowerMeterMeasurement>>


    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $POWER_METER ORDER BY timestamp ASC")
    suspend fun loadPowerMeter(tripId: Long): Array<PowerMeterMeasurement>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $POWER_METER ORDER BY timestamp ASC")
    fun subscribePowerMeter(tripId: Long): LiveData<Array<PowerMeterMeasurement>>

    @Query("SELECT * FROM PowerMeterMeasurement WHERE timestamp = (SELECT max(timestamp) FROM PowerMeterMeasurement WHERE tripId = :tripId and sensorType = $POWER_METER) ORDER BY timestamp ASC")
    fun subscribeLatestPowerMeter(tripId: Long): LiveData<Array<PowerMeterMeasurement>>

    /*
    @Query(
        """
        select 
            case 
                when csm.watts >= :wattsThreshold then 2
                when csm.watts < :wattsThreshold then 1
            end as timeState, 
            max(csm.timestamp) as timestamp,
            case
                when csm.watts >= :wattsThreshold then :referenceTime - max(csm.timestamp) > :pauseThreshold
                when csm.watts < :wattsThreshold then :referenceTime - max(csm.timestamp) > :resumeThreshold
            end as triggered,
            csm.sensorType
        from PowerMeterMeasurement as csm
        where csm.tripId = :tripId and csm.timestamp < :referenceTime and csm.sensorType = 1
        group by csm.watts < :wattsThreshold
    """
    )
    suspend fun getPowerMeterAutoTimeStates(
        tripId: Long,
        referenceTime: Long = System.currentTimeMillis(),
        pauseThreshold: Long = 5000,
        resumeThreshold: Long = 5000,
        wattsThreshold: Float = 50f
    ): Array<AutoTimeState>

     */
}
