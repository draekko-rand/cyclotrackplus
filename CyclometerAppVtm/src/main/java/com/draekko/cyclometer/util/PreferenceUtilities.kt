package com.draekko.cyclometer.util

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Rect
import android.util.Log
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.draekko.cyclometer.R
import com.draekko.cyclometer.userCircumferenceToMeters

val PREFS_DISPLAY_SETTINGS_SPEED = "preferences_display_settings_speed"
val PREFS_DISPLAY_SETTINGS_DISTANCE = "preferences_display_settings_distance"
val PREFS_DISPLAY_SETTINGS_AVG_SPEED = "preferences_display_settings_avg_speed"
val PREFS_DISPLAY_SETTINGS_SLOPE = "preferences_display_settings_slope"
val PREFS_DISPLAY_SETTINGS_GRADE = "preferences_display_settings_grade"
val PREFS_DISPLAY_SETTINGS_SPLITS = "preferences_display_settings_splits"
val PREFS_DISPLAY_SETTINGS_DURATION = "preferences_display_settings_duration"
val PREFS_DISPLAY_SETTINGS_HEART_RATE = "preferences_display_settings_heart_rate"
val PREFS_DISPLAY_SETTINGS_CADENCE = "preferences_display_settings_cadence"
val PREFS_DISPLAY_SETTINGS_POWER_METER = "preferences_display_settings_power_meter"
val PREFS_DISPLAY_SETTINGS_TEMPERATURE = "preferences_display_settings_temperature"
val PREFS_DISPLAY_SETTINGS_RH = "preferences_display_settings_rh"
val PREFS_DISPLAY_SETTINGS_AP = "preferences_display_settings_ap"
val PREFS_DISPLAY_SETTINGS_WIND = "preferences_display_settings_wind"
val PREFS_DISPLAY_SETTINGS_GPS_BEARING = "preferences_display_settings_gps_bearing"
val PREFS_DISPLAY_SETTINGS_MAG_BEARING = "preferences_display_settings_mag_bearing"
val PREFS_DISPLAY_SETTINGS_ELEVATION = "preferences_display_settings_elevation"

val DISPLAY_SETTINGS_DISABLED = 0
val DISPLAY_SETTINGS_TOP = 1
val DISPLAY_SETTINGS_TOP_LEFT = 2
val DISPLAY_SETTINGS_TOP_RIGHT = 3
val DISPLAY_SETTINGS_MID_UP_LEFT = 4
val DISPLAY_SETTINGS_MID_UP_RIGHT = 5
val DISPLAY_SETTINGS_MID_LO_LEFT = 6
val DISPLAY_SETTINGS_MID_LO_RIGHT = 7
val DISPLAY_SETTINGS_BOTTOM_LEFT = 8
val DISPLAY_SETTINGS_BOTTOM_RIGHT = 9

fun putSafeZoneMargins(context: Context, margins: Rect) =
    PreferenceManager.getDefaultSharedPreferences(context).edit {
        putInt(context.getString(R.string.preferences_dashboard_safe_zone_top_margin), margins.top)
        putInt(
            context.getString(R.string.preferences_dashboard_safe_zone_bottom_margin),
            margins.bottom
        )
        putInt(
            context.getString(R.string.preferences_dashboard_safe_zone_left_margin),
            margins.left
        )
        putInt(
            context.getString(R.string.preferences_dashboard_safe_zone_right_margin),
            margins.right
        )
        commit()
    }

fun getSafeZoneMargins(context: Context) = Rect(
    getSafeZoneLeftMarginPreference(context),
    getSafeZoneTopMarginPreference(context),
    getSafeZoneRightMarginPreference(context),
    getSafeZoneBottomMarginPreference(context),
)

fun setSafeZoneTopMarginPreference(context: Context, margin: Int) =
    PreferenceManager.getDefaultSharedPreferences(context).edit {
        putInt(context.getString(R.string.preferences_dashboard_safe_zone_top_margin), margin)
        commit()
    }

fun setSafeZoneBottomMarginPreference(context: Context, margin: Int) =
    PreferenceManager.getDefaultSharedPreferences(context).edit {
        putInt(context.getString(R.string.preferences_dashboard_safe_zone_bottom_margin), margin)
        commit()
    }

fun getSafeZoneTopMarginPreference(context: Context): Int =
    getPreferences(context).getInt(
        context.getString(R.string.preferences_dashboard_safe_zone_top_margin), 0
    )

fun getSafeZoneBottomMarginPreference(context: Context): Int =
    getPreferences(context).getInt(
        context.getString(R.string.preferences_dashboard_safe_zone_bottom_margin), 0
    )

fun getSafeZoneLeftMarginPreference(context: Context): Int =
    getPreferences(context).getInt(
        context.getString(R.string.preferences_dashboard_safe_zone_left_margin), 0
    )

fun getSafeZoneRightMarginPreference(context: Context): Int =
    getPreferences(context).getInt(
        context.getString(R.string.preferences_dashboard_safe_zone_right_margin), 0
    )

fun getBrightnessPreference(context: Context): Float {
    return if (getPreferences(context)
            .getBoolean(
                context.getString(R.string.preferences_dashboard_brightness_toggle_key),
                true
            )
    ) {
        getPreferences(context)
            .getInt(context.getString(R.string.preferences_dashboard_brightness_key), 50) / 100f
    } else -1f
}

fun userMassToKilograms(context: Context, input: String?): Float? {
    return try {
        (input?.toFloat()
            ?: Float.NEGATIVE_INFINITY) * (when (PreferenceManager.getDefaultSharedPreferences(
            context
        )
            .getString("mass_display_units", "1")) {
            "1" -> POUNDS_TO_KG
            else -> 1.0
        }).toFloat()
    } catch (e: NumberFormatException) {
        Log.e("TRIP_UTILS_PREF", "userCircumferenceToMeters: Couldn't parse wheel circumference")
        null
    }
}

fun getBikeMassOrNull(context: Context): Float? {
    var mass = getBikeMassOrNull(getPreferences(context), context.getString(R.string.preference_key_bike_mass))
    return userMassToKilograms(context, mass)
}

fun getBikeMassOrNull(prefs: SharedPreferences, key: String): String? {
    val stored = prefs.getString(key, "")
    Log.d("PreferenceUtilities", "Bike mass preference: $stored")
    return stored
}

fun getUserCircumferenceOrNull(context: Context): Float? =
    userCircumferenceToMeters(
        getPreferences(context).getString(
            context.getString(R.string.preference_key_wheel_circumference),
            ""
        )
    )


fun metersToUserCircumference(context: Context, meters: Float): String {
    return metersToUserCircumference(meters, getPreferences(context))
}

fun metersToUserCircumference(meters: Float, prefs: SharedPreferences): String {
    /* Default wheel circumference set for 29 in. wheels, original default was 2037, 2314 */
    val storedCircumference = prefs.getString("wheel_circumference", "2356")
    return com.draekko.cyclometer.metersToUserCircumference(meters, storedCircumference)
}

fun getDistanceSystemOfMeasurement(context: Context): String? =
    getPreferences(context)
        .getString("dist_display_units", "2")

fun getTimeSystemOfMeasurement(context: Context): String? =
    getPreferences(context)
        .getString("time_display_units", "2")

fun getTemperatureSystemOfMeasurement(context: Context): String? =
    getPreferences(context)
        .getString("temp_display_units", "2")

fun getPreferences(context: Context): SharedPreferences =
    PreferenceManager.getDefaultSharedPreferences(context)

fun shouldCollectOnboardSensors(context: Context) = getPreferences(context).getBoolean(
    context.getString(R.string.preferences_key_advanced_onboard_sensors),
    FeatureFlags.betaBuild
)

fun useVo2maxCalorieEstimate(context: Context) = getPreferences(context).getBoolean(
    context.getString(R.string.preferences_key_advanced_use_vo2max_calorie_estimate), false
)

fun isStravaSynced(context: Context) = getPreferences(context).getString(
    context.getString(R.string.preference_key_strava_refresh_token),
    null
).isNullOrBlank().not()
