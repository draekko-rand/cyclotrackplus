package com.draekko.cyclometer.util

import android.location.Location

private var p = Math.PI / 180;
private var r = 6371; // km

fun distance(latStart: Double, lonStart: Double, latEnd: Double, lonEnd: Double): Double {
    var a = 0.5 - Math.cos((latEnd - latStart) * p) / 2
    + Math.cos(latStart * p) * Math.cos(latEnd * p) *
            (1 - Math.cos((lonEnd - lonStart) * p)) / 2;

    return 2 * r * Math.asin(Math.sqrt(a));
}


fun distance(locStart: Location, locEnd: Location): Double {
    return distance(locStart.latitude, locStart.longitude, locEnd.latitude, locEnd.longitude)
}
