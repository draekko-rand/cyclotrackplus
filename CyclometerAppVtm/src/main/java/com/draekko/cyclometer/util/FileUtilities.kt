package com.draekko.cyclometer.util

import android.content.Context
import android.os.Environment
import java.io.File

public val mainStoragePath = "/storage/emulated/0"

fun getFileName(file: File): String? {
    return file.name
}

fun getStorageDirectories(context: Context): Array<File> {
    val listDirs: Array<File>
    val files = context.externalCacheDirs
    if (files.size > 1) {
        val filepath = files[1].absolutePath.split("/".toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()
        var startIndex = 0;
        var count = 0;
        filepath.forEach {
            if (it.lowercase().contains("storage")) {
                startIndex = count
            }
            count++
        }
        val sdcard = "/" + filepath[startIndex] + "/" + filepath[startIndex+1]
        listDirs = arrayOf(
            File(mainStoragePath),
            File(sdcard)
        )
    } else {
        listDirs = arrayOf(
            File(mainStoragePath)
        )
    }
    return listDirs
}


fun getRootPaths(context: Context): Array<String> {
    val fileList = getStorageDirectories(context)
    val listDirs: Array<String>
    if (fileList.size > 1) {
        listDirs = arrayOf(fileList.get(0).absolutePath, fileList.get(1).absolutePath)
    } else {
        listDirs = arrayOf(fileList.get(0).absolutePath)
    }
    return listDirs
}

fun checkSubDirs(context: Context) {
    var storages = getStorageDirectories(context)
    if (storages.size > 1){
        for (storage: File in storages) {
            var externalDir = storage
            var cyclometerdir = externalDir.absolutePath + "/cyclometer"
            var dir = File(cyclometerdir)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            val subDirs = arrayOf("maps", "tracks", "data")
            for (subdirOf in subDirs) {
                var subdir = File(dir, subdirOf)
                if (!subdir.exists()) {
                    subdir.mkdirs()
                }
            }
        }
    } else {
        var externalDir = Environment.getExternalStorageDirectory()
        var cyclometerdir = externalDir.absolutePath + "/cyclometer"
        var dir = File(cyclometerdir)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        val subDirs = arrayOf("maps", "tracks", "data")
        for (subdirOf in subDirs) {
            var subdir = File(dir, subdirOf)
            if (!subdir.exists()) {
                subdir.mkdirs()
            }
        }
    }
}

