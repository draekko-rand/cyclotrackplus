package com.draekko.cyclometer

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Criteria.ACCURACY_FINE
import android.location.Criteria.ACCURACY_HIGH
import android.location.Criteria.NO_REQUIREMENT
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.OnNmeaMessageListener
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class GpsService @Inject constructor(context: Application) : LiveData<Location>() {
    private val logTag = "GpsService"
    private val context = context

    private var nmeaMessage: String? = null
    private var useMsl = true
    private var mslAlitude = -7676.0
    private var mslAlitudeUnit = ""
    private var hdop = -1.0
    private var fixQuality = -1

    var accessGranted = MutableLiveData(false)

    private val locationManager =
        (context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.v(logTag, "New location result")

            if (useMsl && nmeaMessage != null && mslAlitude != -7676.0) {
                location.altitude = mslAlitude
            }
            if (BuildConfig.DEBUG && false) {
                Log.v(
                    logTag,
                    "location: ${location.latitude},${location.longitude} +/- ${location.accuracy}m"
                )
                Log.v(
                    logTag,
                    "bearing: ${location.bearing} +/- ${location.bearingAccuracyDegrees}deg"
                )
                Log.v(
                    logTag,
                    "speed: ${location.speed} +/- ${location.speedAccuracyMetersPerSecond}m/s"
                )
                Log.v(
                    logTag,
                    "altitude: ${location.altitude} +/- ${location.verticalAccuracyMeters}m"
                )
                Log.v(
                    logTag,
                    "timestamp: ${location.elapsedRealtimeNanos}; ${location.time}"
                )
            }

            value = location
        }

        @Deprecated("Uncommented for version 8.1")
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            Log.d(logTag, "GPS status changed: $status")
        }

        override fun onProviderEnabled(provider: String) {
            Log.d(logTag, "GPS provider enabled")
            accessGranted.value = true
        }

        override fun onProviderDisabled(provider: String) {
            Log.d(logTag, "GPS provider disabled")
            accessGranted.value = false
        }
    }

    init {
        accessGranted.value = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        startListening()
        Log.d(logTag, "GPS service initialized")
    }

    fun stopListening() {
        locationManager.removeUpdates(locationListener)
        locationManager.removeNmeaListener(nmeaMessageListener)
    }

    var criteria = Criteria()
    val nmeaMessageListener = OnNmeaMessageListener { message: String, timestamp: Long ->
        if (useMsl) {
            if (message != null && message.lowercase().contains("gga")) {
                var nmeaArray = message.split(",")
                if (nmeaArray[9] != null && !nmeaArray[9].isNullOrEmpty()) {
                    mslAlitude = nmeaArray[9].toDouble()
                }
                if (nmeaArray[10] != null && !nmeaArray[10].isNullOrEmpty()) {
                    mslAlitudeUnit = nmeaArray[10]
                }
                if (nmeaArray[6] != null && !nmeaArray[6].isNullOrEmpty()) {
                    fixQuality = nmeaArray[6].toInt()
                } else {
                    fixQuality = 0
                }
                if (nmeaArray[8] != null && !nmeaArray[8].isNullOrEmpty()) {
                    hdop = nmeaArray[8].toDouble()
                } else {
                    hdop = 0.0
                }

                nmeaMessage = message
            }
        }
    }

    fun startListening() {
        accessGranted.value = true
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(logTag, "User has not granted permission to access fine location data")
            accessGranted.value = false
            return
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                locationManager.addNmeaListener(ContextCompat.getMainExecutor(context), nmeaMessageListener)
            } else {
                locationManager.addNmeaListener(nmeaMessageListener, Handler(Looper.getMainLooper()))
            }
        } catch (e: Exception) {
            Log.e(logTag, "Exception in nmea location flow: $e")
        }

        Log.d(logTag, "Registering GPS location provider")
        //This can safely be called multiple times, will only be registered once
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            1000L,
            1f,
            locationListener
        )
    }

    public fun useMSL(msl: Boolean) {
        useMsl = msl
    }

    public fun getMslStatus():Boolean {
        return useMsl
    }

    public fun getHdop(): Double {
        return hdop
    }

    public fun getFixQuality(): Int {
        return fixQuality
    }
}
