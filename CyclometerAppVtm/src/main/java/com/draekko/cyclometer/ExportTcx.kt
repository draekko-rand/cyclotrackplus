package com.draekko.cyclometer

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.widget.Toast
import com.draekko.cyclometer.data.TimeStateEnum
import com.draekko.cyclometer.util.distance
import com.draekko.cyclometer.util.getFileName
import com.draekko.cyclometer.data.Measurements;
import java.io.File
import java.util.Date

import okhttp3.internal.toHexString
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.TimeZone

@SuppressLint("HardwareIds")
fun writeTcxFile(
    context: Context,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    /*
    val creator = context.getString(R.string.cyclometer)
    val gpxversion = "1.1"
    val metadata_link = "https://draekko.com"
    val metadata_link_text = creator
    val creator_version = BuildConfig.VERSION_NAME

    var minlat = 0.0
    var minlng = 0.0
    var maxlat = 0.0
    var maxlng = 0.0

    var datetimeFormat1 = SimpleDateFormat("yyyy/mm/dd HH:mm", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy/mm/dd HH:mm:ssZ", Locale.US)
    val date = Date(System.currentTimeMillis())
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date).replace(" ", "T").replace("/", "-")
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $startTime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $startTime"
    } catch (e: Exception) {
    }

    var sb = StringBuilder()
    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n")
    sb.append("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\"\n  xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\n  creator=\"$creator v$creator_version\"\n  version=\"$gpxversion\"\n  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n")
    sb.append("  <metadata>\n")
    sb.append("    <name><![CDATA[$datetime]]></name>\n")
    sb.append("    <link href=\"$metadata_link\">\n")
    sb.append("      <text>$metadata_link_text</text>\n")
    sb.append("    </link>\n")
    sb.append("    <time>$datetimeZulu</time>\n")
    sb.append("    <bounds maxlat=\"$maxlat\" maxlon=\"$maxlng\" minlat=\"$minlat\" minlon=\"$minlng\"/>\n")
    sb.append("  </metadata>\n")
    sb.append("  <trk>\n")
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("    <name>$trackname</name>\n")
    } else {
        sb.append("    <name>Undefined</name>\n")
    }
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("    <desc>$trackdesc</desc>\n")
    }
    sb.append("    <type>Undefined</type>\n")
    sb.append("    <trkseg>\n")
    var handleSplitSegments = false
    try {
        var splitsize = exportData.splits!!.size
        if (splitsize > 0) {
            handleSplitSegments = true
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    try {
        var splitCount = 0
        var trksize = exportData.measurements!!.size
        if (trksize > 2) {
            for (measurement: Measurements in exportData.measurements!!) {
                val ptTime = measurement.time
                if (handleSplitSegments) {
                    var splitTime = exportData.splits!!.get(splitCount).timestamp
                    if (ptTime >= splitTime) {
                        sb.append("    </trkseg>\n")
                        sb.append("    <trkseg>\n")
                        splitCount++
                    }
                }
                val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val timedate = sdf.format(Date(ptTime)).replace(" ", "T").replace("/", "-") + "Z"
                val lat = measurement.latitude
                val lng = measurement.longitude
                val elevation = measurement.altitude
                sb.append("      <trkpt lat=\"$lat\" lon=\"$lng\">\n")
                sb.append("        <ele>$elevation</ele>\n")
                sb.append("        <time>$timedate</time>\n")
                sb.append("      </trkpt>\n")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    sb.append("    </trkseg>\n")
    sb.append("  </trk>\n")
    sb.append("</gpx>\n")

    if (BuildConfig.DEBUG) {
        Log.i("GPX", sb.toString())
    }

    try {
        FileOutputStream(file).use {
            val output = sb.toString().toByteArray()
            it.write(output, 0, output.size)
            it.flush()
            it.close()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
     */

    try {
        /*
            var startTime = exportData.timeStates!!.get(0).timestamp
            var endTime = 0L
            var lapCounts = 0
            var usePauseSegments = false
            for (timeState: TimeState in exportData.timeStates) {
                if (timeState.state == TimeStateEnum.STOP) {
                    endTime = timeState.timestamp
                }
                if (timeState.state == TimeStateEnum.RESUME) {
                    lapCounts++
                }
            }
            if (lapCounts == 0) {
                for (timeState: TimeState in exportData.timeStates) {
                    if (timeState.state == TimeStateEnum.PAUSE) {
                        lapCounts++
                        usePauseSegments = true
                    }
                }
            }

            val fileSerializer = FileSerializer(file)
            var trainingCenderDatabase = trainingCenterDatabase()

            // Creation date
            var startDate = Date()
            var endDate = Date()
            startDate.time = startTime
            endDate.time = endTime

            var tcxDate = TCXDate(startDate)

            var track = Track()

            var timestampDate = Date()
            timestampDate.time = 0L

            var position = Position(0.0, 0.0)
            var altitude = 0.0
            var distance = 0.0
            var heartRate = HeartRate(0)
            var cadence = Cadence(0)
            var sensorState = SensorState(SensorState.ABSENT.name)

            var trackPntDate = TCXDate(timestampDate)
            var trackPnt = Trackpoint(trackPntDate, position, altitude, distance, heartRate, cadence, sensorState)
            track.add(trackPnt)

            var lapdate = Date()
            var lapStartDate = TCXDate(lapdate)
            var lap = Lap(lapStartDate)
            var laps = ArrayList<Lap>()
            //laps.add(lap)

            var notes = Notes("")
            var majorVersion = 0
            var minorVersion = 0
            var version = Version(majorVersion, minorVersion)
            var unitId = 1
            var appName = "Cyclometer"
            var productId = appName.hashCode()
            var creator = Device(appName, unitId, productId, version)
            var activity = Activity(tcxDate, laps, notes, creator, Sport.BIKING)
            var activities = Activities(activity)

            trainingCenderDatabase.withActivities(activities)
            trainingCenderDatabase.build().serialize(fileSerializer)
            fileSerializer.save()
            */

        parseOutputTcxFile(context, startTime, file, exportData)
        Toast.makeText(context, "Exported " + file.name, Toast.LENGTH_LONG).show()
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(context, "Failed to export " + file.name, Toast.LENGTH_LONG).show()
    }
}

private class TrackSegment {
    var timestampPause: Long = 0L
    var timestampResume: Long = 0L
}

private class TrackPointData {
    var timeStamp: Long = 0L

    var latitude: Double = -7777.0
    var longitude: Double = -7777.0
    var altitude: Double = -7777.0
    var hasLocation = false

    var slope: Float = -7777f

    var heartRate: Int = -7777
    var hasHr = false

    var rpm: Int = -7777
    var hasCadence = false

    var power: Int = -7777
    var hasPower = false

    var speed: Float = -7777f
    var hasSpeed = false

    var temperature: Float = -7777f
    var hastemp = false

    var airpressure: Float = -7777f
    var hasAirPres = false

    var relhumidity: Float = -7777f
    var hasRelHum = false
}

fun parseOutputTcxFile(context: Context,
                         startTime: DateTime,
                         file: File,
                         exportData: TripDetailsViewModel.ExportData) {

    var allTpt = ArrayList<TrackPointData>()
    var allSeg = ArrayList<TrackSegment>()
    if (exportData != null && exportData.measurements != null && exportData.measurements.size > 0) {
        var pauseCount = 0
        var resumeCount = 0

        if (exportData.timeStates != null && exportData.timeStates.size > 2) {
            var size = exportData.timeStates.size
            for (index in 1..(size-1)) {
                var ts1 = exportData.timeStates.get(index-1)
                var ts2 = exportData.timeStates.get(index)
                if (ts1.state == TimeStateEnum.PAUSE && ts2.state == TimeStateEnum.RESUME) {
                    resumeCount++
                    var segment = TrackSegment()
                    segment.timestampPause = ts1.timestamp
                    segment.timestampResume = ts2.timestamp
                    allSeg.add(segment)
                }
                if (ts1.state == TimeStateEnum.PAUSE && ts2.state == TimeStateEnum.PAUSE) {
                    pauseCount++
                    var segment = TrackSegment()
                    segment.timestampPause = ts1.timestamp
                    segment.timestampResume = -7777L
                    allSeg.add(segment)
                }
            }
        }

        for (measurement: Measurements in exportData.measurements) {
            var tpt = TrackPointData()
            tpt.timeStamp = measurement.time
            tpt.latitude = measurement.latitude
            tpt.longitude = measurement.longitude
            tpt.altitude = measurement.altitude
            tpt.speed = measurement.speed
            tpt.hasLocation = true
            allTpt.add(tpt)
        }
    } else {
        Toast.makeText(context, "ERROR: Missing data file not exported", Toast.LENGTH_LONG).show()
        return
    }

    var tab = "  "
    var appName = context.getString(R.string.app_name)
    var versionMajor = BuildConfig.VERSION_NAME.split(".")[0]
    var versionMinor = BuildConfig.VERSION_NAME.split(".")[1]
    var buildMajor = BuildConfig.VERSION_CODE
    var buildMinor = "0"
    var partNumber = appName.hashCode().toHexString()
    val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    val timedate = sdf.format(startTime.toDate()).replace(" ", "T").replace("/", "-") + ".000Z"

    var totalSegments = 1
    var totalTrackPoints = 1

    var sb = StringBuilder()

    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    sb.append("<TrainingCenterDatabase" +
            "  xsi:schemaLocation=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2" +
            "  http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd\"" +
            "  xmlns:ns5=\"http://www.garmin.com/xmlschemas/ActivityGoals/v1\"" +
            "  xmlns:ns3=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\"" +
            "  xmlns:ns2=\"http://www.garmin.com/xmlschemas/UserProfile/v2\"" +
            "  xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\"" +
            "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
            "  xmlns:ns4=\"http://www.garmin.com/xmlschemas/ProfileExtension/v1\">\n")

    // Activities
    sb.append(tab+"<Activities>\n")
    sb.append(tab+tab+"<Activity Sport=\"Biking\">")
    sb.append(tab+tab+tab+"<Id>$timedate</Id>\n")

    // Lap Data
    sb.append(tab+tab+tab+"<Lap StartTime=\"$timedate\">\n")
    /*
    sb.append(tab+tab+tab+tab+"<TotalTimeSeconds>$totalTimeSeconds</TotalTimeSeconds>\n")
    sb.append(tab+tab+tab+tab+"<DistanceMeters>$distanceMeters</DistanceMeters>\n")
    sb.append(tab+tab+tab+tab+"<MaximumSpeed>$maxSpeed</MaximumSpeed>\n")
    sb.append(tab+tab+tab+tab+"<Calories>$calories</Calories>\n")
    sb.append(tab+tab+tab+tab+"<AverageHeartRateBpm>\n")
    sb.append(tab+tab+tab+tab+tab+"<Value>$avgHeartRate</Value>\n")
    sb.append(tab+tab+tab+tab+"</AverageHeartRateBpm>\n")
    sb.append(tab+tab+tab+tab+"<MaximumHeartRateBpm>\n")
    sb.append(tab+tab+tab+tab+tab+"<Value>$maxHeartRate</Value>\n")
    sb.append(tab+tab+tab+tab+"</MaximumHeartRateBpm>\n")
     */

    var segmentTimes = ArrayList<Long>()
    if (exportData.timeStates != null) {
        var size = exportData.timeStates!!.size - 1
        if (size > 0) {
            var timestamp: Long = 0L
            for (index in 0..size) {
                if (exportData.timeStates.get(index).state == TimeStateEnum.PAUSE) {
                   timestamp = exportData.timeStates.get(index).timestamp
                   segmentTimes.add(timestamp)
                }
            }
        } else {
            totalTrackPoints = 0
        }
    } else {
        totalTrackPoints = 0
    }

    // Track data
    for (segment in 0..totalSegments) {
        sb.append(tab + tab + tab + tab + "<Track>\n")

        //Loop through track points
        for (trackPoint in 0..totalTrackPoints) {
            var hasLocation = false
            var hasCadence = false
            var hasSpeed = false
            var hasWatts = false
            var hasHR = false
            var hasTemp = false
            var hasAP = false
            var hasRH = false
            var trackPointTime = "1970-01-01T00:00:00.0000Z"
            var latitude = 0.0
            var longitude = 0.0
            var elevation = 0.0
            var distanceMeters = 0.0
            var heartRate = 0
            var speed = 0.0
            var power = 0
            var cadence = 0
            var temp = 0.0
            var relhum = 0.0
            var airpres = 0.0
            if (trackPoint > 0) {
                var latS = exportData.measurements!!.get(trackPoint-1).latitude
                var lonS = exportData.measurements!!.get(trackPoint-1).longitude
                var latE = exportData.measurements!!.get(trackPoint).latitude
                var lonE = exportData.measurements!!.get(trackPoint).longitude
                distanceMeters = distance(latS, lonS, latE, lonE) * 1000.0
            } else {
                distanceMeters = 0.0
            }
            sb.append(tab+tab+tab+tab+tab+"<Trackpoint>\n")
            sb.append(tab+tab+tab+tab+tab+tab+"<Time>$trackPointTime</Time>\n")
            if (hasLocation) {
                sb.append(tab+tab+tab+tab+tab+tab+"<Position>\n")
                sb.append(tab+tab+tab+tab+tab+tab+tab+"<LatitudeDegrees>$latitude</LatitudeDegrees>\n")
                sb.append(tab+tab+tab+tab+tab+tab+tab+"<LongitudeDegrees>$longitude</LongitudeDegrees>\n")
                sb.append(tab+tab+tab+tab+tab+tab+"</Position>\n")
                sb.append(tab+tab+tab+tab+tab+tab+"<AltitudeMeters>$elevation</AltitudeMeters>\n")
                sb.append(tab+tab+tab+tab+tab+tab+"<DistanceMeters>$distanceMeters</DistanceMeters>\n")
            }
            if (hasHR) {
                sb.append(tab+tab+tab+tab+tab+tab+"<HeartRateBpm>\n")
                sb.append(tab+tab+tab+tab+tab+tab+tab+"<Value>$heartRate</Value>\n")
                sb.append(tab+tab+tab+tab+tab+tab+"</HeartRateBpm>\n")
            }
            if (hasCadence) {
                sb.append(tab+tab+tab+tab+tab+tab+"<Cadence>$cadence</Cadence>\n")
            }
            if (hasSpeed || hasWatts) {
                sb.append(tab+tab+tab+tab+tab+tab+"<Extensions>\n")
                sb.append(tab+tab+tab+tab+tab+tab+tab+"<ns3:TPX>\n")
                if (hasSpeed) {
                    sb.append(tab+tab+tab+tab+tab+tab+tab+tab+"<ns3:Speed>$speed</ns3:Speed>\n")
                }
                if (hasWatts) {
                    sb.append(tab+tab+tab+tab+tab+tab+tab+tab+"<ns3:Watts>$power</ns3:Watts>\n")
                }
                sb.append(tab+tab+tab+tab+tab+tab+tab+"</ns3:TPX>\n")
                sb.append(tab+tab+tab+tab+tab+tab+"</Extensions>\n")
            }
            sb.append(tab+tab+tab+tab+tab+"</Trackpoint>\n")
        }

        sb.append(tab + tab + tab + tab + "</Track>\n")
    }

    sb.append(tab+tab+tab+"</Lap>\n")

    // Creator details
    sb.append(tab+tab+tab+"<Creator xsi:type=\"Device_t\">\n")
    sb.append(tab+tab+tab+tab+"<Name>$appName</Name>\n")
    sb.append(tab+tab+tab+tab+"<UnitId>${Build.ID.hashCode().toHexString()}</UnitId>\n")
    sb.append(tab+tab+tab+tab+"<ProductID>$partNumber</ProductID>\n")
    sb.append(tab+tab+tab+tab+"<Version>\n")
    sb.append(tab+tab+tab+tab+tab+"<VersionMajor>$versionMajor</VersionMajor>\n")
    sb.append(tab+tab+tab+tab+tab+"<VersionMinor>$versionMinor</VersionMinor>\n")
    sb.append(tab+tab+tab+tab+tab+"<BuildMajor>$buildMajor</BuildMajor>\n")
    sb.append(tab+tab+tab+tab+tab+"<BuildMinor>$buildMinor</BuildMinor>\n")
    sb.append(tab+tab+tab+tab+"</Version>\n")
    sb.append(tab+tab+tab+"</Creator>\n")

    sb.append(tab+tab+"</Activity>\n")
    sb.append(tab+"</Activities>\n")

    // Author details
    sb.append(tab+"<Author xsi:type=\"Application_t\">\n")
    sb.append(tab+tab+"<Name>$appName</Name>\n")
    sb.append(tab+tab+"<Build>\n")
    sb.append(tab+tab+tab+"<Version>\n")
    sb.append(tab+tab+tab+tab+"<VersionMajor>$versionMajor</VersionMajor>\n")
    sb.append(tab+tab+tab+tab+"<VersionMinor>$versionMinor</VersionMinor>\n")
    sb.append(tab+tab+tab+tab+"<BuildMajor>$buildMajor</BuildMajor>\n")
    sb.append(tab+tab+tab+tab+"<BuildMinor>$buildMinor</BuildMinor>\n")
    sb.append(tab+tab+tab+"</Version>\n")
    sb.append(tab+tab+"</Build>\n")
    sb.append(tab+tab+"<LangID>en</LangID>\n")
    sb.append(tab+tab+"<PartNumber>$partNumber</PartNumber>\n")
    sb.append(tab+"</Author>\n")

    sb.append("</TrainingCenterDatabase>\n")
}

fun exportRideToTcx(
    context: Context,
    destination: File,
    exportData: TripDetailsViewModel.ExportData,
) {

    val privateAppFile = File(
        File("/storage/emulated/0/cyclometer/data"),
        getFileName(destination)
    )

    writeTcxFile(
        context,
        DateTime(Date(exportData.summary!!.timestamp)),
        privateAppFile,
        exportData
    )
}
