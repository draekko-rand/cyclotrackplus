package com.draekko.cyclometer.events

data class PermissionDeniedEvent constructor(val permission: String)
