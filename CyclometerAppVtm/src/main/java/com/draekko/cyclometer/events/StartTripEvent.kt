package com.draekko.cyclometer.events

data class StartTripEvent constructor(val tripId: Long)