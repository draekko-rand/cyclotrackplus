package com.draekko.cyclometer.events

import com.draekko.cyclometer.data.Bike

data class ConnectedBikeEvent constructor(val bike: Bike)
