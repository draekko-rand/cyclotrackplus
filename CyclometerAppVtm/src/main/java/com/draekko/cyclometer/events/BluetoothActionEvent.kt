package com.draekko.cyclometer.events

data class BluetoothActionEvent constructor(val action: String)