package com.draekko.cyclometer.events

import com.draekko.cyclometer.TripProgress

data class TripProgressEvent constructor(val tripProgress: TripProgress)
