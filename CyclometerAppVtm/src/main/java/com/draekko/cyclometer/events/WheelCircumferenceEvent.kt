package com.draekko.cyclometer.events

data class WheelCircumferenceEvent constructor(
    val circumference: Float?,
    val variance: Double? = null,
)