package com.draekko.cyclometer

import com.draekko.cyclometer.data.CadenceSpeedMeasurement
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.data.Measurements
import kotlin.math.roundToInt

fun getElevationChange(measurements: Array<Measurements>): Pair<Double, Double> =
    accumulateAscentDescent(measurements.map {
        Pair(
            it.altitude,
            it.verticalAccuracyMeters.toDouble()
        )
    })

fun getAverageSpeedRpm(measurements: Array<CadenceSpeedMeasurement>): Float? {
    return try {
        val totalRevs = measurements.lastOrNull()?.revolutions?.let {
            getDifferenceRollover(
                it,
                measurements.first().revolutions
            )
        }
        val duration =
            (measurements.last().timestamp - measurements.first().timestamp) / 1000 / 60

        totalRevs?.toFloat()?.div(duration).takeIf { it?.isFinite() ?: false }
    } catch (e: Exception) {
        null
    }
}

fun getAverageHeartRate(measurements: Array<HeartRateMeasurement>): Short? {
    var sum = 0f
    var count = 0
    measurements.forEach {
        sum += it.heartRate
        ++count
    }
    return if (count == 0) {
        null
    } else {
        (sum / count).roundToInt().toShort()
    }
}
