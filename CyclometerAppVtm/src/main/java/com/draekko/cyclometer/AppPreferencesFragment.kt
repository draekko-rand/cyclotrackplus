package com.draekko.cyclometer

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import androidx.core.view.setPadding
import androidx.navigation.findNavController
import androidx.preference.*
import com.draekko.cyclometer.services.BleService
import com.draekko.cyclometer.util.*
import com.draekko.iconcontextmenu.IIconContextItemSelectedListener
import com.draekko.iconcontextmenu.IconContextMenu
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.io.IOException
import java.util.regex.Pattern

class AppPreferencesFragment : PreferenceFragmentCompat() {
    private val logTag = "AppPreferencesFragment"

    private lateinit var currentContext : Context
    private lateinit var localPrefernce: Preference

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onStartProfile(@Suppress("UNUSED_PARAMETER") event: Preference) {
    }

    private fun configureDisconnectStrava(
        context: Context,
        preference: Preference,
        refreshToken: String
    ) {
        preference.apply {
            title = context.getString(R.string.preferences_disconnect_strava_title)
            summary = context.getString(R.string.preferences_disconnect_strava_summary)
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                AlertDialog.Builder(context).apply {
                    setPositiveButton("DISCONNECT") { _, _ ->
                        disconnectStrava(context, refreshToken)
                    }
                    setTitle(getString(R.string.preferences_disconnect_strava_title))
                    setMessage(getString(R.string.strava_logout_dialog_message))
                }.create().show()
                true
            }
        }
    }

    private fun disconnectStrava(context: Context, refreshToken: String) {
        CoroutineScope(Dispatchers.IO).launch {
            updateStravaAuthToken(
                context = context,
                refreshToken = refreshToken
            )?.let { accessToken ->
                try {
                    deauthorizeStrava(accessToken, context)
                } catch (e: IOException) {
                    Toast.makeText(
                        context,
                        "Failed to disconnect from Strava. Please try again.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun configureConnectStrava(context: Context, preference: Preference) {
        preference.apply {
            title = context.getString(R.string.preferences_sync_with_strava_title)
            summary = context.getString(R.string.preferences_sync_with_strava_summary)
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                val connectStravaButton = ImageView(context).apply {
                    setImageResource(R.drawable.ic_btn_strava_connectwith_orange)
                    setOnClickListener {
                        startStravaConnectActivity()
                    }
                    val marginDip = 18f
                    val marginPx = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        marginDip,
                        resources.displayMetrics
                    ).toInt()
                    setPadding(marginPx)
                }
                AlertDialog.Builder(context).apply {
                    setTitle(getString(R.string.preferences_sync_with_strava_title))
                    setMessage(getString(R.string.strava_sync_dialog_description))
                    setView(connectStravaButton)
                }.create().show()
                true
            }
        }
    }

    private fun Preference.startStravaConnectActivity() {
        val intentUri = Uri.parse("https://www.strava.com/oauth/mobile/authorize")
            .buildUpon()
            .appendQueryParameter("client_id", getString(R.string.strava_client_id))
            .appendQueryParameter(
                "redirect_uri",
                "cyclometer://draekko.com/strava-auth"
            )
            .appendQueryParameter("response_type", "code")
            .appendQueryParameter("approval_prompt", "auto")
            .appendQueryParameter("scope", "activity:write,read")
            .build()

        Log.d(logTag, "${this.fragment}")
        startActivity(Intent(Intent.ACTION_VIEW, intentUri))
        requireActivity().finish()
    }

    private fun configureStravaConnectPref() {
        findPreference<Preference>(getString(R.string.preferences_key_strava))?.apply {
            getPreferences(context).getString(
                requireContext().getString(R.string.preference_key_strava_refresh_token),
                null
            ).let { refreshToken ->
                if (refreshToken.isNullOrBlank()) {
                    configureConnectStrava(context, this)
                } else {
                    configureDisconnectStrava(context, this, refreshToken)
                }
            }
        }
    }

    private fun configureClearPreferences() {
        findPreference<Preference>(getString(R.string.preferences_clear_preferences_key))?.apply {
            isVisible = true
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                AlertDialog.Builder(context).apply {
                    setPositiveButton("CLEAR") { _, _ ->
                        PreferenceManager.getDefaultSharedPreferences(context).edit().clear()
                            .commit()
                    }
                    setTitle("Clear Preferences?")
                    setMessage("You are about to clear all shared preferences. This cannot be undone.")
                }.create().show()
                true
            }
        }
    }

    var prefListener =
        SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            when (key) {
                requireContext().getString(R.string.preference_key_strava_refresh_token) -> configureStravaConnectPref()
            }
        }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().findViewById<Toolbar>(R.id.preferences_toolbar).title = "Settings"
        Log.d(logTag, "$this")
        getPreferences(requireContext()).registerOnSharedPreferenceChangeListener(prefListener)
    }

    override fun onPause() {
        super.onPause()
        getPreferences(requireContext()).unregisterOnSharedPreferenceChangeListener(prefListener)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.app_preferences, rootKey)

        val firstTime = getPreferences(currentContext).getBoolean("preferences_key_first_time", true)
        if (firstTime) {

            /* HEADER TOP CENTER */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_SPEED)?.apply {
                PREFS_DISPLAY_SETTINGS_SPEED
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_TOP).commit()
            }

            /* TOP LEFT */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_DURATION)?.apply {
                PREFS_DISPLAY_SETTINGS_DURATION
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_TOP_LEFT).commit()
            }

            /* TOP RIGHT */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_DISTANCE)?.apply {
                PREFS_DISPLAY_SETTINGS_DISTANCE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_TOP_RIGHT).commit()
            }

            /* MIDDLE UPPER LEFT */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_AVG_SPEED)?.apply {
                PREFS_DISPLAY_SETTINGS_AVG_SPEED
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_MID_UP_LEFT).commit()
            }

            /* MIDDLE UPPER LEFT */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_SPLITS)?.apply {
                PREFS_DISPLAY_SETTINGS_SPLITS
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT).commit()
            }

            /* MIDDLE LOWER LEFT */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_SLOPE)?.apply {
                PREFS_DISPLAY_SETTINGS_SLOPE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_MID_LO_LEFT).commit()
            }

            /* =========================================================== */
            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_GRADE)?.apply {
                PREFS_DISPLAY_SETTINGS_GRADE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_HEART_RATE)?.apply {
                PREFS_DISPLAY_SETTINGS_HEART_RATE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_CADENCE)?.apply {
                PREFS_DISPLAY_SETTINGS_CADENCE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_POWER_METER)?.apply {
                PREFS_DISPLAY_SETTINGS_POWER_METER
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_TEMPERATURE)?.apply {
                PREFS_DISPLAY_SETTINGS_TEMPERATURE
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_RH)?.apply {
                PREFS_DISPLAY_SETTINGS_RH
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_AP)?.apply {
                PREFS_DISPLAY_SETTINGS_AP
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_WIND)?.apply {
                PREFS_DISPLAY_SETTINGS_WIND
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_GPS_BEARING)?.apply {
                PREFS_DISPLAY_SETTINGS_GPS_BEARING
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_MAG_BEARING)?.apply {
                PREFS_DISPLAY_SETTINGS_MAG_BEARING
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
            }

            findPreference<Preference>(PREFS_DISPLAY_SETTINGS_ELEVATION)?.apply {
                PREFS_DISPLAY_SETTINGS_ELEVATION
                getPreferences(currentContext).edit().putInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED).commit()
            }

            getPreferences(currentContext).edit().putBoolean("preferences_key_first_time", false).commit()
        }


        findPreference<Preference>(getString(R.string.preference_key_dashboard_safe_zone))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_set_dashboard_safe_zone)
                    true
                } == true
            }
        }

        findPreference<Preference>(getString(R.string.preference_key_bike_specs))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_edit_bike_specs)
                    true
                } == true
            }
        }

        findPreference<Preference>(getString(R.string.preferences_paired_ble_devices_key))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_show_linked_sensors)
                    true
                } == true
            }
        }

        findPreference<Preference>(getString(R.string.preferences_key_strava))?.apply {
            getPreferences(context).getString(
                requireContext().getString(R.string.preference_key_strava_refresh_token),
                null
            ).let { refreshToken ->
                if (refreshToken.isNullOrBlank()) {
                    configureConnectStrava(context, this)
                } else {
                    configureDisconnectStrava(context, this, refreshToken)
                }
            }
        }

        findPreference<Preference>(getString(R.string.preferences_tcx_gpx_track_filename_key))?.apply {
            var value = preferenceManager.sharedPreferences!!.getString(requireContext().getString(R.string.preferences_tcx_gpx_track_filename_key), "")
            if (value.isNullOrEmpty()) {
                this.summary = "No TCX/GPX file selected"
            } else {
                var file = File(value)
                this.summary = "Tcx/Gpx File: " + file.name
            }
        }

        findPreference<Preference>(getString(R.string.preferences_map_filename_key))?.apply {
            var value = preferenceManager.sharedPreferences!!.getString(requireContext().getString(R.string.preferences_map_filename_key), "")
            if (value.isNullOrEmpty()) {
                this.summary = "No MAP file selected"
            } else {
                var file = File(value)
                this.summary = "Map File: " + file.name
            }
        }

        findPreference<Preference>(getString(R.string.preference_key_autopause_preferences))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_autopause_preferences)
                    true
                } == true
            }
        }

        findPreference<Preference>(getString(R.string.preference_key_advanced_preferences))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_advanced_preferences)
                    true
                } == true
            }
        }

        findPreference<Preference>(getString(R.string.preference_key_advanced_api_preferences))?.apply {
            onPreferenceClickListener = Preference.OnPreferenceClickListener {
                view?.findNavController()?.let {
                    Log.d(logTag, it.toString())
                    it.navigate(R.id.action_advanced_api_preferences)
                    true
                } == true
            }
        }

        val brightnessToggle =
            findPreference<SwitchPreference>(getString(R.string.preferences_dashboard_brightness_toggle_key))
        findPreference<SeekBarPreference>(getString(R.string.preferences_dashboard_brightness_key))?.apply {
            setOnPreferenceChangeListener { _, _ ->
                brightnessToggle?.isChecked = true
                true
            }
        }

        if (!BleService.isBluetoothSupported(requireContext())) {
            findPreference<Preference>(getString(R.string.preferences_paired_ble_devices_key))?.apply {
                isVisible = false
            }
        }

        if (FeatureFlags.devBuild) {
            configureClearPreferences()
        }

        if (FeatureFlags.betaBuild) {
            findPreference<Preference>(getString(R.string.preferences_display_app_version))?.apply {
                isVisible = true
                summary =
                    "v${BuildConfig.VERSION_CODE}: ${BuildConfig.VERSION_NAME} (${BuildConfig.GIT_HASH})"
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        if (!BleService.isBluetoothSupported(requireContext())) {
            preferenceManager.findPreference<Preference>("paired_blue_devices")?.isEnabled =
                false
        }

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDivider(null)
    }

    private fun handleGpxFileSelection(preference: Preference, showClear: Boolean) {
        if (showClear) {
            val orientationMenu = IconContextMenu(currentContext, R.menu.menu_settings_tcx_gpx_track_file_selection_clear,
                IconContextMenu.THEME_DARK,
                IconContextMenu.SANS
            )
            orientationMenu.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
                if (item.itemId == R.id.action_clear_tcx_gpx_track) {
                    preference.preferenceManager.sharedPreferences!!.edit().
                        putString(getString(R.string.preferences_tcx_gpx_track_filename_key), "").commit()
                    preference.setSummary("No TCX/GPX file selected")
                }
                if (item.itemId == R.id.action_select_tcx_gpx_track_file) {
                    selectGpxFile(preference)
                }
                orientationMenu.dismiss()
            })
            orientationMenu.setOnCancelListener(DialogInterface.OnCancelListener {
                orientationMenu.dismiss()
            })
            orientationMenu.show()
        } else {
            selectGpxFile(preference)
        }
    }

    private var gpxActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult<Intent, ActivityResult>(
            ActivityResultContracts.StartActivityForResult()
        ) { result: ActivityResult ->
            if (Activity.RESULT_OK == result.resultCode) {
                val intent = result.data
                if (intent != null) {
                    val path = intent.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                    if (!path.isNullOrEmpty()) {
                        var file = File(path)
                        if (::localPrefernce.isInitialized) {
                            localPrefernce.preferenceManager.sharedPreferences!!.edit().putString(getString(R.string.preferences_tcx_gpx_track_filename_key), path).commit()
                            localPrefernce.setSummary("Tcx/Gpx File: " + file.name)
                            Log.i(logTag, "Tcx/Gpx File: " + file.name)
                        } else {
                            Log.i(logTag, "ERROR: preference not initialized, cannot load Tcx/Gpx File: " + file.name)
                        }
                    }
                }
            }
        }

    private var mapActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult<Intent, ActivityResult>(
            ActivityResultContracts.StartActivityForResult()
        ) { result: ActivityResult ->
            if (Activity.RESULT_OK == result.resultCode) {
                val intent = result.data
                if (intent != null) {
                    val path = intent.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                    if (!path.isNullOrEmpty()) {
                        var file = File(path)
                        if (::localPrefernce.isInitialized) {
                            localPrefernce.preferenceManager.sharedPreferences!!.edit().putString(getString(R.string.preferences_map_filename_key), path).commit()
                            localPrefernce.setSummary("Map File: " + file.name)
                            Log.i(logTag, "Map File: " + file.name)
                        } else {
                            Log.i(logTag, "ERROR: preference not initialized, cannot load Map File: " + file.name)
                        }
                    }
                }
            }
        }

    private fun selectGpxFile(preference: Preference) {
        localPrefernce = preference
        requireActivity().runOnUiThread {
            var materialFilePicker = MaterialFilePicker()
                .withCallbackSupportFragment(this)
                .withCallbackActivityResults(gpxActivityResult)
                .withHiddenFiles(false)
                .withRootPaths(getRootPaths(currentContext))
                .withPath("/storage/emulated/0/cyclometer/tracks")
                .withFilter(Pattern.compile(".*\\.(gpx|tcx)$"))
                .withTitle("Select New TCX/GPX Track File")
            materialFilePicker.start()
        }
    }

    private fun selectMapFile(preference: Preference) {
        localPrefernce = preference
        requireActivity().runOnUiThread {
            var materialFilePicker = MaterialFilePicker()
                .withCallbackSupportFragment(this)
                .withCallbackActivityResults(mapActivityResult)
                .withHiddenFiles(false)
                .withFilter(Pattern.compile(".*\\.(map)$"))
                .withRootPaths(getRootPaths(currentContext))
                .withPath("/storage/emulated/0/cyclometer/maps")
                .withTitle("Select New Map Track File")
            materialFilePicker.start()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        val key = preference.key
        when(key) {
            getString(R.string.preferences_tcx_gpx_track_filename_key) -> {
                Log.i(logTag, "clicked gpx preference")
                val gpxValue = preference.preferenceManager.sharedPreferences!!.getString(getString(R.string.preferences_tcx_gpx_track_filename_key), "")
                var showClear = false
                if (!gpxValue.isNullOrEmpty()) {
                    showClear = true
                }
                handleGpxFileSelection(preference, showClear)
            }
            getString(R.string.preferences_map_filename_key) -> {
                Log.i(logTag, "clicked map preference")
                selectMapFile(preference)
            }
        }
        return super.onPreferenceTreeClick(preference)
    }
}
