package com.draekko.cyclometer.export

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.draekko.cyclometer.BuildConfig
import com.draekko.cyclometer.data.Measurements
import com.draekko.cyclometer.R
import com.draekko.cyclometer.TripDetailsViewModel
import com.draekko.cyclometer.data.CadenceSpeedMeasurement
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.data.PowerMeterMeasurement
import com.draekko.cyclometer.data.TempRelHumMeasurement
import com.draekko.cyclometer.data.Weather
import com.draekko.cyclometer.getUserDistance
import com.draekko.cyclometer.getUserDistanceUnitLong
import com.draekko.cyclometer.util.getFileName
import org.joda.time.DateTime
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone

val tab = "  "

@SuppressLint("HardwareIds")
fun writeGpxFileNew(
    context: Context,
    useStrictXsd: Boolean,
    useMultiSegments: Boolean,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    if (exportData.measurements != null && exportData.measurements!!.size != null && exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }

    val creator = context.getString(R.string.cyclometer)
    val gpxversion = "1.1"
    val metadata_link = "https://gitlab.com/draekko-rand/cyclometer"
    val metadata_link_text = creator
    val creator_version = BuildConfig.VERSION_NAME

    var minlat = 0.0
    var minlng = 0.0
    var maxlat = 0.0
    var maxlng = 0.0

    var datetimeFormat1 = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    datetimeFormat1.timeZone = TimeZone.getTimeZone("UTC")
    datetimeFormat2.timeZone = TimeZone.getTimeZone("UTC")
    val date = Date(startTime.millis)
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date)
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $datetime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $datetime"
    } catch (e: Exception) {
    }

    var sb = StringBuilder()
    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n")
    if (useStrictXsd) {
        sb.append("<gpx " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns=\"http://www.topografix.com/GPX/1/1\" " +
                "xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v2\" "+
                "creator=\"$creator v$creator_version\" " +
                "version=\"$gpxversion\" " +
                "xmlns:xsi=\"" +
                "http://www.w3.org/2001/XMLSchema-instance\" " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " +
                "http://www.topografix.com/GPX/1/1/gpx.xsd "+
                "http://www.garmin.com/xmlschemas/TrackPointExtension/v2 "+
                "https://www8.garmin.com/xmlschemas/TrackPointExtensionv2.xsd" +
                "\">\n")
    } else {
        sb.append("<gpx " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns=\"http://www.topografix.com/GPX/1/1\" " +
                "xmlns:gpxtpx=\"http://www.draekko.com/xmlschemas/CyclometerExtension/v3\" "+
                "creator=\"$creator v$creator_version\" " +
                "version=\"$gpxversion\" " +
                "xmlns:xsi=\"" +
                "http://www.w3.org/2001/XMLSchema-instance\" " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " +
                "http://www.topografix.com/GPX/1/1/gpx.xsd "+
                "http://www.draekko.com/xmlschemas/CyclometerExtension/v3 "+
                "https://gitlab.com/draekko-rand/xmlschemas/-/raw/fa2eb892094186bfa20862ed509df2a335a14b3d/CyclometerExtensionv3.xsd" +
                "http://www.garmin.com/xmlschemas/TrackPointExtension/v2 "+
                "https://www8.garmin.com/xmlschemas/TrackPointExtensionv2.xsd" +
                "\">\n")
    }
    sb.append("${tab}<metadata>\n")
    sb.append("${tab}${tab}<name><![CDATA[$datetime]]></name>\n")
    sb.append("${tab}${tab}<link href=\"$metadata_link\">\n")
    sb.append("${tab}${tab}${tab}<text>$metadata_link_text</text>\n")
    sb.append("${tab}${tab}</link>\n")
    sb.append("${tab}${tab}<time>$datetimeZulu</time>\n")
    sb.append("${tab}${tab}<bounds maxlat=\"$maxlat\" maxlon=\"$maxlng\" minlat=\"$minlat\" minlon=\"$minlng\"/>\n")
    sb.append("${tab}</metadata>\n")
    sb.append("${tab}<trk>\n")
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("${tab}${tab}<name>$trackname</name>\n")
    } else {
        sb.append("${tab}${tab}<name>Undefined</name>\n")
    }
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("${tab}${tab}<desc>$trackdesc</desc>\n")
    }
    sb.append("${tab}${tab}<type>Undefined</type>\n")
    sb.append("${tab}${tab}<trkseg>\n")
    var handleSplitSegments = false
    try {
        var splitsize = exportData.splits!!.size
        if (splitsize > 0) {
            handleSplitSegments = true
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    try {
        var splitCount = 0
        var trksize = exportData.measurements!!.size
        if (trksize > 2) {
            var hasHeartRate = false
            var hasCadenceSensor = false
            var hasSpeedSensor = false
            var hasPowerCadenceSensor = false
            var hasTempSensor = false
            var hasTempWeather = false
            var hasPower = false
            var hasRelHum = false

            if (exportData.heartRateMeasurements != null && exportData.heartRateMeasurements.size > 0) {
                hasHeartRate = true
            }
            if (exportData.cadenceMeasurements != null && exportData.cadenceMeasurements.size > 0) {
                hasCadenceSensor = true
            }
            if (exportData.speedMeasurements != null && exportData.speedMeasurements.size > 0) {
                hasSpeedSensor = true
            }
            if (exportData.powerMeasurements != null && exportData.powerMeasurements.size > 0) {
                hasPowerCadenceSensor = true
            }
            if (!hasTempSensor && exportData.tempMeasurements != null && exportData.tempMeasurements.size > 0) {
                hasTempSensor = true
            }
            if (!hasRelHum && exportData.relHumMeasurements != null && exportData.relHumMeasurements.size > 0) {
                hasRelHum = true
            }
            if (!hasTempWeather && exportData.weather != null && exportData.weather.size > 0) {
                hasTempWeather = true
            }
            if (exportData.powerMeasurements != null && exportData.powerMeasurements.size > 0) {
                hasPower = true
            }

            var pwrwatts: Int = 0
            var heartRate: Int = 0
            var cadenceRpm: Int = 0
            var temperature: Float = 0.0f
            var relativeHumidty: Float = 0.0f
            var speedMPS: Float = 0.0f

            var powerSize:Int = 0
            if (hasPower) {
                powerSize = exportData.powerMeasurements!!.size
            }

            var speedSize:Int = 0
            if (hasSpeedSensor) {
                speedSize = exportData.speedMeasurements!!.size
            }

            var heartRateSize:Int = 0
            if (hasHeartRate) {
                heartRateSize = exportData.heartRateMeasurements!!.size
            }

            var cadenceSize:Int = 0
            if (hasCadenceSensor) {
                cadenceSize = exportData.cadenceMeasurements!!.size
            } else if (hasPowerCadenceSensor) {
                cadenceSize = exportData.powerMeasurements!!.size
            }

            var tempSize:Int = 0
            if (hasTempSensor) {
                tempSize = exportData.tempMeasurements!!.size
            } else if (hasTempWeather) {
                tempSize = exportData.weather!!.size
                exportData.weather!!.get(0).windDirection
            }

            var relHumSize:Int = 0
            if (hasRelHum) {
                relHumSize = exportData.relHumMeasurements!!.size
            }

            var size = exportData.measurements!!.size
            for (loop in 0..size-2) {
                var measurementCurrent = exportData.measurements!!.get(loop)
                var alt = measurementCurrent.altitude
                var lng = measurementCurrent.longitude
                var lat = measurementCurrent.latitude
                var speedMPS = measurementCurrent.speed
                var bearing = measurementCurrent.bearing
                var timestampCurrent = measurementCurrent.time
                var timestampNext = timestampCurrent + 1000
                if (loop <= size-2) {
                    var measurementNext = exportData.measurements!!.get(loop+1)
                    timestampNext = measurementNext.time
                }

                if (hasPower && powerSize > 0) {
                    for (loopitem in 0..powerSize-1) {
                        var timestamp = exportData.powerMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            pwrwatts = exportData.powerMeasurements!!.get(loopitem).watts!!.toInt()
                            break
                        }
                    }
                }

                if (hasHeartRate && heartRateSize > 0) {
                    for (loopitem in 0..heartRateSize-1) {
                        var timestamp = exportData.heartRateMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            heartRate = exportData.heartRateMeasurements!!.get(loopitem).heartRate!!.toInt()
                            break
                        }
                    }
                }

                if (hasSpeedSensor && speedSize > 0) {
                    for (loopitem in 0..speedSize-1) {
                        var timestamp = exportData.speedMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            var rpm = exportData.speedMeasurements!!.get(loopitem).rpmOrSpeed!!.toFloat()
                            var wheel = 2.356f
                            var circumference = wheel / 1000.0f
                            speedMPS = rpm * circumference / 60.0f
                            Log.i("FILE", "SPEED SENSOR " + rpm + " | " + speedMPS + " | " + measurementCurrent.speed)
                            break
                        }
                    }
                }

                if (hasCadenceSensor && cadenceSize > 0) {
                    for (loopitem in 0..cadenceSize-1) {
                        var timestamp = exportData.cadenceMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            cadenceRpm = exportData.cadenceMeasurements!!.get(loopitem).rpmOrSpeed!!.toInt()
                            break
                        }
                    }
                } else if (hasPowerCadenceSensor && cadenceSize > 0) {
                    for (loopitem in 0..cadenceSize-1) {
                        var timestamp = exportData.powerMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            cadenceRpm = exportData.powerMeasurements!!.get(loopitem).rpm!!.toInt()
                            break
                        }
                    }
                }

                if (hasRelHum && relHumSize > 0) {
                    for (loopitem in 0..tempSize-1) {
                        var timestamp = exportData.relHumMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            relativeHumidty = exportData.relHumMeasurements!!.get(loopitem).relativeHumidity!!.toFloat()
                            break
                        }
                    }
                }

                if (hasTempSensor && tempSize > 0) {
                    for (loopitem in 0..tempSize-1) {
                        var timestamp = exportData.tempMeasurements!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            temperature = exportData.tempMeasurements!!.get(loopitem).temperature!!.toFloat()
                            break
                        }
                    }
                } else if (hasTempWeather && tempSize > 0) {
                    for (loopitem in 0..tempSize-1) {
                        var timestamp = exportData.weather!!.get(loopitem).timestamp
                        if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                            temperature = exportData.weather!!.get(loopitem).temperature!!.toFloat()
                            break
                        }
                    }
                }

                val ptTime = measurementCurrent.time
                if (handleSplitSegments) {
                    var splitTime = exportData.splits!!.get(splitCount).timestamp
                    if (ptTime >= splitTime) {
                        sb.append("${tab}${tab}</trkseg>\n")
                        sb.append("${tab}${tab}<trkseg>\n")
                        splitCount++
                    }
                }
                val sdf = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'")
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val timedate = sdf.format(Date(ptTime))

                sb.append("${tab}${tab}${tab}<trkpt lat=\"$lat\" lon=\"$lng\">\n")
                sb.append("${tab}${tab}${tab}${tab}<ele>$alt</ele>\n")
                sb.append("${tab}${tab}${tab}${tab}<time>$timedate</time>\n")

                if (hasHeartRate || hasCadenceSensor || hasPowerCadenceSensor || hasTempSensor || hasTempWeather || hasPower) {
                    sb.append("${tab}${tab}${tab}${tab}<extensions>\n")
                    if (hasHeartRate || hasCadenceSensor || hasPowerCadenceSensor || hasTempSensor || hasTempWeather) {
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:TrackPointExtension>\n")
                        if (hasTempSensor || hasTempWeather) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:atemp>${temperature}</gpxtpx:atemp>\n")
                        }
                        if (hasHeartRate && heartRate != 0) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:hr>${heartRate}</gpxtpx:hr>\n")
                        }
                        if (hasCadenceSensor || hasPowerCadenceSensor) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:cad>${cadenceRpm}</gpxtpx:cad>\n")
                        }
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:speed>${speedMPS}</gpxtpx:speed>\n")
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:bearing>${bearing}</gpxtpx:beaaring>\n")
                        if (hasPower) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:power>${pwrwatts}</gpxtpx:power>\n")
                        }
                        if (!useStrictXsd) {
                            if (hasRelHum) {
                                sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:relativehumidity>${relativeHumidty}</gpxtpx:relativehumidity>\n")
                            }
                            if (hasRelHum) {
                                sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:airpressure>${relativeHumidty}</gpxtpx:airpressure>\n")
                            }
                        }
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}</gpxtpx:TrackPointExtension>\n")
                    }
                    sb.append("${tab}${tab}${tab}${tab}</extensions>\n")
                }
                sb.append("${tab}${tab}${tab}</trkpt>\n")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    sb.append("${tab}${tab}</trkseg>\n")
    sb.append("${tab}</trk>\n")
    sb.append("</gpx>\n")

    if (BuildConfig.DEBUG) {
        Log.i("GPX", sb.toString())
    }

    outputFile(context, file, sb.toString())
}

@SuppressLint("HardwareIds")
fun writeGpxFileOld(
    context: Context,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    if (exportData.measurements != null && exportData.measurements!!.size != null && exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }

    val creator = context.getString(R.string.cyclometer)
    val gpxversion = "1.1"
    val metadata_link = "https://draekko.com"
    val metadata_link_text = creator
    val creator_version = BuildConfig.VERSION_NAME

    var minlat = 0.0
    var minlng = 0.0
    var maxlat = 0.0
    var maxlng = 0.0

    var datetimeFormat1 = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    datetimeFormat1.timeZone = TimeZone.getTimeZone("UTC")
    datetimeFormat2.timeZone = TimeZone.getTimeZone("UTC")
    val date = Date(startTime.millis)
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date)
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $startTime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $startTime"
    } catch (e: Exception) {
    }

    var sb = StringBuilder()
    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n")
    sb.append("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" " +
            "xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" "+
            "creator=\"$creator v$creator_version\"  version=\"$gpxversion\" " +
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
            "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " +
            "http://www.topografix.com/GPX/1/1/gpx.xsd\">\n")
    sb.append("${tab}<metadata>\n")
    sb.append("${tab}${tab}<name><![CDATA[$datetime]]></name>\n")
    sb.append("${tab}${tab}<link href=\"$metadata_link\">\n")
    sb.append("${tab}${tab}${tab}<text>$metadata_link_text</text>\n")
    sb.append("${tab}${tab}</link>\n")
    sb.append("${tab}${tab}<time>$datetimeZulu</time>\n")
    sb.append("${tab}${tab}<bounds maxlat=\"$maxlat\" maxlon=\"$maxlng\" minlat=\"$minlat\" minlon=\"$minlng\"/>\n")
    sb.append("${tab}</metadata>\n")
    sb.append("${tab}<trk>\n")
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("${tab}${tab}<name>$trackname</name>\n")
    } else {
        sb.append("${tab}${tab}<name>Undefined</name>\n")
    }
    if (!trackdesc.isNullOrEmpty()) {
        sb.append("${tab}${tab}<desc>$trackdesc</desc>\n")
    }
    sb.append("${tab}${tab}<type>Undefined</type>\n")
    sb.append("${tab}${tab}<trkseg>\n")
    var handleSplitSegments = false
    try {
        var splitsize = exportData.splits!!.size
        if (splitsize > 0) {
            handleSplitSegments = true
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    try {
        var splitCount = 0
        var trksize = exportData.measurements!!.size
        if (trksize > 2) {
            for (measurement: Measurements in exportData.measurements!!) {
                val ptTime = measurement.time
                if (handleSplitSegments) {
                    var splitTime = exportData.splits!!.get(splitCount).timestamp
                    if (ptTime >= splitTime) {
                        sb.append("${tab}${tab}</trkseg>\n")
                        sb.append("${tab}${tab}<trkseg>\n")
                        splitCount++
                    }
                }

                val sdf = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'")
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val timedate = sdf.format(Date(ptTime))

                val lat = measurement.latitude
                val lng = measurement.longitude
                val elevation = measurement.altitude
                sb.append("${tab}${tab}${tab}<trkpt lat=\"$lat\" lon=\"$lng\">\n")
                sb.append("${tab}${tab}${tab}${tab}<ele>$elevation</ele>\n")
                sb.append("${tab}${tab}${tab}${tab}<time>$timedate</time>\n")

                cadence = 0
                heartrate = 0.0f
                watts = 0
                temp = 0.0f

                var hasHeartRate = false
                var hasCadence = false
                var hasTemp = false
                var hasPower = false

                if (exportData.heartRateMeasurements != null && exportData.heartRateMeasurements.size > 0) {
                    hasHeartRate = findHeartRateForTime(ptTime, exportData.heartRateMeasurements)
                }
                if (!hasCadence && exportData.cadenceMeasurements != null && exportData.cadenceMeasurements.size > 0) {
                    hasCadence = findCadenceForTime(ptTime, exportData.cadenceMeasurements)
                }
                if (!hasCadence && exportData.powerMeasurements != null && exportData.powerMeasurements.size > 0) {
                    hasCadence = findPowerCadenceForTime(ptTime, exportData.powerMeasurements)
                }
                if (!hasTemp && exportData.tempMeasurements != null && exportData.tempMeasurements.size > 0) {
                    hasTemp = findTemperatureForTime(ptTime, exportData.tempMeasurements)
                }
                if (!hasTemp && exportData.weather != null && exportData.weather.size > 0) {
                    hasTemp = findWeatherTemperatureForTime(ptTime, exportData.weather)
                }
                if (exportData.powerMeasurements != null && exportData.powerMeasurements.size > 0) {
                    hasPower = findPowerForTime(ptTime, exportData.powerMeasurements)
                }

                if (hasHeartRate || hasCadence || hasTemp || hasPower) {
                    sb.append("${tab}${tab}${tab}${tab}<extensions>")
                    if (hasPower) {
                        sb.append("${tab}${tab}${tab}${tab}<power>${watts}</power>")
                    }
                    if (hasHeartRate || hasCadence || hasTemp) {
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:TrackPointExtension>")
                        if (hasTemp) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:atemp>${temp.toInt()}</gpxtpx:atemp>")
                        }
                        if (hasHeartRate) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:hr>${heartrate.toInt()}</gpxtpx:hr>")
                        }
                        if (hasCadence) {
                            sb.append("${tab}${tab}${tab}${tab}${tab}${tab}${tab}${tab}<gpxtpx:cad>${cadence.toInt()}</gpxtpx:cad>")
                        }
                        sb.append("${tab}${tab}${tab}${tab}${tab}${tab}</gpxtpx:TrackPointExtension>")
                    }
                    sb.append("${tab}${tab}${tab}${tab}</extensions>")
                }

                sb.append("${tab}${tab}${tab}</trkpt>\n")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    sb.append("${tab}${tab}</trkseg>\n")
    sb.append("${tab}</trk>\n")
    sb.append("</gpx>\n")

    if (BuildConfig.DEBUG) {
        Log.i("GPX", sb.toString())
    }

    outputFile(context, file, sb.toString())
}

var cadence = 0
var heartrate = 0.0f
var watts = 0
var temp = 0.0f

fun findTemperatureForTime(timeStamp: Long, tempMeasurements: Array<TempRelHumMeasurement>?): Boolean {
    if (tempMeasurements != null && tempMeasurements.size > 0) {
        for (ii in 0..(tempMeasurements.size-1)) {
            if (tempMeasurements.get(ii).timestamp >= timeStamp) {
                temp = tempMeasurements.get(ii).temperature
                return true
            }
        }
    }

    return false
}

fun findWeatherTemperatureForTime(timeStamp: Long, tempMeasurements: Array<Weather>?): Boolean {
    if (tempMeasurements != null && tempMeasurements.size > 0) {
        for (ii in 0..(tempMeasurements.size-1)) {
            if (tempMeasurements.get(ii).timestamp >= timeStamp) {
                temp = tempMeasurements.get(ii).temperature.toFloat()
                return true
            }
        }
    }

    return false
}

fun findCadenceForTime(timeStamp: Long, cadenceMeasurements: Array<CadenceSpeedMeasurement>?): Boolean {
    if (cadenceMeasurements != null && cadenceMeasurements.size > 0) {
        for (ii in 0..(cadenceMeasurements.size-1)) {
            if (cadenceMeasurements.get(ii).timestamp >= timeStamp) {
                cadence = cadenceMeasurements.get(ii).rpmOrSpeed!!.toInt()
                return true
            }
        }
    }

    return false
}

fun findHeartRateForTime(timeStamp: Long, heartRateMeasurements: Array<HeartRateMeasurement>?): Boolean {
    if (heartRateMeasurements != null && heartRateMeasurements.size > 0) {
        for (ii in 0..(heartRateMeasurements.size-1)) {
            if (heartRateMeasurements.get(ii).timestamp >= timeStamp) {
                heartrate = heartRateMeasurements.get(ii).heartRate.toFloat()
                return true
            }
        }
    }

    return false
}

fun findPowerForTime(timeStamp: Long, powerMeterMeasurements: Array<PowerMeterMeasurement>?): Boolean {
    if (powerMeterMeasurements != null && powerMeterMeasurements.size > 0) {
        for (ii in 0..(powerMeterMeasurements.size-1)) {
            if (powerMeterMeasurements.get(ii).timestamp >= timeStamp) {
                watts = powerMeterMeasurements.get(ii).watts!!
                return true
            }
        }
    }

    return false
}

fun findPowerCadenceForTime(timeStamp: Long, powerMeterMeasurements: Array<PowerMeterMeasurement>?): Boolean {
    if (powerMeterMeasurements != null && powerMeterMeasurements.size > 0) {
        for (ii in 0..(powerMeterMeasurements.size-1)) {
            if (powerMeterMeasurements.get(ii).timestamp >= timeStamp) {
                cadence = powerMeterMeasurements.get(ii).rpm!!
                return true
            }
        }
    }

    return false
}

/*
@SuppressLint("HardwareIds")
fun writeGpxFileNew(
    context: Context,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    var creator = context.getString(R.string.cyclometer)
    var creator_version = BuildConfig.VERSION_NAME
    //val gpxversion = "1.1"
    //val metadata_link = "https://draekko.com"
    //val metadata_link_text = creator

    var gpx: Gpx.Builder
    gpx = Gpx.Builder()
    gpx.setCreator(creator)
    gpx.setVersion(creator_version)

    // Set Metadata
    var metadata = Metadata.Builder()

    var minlat = 180.0
    var minlng = 180.0
    var maxlat = -180.0
    var maxlng = -180.0

    var listsize = 0
    try {
        listsize = exportData.measurements!!.size
        if (listsize > 2) {
            for (m in 0..(listsize-1)) {
                var altitude = exportData.measurements[m].altitude
                var latitude = exportData.measurements[m].latitude
                var longitude = exportData.measurements[m].longitude
                if (latitude >= maxlat) maxlat = latitude
                if (longitude >= maxlng) maxlng = longitude
                if (latitude < maxlat) minlat = latitude
                if (longitude < maxlng) minlng = longitude
            }
        } else {
            minlat = 0.0
            minlng = 0.0
            maxlat = 0.0
            maxlng = 0.0
        }
    } catch (e: Exception) {
        minlat = 0.0
        minlng = 0.0
        maxlat = 0.0
        maxlng = 0.0
    }

    var bounds = Bounds.Builder()
    if (listsize > 2) {
        bounds.setMaxLat(maxlat)
        bounds.setMaxLon(maxlng)
        bounds.setMinLat(minlat)
        bounds.setMinLon(minlng)
    }

    var link = Link.Builder()
    link.setLinkHref("https://draekko.com")
    link.setLinkText("$creator $creator_version")

    var datetimeFormat1 = SimpleDateFormat("yyyy/mm/dd HH:mm", Locale.US)
    var datetimeFormat2 = SimpleDateFormat("yyyy/mm/dd HH:mm:ssZ", Locale.US)
    val date = Date(System.currentTimeMillis())
    val datetime = datetimeFormat1.format(date).replace("/", "-")
    val datetimeZulu = datetimeFormat2.format(date).replace(" ", "T").replace("/", "-")
    var summary = "Undefined"
    try {
        summary = exportData.summary!!.name!!
    } catch (e: Exception) {
    }
    val trackname = "$summary $startTime"
    var trackdesc = ""
    try {
        var distVal = Math.round(getUserDistance(context, exportData.summary!!.distance!!) * 100.0) / 100.0
        var distValUnit = getUserDistanceUnitLong(context)
        val distance = "$distVal $distValUnit"
        val totalSecs = exportData.summary!!.duration!!.toInt()
        val hours = totalSecs / 3600;
        val minutes = (totalSecs % 3600) / 60;
        val seconds = totalSecs % 60;
        trackdesc = "Ride duration $hours:$minutes:$seconds for a distance of $distance started at $startTime"
    } catch (e: Exception) {
    }

    var time: DateTime = DateTime.parse(datetimeZulu)

    //metadata.setAuthor()
    //metadata.setCopyright()
    //metadata.setKeywords()
    metadata.setDesc(trackdesc)
    if (listsize > 2) {
        metadata.setBounds(bounds.build())
    }
    metadata.setLink(link.build())
    metadata.setTime(time)
    metadata.setName(trackdesc)
    gpx.setMetadata(metadata.build())



    var trackSegs: MutableList<TrackSegment> = ArrayList<TrackSegment>()
    var track = Track.Builder()
    var trackType = "Biking"
    track.setTrackCmt(trackdesc)
    track.setTrackDesc(trackdesc)
    track.setTrackType(trackType)
    track.setTrackSrc("$creator $creator_version")

    var trackSegment = TrackSegment.Builder()
    trackSegment.setTrackPoints()

    try {
        var trkSegSize = exportData.splits!!.size

        listsize = exportData.measurements!!.size
        if (listsize > 2) {
            for (m in 0..(listsize-1)) {
                trackSegs.add()
                var altitude = exportData.measurements[m].altitude
                var latitude = exportData.measurements[m].latitude
                var longitude = exportData.measurements[m].longitude
                if (latitude >= maxlat) maxlat = latitude
                if (longitude >= maxlng) maxlng = longitude
                if (latitude < maxlat) minlat = latitude
                if (longitude < maxlng) minlng = longitude
            }
        } else {
            minlat = 0.0
            minlng = 0.0
            maxlat = 0.0
            maxlng = 0.0
        }
    } catch (e: Exception) {
        minlat = 0.0
        minlng = 0.0
        maxlat = 0.0
        maxlng = 0.0
    }

    track.setTrackSegments(trackSegs)
    tracking

    gpx.setTracks(tracking)

    if (BuildConfig.DEBUG) {
        Log.i("GPX", sb.toString())
    }
}
*/

private fun outputFile(context:Context, file:File, data:String) {
    var exportMsg: String = ""
    try {
        FileOutputStream(file).use {
            val output = data.toByteArray()
            it.write(output, 0, output.size)
            it.flush()
            it.close()
        }
        exportMsg = "Exported " + file.name
    } catch (e: Exception) {
        e.printStackTrace()
        exportMsg = "Failed to export " + file.name
    }

    Handler(Looper.getMainLooper()).post(Runnable {
        Toast.makeText(context, exportMsg, Toast.LENGTH_LONG).show()
    })
}

fun exportRideToGpx(
    context: Context,
    useOld: Boolean,
    useStrictXsd: Boolean,
    useMultiSegments: Boolean,
    destination: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    if (exportData.measurements != null && exportData.measurements!!.size != null && exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }

    Handler(Looper.getMainLooper()).post(Runnable {
        Toast.makeText(context, "Started exporting gpx file", Toast.LENGTH_SHORT).show()
    })

    var filename = getFileName(destination)!!.replace("-", "_")

    val privateAppFile = File(
        File("/storage/emulated/0/cyclometer/data"),
        filename
    )

    if (useOld) {
        writeGpxFileOld(
            context,
            DateTime(Date(exportData.summary!!.timestamp)),
            privateAppFile,
            exportData
        )
    } else {
        writeGpxFileNew(
            context,
            useStrictXsd,
            useMultiSegments,
            DateTime(Date(exportData.summary!!.timestamp)),
            privateAppFile,
            exportData
        )
    }
}
