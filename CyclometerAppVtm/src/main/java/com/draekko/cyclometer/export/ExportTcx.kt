package com.draekko.cyclometer.export

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.draekko.cyclometer.BuildConfig
import com.draekko.cyclometer.TripDetailsViewModel
import com.draekko.cyclometer.data.HeartRateMeasurement
import com.draekko.cyclometer.data.Measurements
import com.draekko.cyclometer.data.TimeStateEnum
import com.draekko.cyclometer.data.Trip
import com.draekko.cyclometer.getAverageHeartRate
import com.draekko.cyclometer.util.estimateCaloriesBurned
import com.draekko.cyclometer.util.estimateCaloriesBurnedMets
import com.draekko.cyclometer.util.estimateCaloriesBurnedVo2max
import com.draekko.cyclometer.util.getFileName
import com.draekko.cyclometer.util.useVo2maxCalorieEstimate
import com.google.android.gms.maps.model.LatLng
import org.joda.time.DateTime
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import kotlin.math.roundToInt


val tabsp = "  "
val tabsp2 = tabsp+tabsp
val tabsp3 = tabsp+tabsp+tabsp
val tabsp4 = tabsp+tabsp+tabsp+tabsp
val tabsp5 = tabsp+tabsp+tabsp+tabsp+tabsp
val tabsp6 = tabsp+tabsp+tabsp+tabsp+tabsp+tabsp
val tabsp7 = tabsp+tabsp+tabsp+tabsp+tabsp+tabsp+tabsp
val tabsp8 = tabsp+tabsp+tabsp+tabsp+tabsp+tabsp+tabsp+tabsp

class TrackSegment {
    var timestampPause: Long = 0L
    var timestampResume: Long = 0L
}

@SuppressLint("HardwareIds")
fun writeTcxFile(
    context: Context,
    useStrictXsd: Boolean,
    useMultiSegments: Boolean,
    startTime: DateTime,
    file: File,
    exportData: TripDetailsViewModel.ExportData
) {
    if (exportData.measurements != null &&
            exportData.measurements!!.size != null &&
            exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }

    //val md5 = MessageDigest.getInstance("md5")
    //md5.update(BuildConfig.APPLICATION_ID.toByteArray())
    //var digest = md5.digest().toString()
    //var unit_id = digest.toBigDecimal().toString().substring(0, 10)
    //var product_id = digest.substring(digest.length - 5, digest.length - 1)
    var unit_id = "3203383023" // 0xBEEFBEEF
    var product_id = "48879" // 0xBEEF

    var versionName = BuildConfig.VERSION_NAME.split(".")
    var app_name = "Cyclometer"
    if (BuildConfig.BUILD_TYPE == "debug") {
        app_name += " (Debug)"
    } else if (BuildConfig.BUILD_TYPE == "prod") {
        app_name += " (Prod)"
    } else if (BuildConfig.BUILD_TYPE == "dev") {
        app_name += " (Dev)"
    }
    app_name += " (${BuildConfig.GIT_HASH})"

    var activity = "Biking"

    var version_major:Int = versionName[0].toInt()
    var version_minor:Int = versionName[1].toInt()
    //var build_major:Int = 0//BigInteger(BuildConfig.GIT_HASH, 16).toInt()
    //var build_minor:Int = 0

    var datetimeformat = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    datetimeformat.timeZone = TimeZone.getTimeZone("UTC")
    val date = Date(startTime.millis)
    var creation_time_zulu:String = datetimeformat.format(date)

    var sb: StringBuilder = StringBuilder()

    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    if (useStrictXsd) {
        sb.append("<TrainingCenterDatabase\n" +
                "  xsi:schemaLocation=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd\"\n" +
                "  xmlns:ns5=\"http://www.garmin.com/xmlschemas/ActivityGoals/v1\"\n" +
                "  xmlns:ns3=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\"\n" +
                "  xmlns:ns2=\"http://www.garmin.com/xmlschemas/UserProfile/v2\"\n" +
                "  xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\"\n" +
                "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "  xmlns:ns4=\"http://www.garmin.com/xmlschemas/ProfileExtension/v1\">\n")
    } else {
        sb.append("<TrainingCenterDatabase\n" +
                "  xsi:schemaLocation=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd\n" +
                "                       http://www.draekko.com/xmlschemas/TrainingCenterDatabase/v3 http://www.draekko.com/xmlschemas/TrainingCenterDatabasev3.xsd\"\n" +
                "  xmlns:ns6=\"http://www.draekko.com/xmlschemas/ActivityExtension/v3\"\n" +
                "  xmlns:ns5=\"http://www.garmin.com/xmlschemas/ActivityGoals/v1\"\n" +
                "  xmlns:ns3=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\"\n" +
                "  xmlns:ns2=\"http://www.garmin.com/xmlschemas/UserProfile/v2\"\n" +
                "  xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\"\n" +
                "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "  xmlns:ns4=\"http://www.garmin.com/xmlschemas/ProfileExtension/v1\">\n")
    }

    // Activities Start
    sb.append(tabsp+"<Activities>\n")

    // Activity Start
    sb.append(tabsp2+"<Activity Sport=\"${activity}\">\n")
    sb.append(tabsp3+"<Id>${creation_time_zulu}</Id>\n")

    // Lap Start
    sb.append(tabsp3+"<Lap StartTime=\"${creation_time_zulu}\">\n")

    var total_time = "0.0"
    var total_distance = "0.0"
    var intensity = "Active"
    var trigger_method = "Manual"

    var allSeg = ArrayList<TrackSegment>()
    var pauseCount = 0
    var resumeCount = 0

    if (exportData.measurements != null && exportData.timeStates != null) {
        var size = exportData.measurements!!.size
        if (size < 2) {
            return
        }
        var ts_size = exportData.timeStates!!.size
        if (ts_size < 2) {
            return
        }
        if (useMultiSegments) {
            if (ts_size > 1) {
                for (index in 1..(ts_size - 1)) {
                    var ts1 = exportData.timeStates!!.get(index-1)
                    var ts2 = exportData.timeStates!!.get(index)
                    if (ts1.state == TimeStateEnum.PAUSE && ts2.state == TimeStateEnum.RESUME) {
                        resumeCount++
                        var segment = TrackSegment()
                        segment.timestampPause = ts1.timestamp
                        segment.timestampResume = ts2.timestamp
                        allSeg.add(segment)
                    }
                    if (ts1.state == TimeStateEnum.PAUSE && ts2.state == TimeStateEnum.PAUSE) {
                        pauseCount++
                        var segment = TrackSegment()
                        segment.timestampPause = ts1.timestamp
                        segment.timestampResume = -7777L
                        allSeg.add(segment)
                    }
                }
            } else {
                return
            }
        }
    } else {
        return
    }

    var hasCadence: Boolean  = exportData.cadenceMeasurements != null && exportData.cadenceMeasurements!!.size > 1
    var hasHeartRate: Boolean  = exportData.heartRateMeasurements != null && exportData.heartRateMeasurements!!.size > 1
    var hasPower: Boolean  = exportData.powerMeasurements != null && exportData.powerMeasurements!!.size > 1
    var hasSpeed: Boolean  = exportData.speedMeasurements != null && exportData.speedMeasurements!!.size > 1
    var hasWeather: Boolean  = exportData.weather != null && exportData.weather!!.size > 1
    var hasRelHum: Boolean  = exportData.relHumMeasurements != null && exportData.relHumMeasurements!!.size > 1
    var hasTemp: Boolean  = exportData.tempMeasurements != null && exportData.tempMeasurements!!.size > 1
    var hasOnBoardSens: Boolean  = exportData.onboardSensors != null && exportData.onboardSensors!!.size > 1

    var avg_cadence = "1"
    if (hasCadence) {
        exportData.summary!!.distance
        var avgcad: Int = 0
        for (cadcount in 0..exportData.cadenceMeasurements!!.size-1) {
            avgcad += exportData.cadenceMeasurements!!.get(cadcount)!!.rpmOrSpeed!!.toInt()
        }
        avg_cadence = (avgcad /  exportData.cadenceMeasurements!!.size).toInt().toString()
    } else if (hasPower) {
        var avgcad: Int = 0
        for (cadcount in 0..exportData.powerMeasurements!!.size-1) {
            avgcad += exportData.powerMeasurements!!.get(cadcount)!!.rpm!!.toInt()
        }
        avg_cadence = (avgcad /  exportData.powerMeasurements!!.size).toInt().toString()
    }

    var avg_hr = "1"
    if (hasHeartRate) {
        avg_hr = getAverageHeartRate(exportData.heartRateMeasurements!!).toString()
    }

    var avg_calories = "1"
    if (hasHeartRate) {
        Log.w("TCX", "Using avg calories based on heart rate and trip data")
        avg_calories = getCalories(context, exportData.summary!!, exportData.heartRateMeasurements!!).toString()
    } else {
        Log.w("TCX", "Warning: Using estimated avg calories based on speed, distance, duration, and user weight")
        avg_calories = getEstimatedCalories(exportData.summary!!, null).toString()
    }


    total_time = exportData.summary!!.duration!!.roundToInt().toString()
    var tdist = Math.round(exportData.summary!!.distance!! * 100.0) / 100.0
    total_distance = tdist.toString()

    sb.append(tabsp4+"<TotalTimeSeconds>${total_time}</TotalTimeSeconds>\n")
    sb.append(tabsp4+"<DistanceMeters>${total_distance}</DistanceMeters>\n")
    sb.append(tabsp4+"<Calories>${avg_calories}</Calories>\n")
    if (hasHeartRate) {
        sb.append(tabsp4+"<AverageHeartRateBpm>\n")
        sb.append(tabsp5+"<Value>${avg_hr}</Value>\n")
        sb.append(tabsp4+"</AverageHeartRateBpm>\n")
    }
    sb.append(tabsp4+"<Intensity>${intensity}</Intensity>\n")
    if (hasCadence) {
        sb.append(tabsp4+"<Cadence>${avg_cadence}</Cadence>\n")
    }
    sb.append(tabsp4+"<TriggerMethod>${trigger_method}</TriggerMethod>\n")

    // Track start
    sb.append(tabsp4+"<Track>\n")

    var track_count = 0

    for (count: Int in 0..exportData.measurements.size - 2) {
        var measurementCurrent: Measurements = exportData.measurements.get(count)
        var measurementNext: Measurements = exportData.measurements.get(count+1)

        if (useMultiSegments) {

        }

        if (allSeg != null && allSeg.size > 0) {
            if (measurementCurrent.time >= allSeg.get(track_count).timestampResume &&
                allSeg.get(track_count).timestampResume == -7777L) {
                sb.append(tabsp4+"</Track>\n")
                sb.append(tabsp4+"<Track>\n")
                track_count++
            }

            if (measurementCurrent.time >= allSeg.get(track_count).timestampResume &&
                measurementNext.time < allSeg.get(track_count).timestampResume) {
                sb.append(tabsp4+"</Track>\n")
                sb.append(tabsp4+"<Track>\n")
                track_count++
            }
        }

        var timestampCurrent = measurementCurrent.time
        var timestampNext = timestampCurrent + 1000
        if (count <= count-2) {
            var measurementNext = exportData.measurements!!.get(count+1)
            timestampNext = measurementNext.time
        }

        // Trackpoint start
        sb.append(tabsp5+"<Trackpoint>\n")

        var lat = measurementCurrent.latitude.toString()
        var lng = measurementCurrent.longitude.toString()
        var alt = measurementCurrent.altitude.toString()
        var dist = "0"
        var spd = "0"
        var hr = "0"
        var cad = "0"
        var watts = "0"

        if (count > 0) {
            var measurementPrevious = exportData.measurements.get(count-1)
            var from: LatLng = LatLng(measurementPrevious!!.latitude!!, measurementPrevious!!.longitude!!)
            var to: LatLng = LatLng(measurementCurrent!!.latitude!!, measurementCurrent!!.longitude!!)
            var d = Math.round(computeDistanceBetween(from, to) * 100.0) / 100.0
            dist = d.toString()
        }

        if (count > 1) {
            //measurement.speed
            if (hasSpeed) {
                var speedSize = exportData.speedMeasurements!!.size
                for (loopitem in 0..speedSize-1) {
                    var timestamp = exportData.speedMeasurements!!.get(loopitem).timestamp
                    if (timestamp >= measurementCurrent.time && timestamp <  measurementNext.time) {
                        var rpm = exportData.speedMeasurements!!.get(loopitem).rpmOrSpeed!!.toFloat()
                        var wheel = 2.356f
                        var circumference = wheel / 1000.0f
                        spd = (rpm * circumference / 60.0f).toString()
                        //Log.i("FILE", "SPEED SENSOR " + rpm + " | " + spd + " | " + measurementCurrent.speed)
                        break
                    }
                }
            } else {
                spd = measurementCurrent.speed.toString()
            }
        }

        var datetimeformat = SimpleDateFormat("yyyy'-'MM'-'dd'T'HH:mm:ss.SSS'Z'", Locale.US)
        datetimeformat.timeZone = TimeZone.getTimeZone("UTC")
        val date = Date(measurementCurrent.time)
        var pt_time:String = datetimeformat.format(date)

        if (hasHeartRate) {
            for (loopitem in 0..exportData.heartRateMeasurements!!.size-1) {
                var timestamp = exportData.heartRateMeasurements!!.get(loopitem).timestamp
                if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                    var heartRate = exportData.heartRateMeasurements!!.get(loopitem).heartRate!!.toInt()
                    hr = heartRate.toString()
                    break
                }
            }
        }

        if (hasCadence || hasPower) {
            if (hasCadence && exportData.cadenceMeasurements!!.size > 0) {
                for (loopitem in 0..exportData.cadenceMeasurements!!.size-1) {
                    var timestamp = exportData.cadenceMeasurements!!.get(loopitem).timestamp
                    if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                        var cadenceRpm = exportData.cadenceMeasurements!!.get(loopitem).rpmOrSpeed!!.toInt()
                        cad = cadenceRpm.toString()
                        break
                    }
                }
            } else if (hasPower && exportData.powerMeasurements!!.size > 0) {
                for (loopitem in 0..exportData.powerMeasurements!!.size-1) {
                    var timestamp = exportData.powerMeasurements!!.get(loopitem).timestamp
                    if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                        var cadenceRpm = exportData.powerMeasurements!!.get(loopitem).rpm!!.toInt()
                        cad = cadenceRpm.toString()
                        break
                    }
                }
            }
        }

        if (hasPower) {
            for (loopitem in 0..exportData.powerMeasurements!!.size-1) {
                var timestamp = exportData.powerMeasurements!!.get(loopitem).timestamp
                if (timestamp >= timestampCurrent && timestamp <  timestampNext) {
                    var pwrwatts = exportData.powerMeasurements!!.get(loopitem).watts!!.toInt()
                    watts = pwrwatts.toString()
                    break
                }
            }
        }

        var temperature = ""
        var airpressure = ""
        var relhum = ""
        if (!useStrictXsd) {
            if (hasRelHum) {

            } else if (hasWeather) {

            }

            if (hasTemp) {

            } else if (hasWeather) {

            }

            if (hasOnBoardSens) {

            } else if (hasWeather) {

            }
        }

        sb.append(tabsp6+"<Time>${pt_time}</Time>\n")
        sb.append(tabsp6+"<Position>\n")
        sb.append(tabsp7+"<LatitudeDegrees>${lat}</LatitudeDegrees>\n")
        sb.append(tabsp7+"<LongitudeDegrees>${lng}</LongitudeDegrees>\n")
        sb.append(tabsp6+"</Position>\n")
        sb.append(tabsp6+"<AltitudeMeters>${alt}</AltitudeMeters>\n")
        sb.append(tabsp6+"<DistanceMeters>${dist}</DistanceMeters>\n")
        if (hasHeartRate && hr.toInt() > 0) {
            sb.append(tabsp6+"<HeartRateBpm>\n")
            sb.append(tabsp7+"<Value>${hr}</Value>\n")
            sb.append(tabsp6+"</HeartRateBpm>\n")
        }
        if (hasCadence) {
            sb.append(tabsp6+"<Cadence>${cad}</Cadence>\n")
        }
        sb.append(tabsp6+"<SensorState>Absent</SensorState>\n")
        if (hasSpeed || hasPower) {
            sb.append(tabsp6+"<Extensions>\n")
            sb.append(tabsp7+"<ns3:TPX>\n")
            if (hasSpeed) {
                sb.append(tabsp8+"<ns3:Speed>${spd}</ns3:Speed>\n")
            }
            if (hasPower) {
                sb.append(tabsp8+"<ns3:Watts>${watts}</ns3:Watts>\n")
            }
            sb.append(tabsp7+"</ns3:TPX>\n")
            if (!useStrictXsd) {
                if (hasTemp || hasRelHum || hasOnBoardSens || hasWeather) {
                    sb.append(tabsp7+"<ns6:ATPX>\n")
                    if (hasTemp || hasWeather) {
                        sb.append(tabsp8+"<ns6:Temperature>${temperature}</ns3:Temperature>\n")
                    }
                    if (hasRelHum || hasWeather) {
                        sb.append(tabsp8+"<ns6:RelativeHumidity>${relhum}</ns3:RelativeHumidity>\n")
                    }
                    if (hasOnBoardSens || hasWeather) {
                        sb.append(tabsp8+"<ns6:AirPressure>${airpressure}</ns3:AirPressure>\n")
                    }
                    sb.append(tabsp7+"</ns6:ATPX>\n")
                }
            }
            sb.append(tabsp6+"</Extensions>\n")
        }

        // Trackpoint end
        sb.append(tabsp5+"</Trackpoint>\n")
    }

    // Track end
    sb.append(tabsp4+"</Track>\n")

    // Lap End
    sb.append(tabsp3+"</Lap>\n")

    // Creator Start
    sb.append(tabsp3+"<Creator xsi:type=\"Device_t\">\n")

    sb.append(tabsp4+"<Name>${app_name}</Name>\n")
    sb.append(tabsp4+"<UnitId>${unit_id}</UnitId>\n")
    sb.append(tabsp4+"<ProductID>${product_id}</ProductID>\n")
    sb.append(tabsp4+"<Version>\n")
    sb.append(tabsp5+"<VersionMajor>${version_major}</VersionMajor>\n")
    sb.append(tabsp5+"<VersionMinor>${version_minor}</VersionMinor>\n")
    //sb.append(tabsp5+"<BuildMajor>${build_major}</BuildMajor>\n")
    //sb.append(tabsp5+"<BuildMinor>${build_minor}</BuildMinor>\n")
    sb.append(tabsp4+"</Version>\n")

    // Creator End
    sb.append(tabsp3+"</Creator>\n")

    // Activity End
    sb.append(tabsp2+"</Activity>\n")

    // Activities End
    sb.append(tabsp+"</Activities>\n")

    // TrainingCenterDatabase End
    sb.append("</TrainingCenterDatabase>\n")

    outputFile(context, file, sb.toString())
}

private fun outputFile(context:Context, file:File, data:String) {
    var exportMsg: String = ""
    try {
        FileOutputStream(file).use {
            val output = data.toByteArray()
            it.write(output, 0, output.size)
            it.flush()
            it.close()
        }
        exportMsg = "Exported " + file.name
    } catch (e: Exception) {
        e.printStackTrace()
        exportMsg = "Failed to export " + file.name
    }

    Handler(Looper.getMainLooper()).post(Runnable {
        Toast.makeText(context, exportMsg, Toast.LENGTH_LONG).show()
    })
}

private fun getCalories(
    context: Context,
    overview: Trip,
    measurements: Array<HeartRateMeasurement>,
): Int {
    val avgHr = getAverageHeartRate(measurements)
    return getCaloriesBurned(
        context,
        overview,
        avgHr)
}

private fun getEstimatedCalories(
    overview: Trip,
    heartRate: Short?,
): Int {
    return estimateCaloriesBurned(
        overview.userSex.toString(),
        overview.userAge!!.toInt(),
        overview.userWeight!!,
        overview.duration!!.toFloat(),
        overview.distance!!.toFloat(),
        heartRate)
}

fun getCaloriesBurned(
    context: Context,
    overview: Trip,
    heartRate: Short?,
): Int = when (useVo2maxCalorieEstimate(context)) {
    false -> estimateCaloriesBurned(
        overview.userSex.toString(),
        overview.userAge!!.toInt(),
        overview.userWeight!!.toFloat(),
        overview.duration?.toFloat()!!,
        overview.distance?.toFloat()!!,
        heartRate
    )
    else -> estimateCaloriesBurnedVo2max(
        overview.userSex.toString(),
        overview.userAge!!.toInt(),
        overview.userWeight!!.toFloat(),
        overview.userHeight,
        overview.userVo2max,
        overview.userRestingHeartRate,
        overview.userMaxHeartRate,
        overview.duration?.toFloat()!!,
        overview.distance?.toFloat()!!,
        heartRate
    )
}

var EARTH_RADIUS = 6371009.0

fun computeDistanceBetween(from: LatLng?, to: LatLng?): Double {
    return computeAngleBetween(from!!, to!!) * EARTH_RADIUS
}

fun computeAngleBetween(from: LatLng, to: LatLng): Double {
    return distanceRadians(
        Math.toRadians(from.latitude), Math.toRadians(from.longitude),
        Math.toRadians(to.latitude), Math.toRadians(to.longitude)
    )
}

fun distanceRadians(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double {
    return arcHav(havDistance(lat1, lat2, lng1 - lng2))
}

fun arcHav(x: Double): Double {
    return 2 * Math.asin(Math.sqrt(x))
}

fun havDistance(lat1: Double, lat2: Double, dLng: Double): Double {
    return hav(lat1 - lat2) + hav(dLng) * Math.cos(lat1) * Math.cos(lat2)
}

fun hav(x: Double): Double {
    val sinHalf: Double = Math.sin(x * 0.5)
    return sinHalf * sinHalf
}

fun exportRideToTcx(
    context: Context,
    useStrictXsd: Boolean,
    useMultiSegments: Boolean,
    destination: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    if (exportData.measurements != null && exportData.measurements!!.size != null && exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }

    Handler(Looper.getMainLooper()).post(Runnable {
        Toast.makeText(context, "Started exporting tcx file", Toast.LENGTH_SHORT).show()
    })

    val privateAppFile = File(
        File("/storage/emulated/0/cyclometer/data"),
        getFileName(destination)
    )

    try {
        writeTcxFile(
            context,
            useStrictXsd,
            useMultiSegments,
            DateTime(Date(exportData.summary!!.timestamp)),
            privateAppFile,
            exportData
        )
    } catch (e: Exception) {
        e.printStackTrace()
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "ERROR: writeTcxFile failed. No data exported!", Toast.LENGTH_LONG).show()
        })
    }
}
