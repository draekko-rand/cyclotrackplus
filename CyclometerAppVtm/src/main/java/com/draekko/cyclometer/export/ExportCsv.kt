package com.draekko.cyclometer.export

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.draekko.cyclometer.TripDetailsViewModel
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


fun exportRideToCsv(
    context: Context,
    file: File,
    exportData: TripDetailsViewModel.ExportData,
) {
    if (exportData.measurements != null && exportData.measurements!!.size != null && exportData.measurements!!.size < 2) {
        Handler(Looper.getMainLooper()).post(Runnable {
            Toast.makeText(context, "No data to export", Toast.LENGTH_LONG).show()
        })
        return
    }
    Handler(Looper.getMainLooper()).post(Runnable {
        Toast.makeText(context, "Started exporting csv file(s)", Toast.LENGTH_SHORT).show()
    })

    try {
        Log.d("EXPORT_SERVICE", "Exporting...")
        FileOutputStream(file).use {
            Log.d("EXPORT_SERVICE", "Opening...")
            ZipOutputStream(BufferedOutputStream(it)).use { stream ->
                Log.d("EXPORT_SERVICE", "Zipping...")

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary!!.id
                            )
                        }_measurements.csv"
                    )
                )
                getDataCsv(exportData.measurements!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary.id
                            )
                        }_onboard_sensors.csv"
                    )
                )
                getDataCsv(exportData.onboardSensors!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                try {
                    if (exportData.cadenceMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_cadence_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.cadenceMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.speedMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_speed_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.speedMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.powerMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_powermeter_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.powerMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.heartRateMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_hrm_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.heartRateMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.relHumMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_relhum_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.relHumMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.tempMeasurements!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_temp_sensor.csv"
                            )
                        )
                        getDataCsv(exportData.tempMeasurements!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                try {
                    if (exportData.weather!!.size > 0) {
                        stream.putNextEntry(
                            ZipEntry(
                                "${
                                    String.format(
                                        "%06d",
                                        exportData.summary.id
                                    )
                                }_weather.csv"
                            )
                        )
                        getDataCsv(exportData.weather!!).forEach { row ->
                            stream.write("$row\n".toByteArray())
                        }
                        stream.closeEntry()
                    }
                } catch (e: Exception) {
                }

                stream.putNextEntry(
                    ZipEntry(
                        "${
                            String.format(
                                "%06d",
                                exportData.summary.id
                            )
                        }_timestates.csv"
                    )
                )
                getDataCsv(exportData.timeStates!!).forEach { row ->
                    stream.write("$row\n".toByteArray())
                }
                stream.closeEntry()

                stream.finish()
                stream.close()
            }
        }
        Toast.makeText(context, "Exported " + file.name, Toast.LENGTH_LONG).show()
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(context, "Failed to export " + file.name, Toast.LENGTH_LONG).show()
    }
}
