package com.draekko.cyclometer.export

import android.content.ContentResolver
import android.content.Context
import android.os.Build
import android.os.Build.VERSION_CODES
import android.util.Log
import androidx.hilt.work.HiltWorker
import androidx.preference.PreferenceManager
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.draekko.cyclometer.BuildConfig
import com.draekko.cyclometer.R
import com.draekko.cyclometer.util.FeatureFlags
import com.draekko.cyclometer.data.MeasurementsRepository
import com.draekko.cyclometer.TripDetailsViewModel
import com.google.android.gms.common.util.concurrent.HandlerExecutor
import com.draekko.cyclometer.data.CadenceSpeedMeasurementRepository
import com.draekko.cyclometer.data.ExportRepository
import com.draekko.cyclometer.data.HeartRateMeasurementRepository
import com.draekko.cyclometer.data.OnboardSensorsRepository
import com.draekko.cyclometer.data.SplitRepository
import com.draekko.cyclometer.data.TempRelHumMeasurementRepository
import com.draekko.cyclometer.data.TimeStateRepository
import com.draekko.cyclometer.data.TripRepository
import com.draekko.cyclometer.data.WeatherRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import java.io.File
import java.text.SimpleDateFormat
import java.util.concurrent.Executor
import javax.inject.Inject

fun Context.mainExecutor(): Executor {
    return if (Build.VERSION.SDK_INT >= VERSION_CODES.P) {
        mainExecutor
    } else {
        HandlerExecutor(mainLooper)
    }
}

@HiltWorker
class ExportTripWorker @AssistedInject constructor(
    @Assisted val appContext: Context,
    @Assisted params: WorkerParameters,
) : CoroutineWorker(appContext, params) {
    val logTag = "ExportTripWorker"
    private val xlsxMime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    private val binaryMime = "application/octet-stream"

    @Inject
    lateinit var tripsRepository: TripRepository

    @Inject
    lateinit var measurementsRepository: MeasurementsRepository

    @Inject
    lateinit var cadenceSpeedMeasurementRepository: CadenceSpeedMeasurementRepository

    @Inject
    lateinit var heartRateMeasurementRepository: HeartRateMeasurementRepository

    @Inject
    lateinit var tempRelHumMeasurementRepository: TempRelHumMeasurementRepository

    @Inject
    lateinit var timeStateRepository: TimeStateRepository

    @Inject
    lateinit var splitRepository: SplitRepository

    @Inject
    lateinit var onboardSensorsRepository: OnboardSensorsRepository

    @Inject
    lateinit var weatherRepository: WeatherRepository

    @Inject
    lateinit var exportRepository: ExportRepository

    private suspend fun exportTripData(
        contentResolver: ContentResolver,
        useMultiSegments: Boolean,
        tripId: Long,
        file: File,
        fileType: String,
    ) {
        Log.d(logTag, "exportTripData")
        Log.d(logTag, file.absolutePath)

        Log.d(logTag, "building export data")
        val exportData = TripDetailsViewModel.ExportData(
            summary = tripsRepository.get(tripId),
            measurements = measurementsRepository.get(tripId),
            heartRateMeasurements = heartRateMeasurementRepository.get(tripId),
            speedMeasurements = cadenceSpeedMeasurementRepository.getSpeedMeasurements(tripId),
            cadenceMeasurements = cadenceSpeedMeasurementRepository.getCadenceMeasurements(tripId),
            tempMeasurements = tempRelHumMeasurementRepository.getTemperatureMeasurements(tripId),
            relHumMeasurements = tempRelHumMeasurementRepository.getRelativeHumidityMeasurements(
                tripId
            ),
            timeStates = timeStateRepository.getTimeStates(tripId),
            splits = splitRepository.getTripSplits(tripId),
            onboardSensors = onboardSensorsRepository.get(tripId),
            weather = weatherRepository.getTripWeather(tripId)
        )
        if (exportData.summary != null &&
            exportData.measurements != null &&
            exportData.heartRateMeasurements != null &&
            exportData.cadenceMeasurements != null &&
            exportData.speedMeasurements != null &&
            exportData.tempMeasurements != null &&
            exportData.relHumMeasurements != null &&
            exportData.timeStates != null &&
            exportData.splits != null &&
            exportData.onboardSensors != null &&
            exportData.weather != null
        ) {
            Log.d(logTag, "Exporting trip $tripId...")
            try {
                if (fileType.lowercase().contains("xlsx")) {
                    exportRideToXlsx(appContext, file, exportData)
                } else if (fileType.lowercase().contains("fit")) {
                    exportRideToFit(appContext, file, exportData)
                } else if (fileType.lowercase().contains("gpx")) {
                    var useOld = false
                    var useStrictXsd = true
                    exportRideToGpx(appContext, useOld, useStrictXsd, useMultiSegments, file, exportData)
                } else if (fileType.lowercase().contains("tcx")) {
                    var useStrictXsd = true
                    exportRideToTcx(appContext, useStrictXsd, useMultiSegments, file, exportData)
                } else if (fileType.lowercase().contains("csv")) {
                    exportRideToCsv(appContext, file, exportData)
                } else {
                    throw RuntimeException("Unknown file type")
                }
            } catch (e: Exception) {
                Log.e(logTag, "Export failed", e)

                throw e
            }
        }
    }

    private suspend fun exportFile(tripId: Long, useMultiSegments: Boolean,
                                   fileName: String, fileType: String): Result {
        try {
            var ext = ""
            if (fileType.lowercase().contains("xlsx") && !fileName.lowercase().contains(".xlsx")) {
                ext = ".xlsl"
            }
            if (fileType.lowercase().contains("fit") && !fileName.lowercase().contains(".fit")) {
                ext = ".fit"
            }
            if (fileType.lowercase().contains("csv") && !fileName.lowercase().contains(".zip")) {
                ext = ".zip"
            }
            if (fileType.lowercase().contains("gpx") && !fileName.lowercase().contains(".gpx")) {
                ext = ".gpx"
            }
            if (fileType.lowercase().contains("tcx") && !fileName.lowercase().contains(".tcx")) {
                ext = ".tcx"
            }
            var exportfilename = "$fileName$ext"
            var file = File("/storage/emulated/0/cyclometer/data/$exportfilename")
            exportTripData(appContext.contentResolver, useMultiSegments, tripId, file, fileType)
            return Result.success()
        } catch (e: SecurityException) {
            Log.e(logTag, "Could not prep destination path", e)
            return Result.failure()
        }
    }

    override suspend fun doWork(): Result {
        Log.d(logTag, "starting ExportTripWorker")
        var key = appContext.getString(R.string.preference_gpx_tcx_track_segments_key)
        var exportMultiSegmentsStr = PreferenceManager.getDefaultSharedPreferences(appContext).getString(key, "2")
        var exportMultiSegments = true
        when (exportMultiSegmentsStr) {
            "1" -> exportMultiSegments = false
            "2" -> exportMultiSegments = true
            else -> exportMultiSegments = true
        }
        val fileType = inputData.getString("fileType") ?: "csv"
        var result = Result.success()
        inputData.getLong("tripId", -1).takeIf { it >= 0 }?.let { tripId ->
            var prefix = "cyclometer"
            if (BuildConfig.BUILD_TYPE == "debug") {
                prefix += "_dbg"
            } else if (BuildConfig.BUILD_TYPE == "dev") {
                prefix += "_dev"
            } else if (BuildConfig.BUILD_TYPE == "beta") {
                prefix += "_beta"
            } else if (BuildConfig.DEBUG && !FeatureFlags.devBuild && !FeatureFlags.betaBuild) {
                prefix += "_dbg"
            } else if (FeatureFlags.devBuild) {
                prefix += "_dev"
            } else if (FeatureFlags.betaBuild) {
                prefix += "_beta"
            }
            val trip = tripsRepository.get(tripId)
            val formatter = SimpleDateFormat("yyyyMMdd_HHmm")
            val tripDateAndTime = formatter.format(trip.timestamp)
            val fileName = "${prefix}_${
                String.format("%06d", trip.id)
            }_${trip.name?.trim()?.replace(" ", "_") ?: "unknown"}_${tripDateAndTime}.$fileType"
            if (fileType.lowercase().contains("csv")) {
                fileName.replace(".csv", ".zip")
            }

            result = exportFile(tripId, exportMultiSegments, fileName, fileType)
        }
        return result
    }
}
