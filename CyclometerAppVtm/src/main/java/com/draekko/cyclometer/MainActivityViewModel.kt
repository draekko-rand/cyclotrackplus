package com.draekko.cyclometer

import androidx.lifecycle.ViewModel
import com.draekko.cyclometer.data.TripRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    tripsRepository: TripRepository,
) : ViewModel() {
    val latestTrip = tripsRepository.observeNewest()
}