package com.draekko.cyclometer

import android.annotation.SuppressLint
import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.bluetooth.le.ScanSettings.MATCH_MODE_AGGRESSIVE
import android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_LATENCY
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.coroutineScope
import com.draekko.cyclometer.data.Bike
import com.draekko.cyclometer.data.BikeRepository
import com.draekko.cyclometer.data.ExternalSensorRepository
import com.draekko.cyclometer.events.BluetoothActionEvent
import com.draekko.cyclometer.events.ConnectedBikeEvent
import com.draekko.cyclometer.util.SystemUtils
import com.draekko.cyclometer.util.getFloatValue
import com.draekko.cyclometer.util.getIntValue
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


data class HrmData(
    val batteryLevel: Byte?,
    val bpm: Short?,
    val energyExpended: Short? = null,
    val rrIntervals: String? = null,
    val timestamp: Long? = null,
)

data class SpeedData(
    val batteryLevel: Byte?,
    val revolutionCount: Int?,
    val lastEvent: Int?,
    val speed: Float?,
    val timestamp: Long? = null,
)

data class CadenceData(
    val batteryLevel: Byte?,
    val revolutionCount: Int?,
    val lastEvent: Int?,
    val rpm: Float?,
    val timestamp: Long? = null,
)

data class TemperatureData(
    val batteryLevel: Byte?,
    val temperature: Double?,
    val timestamp: Long? = null,
)

data class RelHumData(
    val batteryLevel: Byte?,
    val relativeHumidity: Double?,
    val timestamp: Long? = null,
)

data class AirPresData(
    val batteryLevel: Byte?,
    val airpressure: Double?,
    val timestamp: Long? = null,
)

data class PowerData(
    val batteryLevel: Byte?,
    val Power: Double?,
    val lastEvent: Int?,
    val timestamp: Long? = null,
)

@AndroidEntryPoint
class BleService @Inject constructor(
    private val context: Application,
    private val externalSensorRepository: ExternalSensorRepository,
    private val bikeRepository: BikeRepository,
) : LifecycleService() {
    private var connectedBike: Bike? = null
    private val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    private val addresses = object {
        var hrm: String? = null
        var speed: String? = null
        var cadence: String? = null
        var power: String? = null
        var temperature: String? = null
        var relativehumidity: String? = null
        var airpressure: String? = null
    }
    private val logTag = "BleService"
    private var scanCallbacks = ArrayList<ScanCallback>()
    private var gatts = ArrayList<BluetoothGatt>()

    private var hrmSensor = HrmData(null, null)
    private var cadenceSensor = CadenceData(null, null, null, null)
    private var speedSensor = SpeedData(null, null, null, null)
    private var tempSensor = TemperatureData(null, null, null)
    private var humiditySensor = RelHumData(null, null, null)
    private var airpresSensor = AirPresData(null, null, null)
    private var powerSensor = PowerData(null, null, null, null)

    private fun getFitnessMachineFeatures(gatt: BluetoothGatt) {
        val featureChar = gatt
            .getService(getGattUuid(fitnessMachineServiceId))
            ?.getCharacteristic(getGattUuid(fitnessMachineFeatureCharacteristicId))
        Log.v(logTag, "read supported fitness machine features: ${featureChar?.uuid}")
        try {
            featureChar?.let { gatt.readCharacteristic(it) }
        } catch (e: SecurityException) {
            Log.w(logTag, "Bluetooth permissions have not been granted", e)
        }
    }

    private val receiveBluetoothStateChanges = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                BluetoothAdapter.ACTION_STATE_CHANGED -> {
                    when (intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR
                    )) {
                        BluetoothAdapter.STATE_ON -> {
                            Log.d(logTag, "Detected Bluetooth ON")
                            initialize()
                        }

                        BluetoothAdapter.STATE_OFF -> {
                            Log.d(logTag, "Detected Bluetooth OFF")
                            disconnect()
                        }
                    }
                }
            }
        }
    }

    init {
        context.registerReceiver(
            receiveBluetoothStateChanges,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
    }

    // Various callback methods defined by the BLE API.
    private val genericGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
            gatt: BluetoothGatt,
            status: Int,
            newState: Int,
        ) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    Log.i(logTag, "Connected to GATT server ${gatt.device.address}.")
                    try {
                        Log.i(
                            logTag,
                            "Attempting to start service discovery: ${gatt.discoverServices()}"
                        )
                    } catch (e: SecurityException) {
                        Log.w(logTag, "Bluetooth permissions have not been granted", e)
                    }
                }

                BluetoothProfile.STATE_DISCONNECTED -> {
                    Log.i(logTag, "Disconnected ${gatt.device.address} from GATT server.")
                    if (gatts.any { it.device.address == gatt.device.address }) {
                        try {
                            gatt.connect()
                        } catch (e: SecurityException) {
                            Log.w(logTag, "Bluetooth permissions have not been granted", e)
                        }
                    }
                }
            }
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int,
        ) {
            Log.d(logTag, "Write descriptor finished")
            super.onDescriptorWrite(gatt, descriptor, status)
            if (gatt != null && descriptor != null) {
                try {
                    gatt.setCharacteristicNotification(descriptor.characteristic, true)
                } catch (e: SecurityException) {
                    Log.w(logTag, "Bluetooth permissions have not been granted", e)
                }
                //TODO: Trigger next stage in pipeline here that would allow setup of
                // other notifications or reads like battery level below
                // Be careful you can cause an infinite loop!
            }
        }

        // New services discovered
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            with(gatt) {
                Log.d(logTag, "Discovered services: $status")
                printGattTable()
                when {
                    hasCharacteristic(
                        heartRateServiceUuid,
                        hrmCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has HRM")
                        enableNotifications(gatt,
                            gatt.getService(heartRateServiceUuid).
                                getCharacteristic(hrmCharacteristicUuid))
                    }

                    hasCharacteristic(
                        enviroSensingServiceUuid,
                        temperatureCharacteristicUuid,
                        relativeHumidityCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has TEMP & RELHUM")
                        hasTempRelHum = true
                        enableNotifications(gatt,
                            gatt.getService(enviroSensingServiceUuid).
                            getCharacteristic(temperatureCharacteristicUuid))
                        enableNotifications(gatt,
                            gatt.getService(enviroSensingServiceUuid).
                                getCharacteristic(relativeHumidityCharacteristicUuid))
                    }

                    hasCharacteristic(
                        enviroSensingServiceUuid,
                        temperatureCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has TEMP")
                        enableNotifications(gatt,
                            gatt.getService(enviroSensingServiceUuid).
                            getCharacteristic(temperatureCharacteristicUuid))
                    }

                    hasCharacteristic(
                        enviroSensingServiceUuid,
                        relativeHumidityCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has RELHUM")
                        enableNotifications(gatt,
                            gatt.getService(enviroSensingServiceUuid).
                            getCharacteristic(relativeHumidityCharacteristicUuid))
                    }

                    hasCharacteristic(
                        enviroSensingServiceUuid,
                        barometricPressureCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has AP")
                        enableNotifications(gatt,
                            gatt.getService(enviroSensingServiceUuid).
                                getCharacteristic(barometricPressureCharacteristicUuid))
                    }

                    hasCharacteristic(
                        powerMeterServiceUuid,
                        measurementPowerMeterCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has POWER")
                        enableNotifications(gatt,
                            gatt.getService(powerMeterServiceUuid).
                                getCharacteristic(measurementPowerMeterCharacteristicUuid))
                    }

                    hasCharacteristic(
                        cadenceSpeedServiceUuid,
                        cscMeasurementCharacteristicUuid
                    ) -> {
                        Log.i(logTag, "has CADENCE")
                        enableNotifications(gatt,
                            gatt.getService(cadenceSpeedServiceUuid).
                                getCharacteristic(cscMeasurementCharacteristicUuid))
                    }

                    hasCharacteristic(
                        getGattUuid(fitnessMachineServiceId),
                        getGattUuid(indoorBikeDataCharacteristicId)
                    ) -> {
                        Log.i(logTag, "has FITNESS MACHINE")
                        enableNotifications(gatt,
                            gatt.getService(getGattUuid(fitnessMachineServiceId)).
                                getCharacteristic(getGattUuid(indoorBikeDataCharacteristicId))
                        )
                    }

                    else -> {
                        Log.d(logTag, "No supported characteristics")
                    }
                }
            }
        }

        // Result of a characteristic read operation
        @Deprecated("Deprecated in Java")
        @Suppress("DEPRECATION")
        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int,
        ) {
            Log.v(logTag, "DEPRECATED -- onCharacteristicRead")
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    broadcastUpdate(gatt, characteristic, characteristic.value)
                }
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            byteArray: ByteArray,
            status: Int,
        ) {
            Log.v(logTag, "onCharacteristicRead")
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    broadcastUpdate(gatt, characteristic, byteArray)
                }
            }
        }

        @Deprecated("Deprecated in Java")
        @Suppress("DEPRECATION")
        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
        ) {
            Log.v(logTag, "DEPRECATED -- onCharacteristicChanged")
            broadcastUpdate(gatt, characteristic, characteristic.value)
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            byteArray: ByteArray
        ) {
            Log.v(logTag, "onCharacteristicChanged")
            broadcastUpdate(gatt, characteristic, byteArray)
        }
    }

    // Device scan callback.
    private fun scanForDevice(mac: String): ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            try {
                super.onScanResult(callbackType, result)
                Log.d(
                    logTag,
                    "Found device ${result.device.name}, ${result.device.type}: ${result.device}"
                )
                if (result.device.address == mac) {
                    Log.d(
                        logTag,
                        "Connecting to ${result.device.name}, ${result.device.type}: ${result.device}"
                    )
                    //gatts.add(result.device.connectGatt(context, true, genericGattCallback))
                    result.device.connectGatt(context, true, genericGattCallback)
                        ?.let { gatts.add(it) }
                    bluetoothManager.adapter?.bluetoothLeScanner?.stopScan(this)
                    scanCallbacks.remove(this)
                }
            } catch (e: SecurityException) {
                Log.w(logTag, "Bluetooth permissions have not been granted.", e)
            }
        }
    }

    private fun forceReadTempRelHum(gatt: BluetoothGatt) {
        forceReadBleTemperature(gatt)
        forceReadBleRelHum(gatt)
    }

    @SuppressLint("MissingPermission")
    private fun forceReadBleTemperature(gatt: BluetoothGatt) {
    }

    private fun forceReadBleRelHum(gatt: BluetoothGatt) {
    }

    private fun broadcastUpdate(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        characteristicValue: ByteArray
    ) {
        val eventTime = SystemUtils.currentTimeMillis()
        Log.d(logTag, "broadcast update for ${characteristic.uuid} on ${gatt.device.address}")
        when (characteristic.uuid) {
            temperatureCharacteristicUuid -> {
                if (addresses.temperature == null) {
                    addresses.temperature = gatt.device.address
                    readBatteryLevel(gatt)
                }
                var temperatureC = -0.0
                var temperatureF = -0.0
                try {
                    if (characteristicValue != null) {
                        var u1 = ((characteristicValue[1].toInt() and 0xff) * 256) as Int
                        var u2 = (characteristicValue[0].toInt() and 0xff) as Int
                        var unit_value = u1 + u2
                        temperatureC = unit_value.toDouble() / 100.0
                        temperatureF = ((temperatureC * 9.0) / 5.0) + 32.0
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                Log.d("DEBUG", "Temperature $temperatureC°C | $temperatureF°F"  )
                if (tempSensor.batteryLevel != null)
                    Log.d("DEBUG", "Temperature Battery Level ${tempSensor.batteryLevel}%")
                TemperatureData(
                    batteryLevel = tempSensor.batteryLevel,
                    temperature = temperatureC.toDouble(),
                    timestamp = eventTime
                ).let {
                    tempSensor = it
                    EventBus.getDefault().post(it)
                }

                if (hasTempRelHum) {
                    enableNotifications(gatt, enviroSensingServiceUuid, relativeHumidityCharacteristicUuid)
                    readCharacteristic(gatt, enviroSensingServiceUuid, relativeHumidityCharacteristicUuid)
                }
            }

            relativeHumidityCharacteristicUuid -> {
                if (addresses.relativehumidity == null) {
                    addresses.relativehumidity = gatt.device.address
                    readBatteryLevel(gatt)
                }
                var relativeHumidity = -0.0
                try {
                    if (characteristicValue != null) {
                        var u1 = ((characteristicValue[1].toInt() and 0xff) * 256) as Int
                        var u2 = (characteristicValue[0].toInt() and 0xff) as Int
                        var unit_value = u1 + u2
                        relativeHumidity = unit_value.toDouble() / 100.0
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }/* use temperature data read battery since doesn't work with multi characteristsics */
                Log.d("DEBUG", "Relative Humidity $relativeHumidity%")
                if (humiditySensor.batteryLevel != null)
                    Log.d("DEBUG", "RH Battery Level ${humiditySensor.batteryLevel}%")
                RelHumData(
                    batteryLevel = humiditySensor.batteryLevel,
                    relativeHumidity = relativeHumidity,
                    timestamp = eventTime
                ).let {
                    humiditySensor = it
                    EventBus.getDefault().post(it)
                }

                if (hasTempRelHum) {
                    enableNotifications(gatt, enviroSensingServiceUuid, temperatureCharacteristicUuid)
                    readCharacteristic(gatt, enviroSensingServiceUuid, temperatureCharacteristicUuid)
                }
            }

            barometricPressureCharacteristicUuid -> {
                if (addresses.airpressure == null) {
                    addresses.airpressure = gatt.device.address
                    readBatteryLevel(gatt)
                }
                val airpressure = characteristicValue.getFloatValue(BluetoothGattCharacteristic.FORMAT_FLOAT, 0)!!.toDouble()
                AirPresData(
                    batteryLevel = airpresSensor.batteryLevel,
                    airpressure = airpressure,
                    timestamp = eventTime
                ).let {
                    airpresSensor = it
                    EventBus.getDefault().post(it)
                }
            }

            hrmCharacteristicUuid -> {
                if (addresses.hrm == null) {
                    addresses.hrm = gatt.device.address
                    readBatteryLevel(gatt)
                }
                val flag = characteristic.properties
                Log.d("DEBUG", Integer.toBinaryString(characteristic.properties))
                val format = when (flag and 0b01) {
                    0b01 -> {
                        Log.d(logTag, "Heart rate format UINT16.")
                        BluetoothGattCharacteristic.FORMAT_UINT16
                    }

                    else -> {
                        Log.d(logTag, "Heart rate format UINT8.")
                        BluetoothGattCharacteristic.FORMAT_UINT8
                    }
                }
                if (flag and 0b1000 == 0b1000) {
                    Log.d("DEBUG", "Supports RR interval")
                }
                Log.d("DEBUG", characteristicValue.toString())
                val heartRate = characteristicValue.getIntValue(format, 1) ?: 0
                Log.d(logTag, String.format("Received heart rate: %d", heartRate))
                HrmData(
                    batteryLevel = hrmSensor.batteryLevel,
                    bpm = heartRate.toShort(),
                    timestamp = eventTime
                ).let {
                    hrmSensor = it
                    EventBus.getDefault().post(it)
                }
            }

            batteryLevelCharUuid -> {
                val batteryLevel = characteristicValue[0]
                Log.d(logTag, "Battery level: $batteryLevel")

                when (gatt.device.address) {
                    addresses.hrm -> {
                        hrmSensor.copy(batteryLevel = batteryLevel).let {
                            hrmSensor = it
                            EventBus.getDefault().post(it)
                        }
                    }

                    addresses.speed -> {
                        speedSensor.copy(batteryLevel = batteryLevel).let {
                            speedSensor = it
                            EventBus.getDefault().post(it)
                        }
                    }

                    addresses.cadence -> {
                        cadenceSensor.copy(batteryLevel = batteryLevel).let {
                            cadenceSensor = it
                            EventBus.getDefault().post(it)
                        }
                    }

                    addresses.temperature -> {
                        tempSensor.copy(batteryLevel = batteryLevel).let {
                            tempSensor = it
                            EventBus.getDefault().post(it)
                        }
                        if (hasTempRelHum) {
                            humiditySensor.copy(batteryLevel = batteryLevel).let {
                                humiditySensor = it
                                EventBus.getDefault().post(it)
                            }
                        }
                    }

                    addresses.relativehumidity -> {
                        if (!hasTempRelHum) {
                            humiditySensor.copy(batteryLevel = batteryLevel).let {
                                humiditySensor = it
                                EventBus.getDefault().post(it)
                            }
                        }
                    }

                    addresses.airpressure -> {
                        airpresSensor.copy(batteryLevel = batteryLevel).let {
                            airpresSensor = it
                            EventBus.getDefault().post(it)
                        }
                    }

                    else -> Log.d(
                        logTag,
                        "No sensor associated with device ${gatt.device.address} with battery level $batteryLevel"
                    )
                }
            }

            cscMeasurementCharacteristicUuid -> {
                val timeout = 2000
                val speedId = 0x01
                val cadenceId = 0x02
                val sensorType =
                    characteristicValue.getIntValue(
                        BluetoothGattCharacteristic.FORMAT_UINT8,
                        0
                    ) ?: 0
                when {
                    (sensorType and speedId > 0) -> {
                        if (addresses.speed == null) {
                            addresses.speed = gatt.device.address
                            readBatteryLevel(gatt)
                        }
                        //NOTE: I'm surprised this is allowed since a UINT32 should not be written to an INT32
                        //however in all practicality the value required to induce this bug will never be reached.
                        //Additionally the spec states that this value does not rollover.
                        val revolutionCount =
                            characteristicValue.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_SINT32,
                                1
                            ) ?: 0
                        val lastEvent =
                            characteristicValue.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16,
                                5
                            ) ?: 0
                        if (revolutionCount != speedSensor.revolutionCount ||
                            eventTime - (speedSensor.timestamp
                                ?: 0) > timeout
                        ) {
                            val rpm = getRpm(
                                revolutionCount,
                                (speedSensor.revolutionCount ?: revolutionCount),
                                lastEvent,
                                (speedSensor.lastEvent ?: lastEvent)
                            )
                            Log.d(
                                logTag,
                                "Speed sensor: $revolutionCount :: $lastEvent :: $rpm"
                            )
                            if (connectedBike == null && rpm > 0) {
                                connectBike(gatt)
                            }
                            SpeedData(
                                speedSensor.batteryLevel,
                                revolutionCount,
                                lastEvent, rpm, eventTime
                            ).let {
                                speedSensor = it
                                EventBus.getDefault().post(it)
                            }
                        }
                    }

                    (sensorType and cadenceId > 0) -> {
                        if (addresses.cadence == null) {
                            addresses.cadence = gatt.device.address
                            readBatteryLevel(gatt)
                        }
                        val revolutionCount =
                            characteristicValue.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16,
                                1
                            ) ?: 0
                        val lastEvent =
                            characteristicValue.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16,
                                3
                            ) ?: 0
                        Log.d(
                            logTag,
                            "Cadence sensor changed: $revolutionCount :: $lastEvent"
                        )
                        if (revolutionCount != cadenceSensor.revolutionCount ||
                            eventTime - (cadenceSensor.timestamp
                                ?: 0) > timeout
                        ) {
                            val rpm = getRpm(
                                revolutionCount,
                                (cadenceSensor.revolutionCount ?: revolutionCount),
                                lastEvent,
                                (cadenceSensor.lastEvent ?: lastEvent)
                            )
                            Log.d(
                                logTag,
                                "Cadence sensor update: $revolutionCount :: $lastEvent :: $rpm"
                            )
                            if (connectedBike == null && rpm > 0) {
                                connectBike(gatt)
                            }
                            CadenceData(
                                cadenceSensor.batteryLevel,
                                revolutionCount,
                                lastEvent, rpm, eventTime
                            ).let {
                                cadenceSensor = it
                                EventBus.getDefault().post(it)
                            }
                        }
                    }

                    else -> {
                        Log.d(logTag, "Unknown CSC sensor type")
                        val data: ByteArray = characteristicValue
                        if (data.isNotEmpty()) {
                            val hexString: String = data.joinToString(separator = " ") {
                                String.format("%02X", it)
                            }
                            Log.d(
                                logTag,
                                String.format("Received ${characteristic.uuid}: $hexString")
                            )
                        }
                    }
                }
            }

            getGattUuid(indoorBikeDataCharacteristicId) -> {
                val data: ByteArray = characteristicValue
                if (data.isNotEmpty()) {
                    val hexString: String = data.joinToString(separator = " ") {
                        String.format("%02X", it)
                    }
                    Log.d(
                        logTag,
                        String.format("Indoor bike data: ${characteristic.uuid}: $hexString")
                    )
                }
                getFitnessMachineFeatures(gatt)
            }

            getGattUuid(fitnessMachineFeatureCharacteristicId) -> {
                val data: ByteArray = characteristicValue
                if (data.isNotEmpty()) {
                    val supportedFeatures =
                        characteristicValue.getIntValue(
                            BluetoothGattCharacteristic.FORMAT_UINT32,
                            0
                        ) ?: 0
                    val targetFeatures =
                        characteristicValue.getIntValue(
                            BluetoothGattCharacteristic.FORMAT_UINT32,
                            4
                        ) ?: 0
                    val hexString = "${Integer.toBinaryString(supportedFeatures)} ${
                        Integer.toBinaryString(targetFeatures)
                    }"
                    Log.d(
                        logTag,
                        String.format("Supported fitness features: ${characteristic.uuid}: $hexString")
                    )
                }
                if (data.isNotEmpty()) {
                    val hexString: String = data.joinToString(separator = " ") {
                        String.format("%02X", it)
                    }
                    Log.d(logTag, String.format("Received ${characteristic.uuid}: $hexString"))
                }
            }

            else -> {
                // For all other profiles, writes the data formatted in HEX.
                val data: ByteArray = characteristicValue
                if (data.isNotEmpty()) {
                    val hexString: String = data.joinToString(separator = " ") {
                        String.format("%02X", it)
                    }
                    Log.d(logTag, String.format("Received ${characteristic.uuid}: $hexString"))
                }
            }
        }
    }

    private fun connectBike(gatt: BluetoothGatt) {
        lifecycle.coroutineScope.launch {
            externalSensorRepository.get(gatt.device.address)?.bikeId?.let { bikeId ->
                bikeRepository.get(bikeId)?.let { bike ->
                    connectedBike = bike
                    EventBus.getDefault()
                        .post(ConnectedBikeEvent(bike = bike))
                }
            }
        }
    }

    companion object {
        private const val logTag = "BleServiceCompanion"
        fun isBluetoothSupported(context: Context): Boolean {
            return (context.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
        }

        fun isBluetoothEnabled(context: Context): Boolean {
            Log.d(logTag, "isBluetoothEnabled")
            return (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter?.isEnabled.also {
                Log.d(
                    logTag,
                    "bluetoothEnabled=${it}"
                )
            } ?: false.also {
                Log.d(logTag, "Bluetooth not supported")
            }
        }

        fun isBluetoothEnabled(): Boolean {
            val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            return mBluetoothAdapter.isEnabled
        }

        fun enableBluetooth(context: Context) {
            (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).let { bluetoothManager ->
                bluetoothManager.adapter?.let { bluetoothAdapter ->
                    if (!bluetoothAdapter.isEnabled) {
                        Log.d(logTag, "Requesting to enable Bluetooth")
                        EventBus.getDefault()
                            .post(BluetoothActionEvent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
                    }
                } ?: Log.d(logTag, "Cannot enable bluetooth, not supported")
            }
        }
    }

    private var forceBtRequest = false
    fun initialize() {
        Log.d(logTag, "initializing bluetooth service")
        if (!isBluetoothSupported(context)) {
            Log.d(logTag, "BLE not supported on this device")
            return
        }

        if (forceBtRequest) {
            enableBluetooth(context)
        } else {
            if (!isBluetoothEnabled(context)) {
                Toast.makeText(context, "Bluetooth is not enabled", Toast.LENGTH_LONG).show()
            }
        }

        lifecycle.coroutineScope.launch {
            externalSensorRepository.all().takeIf { it.isNotEmpty() }?.let { myMacs ->
                bluetoothManager.adapter?.let { bluetoothAdapter ->
                    myMacs.forEach { sensor ->
                        try {
                            connectToSensor(bluetoothAdapter, sensor.address)
                        } catch (e: IllegalArgumentException) {
                            Log.e(logTag, "Invalid sensor address", e)
                            externalSensorRepository.removeSensor(sensor)
                        }
                    }
                } ?: Log.d(logTag, "Cannot connect to sensors. Bluetooth not supported.")
            }
        }
    }

    private fun connectToSensor(
        bluetoothAdapter: BluetoothAdapter,
        sensorAddress: String
    ) {
        try {
            val device = bluetoothAdapter.getRemoteDevice(sensorAddress)
            if (device.type == BluetoothDevice.DEVICE_TYPE_UNKNOWN) {
                Log.d(
                    logTag,
                    "Scanning for uncached device: ${device.address}"
                )
                bluetoothAdapter.bluetoothLeScanner?.let { bluetoothLeScanner ->
                    val callback = scanForDevice(device.address)
                    val scanFilter: List<ScanFilter> = ArrayList()
                    val scanSettings = ScanSettings.Builder().
                                        setScanMode(SCAN_MODE_LOW_LATENCY).
                                        setMatchMode(MATCH_MODE_AGGRESSIVE).
                                        setLegacy(true).build()
                    scanCallbacks.add(callback)
                    bluetoothLeScanner.startScan(scanFilter, scanSettings, callback)
                } ?: Log.w(logTag, "BLE scanner not available")
            } else {
                Log.i(
                    logTag,
                    "Connecting to ${device.name}, ${device.type}: ${device.address}"
                )
                //TODO: Store BLE service for each device and use corresponding callback
                device.connectGatt(context, true, genericGattCallback)
                    ?.let { gatts.add(it) }
            }
        } catch (e: SecurityException) {
            Log.w(logTag, "BLE permissions have not been granted", e)
        }
    }

    fun stopAllScans() {
        try {
            Log.d(logTag, "Stop all scans")
            scanCallbacks.forEach {
                Log.d(logTag, "Stopping device scan")
                bluetoothManager.adapter?.bluetoothLeScanner?.stopScan(it)
            }
            scanCallbacks.clear()
        } catch (e: SecurityException) {
            Log.w(logTag, "Bluetooth permissions have not been granted", e)
        }
    }

    fun disconnect() {
        Log.d(logTag, "disconnect")
        stopAllScans()
        gatts.forEach { gatt ->
            Log.d(logTag, "Disconnecting ${gatt.device?.address}")
            try {
                gatt.close()
            } catch (e: SecurityException) {
                Log.w(logTag, "Bluetooth permissions have not been granted.", e)
            }
        }
        gatts.clear()

        addresses.hrm = null
        addresses.cadence = null
        addresses.speed = null

        hrmSensor = HrmData(null, null)
        cadenceSensor = CadenceData(null, null, null, null)
        speedSensor = SpeedData(null, null, null, null)
    }

    private fun shouldConnect(gatt: BluetoothGatt) =
        try {
            when (bluetoothManager.getConnectionState(
                gatt.device,
                BluetoothProfile.GATT
            )) {
                BluetoothProfile.STATE_CONNECTING, BluetoothProfile.STATE_CONNECTED -> false
                else -> true
            }
        } catch (e: SecurityException) {
            Log.w(logTag, "Bluetooth permissions have not been granted", e)
            false
        }

    fun restore() {
        Log.d(logTag, "called restore")
        gatts.filter { shouldConnect(it) }.forEach { gatt ->
            bluetoothManager.adapter?.let { bluetoothAdapter ->
                connectToSensor(bluetoothAdapter, gatt.device.address)
            }
        }
    }
}
