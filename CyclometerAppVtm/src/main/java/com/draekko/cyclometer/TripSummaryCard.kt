package com.draekko.cyclometer

//import com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds
//import com.google.android.gms.maps.GoogleMap
//import com.google.android.gms.maps.MapView
//import com.google.android.gms.maps.model.LatLngBounds
//import com.google.android.gms.maps.model.MapStyleOptions
//import com.google.android.gms.maps.model.PolylineOptions
/*
import org.mapsforge.map.layer.overlay.Polyline
import org.mapsforge.core.graphics.Color
import org.mapsforge.core.graphics.Style
import org.mapsforge.core.model.BoundingBox
import org.mapsforge.core.model.LatLong
import org.mapsforge.core.util.LatLongUtils
import org.mapsforge.map.android.graphics.AndroidGraphicFactory
import org.mapsforge.map.android.util.AndroidUtil
import org.mapsforge.map.android.view.MapView
import org.mapsforge.map.datastore.MapDataStore
import org.mapsforge.map.layer.renderer.TileRendererLayer
import org.mapsforge.map.reader.MapFile
import org.mapsforge.map.rendertheme.InternalRenderTheme
 */

import android.content.Context
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.format.DateUtils
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.preference.PreferenceManager
import com.draekko.cyclometer.data.Measurements
import com.draekko.cyclometer.util.SystemUtils
import org.oscim.backend.CanvasAdapter
import org.oscim.backend.canvas.Bitmap
import org.oscim.backend.canvas.Color
import org.oscim.backend.canvas.Paint.Cap
import org.oscim.core.BoundingBox
import org.oscim.core.GeoPoint
import org.oscim.core.MapPosition
import org.oscim.core.MercatorProjection.latitudeToY
import org.oscim.core.MercatorProjection.longitudeToX
import org.oscim.core.Tile
import org.oscim.event.Event
import org.oscim.event.MotionEvent
import org.oscim.layers.PathLayer
import org.oscim.layers.marker.ItemizedLayer
import org.oscim.layers.marker.MarkerInterface
import org.oscim.layers.marker.MarkerItem
import org.oscim.layers.marker.MarkerSymbol
import org.oscim.layers.marker.MarkerSymbol.HotspotPlace
import org.oscim.layers.tile.buildings.BuildingLayer
import org.oscim.layers.tile.vector.VectorTileLayer
import org.oscim.layers.tile.vector.labeling.LabelLayer
import org.oscim.map.Map
import org.oscim.renderer.GLViewport
import org.oscim.scalebar.DefaultMapScaleBar
import org.oscim.scalebar.MapScaleBar
import org.oscim.scalebar.MapScaleBarLayer
import org.oscim.theme.RenderTheme
import org.oscim.theme.VtmThemes
import org.oscim.theme.styles.LineStyle
import org.oscim.tiling.source.mapfile.MapFileTileSource
import org.oscim.utils.FastMath
import org.oscim.utils.IOUtils
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.math.abs


class TripSummaryCard : CardView, Map.InputListener, ItemizedLayer.OnItemGestureListener<MarkerInterface> {
    private lateinit var defaultBackgroundColor: ColorStateList
    private lateinit var mapTheme: RenderTheme
    private var dateView: TextView
    private var titleView: TextView
    private var startTimeView: TextView
    private var durationView: TextView
    private var mapView: org.oscim.android.MapView
    private lateinit var sharedPreferences: SharedPreferences

    var tripId: Long = 0L
    var showSelectionIndicator = false

    private var currentContext: Context

    private val logTag = "TripSummaryCard"

    init {
        Log.d(logTag, "initialize")
        View.inflate(context, R.layout.view_trip_summary_card, this)
        titleView = findViewById(R.id.trip_summary_title)
        startTimeView = findViewById(R.id.trip_summary_start_time)
        dateView = findViewById(R.id.trip_summary_date)
        durationView = findViewById((R.id.trip_summary_duration))
        mapView = findViewById(R.id.trip_summary_map)
    }

    constructor(context: Context) : super(context) {
        currentContext = context
        initMapView(context)

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        currentContext = context
        initMapView(context)

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        currentContext = context
        initMapView(context)

    }

    override fun setSelected(selected: Boolean) {
        when (selected) {
            true -> setCardBackgroundColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.accentColor, null
                )
            )
            else -> if (this::defaultBackgroundColor.isInitialized) setCardBackgroundColor(
                defaultBackgroundColor
            )
        }
        super.setSelected(selected)
    }

    var title: String
        get() = titleView.text.toString()
        set(value) {
            titleView.text = value
        }

    var startTime: String
        get() = startTimeView.text.toString()
        set(value) {
            startTimeView.text = value
        }

    var date: String
        get() = dateView.text.toString()
        set(value) {
            dateView.text = value
        }

    var duration: String
        get() = durationView.text.toString()
        set(value) {
            durationView.text = value
        }

    fun setStartTime(value: Long) {
        var pattern = "hh:mm a"
        if(sharedPreferences.getString(currentContext.getString(R.string.preferences_time_system_of_measurement_key), "2")!!.contains("2")) {
            pattern = "HH:mm"
        }
        startTime = "at " + SimpleDateFormat(pattern).format(Date(value))
    }

    fun setDate(value: Long) {
        date =
            if (DateUtils.isToday(value)) "earlier today" else DateUtils.getRelativeTimeSpanString(
                value,
                SystemUtils.currentTimeMillis(),
                DateUtils.DAY_IN_MILLIS
            ).toString()
    }

    fun setDuration(value: Double) {
        duration = "for ${formatDuration(value)}"
    }

    fun setTripDetails(_duration: Double, _distance: Double) {
        duration =
            "${
                String.format(
                    "%.2f",
                    getUserDistance(context, _distance)
                )
            }${getUserDistanceUnitShort(context)}  in  ${formatDuration(_duration)}"
    }

    fun setTripInProgress(_duration: Double, _distance: Double) {
        title = "Ride in progress"
        date = "Tap to continue"
        startTime = String.format(
            "%.2f %s",
            getUserDistance(context, _distance), getUserDistanceUnitShort(context)
        )
        duration = formatDuration(_duration)
    }

    fun drawPath(measurements: Array<Measurements>) {
        Log.d("TRIP_SUMMARY_CARD", "DRAW PATH")

        if (measurements == null) {
            Log.e(logTag, "Measurements is null")
            return
        }
        if (measurements.isEmpty()) {
            Log.e(logTag, "Measurements is empty")
            return
        }
        if (measurements.size < 2) {
            Log.e(logTag, "Measurements size < 2")
            Toast.makeText(currentContext, "Cannot display track due to missing data", Toast.LENGTH_LONG).show()
            return
        }

        var maxLat = -180.0
        var maxLng = -180.0
        var minLat = 180.0
        var minLng = 180.0
        Log.i(logTag, "parsing measurements")
        for (measurement in measurements) {
            if (measurement.latitude > maxLat) {
                maxLat = measurement.latitude
            }
            if (measurement.longitude > maxLng) {
                maxLng = measurement.longitude
            }
            if (measurement.latitude < minLat) {
                minLat = measurement.latitude
            }
            if (measurement.longitude < minLng) {
                minLng = measurement.longitude
            }
        }
        var bbox = BoundingBox(minLat, minLng, maxLat, maxLng)

        var zoomLevel = 1 shl 17

        val dx = abs(
            longitudeToX(bbox.maxLongitude)
                    - longitudeToX(bbox.minLongitude)
        )

        val dy = abs(
            latitudeToY(bbox.minLatitude)
                    - latitudeToY(bbox.maxLatitude)
        )

        var map = mapView.map()

        var zx: Double = map.getWidth() / (dx * Tile.SIZE)
        val zy: Double = map.getHeight() / (dy * Tile.SIZE)
        val scale = Math.min(zx, zy)
        val scaleOffset = scale * 0.14

        /* SET UP MAP VIEWPORT SCALING AND CENTERING */
        zoomLevel = FastMath.log2(scale.toInt())
        val p = bbox.centerPoint
        val centerMapPosition = MapPosition()
            .setX(longitudeToX(p.getLongitude()))
            .setY(latitudeToY(p.getLatitude()))
            .setZoomLevel(zoomLevel)
            .setScale(scale - scaleOffset)
        map.viewport().setMapPosition(centerMapPosition)

        /* DISPLAY TRACK PATH */
        if (measurements.size >= 2) {
            val pts: ArrayList<GeoPoint> = ArrayList()

            var startAccuracy = measurements.get(0).accuracy
            var start = GeoPoint(
                measurements.get(0).latitude,
                measurements.get(0).longitude)

            var endAccuracy = measurements.get(measurements.size - 1).accuracy
            var end = GeoPoint(
                measurements.get(measurements.size - 1).latitude,
                measurements.get(measurements.size - 1).longitude)

            for (measurement: Measurements in measurements) {
                pts.add(GeoPoint(measurement.latitude, measurement.longitude))
            }

            /* line path */
            if (true) {
                val pathLayerBlue: PathLayer
                val cB: Int = Color.RED
                val styleB: LineStyle = LineStyle.builder()
                    .color(cB)
                    .strokeWidth(4.0f)
                    .cap(Cap.ROUND)
                    .build()
                pathLayerBlue = PathLayer(map, styleB)
                pathLayerBlue.setPoints(pts)
                map.layers().add(pathLayerBlue)
            }

            /* stipple line path */
            if (false) {
                val pathLayerRed: PathLayer
                val cR: Int = Color.RED
                val styleR: LineStyle = LineStyle.builder()
                    .stippleColor(cR)
                    .stipple(24)
                    .stippleWidth(1.0f)
                    .strokeWidth(8.0f)
                    .strokeColor(cR)
                    .fixed(true)
                    .randomOffset(false)
                    .isOutline(true)
                    .build()
                pathLayerRed = PathLayer(map, styleR)
                pathLayerRed.setPoints(pts)
                map.layers().add(pathLayerRed)
            }

            /* start and stop markers */
            if (true) {
                var `is`: InputStream? = null
                var bitmapMarkerStart: Bitmap? = null
                var bitmapMarkerEnd: Bitmap? = null
                try {
                    `is` = resources.openRawResource(R.raw.marker_start)
                    bitmapMarkerStart = CanvasAdapter.decodeSvgBitmap(
                        `is`,
                        (48 * CanvasAdapter.getScale()).toInt(),
                        (48 * CanvasAdapter.getScale()).toInt(), 100
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    IOUtils.closeQuietly(`is`)
                }

                try {
                    `is` = resources.openRawResource(R.raw.marker_end)
                    bitmapMarkerEnd = CanvasAdapter.decodeSvgBitmap(
                        `is`,
                        (48 * CanvasAdapter.getScale()).toInt(),
                        (48 * CanvasAdapter.getScale()).toInt(), 100
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    IOUtils.closeQuietly(`is`)
                }

                var startSymbol = MarkerSymbol(bitmapMarkerStart, HotspotPlace.CENTER, false)
                var endSymbol = MarkerSymbol(bitmapMarkerEnd, HotspotPlace.CENTER, false)
                var startMarkerLayer = ItemizedLayer(map, ArrayList<MarkerInterface>(), startSymbol, this)
                var endMarkerLayer = ItemizedLayer(map, ArrayList<MarkerInterface>(), endSymbol, this)
                map.layers().add(startMarkerLayer);
                map.layers().add(endMarkerLayer);

                val ptsStart: ArrayList<MarkerInterface> = ArrayList()
                val ptsEnd: ArrayList<MarkerInterface> = ArrayList()
                var descriptionStart = "start marker"
                var descriptionEnd = "end marker"

                var startMarker = MarkerItem(
                    start.latitude.toString() + "/" + start.longitude.toString(),
                    descriptionStart,
                    GeoPoint(start.latitude, start.longitude))

                var endMarker = MarkerItem(
                    end.latitude.toString() + "/" + end.longitude.toString(),
                    descriptionEnd,
                    GeoPoint(end.latitude, end.longitude))

                ptsStart.add(startMarker)
                ptsEnd.add(endMarker)
                startMarkerLayer.addItems(ptsStart);
                endMarkerLayer.addItems(ptsEnd);
            }

            if (false) {
                val mapScaleBar: MapScaleBar = DefaultMapScaleBar(map)
                mapScaleBarLayer = MapScaleBarLayer(map, mapScaleBar)
                mapScaleBarLayer.renderer.setPosition(GLViewport.Position.BOTTOM_LEFT)
                mapScaleBarLayer.renderer.setOffset(5 * CanvasAdapter.getScale(), 0f)
                map.layers().add(mapScaleBarLayer)
            }
        }
    }

    private lateinit var card: TripSummaryCard
    override fun onFinishInflate() {
        super.onFinishInflate()

        Log.d(logTag, "onFinishInflate")

        defaultBackgroundColor = cardBackgroundColor

        card = this

        mapView.map().input.bind(this)
    }

    fun onCreateMap(savedInstanceState: Bundle?) {
    }

    fun onStart() {
    }

    fun onResumeMap() {
        mapView.map().input.bind(this)
    }

    fun onPauseMap() {
        mapView.map().input.bind(this)
    }

    fun onStopMap() {
    }

    fun onDestroyMap() {
        mapView.onDestroy()
    }

    fun onSaveMapInstanceState(state: Bundle) {
    }

    fun onMapLowMemory() {
    }

    fun clearMap() {
        if (mapView != null) {
            mapView.map().clearMap()
        }
    }

    private lateinit var mapScaleBarLayer: MapScaleBarLayer
    private fun openMapFile(mapfile: File) {
        try {
            // Tile source
            var fis = FileInputStream(mapfile)
            val tileSource = MapFileTileSource()
            tileSource.setMapFileInputStream(fis)

            var map = mapView.map()
            // Vector layer
            val tileLayer: VectorTileLayer = map.setBaseMap(tileSource)

            // Building layer
            mapView.map().layers().add(BuildingLayer(map, tileLayer))

            // Label layer
            mapView.map().layers().add(LabelLayer(map, tileLayer))

            // Render theme
            var vtmTheme = VtmThemes.DEFAULT
            var maptheme = sharedPreferences.getString(
                currentContext.getString(R.string.preference_map_theme_key),
                "1")
            when (maptheme) {
                "1" -> {
                    vtmTheme = VtmThemes.DEFAULT
                }
                "2" -> {
                    vtmTheme = VtmThemes.NEWTRON
                }
                "3" -> {
                    vtmTheme = VtmThemes.TRONRENDER
                }
                "4" -> {
                    vtmTheme = VtmThemes.BIKER
                }
                "5" -> {
                    vtmTheme = VtmThemes.MAPZEN
                }
                "6" -> {
                    vtmTheme = VtmThemes.OSMARENDER
                }
                "7" -> {
                    vtmTheme = VtmThemes.OSMAGRAY
                }
                "8" -> {
                    vtmTheme = VtmThemes.MOTORIDER
                }
                "9" -> {
                    vtmTheme = VtmThemes.MOTORIDER_DARK
                }
            }

            //var custom = ExternalRenderTheme("/sdcard/motorider-dark.xml")
            mapTheme = map.setTheme(vtmTheme) as RenderTheme
            //mapTheme = map.setTheme(custom) as RenderTheme

            // Scale bar
            if (false) {
                val mapScaleBar: MapScaleBar = DefaultMapScaleBar(map)
                mapScaleBarLayer = MapScaleBarLayer(map, mapScaleBar)
                mapScaleBarLayer.renderer.setPosition(GLViewport.Position.BOTTOM_LEFT)
                mapScaleBarLayer.renderer.setOffset(5 * CanvasAdapter.getScale(), 0f)
                map.layers().add(mapScaleBarLayer)
            }
        } catch (e: java.lang.Exception) {
            /*
             * In case of map file errors avoid crash, but developers should handle these cases!
             */
            e.printStackTrace()
        }
    }

    private fun initMapView(context: Context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        var mapFileName = sharedPreferences.getString(currentContext.getString(R.string.preferences_map_filename_key), "")
        var mapFile = File(mapFileName)
        if (mapFile.exists()) {
            openMapFile(mapFile)

            var vtmTheme = VtmThemes.DEFAULT
            var maptheme = sharedPreferences.getString(
                currentContext.getString(R.string.preference_map_theme_key),
                "1")
            when (maptheme) {
                "1" -> {
                    vtmTheme = VtmThemes.DEFAULT
                }
                "2" -> {
                    vtmTheme = VtmThemes.NEWTRON
                }
                "3" -> {
                    vtmTheme = VtmThemes.TRONRENDER
                }
                "4" -> {
                    vtmTheme = VtmThemes.BIKER
                }
                "5" -> {
                    vtmTheme = VtmThemes.MAPZEN
                }
                "6" -> {
                    vtmTheme = VtmThemes.OSMARENDER
                }
                "7" -> {
                    vtmTheme = VtmThemes.OSMAGRAY
                }
                "8" -> {
                    vtmTheme = VtmThemes.MOTORIDER
                }
                "9" -> {
                    vtmTheme = VtmThemes.MOTORIDER_DARK
                }
            }

            mapTheme = mapView.map().setTheme(vtmTheme) as RenderTheme
            mapView.map().updateMap(true)
            mapView.invalidate()
        } else {
            Log.i(logTag, "No map loaded, select a map file")
        }
    }

    private var lastAction: Int = 0
    override fun onInputEvent(event: Event?, motionEvent: MotionEvent?) {
        //TODO("Not yet implemented")
        Log.i(logTag, "onInputEvent called")
        var action = motionEvent!!.action and MotionEvent.ACTION_MASK
        if (lastAction == MotionEvent.ACTION_DOWN ||
            lastAction == MotionEvent.ACTION_POINTER_DOWN) {
            if (action == MotionEvent.ACTION_UP ||
                action == MotionEvent.ACTION_POINTER_UP) {
                card.performClick()
            }
        }
        lastAction = action
    }

    override fun onItemSingleTapUp(index: Int, item: MarkerInterface?): Boolean {
        //TODO("Not yet implemented")
        return true
    }

    override fun onItemLongPress(index: Int, item: MarkerInterface?): Boolean {
        //TODO("Not yet implemented")
        return true
    }
}