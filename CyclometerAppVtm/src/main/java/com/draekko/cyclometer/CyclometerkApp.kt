package com.draekko.cyclometer

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.hilt.work.HiltWorkerFactory
import androidx.preference.PreferenceManager
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class CyclometerApp : Application(), Configuration.Provider {
    companion object {
        lateinit var instance: CyclometerApp private set
    }

    private fun createNotificationChannel() {
        val exportCompleteChannel =
            NotificationChannel(
                getString(R.string.notification_export_trip_id),
                getString(R.string.notification_export_trip_name),
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = getString(R.string.notification_channel_description_export_trip)
            }
        val exportInProgressChannel =
            NotificationChannel(
                getString(R.string.notification_export_trip_in_progress_id),
                getString(R.string.notification_export_trip_in_progress_name),
                NotificationManager.IMPORTANCE_LOW
            ).apply {
                description = getString(R.string.notification_channel_description_export_trip)
            }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(exportCompleteChannel)
        notificationManager.createNotificationChannel(exportInProgressChannel)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        createNotificationChannel()
        /*PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean(getString(R.string.preference_key_analytics_opt_in_presented),
                false)
            commit()
        }*/
        var key = getString(R.string.preferences_key_enable_analytics)
        PreferenceManager.getDefaultSharedPreferences(this).getBoolean(key, false)
    }

    @Inject
    lateinit var workFactory: HiltWorkerFactory
    override fun getWorkManagerConfiguration() =
        Configuration.Builder().setWorkerFactory(workFactory).build()
}