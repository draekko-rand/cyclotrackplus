package com.draekko.cyclometer

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.BATTERY_SERVICE
import android.content.Context.LOCATION_SERVICE
import android.content.Context.SENSOR_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.GnssStatus
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.OnNmeaMessageListener
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.Surface
import android.view.View
import android.view.View.GONE
import android.view.View.OnClickListener
import android.view.View.OnTouchListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowInsets
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import com.draekko.common.lib.GeomagneticField.GeomagneticField2025
import com.draekko.common.lib.tcx.TcxParser
import com.draekko.cyclometer.data.Bike
import com.draekko.cyclometer.data.PowerMeterMeasurement
import com.draekko.cyclometer.data.PowerMeterSensorType
import com.draekko.cyclometer.data.TimeState
import com.draekko.cyclometer.data.TimeStateEnum
import com.draekko.cyclometer.events.StartTripEvent
import com.draekko.cyclometer.events.WheelCircumferenceEvent
import com.draekko.cyclometer.services.TripInProgressService
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_DISABLED
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_MID_LO_LEFT
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_MID_UP_LEFT
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_MID_UP_RIGHT
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_TOP
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_TOP_LEFT
import com.draekko.cyclometer.util.DISPLAY_SETTINGS_TOP_RIGHT
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_AP
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_AVG_SPEED
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_CADENCE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_DISTANCE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_DURATION
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_ELEVATION
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_GPS_BEARING
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_GRADE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_HEART_RATE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_MAG_BEARING
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_POWER_METER
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_RH
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_SLOPE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_SPEED
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_SPLITS
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_TEMPERATURE
import com.draekko.cyclometer.util.PREFS_DISPLAY_SETTINGS_WIND
import com.draekko.cyclometer.util.getBrightnessPreference
import com.draekko.cyclometer.util.getRootPaths
import com.draekko.cyclometer.util.getSafeZoneMargins
import com.draekko.cyclometer.util.getUserWeight
import com.draekko.cyclometer.util.mainStoragePath
import com.draekko.iconcontextmenu.IIconContextItemSelectedListener
import com.draekko.iconcontextmenu.IconContextMenu
import com.draekko.iconcontextmenu.IconContextMenu.SANS
import com.draekko.iconcontextmenu.IconContextMenu.THEME_DARK
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.model.LatLng
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import dagger.hilt.android.AndroidEntryPoint
import io.ticofab.androidgpxparser.parser.GPXParser
import io.ticofab.androidgpxparser.parser.domain.Gpx
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.oscim.backend.CanvasAdapter
import org.oscim.backend.canvas.Bitmap
import org.oscim.backend.canvas.Color
import org.oscim.backend.canvas.Paint
import org.oscim.core.GeoPoint
import org.oscim.core.MapPosition
import org.oscim.core.MercatorProjection
import org.oscim.event.Event
import org.oscim.layers.PathLayer
import org.oscim.layers.marker.ItemizedLayer
import org.oscim.layers.marker.MarkerInterface
import org.oscim.layers.marker.MarkerItem
import org.oscim.layers.marker.MarkerSymbol
import org.oscim.layers.tile.buildings.BuildingLayer
import org.oscim.layers.tile.vector.VectorTileLayer
import org.oscim.layers.tile.vector.labeling.LabelLayer
import org.oscim.theme.RenderTheme
import org.oscim.theme.VtmThemes
import org.oscim.theme.styles.LineStyle
import org.oscim.tiling.source.mapfile.MapFileTileSource
import org.oscim.utils.IOUtils
import org.xmlpull.v1.XmlPullParserException
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Locale
import java.util.regex.Pattern
import kotlin.math.pow
import kotlin.math.roundToInt


@AndroidEntryPoint
class TripInProgressFragment : Fragment(),
        OnTouchListener,
        ItemizedLayer.OnItemGestureListener<MarkerInterface>,
        org.oscim.map.Map.UpdateListener,
        SensorEventListener {

    private val logTag = "TripInProgressFragment"

    private val showdebug = false

    private var pressureValue: Int = 0
    private var relativeHumidityValue: Int = 0
    private lateinit var airpressure: String
    private lateinit var relativeHumidity: String

    private val displaySelectionStrings = arrayOf( "none",
        "gps speed", "distance", "average distance", "grade", "splits",
        "duration", "heart rate sensor", "cadence sensor", "speed sensor", "power sensor", "temperature",
        "relative humidity", "air pressure", "wind", "gps bearing", "magnetic bearing", "elevation")

    private var declination = 0.0f
    private var view_selection_value_speed = "0.0"
    private var view_selection_value_distance = "0.0"
    private var view_selection_value_avg_speed = "0.0"
    private var view_selection_value_slope = "0.0"
    private var view_selection_value_grade = "0.0"
    private var view_selection_value_splits = "0.0"
    private var view_selection_value_duration = "0.0"
    private var view_selection_value_heart_rate = "0.0"
    private var view_selection_value_cadence = "0.0"
    private var view_selection_value_power_meter = "0.0"
    private var view_selection_value_temperature = "0.0"
    private var view_selection_value_relative_humidity = "0.0"
    private var view_selection_value_air_pressure = "0.0"
    private var view_selection_value_wind = "0.0"
    private var view_selection_value_gps_bearing = "0.0"
    private var view_selection_value_magnetic_bearing = "0.0"
    private var view_selection_value_elevation = "0.0"

    private val viewModel: TripInProgressViewModel by viewModels()
    private val args: TripInProgressFragmentArgs by navArgs()
    private var zoomLevel: Int = 16
    private var hasCoarseLocation: Boolean = false
    private var hasFineLocation: Boolean = false
    private lateinit var locationManager: LocationManager
    private var compassIndicatorColor: Int = 0

    private lateinit var batteryStatus: Intent
    private var ifilter: IntentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)

    private lateinit var settingMenu: IconContextMenu
    private lateinit var mapThemeMenu: IconContextMenu
    private lateinit var displayItemselctionMenu: IconContextMenu
    private lateinit var uiMenuSelections: IconContextMenu

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var startLocation: GeoPoint
    private lateinit var endLocation: GeoPoint
    private lateinit var currentLocation: GeoPoint

    private lateinit var vtmMapView: org.oscim.android.MapView
    private lateinit var mapViewFrame: FrameLayout
    private lateinit var pauseButton: Button
    private lateinit var stopButton: Button
    private lateinit var resumeButton: Button
    private lateinit var autoPauseChip: Button
    private lateinit var bottomRightView: MeasurementView
    private lateinit var middleLowerRightView: MeasurementView
    private lateinit var middleUpperRightView: MeasurementView
    private lateinit var topView: MeasurementView
    private lateinit var bottomLeftView: MeasurementView
    private lateinit var middleLowerLeftView: MeasurementView
    private lateinit var middleUpperLeftView: MeasurementView
    private lateinit var topLeftView: MeasurementView
    private lateinit var topRightView: MeasurementView
    private lateinit var footerLeftView: TextView
    private lateinit var footerRightView: TextView
    private lateinit var trackingImage: ImageView
    private lateinit var compassImage: ImageView
    private lateinit var timeOfDayTextView: TextView
    private lateinit var batteryLevelText: TextView
    private lateinit var chargingImage: ImageView
    private lateinit var settingsButton: ImageButton
    //private lateinit var debugTextView: TextView
    //private lateinit var temperatureTextView: TextView
    //private lateinit var windDirectionArrow: ImageView

    private var storagePath = mainStoragePath

    private lateinit var sensorManager: SensorManager
    private lateinit var accelerometer: Sensor
    private lateinit var magnetometer: Sensor
    private lateinit var pressuresensor: Sensor

    private lateinit var bearing: String
    private lateinit var windSpeed: String
    private lateinit var windDirection: String
    private lateinit var temperature: String

    private lateinit var mapTheme: RenderTheme

    private lateinit var bitmapMarkerCurrent: Bitmap
    private lateinit var currentSymbol: MarkerSymbol
    private lateinit var currentMarkerLayer: ItemizedLayer
    private lateinit var currentMarker: MarkerItem
    private var ptsCurrent: ArrayList<MarkerInterface> = ArrayList()

    private lateinit var bitmapMarkerStart: Bitmap
    private lateinit var startSymbol: MarkerSymbol
    private lateinit var startMarkerLayer: ItemizedLayer
    private lateinit var startMarker: MarkerItem
    private var ptsStart: ArrayList<MarkerInterface> = ArrayList()

    private lateinit var bitmapMarkerEnd: Bitmap
    private lateinit var endSymbol: MarkerSymbol
    private lateinit var endMarkerLayer: ItemizedLayer
    private lateinit var endMarker: MarkerItem
    private var ptsEnd: ArrayList<MarkerInterface> = ArrayList()

    private var debugLog = false;
    private var orientation = 1
    private var useSlope = true
    private var useMetricDistance = true
    private var useTwentyFourHour = true
    private var useCelsiusTemp = true
    private var useKiloMass = true
    private var useMapViewForTripProgress = true
    private var gpsEnabled = true
    private var isTimeTickRegistered = false
    private val lowBatteryThreshold = 15
    private lateinit var sharedPreferences: SharedPreferences
    private var autoCircumference: Float? = null
    private var autoCircumferenceVariance: Double? = null

    private var initCenterMarkerDone = false

    private lateinit var parsedGpx:Gpx
    private lateinit var lastLocation: Location
    //private lateinit var locationRequest: LocationRequest
    private lateinit var currentContext: Context

    private var batteryState: Int = 100
    private var batteryPlugged = false

    private var scaleText: Float = 1.0f

    private val interval: Long = 5000 // 10seconds
    private val fastestInterval: Long = 2000 // 5 seconds
    private var longitude: Double = -9999.0
    private var latitude: Double = -9999.0
    private var altitude: Double = -9999.0
    private var useFixedOirentation = false

    private lateinit var batteryManager: BatteryManager
    private var registeredBatteryIntent = false

    private var speedSensorValue: Float = 0.0f
    private var cadenceSensorValue: Float = 0.0f
    private var powerSensorValue: Float = 0.0f
    private var avgPowerSensorValue: Float = 0.0f
    private var airPressureCalibration: Float = 0.0f
    private var humiditySensor: Int = 0

    private var mslAlitude = -7676.0
    private var mslAlitudeUnit = ""
    private var hdop = -1.0
    private var fixQuality = -1

    private var triggerMapUpdate = false
    private var useSensor: Boolean = false
    private var slope: Float = 0.0001f
    private var grade: Float = 0.0f
    //private var totalMass: Float = 0.0f
    //private var wattsCalc: Float = 0.0f
    private var avg_speed = 0.0f
    private var trophy = R.drawable.ic_trophy

    private var circumference: Float = 0.0f
    private var bikeMass: Float = 0.0f
    private var userMass: Float = 0.0f

    private var usePowerSensor = true
    private var useCadenceWatts = false
    private var useCadenceWattsSensor = true
    private var useWindSpeed = true
    private var lastAltitude = 0.0
    private lateinit var lastLatLng: LatLng

    private val gravityAccel = 9.80665
    private val meterPerSecond = 0.2778
    private val gearLoss = 0.015
    private val chainLossNew = 0.03
    private val chainLossOld = 0.04
    private val chainLossDryOld = 0.05

    private var offset = arrayOf( 1.0, 0.5, -1.0, 0.5 )

    val nmeaMessageListener = OnNmeaMessageListener { message: String, timestamp: Long ->
        if (message != null && message.lowercase().contains("gga")) {
            var nmeaArray = message.split(",")
            if (nmeaArray[9] != null && !nmeaArray[9].isNullOrEmpty()) {
                mslAlitude = nmeaArray[9].toDouble()
            }
            if (nmeaArray[10] != null && !nmeaArray[10].isNullOrEmpty()) {
                mslAlitudeUnit = nmeaArray[10]
            }
            if (nmeaArray[6] != null && !nmeaArray[6].isNullOrEmpty()) {
                fixQuality = nmeaArray[6].toInt()
            } else {
                fixQuality = 0
            }
            if (nmeaArray[8] != null && !nmeaArray[8].isNullOrEmpty()) {
                hdop = nmeaArray[8].toDouble()
            } else {
                hdop = 0.0
            }
        }

        handleGpsServiceFixQuality()
    }

    private val hidePauseHandler = android.os.Handler(Looper.getMainLooper())
    private val hidePauseCallback = Runnable {
        slidePauseDown()
    }

    //private val userCircumference: Float?
    //    get() = viewModel.bikeWheelCircumference

    /*
    val circumference: Float?
        get() = when (sharedPreferences.getBoolean(
            requireContext().applicationContext.getString(
                R.string.preference_key_useAutoCircumference
            ), true
        )) {
            true -> autoCircumference ?: userCircumference
            else -> userCircumference ?: autoCircumference
        }
     */

    private var useAutoCircumference: Boolean = false

    @Subscribe
    fun onWheelCircumferenceEvent(event: WheelCircumferenceEvent) {
        autoCircumference = event.circumference
        autoCircumferenceVariance = event.variance
    }

    private val timeTickReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.v(logTag, "Received time tick")
            updateClock()
        }
    }

    companion object {
        fun newInstance() = TripInProgressFragment()
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            if (useMapViewForTripProgress) {
                zoomLevel = vtmMapView.map().mapPosition.zoomLevel
            }
            locationResult.lastLocation
            val location = locationResult.lastLocation
            if (location != null) {
                declination = GeomagneticField2025(
                    location.latitude.toFloat(),
                    location.longitude.toFloat(),
                    location.altitude.toFloat(),
                    System.currentTimeMillis()
                ).declination
                locationChanged(location)
                latitude = location.latitude
                longitude = location.longitude
                altitude = location.altitude
                if (debugLog) Log.i(logTag, "Last LAT $latitude LNG $longitude")
            }
        }
    }

    fun locationChanged(location: Location) {
        if (location != null) {
            lastLocation = location
            longitude = lastLocation.longitude
            latitude = lastLocation.latitude

            if (useMapViewForTripProgress) {
                currentLocation = GeoPoint(latitude, longitude)

                if (::vtmMapView.isInitialized) {
                    val centerMapPosition = MapPosition()
                        .setX(MercatorProjection.longitudeToX(longitude))
                        .setY(MercatorProjection.latitudeToY(latitude))
                        .setZoomLevel(zoomLevel)

                    /* set map center position */
                    vtmMapView.map().setMapPosition(centerMapPosition)
                    vtmMapView.map().viewport().setMapPosition(centerMapPosition)

                    if (::currentMarker.isInitialized) {
                        /* set map center position marker */
                        ptsCurrent.remove(currentMarker)
                    }
                    if (::currentMarkerLayer.isInitialized) {
                        currentMarkerLayer.removeAllItems();
                    }

                    if (::currentMarker.isInitialized &&
                        ::currentMarkerLayer.isInitialized) {
                        var str = "current Location"
                        currentMarker = MarkerItem("$latitude/$longitude", str, currentLocation)

                        ptsCurrent.add(currentMarker)
                        currentMarkerLayer.addItems(ptsCurrent);
                    }
                }
            }
        }
    }

    private fun batteryLevel() {
        if (!registeredBatteryIntent) {
            batteryStatus = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
                currentContext.registerReceiver(null, ifilter)!!
            }
        }
        if (!::batteryManager.isInitialized) {
            batteryManager = currentContext.getSystemService(BATTERY_SERVICE) as BatteryManager
        }
        batteryState = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        if (::batteryLevelText.isInitialized) {
            batteryLevelText.text = "$batteryState%"
        }

        val chargePlug: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) ?: -1
        batteryPlugged = false
        if (chargePlug == BatteryManager.BATTERY_PLUGGED_USB) {
            batteryPlugged = true
        } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_AC) {
            batteryPlugged = true
        } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS) {
            batteryPlugged = true
        } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_DOCK) {
            batteryPlugged = true
        }

        if (batteryManager.isCharging ||
            batteryPlugged) {
            chargingImage.visibility = View.VISIBLE
        } else {
            chargingImage.visibility = View.GONE
        }

        var secondsPrefs = sharedPreferences.getInt(getString(R.string.preferences_key_battery_check_interval), 1)
        val batteryCheckTime = 1000L * secondsPrefs
        Handler().postDelayed(Runnable {
            batteryLevel()
        }, batteryCheckTime)
    }

    private fun handleFusedLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(logTag, "User has not granted permission to access location data")
            return
        }

        try {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(currentContext)

            if (fusedLocationClient != null) {
                //var locationRequest = LocationRequest.create()
                val locationManager = currentContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e(logTag, "GPS Provider not enabled")
                }

                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        locationManager.addNmeaListener(ContextCompat.getMainExecutor(currentContext), nmeaMessageListener)
                    } else {
                        locationManager.addNmeaListener(nmeaMessageListener, Handler(Looper.getMainLooper()))
                    }
                } catch (e: Exception) {
                    Log.e(logTag, "Exception in nmea location flow: $e")
                }

                hasCoarseLocation = ActivityCompat.checkSelfPermission(currentContext,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                hasFineLocation = ActivityCompat.checkSelfPermission(currentContext,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (fusedLocationClient != null && (hasFineLocation || hasCoarseLocation)) {
                    fusedLocationClient.lastLocation.addOnSuccessListener {
                            location : Location? ->
                        if (location != null) {
                            var lat = location?.latitude
                            var lng = location?.longitude
                            currentLocation = GeoPoint(lat!!.toDouble(), lng!!.toDouble())
                            if (useMapViewForTripProgress) {
                                if (!initCenterMarkerDone) {
                                    initCenterMarkerDone = true
                                    clearCenterPointFocus()
                                    initCenterPointFocus(currentLocation)
                                }
                            }
                            locationChanged(location)
                        }
                    }
                }

                //locationRequest.priority = Priority.PRIORITY_HIGH_ACCURACY
                //locationRequest.interval = interval
                //locationRequest.fastestInterval = fastestInterval

                var locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, interval)
                    .setWaitForAccurateLocation(false)
                    .setMinUpdateIntervalMillis(fastestInterval)
                    //.setMaxUpdateDelayMillis(locationMaxWaitTime)
                    .build()

                val builder = LocationSettingsRequest.Builder()
                builder.addLocationRequest(locationRequest)
                val locationSettingsRequest = builder.build()
                val settingsClient = LocationServices.getSettingsClient(currentContext)
                settingsClient.checkLocationSettings(locationSettingsRequest)

                hasCoarseLocation = ActivityCompat.checkSelfPermission(currentContext,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                hasFineLocation = ActivityCompat.checkSelfPermission(currentContext,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (fusedLocationClient != null && (hasFineLocation || hasCoarseLocation)) {
                    fusedLocationClient!!.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.myLooper()!!)
                }
            } else {
                Log.w(logTag, "Failed to start fused location service listener ... fusedclient == null")
            }
        } catch (e: Exception) {
            Log.e(logTag, "Failed to start fused location service listener!")
            e.printStackTrace()
        }
    }

    private fun handleScreenOrientation(scrOrientation: Int) {
        when(scrOrientation) {
            1 -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR)
            }
            2 -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            }
            3 -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            }
            4 -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE)
            }
            5 -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT)
            }
            else -> {
                requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR)
            }
        }
    }

    private fun handleScreenOrientation(scrOrientation: String) {
        when(scrOrientation) {
            "1" -> {
                orientation = 1
            }
            "2" -> {
                orientation = 2
            }
            "3" -> {
                orientation = 3
            }
            "4" -> {
                orientation = 4
            }
            "5" -> {
                orientation = 5
            }
            else -> {
                orientation = 1
            }
        }

        handleScreenOrientation(orientation)
    }

    private fun handleSharedPrefsSettings() {
        if (::sharedPreferences.isInitialized) {
            useMetricDistance = false
            var strMetricDistance = sharedPreferences.getString(getString(R.string.preferences_dist_system_of_measurement_key), "2")
            if (strMetricDistance!!.contains("2")) {
                useMetricDistance = true
            }

            useTwentyFourHour = false
            var strTimeMetric = sharedPreferences.getString(getString(R.string.preferences_time_system_of_measurement_key), "2")
            if (strTimeMetric!!.contains("2")) {
                useTwentyFourHour = true
            }

            useCelsiusTemp = false
            var strTemp = sharedPreferences.getString(getString(R.string.preferences_temp_system_of_measurement_key), "2")
            if (strTemp!!.contains("2")) {
                useCelsiusTemp = true
            }

            useKiloMass = false
            var strKiloMass = sharedPreferences.getString(getString(R.string.preferences_mass_system_of_measurement_key), "2")
            if (strKiloMass!!.contains("2")) {
                useKiloMass = true
            }

            useMapViewForTripProgress = sharedPreferences.getBoolean(getString(R.string.preferences_use_mapview_in_progress_screen_key), true)

            var scrOrientation = sharedPreferences.getString(getString(R.string.preferences_ui_orientation_key), "1")
            handleScreenOrientation(scrOrientation!!)
        }
    }

    private var defaultBike: Bike? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sensorManager = currentContext.getSystemService(SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        try {
            pressuresensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
        } catch (e: Exception) {
            Log.e(logTag, "ERROR: failed to initialize pressure sensor, device probably doesn't have one!")
            hasAirPressure = false
        }

        compassIndicatorColor = currentContext.getColor(R.color.red)

        handleFusedLocation()

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        handleSharedPrefsSettings()

        locationManager = currentContext.getSystemService(LOCATION_SERVICE) as LocationManager

        var gnssStatus = object : GnssStatus.Callback() {
            // TODO: add your code here!
            override fun onFirstFix(ttffMillis: Int) {
                super.onFirstFix(ttffMillis)
            }

            override fun onSatelliteStatusChanged(status: GnssStatus) {
                super.onSatelliteStatusChanged(status)
            }

            override fun onStarted() {
                super.onStarted()
            }

            override fun onStopped() {
                super.onStopped()
            }
        }

        var locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                if (showdebug) Log.i(logTag, "onLocationChanged")
            }
            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
                if (showdebug) Log.i(logTag, "onStatusChanged")
            }
            override fun onProviderEnabled(provider: String) {
                if (showdebug) Log.i(logTag, "onProviderEnabled")
                //trackingImage.imageTintList = ColorStateList.valueOf(currentContext.getColor(R.color.button_green))
            }
            override fun onProviderDisabled(provider: String) {
                if (showdebug) Log.i(logTag, "onProviderDisabled")
                trackingImage.imageTintList = ColorStateList.valueOf(currentContext.getColor(R.color.icon_white))
            }
        }

        if (ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.registerGnssStatusCallback(gnssStatus)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0.333f, locationListener);
        }
    }

    private fun changeScreenBrightness(context: Context, screenBrightnessValue: Float) {
        Settings.System.putInt(
            context.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS_MODE,
            Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
        )
        Settings.System.putInt(
            context.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS, (screenBrightnessValue * 100f).toInt()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        activity?.window?.apply {
            val params = attributes
            getBrightnessPreference(currentContext).let { brightness ->
                Log.d(logTag, "User brightness $brightness")
                if (brightness > 0) {
                    try {
                        changeScreenBrightness(currentContext, brightness)
                    } catch (e: SecurityException) {
                    }
                    params.screenBrightness = brightness
                    attributes = params
                }
            }
        }

        var layoutId: Int = R.layout.fragment_trip_in_progress
        if (useMapViewForTripProgress) {
            layoutId = R.layout.fragment_map_trip_in_progress
        }
        return inflater.inflate(layoutId, container, false)
    }

    private fun hidePause() {
        pauseButton.translationY = pauseButton.height.toFloat()
    }

    private fun hideResume() {
        resumeButton.translationX = resumeButton.width.toFloat()
    }

    private fun hideStop() {
        stopButton.translationX = -stopButton.width.toFloat()
    }

    private fun hideResumeStop() {
        hideResume()
        hideStop()
    }

    private fun slidePauseDown() =
        pauseButton.animate().setDuration(100).translationY(pauseButton.height.toFloat())

    private fun slidePauseUp() =
        pauseButton.animate().setDuration(100).translationY(0f)

    private fun slideResumeOut() =
        resumeButton.animate().setDuration(100).translationX(resumeButton.width.toFloat())

    private fun slideStopOut() =
        stopButton.animate().setDuration(100).translationX(-stopButton.width.toFloat())

    private fun slideResumeIn() =
        resumeButton.animate().setDuration(100).translationX(0f)

    private fun slideStopIn() =
        stopButton.animate().setDuration(100).translationX(0f)

    private fun slideOutResumeStop() {
        slideResumeOut()
        slideStopOut()
    }

    private fun slideInResumeStop() {
        slideResumeIn()
        slideStopIn()
    }

    private fun turnOnGps() {
        val locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 1000)
            .setMinUpdateIntervalMillis(100)
            .build()
        val builder =
            locationRequest.let {
                LocationSettingsRequest.Builder().addLocationRequest(it)
            }
        val client = LocationServices.getSettingsClient(requireActivity())
        val task = client.checkLocationSettings(builder.build())
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        requireActivity(),
                        1000
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    private fun startTrip() {
        Log.d(logTag, "$host")
        requireContext().startService(Intent(
            requireContext(),
            TripInProgressService::class.java
        ).apply {
            this.action = getString(R.string.action_start_trip_service)
        })
    }

    private fun pauseTrip(tripId: Long) {
        requireActivity().startService(Intent(
            requireContext(),
            TripInProgressService::class.java
        ).apply {
            this.action = getString(R.string.action_pause_trip_service)
            this.putExtra("tripId", tripId)
        })
    }

    private fun resumeTrip(tripId: Long) {
        requireActivity().startService(Intent(
            requireContext(),
            TripInProgressService::class.java
        ).apply {
            this.action = getString(R.string.action_resume_trip_service)
            this.putExtra("tripId", tripId)
        })
    }

    private fun endTrip(tripId: Long) {
        requireActivity().startService(Intent(
            requireContext(),
            TripInProgressService::class.java
        ).apply {
            this.action = getString(R.string.action_stop_trip_service)
            this.putExtra("tripId", tripId)
        })

        when (tripId >= 0) {
            true -> {
                requireActivity().finish()
                findNavController().navigate(
                    TripInProgressFragmentDirections.actionFinishTrip(
                        tripId
                    )
                )
            }

            else -> findNavController()
                .navigate(R.id.action_back_to_summaries)
        }
    }

    private fun setTimeStateButtonState(newState: TimeState) {
        val tripId = newState.tripId
        Log.d(logTag, "Observed currentTimeState change: ${newState.state}")
        pauseButton.setOnClickListener(pauseTripListener(tripId))
        resumeButton.setOnClickListener(resumeTripListener(tripId))
        stopButton.setOnClickListener(stopTripListener(tripId))
        autoPauseChip.visibility = GONE
        when (newState.state) {
            TimeStateEnum.START, TimeStateEnum.RESUME -> {
                view?.doOnPreDraw { hideResumeStop() }
                view?.doOnPreDraw { hidePause() }
                pauseButton.text = getString(R.string.pause_label)
                val colorInt: Int = currentContext.getColor(R.color.button_red)
                pauseButton.backgroundTintList = ColorStateList.valueOf(colorInt)
            }

            TimeStateEnum.PAUSE -> {
                view?.doOnPreDraw { hidePause() }
                autoPauseChip.visibility = when (newState.auto) {
                    true -> VISIBLE
                    else -> GONE
                }
                slideInResumeStop()
            }

            else -> {
                pauseButton.setOnClickListener(startTripListener)
                pauseButton.text = getString(R.string.start_label)
                val colorInt: Int = currentContext.getColor(R.color.button_green)
                pauseButton.backgroundTintList = ColorStateList.valueOf(colorInt)
                slidePauseUp()
            }
        }
    }

    private fun handleTimeStateChanges(tripId: Long) =
        viewModel.currentTimeState(tripId).observe(viewLifecycleOwner) { currentState ->
            currentState?.let {
                setTimeStateButtonState(it)
            }
        }

    private fun initializeAfterTripCreated(tripId: Long) {
        handleTimeStateChanges(tripId)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onStartTripEvent(event: StartTripEvent) {
        event.tripId.takeIf { it >= 0 }?.let { tripId ->
            initializeAfterTripCreated(tripId)
            viewModel.startTrip(tripId, viewLifecycleOwner)
        }
    }

    private val startTripListener: OnClickListener = OnClickListener {
        if (!gpsEnabled) {
            turnOnGps()
        } else {
            startTrip()
            hidePause()
            pauseButton.text = getString(R.string.pause_label)
            val colorInt: Int = currentContext.getColor(R.color.button_red)
            pauseButton.backgroundTintList = ColorStateList.valueOf(colorInt)
        }
    }

    private fun clearOldDisplay(key: String) {
        if (key.contains(PREFS_DISPLAY_SETTINGS_SPEED)) {
            setSpeed("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_DURATION)) {
            setDuration("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_DISTANCE)) {
            setDistance("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_AVG_SPEED)) {
            setAvgSpeed("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_SPLITS)) {
            setSplit("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_SLOPE)) {
            setSlope("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_GRADE)) {
            setGrade("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_HEART_RATE)) {
            setHeartRate("", "", "",0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_CADENCE)) {
            setCadence("", "", "",0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_POWER_METER)) {
            setPowerMeter("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_TEMPERATURE)) {
            setTemperature("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_RH)) {
            setRelativeHumidity("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_AP)) {
            setAirPressure("", "", "", 0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_WIND)) {
            setWind("", "", "",0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_GPS_BEARING)) {
            setGpsBearing("", "", "",0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_MAG_BEARING)) {
            setMagBearing("", "", "",0)
        }

        if (key.contains(PREFS_DISPLAY_SETTINGS_ELEVATION)) {
            setElevation("", "", "", 0)
        }
    }

    private fun findOldAndDisable(viewId: Int, key: String) {
        clearOldDisplay(key)

        val speed = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_TOP)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_SPEED) && viewId == speed) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val duration = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_TOP_LEFT)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_DURATION) && viewId == duration) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val distance = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_TOP_RIGHT)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_DISTANCE) && viewId == distance) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val avg_speed = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_MID_UP_LEFT)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_AVG_SPEED) && viewId == avg_speed) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val splits = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_SPLITS) && viewId == splits) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val slope = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_MID_LO_LEFT)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_SLOPE) && viewId == slope) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val grade = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_GRADE) && viewId == slope) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val heart_rate = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_HEART_RATE) && viewId == heart_rate) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val cadence = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_CADENCE) && viewId == cadence) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val power_meter = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_POWER_METER) && viewId == power_meter) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val temperature = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_TEMPERATURE) && viewId == temperature) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val rh = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_RH) && viewId == rh) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val ap = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_AP) && viewId == ap) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val wind = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_WIND) && viewId == wind) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val gps_bearing = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_GPS_BEARING) && viewId == gps_bearing) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val mag_bearing = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_MAG_BEARING) && viewId == mag_bearing) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val elevation = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED)
        if (!key.contains(PREFS_DISPLAY_SETTINGS_ELEVATION) && viewId == elevation) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED).commit()
        }
    }

    private fun handleDisableView(viewId: Int) {
        val speed = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_TOP)
        if (viewId == speed) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val duration = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_TOP_LEFT)
        if (viewId == duration) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val distance = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_TOP_RIGHT)
        if (viewId == distance) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val avg_distance = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_MID_UP_LEFT)
        if (viewId == avg_distance) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val splits = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT)
        if (viewId == splits) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val slope = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_MID_LO_LEFT)
        if (viewId == slope) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val grade = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED)
        if (viewId == grade) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val heart_rate = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED)
        if (viewId == heart_rate) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val cadence = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED)
        if (viewId == cadence) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val power_meter = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED)
        if (viewId == power_meter) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val temperature = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED)
        if (viewId == temperature) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val rh = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED)
        if (viewId == rh) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val ap = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED)
        if (viewId == ap) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val wind = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED)
        if (viewId == wind) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val gps_bearing = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (viewId == gps_bearing) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val mag_bearing = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (viewId == mag_bearing) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED).commit()
        }

        val elevation = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED)
        if (viewId == elevation) {
            sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED).commit()
        }
    }

    private fun handleViewChange(viewId: Int) {
        displayItemselctionMenu = IconContextMenu(currentContext, R.menu.menu_display_item_selection, THEME_DARK, SANS)
        displayItemselctionMenu.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
            when(item.itemId) {
                R.id.action_disable_view_selection -> {
                    handleDisableView(viewId)
                    //sharedPreferences.edit().putInt(prefkey_prefix+"_"+viewId, DISPLAY_SETTINGS_NONE).commit()
                    //setValue(viewId, "")
                    //setLabel(viewId, "")
                }
                R.id.action_set_view_selection_speed -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_SPEED)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPEED, viewId).commit()
                    setValue(viewId, view_selection_value_speed)
                    //var speed_sensor = "GPS"
                    //setLabel(viewId, "$speed_sensor ${getUserSpeedUnitShort(requireContext()).uppercase(Locale.getDefault())}")
                    setLabel(viewId, "${getUserSpeedUnitShort(currentContext).uppercase(Locale.getDefault())}")
                }
                R.id.action_set_view_selection_distance -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_DISTANCE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DISTANCE, viewId).commit()
                    setValue(viewId, view_selection_value_distance)
                    setLabel(viewId, "${getUserDistanceUnitShort(currentContext).uppercase(Locale.getDefault())}")
                }
                R.id.action_set_view_selection_avg_speed -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_AVG_SPEED)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, viewId).commit()
                    setValue(viewId, view_selection_value_avg_speed)
                    setLabel(viewId, "AVG ${getUserSpeedUnitShort(currentContext).uppercase(Locale.getDefault())}")
                }
                R.id.action_set_view_selection_slope -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_SLOPE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SLOPE, viewId).commit()
                    setValue(viewId, view_selection_value_slope)
                    setLabel(viewId, "SLOPE")
                }
                R.id.action_set_view_selection_grade -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_GRADE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GRADE, viewId).commit()
                    setValue(viewId, view_selection_value_grade)
                    setLabel(viewId, "GRADE")
                }
                R.id.action_set_view_selection_grade -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_GRADE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GRADE, viewId).commit()
                    setValue(viewId, view_selection_value_grade)
                    setLabel(viewId, "GRADE")
                }
                R.id.action_set_view_selection_splits -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_SPLITS)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_SPLITS, viewId).commit()
                    setValue(viewId, view_selection_value_splits)
                    setLabel(viewId, "SPLIT ${getUserSpeedUnitShort(requireContext()).uppercase(Locale.getDefault())}")
                }
                R.id.action_set_view_selection_duration -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_DURATION)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_DURATION, viewId).commit()
                    setValue(viewId, view_selection_value_duration)
                    setLabel(viewId, "DURATION")
                }
                R.id.action_set_view_selection_heart_rate -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_HEART_RATE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, viewId).commit()
                    setValue(viewId, view_selection_value_heart_rate)
                    setLabel(viewId, "BPM")
                }
                R.id.action_set_view_selection_cadence -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_CADENCE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_CADENCE, viewId).commit()
                    setValue(viewId, view_selection_value_cadence)
                    setLabel(viewId, "RPM")
                }
                R.id.action_set_view_selection_power_meter -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_POWER_METER)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_POWER_METER, viewId).commit()
                    setValue(viewId, view_selection_value_power_meter)
                    setLabel(viewId, "POWER")
                }
                R.id.action_set_view_selection_temperature -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_TEMPERATURE)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, viewId).commit()
                    setValue(viewId, view_selection_value_temperature)
                    setLabel(viewId, "TEMP")
                }
                R.id.action_set_view_selection_relative_humidity -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_RH)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_RH, viewId).commit()
                    setValue(viewId, view_selection_value_relative_humidity)
                    setLabel(viewId, "REL. HUM.")
                }
                R.id.action_set_view_selection_air_pressure -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_AP)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_AP, viewId).commit()
                    setValue(viewId, view_selection_value_air_pressure)
                    setLabel(viewId, "AIR P.")
                }
                R.id.action_set_view_selection_wind -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_WIND)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_WIND, viewId).commit()
                    setValue(viewId, view_selection_value_wind)
                    setLabel(viewId, "WIND")
                }
                R.id.action_set_view_selection_gps_bearing -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_GPS_BEARING)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, viewId).commit()
                    setValue(viewId, view_selection_value_gps_bearing)
                    setLabel(viewId, "G|BEARING")
                }
                R.id.action_set_view_selection_magnetic_bearing -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_MAG_BEARING)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, viewId).commit()
                    setValue(viewId, view_selection_value_magnetic_bearing)
                    setLabel(viewId, "M|BEARING")
                }
                R.id.action_set_view_selection_elevation -> {
                    findOldAndDisable(viewId, PREFS_DISPLAY_SETTINGS_ELEVATION)
                    sharedPreferences.edit().putInt(PREFS_DISPLAY_SETTINGS_ELEVATION, viewId).commit()
                    setValue(viewId, view_selection_value_elevation)
                    setLabel(viewId, getUserAltitudeUnitShort(currentContext))
                }
            }

            displayItemselctionMenu.dismiss()
        })
        displayItemselctionMenu.setOnCancelListener(DialogInterface.OnCancelListener {
            displayItemselctionMenu.dismiss()
        })
        displayItemselctionMenu.show()
    }

    private fun handleTopView() {
        var viewId = 1
        handleViewChange(viewId)
    }

    private fun handleTopLeftView() {
        var viewId = 2
        handleViewChange(viewId)
    }

    private fun handleTopRightView() {
        var viewId = 3
        handleViewChange(viewId)
    }

    private fun handleMiddleUpperLeftView() {
        var viewId = 4
        handleViewChange(viewId)
    }

    private fun handleMiddleUpperRightView() {
        var viewId = 5
        handleViewChange(viewId)
    }

    private fun handleMiddleLowerLeftView() {
        var viewId = 6
        handleViewChange(viewId)
    }

    private fun handleMiddleLowerRightView() {
        var viewId = 7
        handleViewChange(viewId)
    }

    private fun handleBottomLeftView() {
        var viewId = 8
        handleViewChange(viewId)
    }

    private fun handleBottomRightView() {
        var viewId = 9
        handleViewChange(viewId)
    }

    private val selectionClickListener: View.OnClickListener = View.OnClickListener {
        Log.i(logTag, "selectionClickListener called")
        handleScreenTouchClick()
        true
    }

    private val selectionDisplayLongClickListener: View.OnLongClickListener = View.OnLongClickListener {
        Log.i(logTag, "selectionDisplayListener called")
        var viewId = it.id
        when (viewId) {
            R.id.measurement_top -> {
                Log.i(logTag, "set tippy top view")
                handleTopView()
                true
            }

            R.id.measurement_topLeft -> {
                Log.i(logTag, "set top left view")
                handleTopLeftView()
                true
            }

            R.id.measurement_topRight -> {
                Log.i(logTag, "set top right view")
                handleTopRightView()
                true
            }

            R.id.measurement_middleLeftUpper -> {
                Log.i(logTag, "set middle upper left view")
                handleMiddleUpperLeftView()
                true
            }

            R.id.measurement_middleRightUpper -> {
                Log.i(logTag, "set middle upper right view")
                handleMiddleUpperRightView()
                true
            }

            R.id.measurement_middleLeftLower -> {
                Log.i(logTag, "set middle lower left view")
                handleMiddleLowerLeftView()
                true
            }

            R.id.measurement_middleRightLower -> {
                Log.i(logTag, "set middle lower right view")
                handleMiddleLowerRightView()
                true
            }

            R.id.measurement_bottomLeft -> {
                Log.i(logTag, "set bottom left view")
                handleBottomLeftView()
                true
            }

            R.id.measurement_bottomRight -> {
                Log.i(logTag, "set bottom right view")
                handleBottomRightView()
                true
            }
            else -> {
                Log.i(logTag, "unknown id")
            }
        }

        false
    }

    private fun pauseTripListener(tripId: Long): OnClickListener = OnClickListener {
        pauseTrip(tripId)
        hidePause()
        slideInResumeStop()
    }

    private fun resumeTripListener(tripId: Long): OnClickListener = OnClickListener {
        resumeTrip(tripId)
        slideOutResumeStop()
    }

    private fun stopTripListener(tripId: Long): OnClickListener = OnClickListener {
        endTrip(tripId)
    }

    override fun onDestroy() {
        Log.d(logTag, "Destroying TIP View")
        super.onDestroy()
        activity?.window?.apply {
            val params = attributes
            params.screenBrightness = -1f
            attributes = params
        }
    }

    private fun hasHeartRate() = (viewModel.hrmSensor.value?.bpm ?: 0) > 0

    private fun scaleMeasurmentTexts() {
        Handler().postDelayed(Runnable {
            while (!::topView.isInitialized) { }
            topView.scaleTextSize(scaleText - 0.06f)
            topLeftView.scaleTextSize(scaleText)
            topRightView.scaleTextSize(scaleText)
            middleUpperLeftView.scaleTextSize(scaleText)
            middleUpperRightView.scaleTextSize(scaleText)
            middleLowerLeftView.scaleTextSize(scaleText)
            middleLowerRightView.scaleTextSize(scaleText)
            bottomLeftView.scaleTextSize(scaleText)
            bottomRightView.scaleTextSize(scaleText)
            footerLeftView.textScaleX = scaleText
            footerRightView.textScaleX = scaleText

            batteryLevelText.textScaleX = scaleText
            timeOfDayTextView.textScaleX = scaleText

            chargingImage.scaleX = scaleText
            chargingImage.scaleY = scaleText

            trackingImage.scaleX = scaleText
            trackingImage.scaleY = scaleText

            settingsButton.scaleX = scaleText
            settingsButton.scaleY = scaleText

            compassImage.scaleX = scaleText
            compassImage.scaleY = scaleText
        }, 1000)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val safeZone = getSafeZoneMargins(requireContext())
        (view.layoutParams as ViewGroup.MarginLayoutParams).apply {
            topMargin = safeZone.top
            bottomMargin = safeZone.bottom
            leftMargin = safeZone.left
            rightMargin = safeZone.right
            var x = leftMargin + rightMargin
            var y = topMargin + bottomMargin
            if (x > 0 || y > 0) {
                val displayMetrics = currentContext.resources.displayMetrics
                val widthPx = displayMetrics.widthPixels - x
                val heightPx = displayMetrics.heightPixels - y
                val density = displayMetrics.density
                val dpi = displayMetrics.densityDpi

                scaleText = heightPx.toFloat() / displayMetrics.heightPixels.toFloat()
            }
        }

        Log.d(logTag, "TripInProgressFragment::onViewCreated")

        savedInstanceState?.getLong("tripId", -1)
            .takeIf { t -> t != -1L }.let { tripId ->
                viewModel.tripId = tripId
            }

        initializeViews(view)
        initializeClockTick()
        view.setOnTouchListener(this)

        defaultBike = viewModel.defaultBike!!

        var key = requireContext().applicationContext.getString(R.string.preference_key_useAutoCircumference)
        useAutoCircumference = sharedPreferences.getBoolean(key, true)

        key = requireContext().applicationContext.getString(R.string.preferences_key_enable_cadence_power)
        useCadenceWattsSensor = sharedPreferences.getBoolean(key, true)

        key = requireContext().applicationContext.getString(R.string.preferences_key_enable_wind_power)
        useWindSpeed = sharedPreferences.getBoolean(key, true)

        key = requireContext().applicationContext.getString(R.string.preferences_key_enable_use_power_sensor)
        usePowerSensor = sharedPreferences.getBoolean(key, true)

        try {
            if (useAutoCircumference) {
                circumference = autoCircumference!!
            } else {
                if (defaultBike != null && defaultBike?.wheelCircumference != null) {
                    circumference = defaultBike!!.wheelCircumference!!
                } else {
                    circumference = 0.0f
                }
            }
        } catch (e: Exception) {
            circumference = 0.0f
        }

        try {
            if (defaultBike != null && defaultBike?.weight != null) {
                bikeMass = defaultBike!!.weight!!
                if (useKiloMass) {
                    bikeMass /= 2.21f
                }
            } else {
                bikeMass = 0.0f
            }
        } catch (e: Exception) {
            bikeMass = 0.0f
        }

        try {
            userMass = getUserWeight(currentContext)!!
            if (useKiloMass) {
                userMass /= 2.21f;
            }
        } catch (e: Exception) {
            userMass = 0.0f
        }

        try {
            Log.i(logTag, "Bike: " + defaultBike!!.name +
                    ",  default: " + defaultBike!!.isDefault +
                    ",  weight: " + bikeMass +
                    ",  circfumference: " + defaultBike!!.wheelCircumference +
                    ",  date of purchase: " + defaultBike!!.dateOfPurchase)
        } catch (e: Exception) {
        }

        initializeLocationServiceStateChangeHandler()
        initializeMeasurementUpdateObservers()
        initializeWeatherObservers()

        if (useMapViewForTripProgress) {
            initMapView()
        }

        if (Build.VERSION.SDK_INT >= 30) {
            requireActivity().window.insetsController?.apply {
                hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
            }
        }
    }

    private fun initializeClockTick() {
        updateClock()
        requireContext().registerReceiver(timeTickReceiver, IntentFilter(Intent.ACTION_TIME_TICK))
        isTimeTickRegistered = true
    }

    private fun initializeLocationServiceStateChangeHandler() {
        viewModel.gpsEnabled.observe(viewLifecycleOwner) { status ->
            if (!status && gpsEnabled) {
                /* HANDLE LOCATION SERVICES DISABLED */
                //trackingImage.visibility = INVISIBLE
                //debugTextView.text = getString(R.string.gps_disabled_message)
                Log.d(logTag, "Location service access disabled")
                activity?.let {
                    val builder = AlertDialog.Builder(it)
                    builder.apply {
                        setPositiveButton(
                            "ENABLE"
                        ) { _, _ ->
                            Log.d("TIP_GPS_DISABLED", "CLICKED ENABLE")
                            turnOnGps()
                        }
                        setNegativeButton(
                            "CANCEL"
                        ) { _, _ ->
                            Log.d("TIP_GPS_DISABLED", "CLICKED CANCEL")
                            endTrip(viewModel.tripId ?: -1)
                        }
                        setTitle("Enable location service")
                        setMessage("Location service has been disabled. Please enable location services before starting your ride.")
                    }

                    builder.create()
                }?.show()
            }
            gpsEnabled = status
        }
    }

    private fun windBehind(userBearing: Double, windBearing: Double): Double {
        var multiplier = 1.0
        var adjustedWindDirection = windBearing - userBearing
        adjustedWindDirection = (adjustedWindDirection + 360) % 360
        var mainQuadrant: Int = Math.round((adjustedWindDirection - 45) / 90).toInt()
        var secondaryQuadrant: Int = Math.round((adjustedWindDirection - 45) / 45).toInt()
        if (mainQuadrant == 1 && secondaryQuadrant == 2) {
            multiplier = -1.0
        }
        if (mainQuadrant == 3 && secondaryQuadrant == 5) {
            multiplier = -1.0
        }
        return offset[mainQuadrant] * multiplier
    }

    private fun distanceInMeter(startLat: Double, startLon: Double,
                                endLat: Double, endLon: Double): Float {
        var results = FloatArray(1)
        Location.distanceBetween(startLat, startLon, endLat, endLon, results)
        return results[0]
    }

    private fun getDistanceInMeters(startLat: Double, startLang: Double,
                            endLat: Double, endLang: Double): Float {
        val locStart = Location("")
        locStart.latitude = startLat
        locStart.longitude = startLang
        val locEnd = Location("")
        locEnd.latitude = endLat
        locEnd.longitude = endLang
        return locStart.distanceTo(locEnd)
    }

    private fun handlePowerMeterEstimation(location: Location, lastLatLng: LatLng) {
        if (!usePowerSensor && ::lastLatLng.isInitialized && lastLatLng != null) {
            var deltaAlt = location.altitude - lastAltitude
            var deltaDist = getDistanceInMeters(location.latitude, location.longitude,
                lastLatLng!!.latitude, lastLatLng!!.longitude).toDouble()
            if (deltaDist < 0) {
                deltaDist *= -1.0
            }

            var gpsSlope = 0.0001
            if (deltaAlt != 0.0 && deltaDist != 0.0) {
                gpsSlope = deltaAlt / deltaDist
            }

            var spdValue = getGpsSpeed(location)
            var spdValueUnit = getUserSpeedUnitShort(currentContext).uppercase(Locale.getDefault())
            setSpeed(spdValue, "GPS $spdValueUnit", "",0)
            /*
            if (spdValue != null) {
                var bikeMass = getBikeMassOrNull(currentContext)
                var userMass = getUserWeight(currentContext)
                if (bikeMass != null && userMass != null) {
                } else if (userMass != null)  {
                        totalMass = userMass
                } else {
                    totalMass = 0.0f
                }
            }
             */
            var totalMass = 0.0
            //var bikeMass = getBikeMassOrNull(currentContext)
            if (bikeMass != null) {
                totalMass += bikeMass
            }
            if (userMass != null) {
                totalMass += userMass
            }
            totalMass = 78.0

            if (debugLog) Log.i(logTag, "MASS: bike " + bikeMass + " | user " + userMass + "| slope " + slope)
            //if (slope == 0.0f) {
            //    wattsCalc = spdValue.toFloat() * 2.5f / 10.0f
            //} else {
            //    wattsCalc = Math.abs(slope)*(spdValue.toFloat()/3.6f)*totalMass*9.8f
            //}
            //wattsCalc = spdValue.toFloat() * 2.5f / 10.0f

            //https://calculator.academy/rpm-to-watts-calculator/
            // RPM to Watts Formula : P(W) = τ ∗ RPM ∗ (2 ∗ π) / 60
            // P is the power in watts (W)
            // τ is the torque in Newton-meters (Nm)
            // RPM is the rotational speed in revolutions per minute

            var minSlope = 0.001f

            // https://www.omnicalculator.com/sports/cycling-wattage

            // Rolling resistance coefficient - Crr
            // Surface type    Slick tires   Knobby tires
            // =============== ============= =============
            // Concrete        0.0020        0.0025
            // Asphalt         0.0050        0.0063
            // Gravel          0.0060        0.0076
            // Grass           0.0070        0.0089
            // Off-road        0.0200        0.0253
            // Sand            0.0300        0.0380
            // Surface type    Slick tires   Knobby tires
            // avg = 0.013216

            // Position    Cd x A
            // =========== ======
            // Tops        0.408
            // Hoods       0.324
            // Drops       0.307
            // Aerobars    0.2914
            // avg = 0.3326

            // Power Loss
            // Cycling power formula assumes a constant 1.5% loss on your pulleys.
            // The losses on the chain are dependent on its condition:
            //  * 3% for a new, well-oiled chain
            //  * 4% for a dry chain (for example, when the oil has been washed away by rain)
            //  * 5% for a dry chain that is so old it became elongated

            // M = user mass in kg
            // m = bike mass in kg
            // g = gravity acceleration at 9.80665 m/s2
            // Crr = Rolling resistance coefficient, avg is 0.013216
            // h = elevation above sea level in meters
            // v = speed (meters per second)
            // w = wind speed (positive headwind, negative tail wind)
            // Cd x A = Drag Coefficient x Frontal Area
            // loss = Power loss

            // Gravity Resistance
            // Fg = g × sin(arctan(slope)) × (M + m)

            // Rolling Resistance
            // Fr = g × cos(arctan(slope)) × (M + m) × Crr

            // Aerodynamic Drag
            // ρ = 1.225 × exp(−0.00011856 × h)
            // Fa = 0.5 × Cd × A × ρ × (v + w)²

            // Power formula
            //     (Fg + Fr + Fa) × v
            // P = ------------------
            //          1 − loss

            //var Crr = 0.013216;
            // Crr and CdxA averages for road resistance
            // and bar hold position
            var Crr = 0.0057
            var CdxA = 0.3326

            //Crr = 0.0063;
            //CdxA = 0.408

            //var localSlope = Math.abs(slope + minSlope).toDouble()
            //var localSlope = /* grade.toDouble() + */ minSlope.toDouble()
            //var localSlope = minSlope.toDouble()
            //var localSlope = gpsSlope
            var localSlope = slope.toDouble()
            //var localSlope = slope.toDouble() / 100.0
            //var displaySlope = localSlope * 100.0

            var slopegrade = String.format("%.4f", localSlope)
            setSlope("${slopegrade}", "SLOPE", "",0)

            //var graded = String.format("%.1f", grade * 100.0)
            //setGrade("$graded%", "GRADE", "",0)

            var windSpeed = 0.0
            if (useWindSpeed){
                if (windSpeedData > 0) {
                    var windDir = windBehind(location.bearing.toDouble(), windDirectionData.toDouble());
                    windSpeed = windSpeedData * meterPerSecond * windDir
                }
            }
            var airDensity = 1.225 * Math.exp(-0.00011856 * (altitude + 0.5))
            var userSpeed = spdValue.toDouble() * meterPerSecond
            var Fg = gravityAccel * Math.sin(Math.atan(localSlope)) * totalMass
            var Fr = gravityAccel * Math.cos(Math.atan(localSlope)) * totalMass * Crr
            var Fa = 0.5 * CdxA * airDensity * Math.pow((userSpeed + windSpeed), 2.0)
            var powerEfficiency = 1 - gearLoss - chainLossDryOld

            var wattsCalc = Math.abs((((Fg + Fr + Fa) * userSpeed) / powerEfficiency)).toFloat()

            if (false) {
                Log.i(logTag, "Mass     : ${totalMass}")
                Log.i(logTag, "Slope    : ${localSlope}")
                Log.i(logTag, "Speed    : ${userSpeed / 0.2778}")
                Log.i(logTag, "userSpeed: ${userSpeed}")
                Log.i(logTag, "Fg       : ${Fg}")
                Log.i(logTag, "Fr       : ${Fr}")
                Log.i(logTag, "Fa       : ${Fa}")
                Log.i(logTag, "Pwr-Ef   : ${powerEfficiency}")
                Log.i(logTag, "Watts    : ${wattsCalc}")
            }

            //wattsCalc = Math.sin(Math.atan(Math.abs(slope + minSlope).toDouble())).toFloat() * (spdValue.toFloat() * 0.2778f) * totalMass * 9.8067f
            //wattsCalc *= powerEfficiency
            var totalWatts = 0
            var sensorType = PowerMeterSensorType.ESTIMATION
            if (useCadenceWatts && useCadenceWattsSensor) {
                sensorType = PowerMeterSensorType.CADENCE_ESTIMATION
                var torque = wattsCalc / userSpeed
                var totalWatts = (torque * ((2.0 * Math.PI * cadenceSensorValue) / 60)).toInt()
                var strWatts = String.format("%d", totalWatts.toInt())
                setPowerMeter("${strWatts} W", "POWER (CE)", "", 0)
            } else {
                sensorType = PowerMeterSensorType.ESTIMATION
                totalWatts = wattsCalc.toInt()
                var strWatts = String.format("%d", totalWatts.toInt())
                setPowerMeter("${strWatts} W", "POWER (E)", "", 0)
            }

            /*
            if (cadenceSensorValue > 0) {
                var torque = 0.0f
                wattsCalc = torque * ((2.0f * Math.PI.toFloat()) / 60.0f)
                var strWatts = String.format("%d", wattsCalc.toInt())
                setPowerMeter("${strWatts} W", "POWER (R)", "", 0)
            } else {
                // watt = sin(atan(slope)) * speed in m/s * mass in kilo * 9.8067 m/s2 free fall gravity acceleration
                wattsCalc = Math.sin(Math.atan(Math.abs(slope).toDouble())).toFloat() * (spdValue.toFloat() * 0.2778f) * totalMass * 9.8067f
                var strWatts = String.format("%d", wattsCalc.toInt())
                setPowerMeter("${strWatts} W", "POWER (E)", "", 0)
            }
             */
            // watt = sin(atan(slope)) * speed in m/s * mass in kilo * 9.8067 m/s2 free fall gravity acceleration
            //POWER = TORQUE*RPM

            if (false) {
                var tripId = 0L
                if (viewModel != null && viewModel.tripId != null) {
                    var time = System.currentTimeMillis()
                    var evenTime = (Math.floor((time / 1024).toDouble()) % 0xFFFF).toInt()
                    var rpm = cadenceSensorValue.toInt()

                    tripId = viewModel.tripId!!

                    val powerMeterMeasurement = PowerMeterMeasurement(
                        tripId = tripId,
                        watts = totalWatts,
                        rpm = rpm,
                        sensorType = sensorType,
                        timestamp = time
                    )
                }
            }
        }
    }

    private fun initializeMeasurementUpdateObservers() {
        viewModel.currentProgress.observe(viewLifecycleOwner) {
            if (debugLog) Log.d(logTag, "Location observer detected change")

            if (!triggerMapUpdate) {
                triggerMapUpdate = true
                vtmMapView.map().updateMap(true)
            }

            var distValue = String.format("%.2f", getUserDistance(requireContext(), it.distance))
            var distValueUnit = getUserDistanceUnitShort(currentContext)
            setDistance("$distValue ${distValueUnit}", "DISTANCE", "", 0)

            var avgspeed = getUserSpeed(requireContext(), it.distance / it.duration).let { averageSpeed ->
                String.format("%.1f", if (averageSpeed.isFinite()) averageSpeed else 0f)
            }
            avg_speed = if (avgspeed != null) avgspeed.toFloat() else 0.0f
            var avgSpeedUnit = getUserSpeedUnitShort(currentContext)
            setAvgSpeed("$avg_speed ${avgSpeedUnit}", "AVG SPEED", "", 0)

            //topLeftView.value = String.format("%.3f", if (it.slope.isFinite()) it.slope else 0f)
            //var slope = String.format("%.3f", if (it.slope.isFinite()) it.slope else 0f)
            //if (useSlope) {
            //} else {
            //}

            if (it.slope.isFinite()) {
                slope = it.slope.toFloat()
            } else {
                slope = 0.0001f
            }

            //var slopegrade = String.format("%.3f", slope)
            //setSlope("$slopegrade", "SLOPE", "",0)

            if (it.grade.isFinite()) {
                grade = it.grade.toFloat()
            } else {
                grade = 0.0001f
            }

            //var graded = String.format("%.1f", grade * 100.0)
            //setGrade("$graded%", "GRADE", "",0)

            //var gradeFromSlope = ((Math.abs(slope) * 1000f).toInt()).toFloat() / 10f
            if (false) {
                Log.i(logTag, "SLOPE : $slope")
                Log.i(logTag, "GRADE : $grade%")
            }
            //trackingImage.visibility = if (it.tracking) VISIBLE else INVISIBLE
        }

        viewModel.location.observe(viewLifecycleOwner) { location ->
            try {
                if (useAutoCircumference) {
                    circumference = autoCircumference!!
                }
            } catch (e: Exception) {
            }

            if (!usePowerSensor && ::lastLatLng.isInitialized && lastLatLng != null) {
                handlePowerMeterEstimation(location, lastLatLng)
            }

            var bearingValue = location.bearing
            bearing = degreesToCardinal(bearingValue)
            setGpsBearing(bearing.uppercase(Locale.getDefault()), "G|BEARING", "${bearingValue.toInt()}° ", 0)

            if (!::temperature.isInitialized) {
                temperature = "0°" + getUserTemperatureUnit(currentContext)
            }
            if (!::relativeHumidity.isInitialized) {
                relativeHumidity = "0%"
            }
            if (!::windDirection.isInitialized) {
                windDirection = "N"
            }
            if (!::windSpeed.isInitialized) {
                windSpeed = "0 " + getUserSpeedUnitShort(currentContext)
            }

            var altVal = getUserAltitude(currentContext, location.altitude)
            var altUnit = getUserAltitudeUnitShort(requireContext())
            setElevation("${altVal} ${altUnit}","ALTITUDE", "", 0)

            footerRightView.text = "$temperature | $relativeHumidity | ${sensorAirPress.toInt()} hPa"
            footerLeftView.text = "$bearing | $windDirection $windSpeed"

            lastAltitude = location.altitude
            lastLatLng = LatLng(location.latitude, location.longitude)
        }

        viewModel.currentTime.observe(viewLifecycleOwner) {
            var duration = DateUtils.formatElapsedTime((it).toLong())
            setDuration(duration, "DURATION", "", 0)
        }

        viewModel.lastCompleteSplit.observe(viewLifecycleOwner) { split ->
            if (debugLog) Log.d(logTag, "Observed last split change")
            var distance = String.format("%.1f", getUserSpeed(requireContext(),
                        (split.distance / split.duration.coerceAtLeast(0.0001))))
            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getFastestDistance(
                    getUserDistance(requireContext(), split.totalDistance).roundToInt(),
                    getUserDistance(requireContext(), 1.0),
                    3
                ).let {
                    val splits = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT)
                    // FIXME SETVISIBILITY
                    if (it.size == 3 && it.firstOrNull()?.tripId == split.tripId) {
                        setIcon(splits, trophy)
                        setIconVisible(splits, true)
                    } else {
                        setIconVisible(splits, false)
                    }
                }
                viewModel.getFastestSplit(
                    getUserDistance(requireContext(), split.totalDistance).roundToInt(),
                    getUserDistance(requireContext(), 1.0),
                    3
                ).let {
                    val splits = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT)
                    // FIXME SETVISIBILITY
                    if (it.size == 3 && it.firstOrNull()?.tripId == split.tripId) {
                        setIcon(splits, trophy)
                        setIconVisible(splits, true)
                    } else {
                        setIconVisible(splits, false)
                    }
                }
            }
        }

        viewModel.hrmSensor.observe(viewLifecycleOwner) { hrm ->
            if (debugLog) Log.d(logTag, "hrm battery: ${hrm.batteryLevel}")
            if (debugLog) Log.d(logTag, "hrm bpm: ${hrm.bpm}")
            var label = ""
            var value = ""
            var extraInfo = ""
            if (hrm.bpm != null) {
                label = "BPM"
                value = hrm.bpm.toString()
            }
            var lowbat = false
            if (hrm.batteryLevel != null && hrm.batteryLevel < lowBatteryThreshold) {
                extraInfo = "${hrm.batteryLevel}%"
                lowbat = true
            }
            setHeartRate(value, label, extraInfo, 0)
        }

        viewModel.powerSensor.observe(viewLifecycleOwner) { power ->
            if (debugLog) Log.d(logTag, "power battery: ${power.batteryLevel}")
            if (debugLog) Log.d(logTag, "watts: ${power.powerWatts}")
            if (debugLog) Log.d(logTag, "rpm: ${power.rpm}")
            var label = ""
            var value = ""
            var extraInfo = ""
            if (power.batteryLevel != null && power.batteryLevel < lowBatteryThreshold) {
                extraInfo = "${power.batteryLevel}%"
            }
            if (power.rpm != null) {
                useCadenceWatts = false
                cadenceSensorValue = power.rpm.toFloat()
                label = "RPM"
                value = power.rpm.toInt().toString()
            }
            setCadence(value, label, extraInfo, 0)
            var watts: Int = -0
            try {
                watts = power.powerWatts!!.toInt()
            } catch (e: Exception) {

            }
            setPowerMeter("${watts} W", "POWER", "", 0)
        }

        viewModel.cadenceSensor.observe(viewLifecycleOwner) { cadence ->
            if (cadence != null && cadence.batteryLevel != null && cadence.rpm != null) {
                if (debugLog) Log.d(logTag, "cadence battery: ${cadence.batteryLevel}")
                if (debugLog) Log.d(logTag, "cadence: ${cadence.rpm}")
                var label = ""
                var value = ""
                var extraInfo = ""
                if (cadence.rpm != null) {
                    useCadenceWatts = true
                    cadenceSensorValue = cadence.rpm
                    label = "RPM"
                    value = cadence.rpm.toInt().toString()
                    //} else {
                    //    cadenceSensorValue = 0.0f
                }
                if (cadence.batteryLevel != null && cadence.batteryLevel < lowBatteryThreshold) {
                    extraInfo = "${cadence.batteryLevel}%"
                }
                setCadence(value, label, extraInfo, 0)
            }
        }

        viewModel.speedSensor.observe(viewLifecycleOwner) { speed ->
            if (speed != null && speed.batteryLevel != null && speed.speed != null) {
                if (debugLog) Log.d(logTag, "speed battery: ${speed.batteryLevel}")
                if (debugLog) Log.d(logTag, "speed: ${speed.speed}")

                var label = ""
                var value = ""
                var extraInfo = ""

                useSensor = true

                if (speed.batteryLevel != null && speed.batteryLevel < lowBatteryThreshold) {
                    extraInfo = "${speed.batteryLevel}%"
                }

                if (true) {
                    if (speed.speed != null && circumference != null) {
                        speedSensorValue = getUserSpeed(requireContext(),speed.speed!! / 60 * circumference!!)
                        //} else {
                        //    speedSensorValue = 0.0f
                    }
                } else {
                    if (speed.speed != null) {
                        speedSensorValue = speed.speed
                    }
                }
                label = getUserSpeedUnitShort(requireContext()).uppercase(Locale.getDefault())
                value = String.format("%.1f", speedSensorValue)
                setSpeed(value, label, extraInfo, 0)

                /*
                //var bikeMass = getBikeMassOrNull(currentContext)
                totalMass = 0.0f
                //var userMass = getUserWeight(currentContext)
                if (userMass != null)  {
                    totalMass += userMass
                }
                if (bikeMass != null)  {
                    totalMass += bikeMass
                }
                //var minSlope = 0.00001f
                //wattsCalc = Math.sin(Math.atan(Math.abs(slope + minSlope).toDouble())).toFloat() * (speedSensorValue.toFloat() * 0.2778f) * totalMass * 9.8067f
                //wattsCalc *= 0.97f
                //var strWatts = String.format("%d", wattsCalc.toInt())
                //setPowerMeter("${strWatts} W", "POWER (E)", "", 0)
                //wattsCalc = Math.abs(slope) * (speedSensorValue / 3.6f) * totalMass * 9.8067f
                //wattsCalc *= 0.97f
                //var strWatts = String.format("%d", wattsCalc.toInt())
                //setPowerMeter("${strWatts} W", "POWER (E)", "", 0)
                 */
            }
        }

        viewModel.temperatureSensor.observe(viewLifecycleOwner) { temperature ->
            if (temperature == null ||
                temperature.timestamp == null ||
                temperature.temperature == null) {
            } else {
                hasTemperatureVal = true
                var temp = -0.0
                //try {
                //    temp = roundOffDecimalOne(temperature.temperature!!)!!
                //} catch (e: Exception) {
                //}
                var tempI: Int = (temperature.temperature * 10f).toInt()
                temp = tempI.toDouble() / 10.0

                if (debugLog) Log.d(logTag, "temperature timestamp : ${temperature.timestamp}")
                if (debugLog) Log.d(logTag, "temperature           : ${temp}")

                var userTemp = getUserTemperature(requireContext(), temp)
                var userUnit = getUserTemperatureUnit(requireContext())
                var tempSensor = "$userTemp $userUnit"
                var icon = 0
                var batteryLevel = ""
                var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED)
                if (temperature.batteryLevel != null) {
                    Log.d(logTag, "temperature battery level : ${temperature.batteryLevel}")
                    if (temperature.batteryLevel > 99) {
                        icon = R.drawable.ic_baseline_battery_full_24
                    } else if (temperature.batteryLevel > 90) {
                        icon = R.drawable.ic_baseline_battery_6_bar_24
                    } else if (temperature.batteryLevel > 75) {
                        icon = R.drawable.ic_baseline_battery_5_bar_24
                    } else if (temperature.batteryLevel > 50) {
                        icon = R.drawable.ic_baseline_battery_4_bar_24
                    } else if (temperature.batteryLevel > 25) {
                        icon = R.drawable.ic_baseline_battery_3_bar_24
                    } else if (temperature.batteryLevel > 15) {
                        icon = R.drawable.ic_baseline_battery_2_bar_24
                    } else if (temperature.batteryLevel > 0) {
                        icon = R.drawable.ic_battery_alert_24
                    } else {
                        icon = R.drawable.ic_baseline_battery_unknown_24
                    }
                    setIconVisible(index, true)
                    batteryLevel = "${temperature.batteryLevel}%"
                }
                setTemperature( "$tempSensor", "TEMP", "$batteryLevel", icon)
                if (icon != 0) {
                    setIcon(index, icon)
                    setIconVisible(index, true)
                }
            }
        }

        viewModel.humiditySensor.observe(viewLifecycleOwner) { humidity ->
            if (humidity == null ||
                humidity.relativeHumidity == null ||
                humidity.timestamp == null) {
            } else {
                hasHumidtyVal = true
                if (debugLog) Log.d(logTag, "humidity timestamp : ${humidity.timestamp}")
                if (debugLog) Log.d(logTag, "humidity           : ${humidity.relativeHumidity}")
                var relhum = -0.0
                try {
                    relhum = humidity.relativeHumidity!!
                } catch (e: Exception) {
                }
                relativeHumidity = "${roundOffDecimalOne(relhum)}%"
                var icon = 0
                var batteryLevel = ""
                var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED)
                if (humidity.batteryLevel != null) {
                    Log.d(logTag, "humidity battery level : ${humidity.batteryLevel}")
                    if (humidity.batteryLevel > 99) {
                        icon = R.drawable.ic_baseline_battery_full_24
                    } else if (humidity.batteryLevel > 90) {
                        icon = R.drawable.ic_baseline_battery_6_bar_24
                    } else if (humidity.batteryLevel > 75) {
                        icon = R.drawable.ic_baseline_battery_5_bar_24
                    } else if (humidity.batteryLevel > 50) {
                        icon = R.drawable.ic_baseline_battery_4_bar_24
                    } else if (humidity.batteryLevel > 25) {
                        icon = R.drawable.ic_baseline_battery_3_bar_24
                    } else if (humidity.batteryLevel > 15) {
                        icon = R.drawable.ic_baseline_battery_2_bar_24
                    } else if (humidity.batteryLevel > 0) {
                        icon = R.drawable.ic_battery_alert_24
                    } else {
                        icon = R.drawable.ic_baseline_battery_unknown_24
                    }
                    batteryLevel = "${humidity.batteryLevel}%"
                }
                setRelativeHumidity( "$relativeHumidity", "REL HUM", "${batteryLevel}", icon)
                if (icon != 0) {
                    setIcon(index, icon)
                    setIconVisible(index, true)
                }
            }
        }
    }

    fun roundOffDecimalOne(number: Double): Double? {
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    fun roundOffDecimalTwo(number: Double): Double? {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    private var hasTemperatureVal = false
    private var hasHumidtyVal = false

    private var windDirectionData: Float = 0.0f
    private var windSpeedData: Int = 0

    private fun initializeWeatherObservers() {
        /*
        val windIcon = requireView().findViewById<ImageView>(R.id.image_wind_icon)
         */
        viewModel.latestWeather.observe(viewLifecycleOwner) { weather ->
            if (weather != null) {
                //temperatureTextView.visibility = VISIBLE
                footerRightView.visibility = VISIBLE
                //windDirectionArrow.visibility = VISIBLE
                //windIcon.visibility = VISIBLE

                //TODO: This will not update dynamically, maybe start a timer
                when (weather.timestamp < (System.currentTimeMillis() / 1000 - 60 * 15)) {
                    true -> {
                        //temperatureTextView.alpha = 0.3f
                        footerRightView.alpha = 0.3f
                        //windDirectionArrow.alpha = 0.3f
                        //windIcon.alpha = 0.3f
                    }

                    else -> {
                        //temperatureTextView.alpha = 1f
                        footerRightView.alpha = 1f
                        //windDirectionArrow.alpha = 1f
                        //windIcon.alpha = 1f
                    }
                }

                if (!hasTemperatureVal) {
                    /* Convert openweather temp from kelvin to celsius */
                    var tempI = ((weather.temperature - 273) * 10f).toInt()
                    var tempD = tempI.toDouble() / 10.0
                    var tempVal = getUserTemperature(requireContext(), tempD)
                    var tempUnit = getUserTemperatureUnit(requireContext())
                    temperature = "$tempVal $tempUnit"
                    var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED)
                    setIconVisible(index, false)
                    setTemperature( "$temperature", "TEMP", "", 0)
                }
                /*
                footerRightView.text =
                    "%.1f %s".format(
                        getUserSpeed(requireContext(), weather.windSpeed),
                        getUserSpeedUnitShort(requireContext())
                    )
                 */

                windSpeedData = getUserSpeed(requireContext(), weather.windSpeed).toInt()
                windSpeed =
                    "%d %s".format(
                        windSpeedData,
                        getUserSpeedUnitShort(requireContext())
                    )
                windDirectionData = weather.windDirection.toFloat()
                windDirection = degreesToCardinal(windDirectionData)
                setWind( "$windSpeed", "WIND", "$windDirection  ", 0)

                pressureValue = weather.pressure
                //if (hasAirPressure) {
                //    pressureValue = (sensorAirPress + airPressureCalibration).toInt()
                //}
                //airpressure = "$pressureValue hPa"

                if (!hasAirPressureVal) {
                    sensorAirPress = pressureValue.toFloat()
                    setAirPressure( "${pressureValue.toInt()} hPa", "AIR P.", "", 0)
                }

                if (!hasHumidtyVal) {
                    relativeHumidityValue = weather.humidity
                    relativeHumidity = "$relativeHumidityValue%"
                    var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED)
                    setIconVisible(index, false)
                    setRelativeHumidity( "$relativeHumidityValue%", "REL HUM", "", 0)
                }

                if (!::bearing.isInitialized || bearing.isNullOrEmpty()) {
                    bearing = "##"
                }

                footerRightView.text = "$temperature | $relativeHumidity | ${sensorAirPress.toInt()} hPa"
                footerLeftView.text = "$bearing | $windDirection $windSpeed"
            }
        }
    }

    private fun handleMapThemeOptions() {
        mapThemeMenu = IconContextMenu(currentContext, R.menu.menu_map_theme_settings, THEME_DARK, SANS)
        mapThemeMenu.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
            when(item.itemId) {
                R.id.action_map_theme_default -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "1").commit()
                    vtmMapView.map().setTheme(VtmThemes.DEFAULT)
                    invalidateMap()
                }
                R.id.action_map_theme_newtron -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "2").commit()
                    vtmMapView.map().setTheme(VtmThemes.NEWTRON)
                    invalidateMap()
                }
                R.id.action_map_theme_tronredner -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "3").commit()
                    vtmMapView.map().setTheme(VtmThemes.TRONRENDER)
                    invalidateMap()
                }
                R.id.action_map_theme_biker -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "4").commit()
                    vtmMapView.map().setTheme(VtmThemes.BIKER)
                    invalidateMap()
                }
                R.id.action_map_theme_mapzen -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "5").commit()
                    vtmMapView.map().setTheme(VtmThemes.MAPZEN)
                    invalidateMap()
                }
                R.id.action_map_theme_osmarender -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "6").commit()
                    vtmMapView.map().setTheme(VtmThemes.OSMARENDER)
                    invalidateMap()
                }
                R.id.action_map_theme_osmagray -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "7").commit()
                    vtmMapView.map().setTheme(VtmThemes.OSMAGRAY)
                    invalidateMap()
                }
                R.id.action_map_theme_motorider -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "8").commit()
                    vtmMapView.map().setTheme(VtmThemes.MOTORIDER)
                    invalidateMap()
                }
                R.id.action_map_theme_motorider_dark -> {
                    sharedPreferences.edit().putString(getString(R.string.preference_map_theme_key), "8").commit()
                    vtmMapView.map().setTheme(VtmThemes.MOTORIDER_DARK)
                    invalidateMap()
                }
            }

            mapThemeMenu.dismiss()
        })
        mapThemeMenu.setOnCancelListener(DialogInterface.OnCancelListener {
            mapThemeMenu.dismiss()
        })
        mapThemeMenu.show()
    }

    private var tcxGpxActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult<Intent, ActivityResult>(
            ActivityResultContracts.StartActivityForResult()
        ) { result: ActivityResult ->
            if (Activity.RESULT_OK == result.resultCode) {
                val intent = result.data
                if (intent != null) {
                    val path = intent.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                    if (!path.isNullOrEmpty()) {
                        sharedPreferences.edit().putString(getString(R.string.preferences_tcx_gpx_track_filename_key), path).commit()
                        val tcxGpxFile = File(path)
                        clearTcxGpx()
                        clearCenterPointFocus()
                        if (tcxGpxFile.name.lowercase().contains("tcx")) {
                            handleTcxFile(tcxGpxFile)
                        } else {
                            handleGpxFile(tcxGpxFile)
                        }
                        if (::currentLocation.isInitialized) {
                            initCenterPointFocus(currentLocation)
                        } else {
                            if (::lastLocation.isInitialized) {
                                var lastGeoPoint = GeoPoint(lastLocation.latitude, lastLocation.longitude)
                                initCenterPointFocus(lastGeoPoint)
                            }
                        }
                        Log.d("Track File : ", tcxGpxFile.name)
                        Toast.makeText(
                            currentContext,
                            "Picked Track file " + tcxGpxFile.name, Toast.LENGTH_LONG
                        ).show()
                    }
                }
            } else {
                Toast.makeText(currentContext, "Unable to load track file.", Toast.LENGTH_LONG).show()
            }
        }

    private var mapActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult<Intent, ActivityResult>(
            ActivityResultContracts.StartActivityForResult()
        ) { result: ActivityResult ->
            if (Activity.RESULT_OK == result.resultCode) {
                val intent = result.data
                if (intent != null) {
                    val path = intent.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                    if (!path.isNullOrEmpty()) {
                        sharedPreferences.edit().putString(getString(R.string.preferences_map_filename_key), path).commit()
                        val mapFile = File(path)
                        handleMapFile(mapFile)
                        Log.d("Map File : ", mapFile.name)
                        Toast.makeText(
                            currentContext,
                            "Selected map file " + mapFile.name, Toast.LENGTH_LONG
                        ).show()
                    }
                }
            } else {
                Toast.makeText(currentContext, "Unable to load map file ", Toast.LENGTH_LONG).show()
            }
        }

    private fun handleNewTcxGpxTrack() {
        requireActivity().runOnUiThread {
            var materialFilePicker = MaterialFilePicker()
                .withCallbackSupportFragment(this)
                .withCallbackActivityResults(tcxGpxActivityResult)
                .withHiddenFiles(false)
                .withRootPaths(getRootPaths(currentContext))
                .withPath("$storagePath/cyclometer/tracks")
                .withFilter(Pattern.compile(".*\\.(gpx|tcx)$"))
                .withTitle("Select New TCX/GPX Track File")

            materialFilePicker.start()
        }
    }

    private fun handleNewMapFile() {
        requireActivity().runOnUiThread {
            var materialFilePicker = MaterialFilePicker()
                .withCallbackSupportFragment(this)
                .withCallbackActivityResults(mapActivityResult)
                .withHiddenFiles(false)
                .withRootPaths(getRootPaths(currentContext))
                .withFilter(Pattern.compile(".*\\.(map)$"))
                .withPath("$storagePath/cyclometer/maps")
                .withTitle("Select New Map File")
            materialFilePicker.start()
        }
    }

    private fun handleScrnOrientation() {
        val orientationMenu = IconContextMenu(currentContext, R.menu.menu_progress_ui_orientation, THEME_DARK, SANS)
        orientationMenu.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
            if (item.itemId == R.id.action_ui_default) {
                handleScreenOrientation("1")
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_orientation_key), "1").commit()
            }
            if (item.itemId == R.id.action_ui_landscape) {
                handleScreenOrientation("2")
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_orientation_key), "2").commit()
            }
            if (item.itemId == R.id.action_ui_portrait) {
                handleScreenOrientation("3")
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_orientation_key), "3").commit()
            }
            if (item.itemId == R.id.action_ui_landscape_reverse) {
                handleScreenOrientation("4")
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_orientation_key), "4").commit()
            }
            if (item.itemId == R.id.action_ui_portrait_reverse) {
                handleScreenOrientation("5")
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_orientation_key), "5").commit()
            }
            orientationMenu.dismiss()
        })
        orientationMenu.setOnCancelListener(DialogInterface.OnCancelListener {
            orientationMenu.dismiss()
        })
        orientationMenu.show()
    }

    private fun handleSettingsMenu() {
        var menu = R.menu.menu_progress_settings
        if (!sharedPreferences.getString(getString(R.string.preferences_tcx_gpx_track_filename_key), "").isNullOrBlank()) {
            menu = R.menu.menu_progress_settings_clear
        }
        settingMenu = IconContextMenu(currentContext, menu, THEME_DARK, SANS)
        settingMenu.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
            if (item.itemId == R.id.action_map_theme) {
                handleMapThemeOptions()
            }
            if (item.itemId == R.id.action_new_map) {
                handleNewMapFile()
            }
            if (item.itemId == R.id.action_new_tcx_gpx_track) {
                handleNewTcxGpxTrack()
            }
            if (item.itemId == R.id.action_clear_tcx_gpx_track) {
                clearTcxGpx()
                sharedPreferences.edit().putString(getString(R.string.preferences_tcx_gpx_track_filename_key), "").commit()
            }
            if (item.itemId == R.id.action_screen_orientation) {
                handleScrnOrientation()
            }
            if (item.itemId == R.id.action_ui_field_selection) {
                handleFieldSelection()
            }
            settingMenu.dismiss()
        })
        settingMenu.setOnCancelListener(DialogInterface.OnCancelListener {
            settingMenu.dismiss()
        })
        settingMenu.show()
    }

    private fun handleInitUiFieldSelection() {
        var uiSelection = sharedPreferences.getString(getString(R.string.preferences_ui_field_selection_key), "4")
        when (uiSelection) {
            "1" -> {
                middleUpperLeftView.visibility = GONE
                middleUpperRightView.visibility = GONE
                middleLowerLeftView.visibility = GONE
                middleLowerRightView.visibility = GONE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
            }
            "2" -> {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = GONE
                middleLowerRightView.visibility = GONE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
            }
            "3" -> {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = VISIBLE
                middleLowerRightView.visibility = VISIBLE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
            }
            "4" -> {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = VISIBLE
                middleLowerRightView.visibility = VISIBLE
                bottomLeftView.visibility = VISIBLE
                bottomRightView.visibility = VISIBLE
            }
            else -> {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = VISIBLE
                middleLowerRightView.visibility = VISIBLE
                bottomLeftView.visibility = VISIBLE
                bottomRightView.visibility = VISIBLE
            }
        }
    }

    private fun handleFieldSelection() {
        uiMenuSelections = IconContextMenu(currentContext, R.menu.menu_progress_fields_selection, THEME_DARK, SANS)
        uiMenuSelections.setOnIconContextItemSelectedListener(IIconContextItemSelectedListener { item, info ->
            if (item.itemId == R.id.action_ui_top_plus_two) {
                middleUpperLeftView.visibility = GONE
                middleUpperRightView.visibility = GONE
                middleLowerLeftView.visibility = GONE
                middleLowerRightView.visibility = GONE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_field_selection_key), "1").commit()
            }
            if (item.itemId == R.id.action_ui_top_plus_four) {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = GONE
                middleLowerRightView.visibility = GONE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_field_selection_key), "2").commit()
            }
            if (item.itemId == R.id.action_ui_top_plus_six) {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = VISIBLE
                middleLowerRightView.visibility = VISIBLE
                bottomLeftView.visibility = GONE
                bottomRightView.visibility = GONE
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_field_selection_key), "3").commit()
            }
            if (item.itemId == R.id.action_ui_top_plus_eight) {
                middleUpperLeftView.visibility = VISIBLE
                middleUpperRightView.visibility = VISIBLE
                middleLowerLeftView.visibility = VISIBLE
                middleLowerRightView.visibility = VISIBLE
                bottomLeftView.visibility = VISIBLE
                bottomRightView.visibility = VISIBLE
                sharedPreferences.edit().putString(getString(R.string.preferences_ui_field_selection_key), "4").commit()
            }
            uiMenuSelections.dismiss()
        })
        uiMenuSelections.setOnCancelListener(DialogInterface.OnCancelListener {
            uiMenuSelections.dismiss()
        })
        uiMenuSelections.show()
    }

    private fun initializeViews(view: View) {
        if (useMapViewForTripProgress) {
            mapViewFrame = view.findViewById(R.id.map_frame)
        }
        pauseButton = view.findViewById(R.id.pause_button)
        resumeButton = view.findViewById(R.id.resume_button)
        autoPauseChip = view.findViewById(R.id.autopause_button)
        stopButton = view.findViewById(R.id.stop_button)
        topView = view.findViewById(R.id.measurement_top)

        topLeftView = view.findViewById(R.id.measurement_topLeft)
        topRightView = view.findViewById(R.id.measurement_topRight)

        middleUpperLeftView = view.findViewById(R.id.measurement_middleLeftUpper)
        middleUpperRightView = view.findViewById(R.id.measurement_middleRightUpper)

        middleLowerLeftView = view.findViewById(R.id.measurement_middleLeftLower)
        middleLowerRightView = view.findViewById(R.id.measurement_middleRightLower)

        bottomLeftView = view.findViewById(R.id.measurement_bottomLeft)
        bottomRightView = view.findViewById(R.id.measurement_bottomRight)

        footerLeftView = view.findViewById(R.id.measurement_footer_left)
        footerRightView = view.findViewById(R.id.measurement_footer_right)

        batteryLevelText = view.findViewById(R.id.dashboard_textview_batteryLevel)
        chargingImage = view.findViewById(R.id.charging_image)
        trackingImage = view.findViewById(R.id.image_tracking)
        compassImage = view.findViewById(R.id.compass_image)
        //debugTextView = view.findViewById(R.id.textview_debug)
        timeOfDayTextView = view.findViewById(R.id.dashboard_textview_timeOfDay)

        //if (FeatureFlags.productionBuild) {
        //    trackingImage.visibility = GONE
        //}

        scaleMeasurmentTexts()

        topView.setOnLongClickListener(selectionDisplayLongClickListener)
        topLeftView.setOnLongClickListener(selectionDisplayLongClickListener)
        topRightView.setOnLongClickListener(selectionDisplayLongClickListener)
        middleUpperLeftView.setOnLongClickListener(selectionDisplayLongClickListener)
        middleUpperRightView.setOnLongClickListener(selectionDisplayLongClickListener)
        middleLowerLeftView.setOnLongClickListener(selectionDisplayLongClickListener)
        middleLowerRightView.setOnLongClickListener(selectionDisplayLongClickListener)
        bottomLeftView.setOnLongClickListener(selectionDisplayLongClickListener)
        bottomRightView.setOnLongClickListener(selectionDisplayLongClickListener)

        topView.setOnClickListener(selectionClickListener)
        topLeftView.setOnClickListener(selectionClickListener)
        topRightView.setOnClickListener(selectionClickListener)
        middleUpperLeftView.setOnClickListener(selectionClickListener)
        middleUpperRightView.setOnClickListener(selectionClickListener)
        middleLowerLeftView.setOnClickListener(selectionClickListener)
        middleLowerRightView.setOnClickListener(selectionClickListener)
        bottomLeftView.setOnClickListener(selectionClickListener)
        bottomRightView.setOnClickListener(selectionClickListener)

        topView.value = "0.0"
        topLeftView.value = "0.0"
        topRightView.value = "0.0"
        middleUpperLeftView.value = "0.0"
        middleUpperRightView.value = "0.0"
        middleLowerLeftView.value = "0.0"
        middleLowerRightView.value = "0.0"
        bottomLeftView.value = "0.0"
        bottomRightView.value = "0.0"

        topView.label = ""
        topLeftView.label = ""
        topRightView.label = ""
        middleUpperLeftView.label = ""
        middleUpperRightView.label = ""
        middleLowerLeftView.label = ""
        middleLowerRightView.label = ""
        bottomLeftView.label = ""
        bottomRightView.label = ""

        footerLeftView.text = ""
        footerRightView.text = ""

        handleInitUiFieldSelection()

        if (useMapViewForTripProgress) {
            settingsButton = view.findViewById(R.id.settings_button)
        }
        //temperatureTextView = view.findViewById(R.id.dashboard_textview_temperature)
        //windDirectionArrow = view.findViewById(R.id.image_arrow_wind_direction)

        if (useMapViewForTripProgress &&
            ::settingsButton.isInitialized) {
            settingsButton.setOnClickListener(OnClickListener {
                handleSettingsMenu()
            })
        }

        timeOfDayTextView.setOnClickListener(OnClickListener {
            if (useTwentyFourHour) {
                sharedPreferences.edit().putString(getString(R.string.preferences_time_system_of_measurement_key), "1").commit()
            } else {
                sharedPreferences.edit().putString(getString(R.string.preferences_time_system_of_measurement_key), "2").commit()
            }
            useTwentyFourHour = !useTwentyFourHour
            updateClock()
        })

        //if (FeatureFlags.productionBuild) {
        //    trackingImage.visibility = GONE
        //    //debugTextView.visibility = GONE
        //}

        for (index in 1..9) {
            setValue(index, "")
            setLabel(index, "")
            setExtraInfo(index, "")
        }

        setSpeed("0.0", "GPS ${getUserSpeedUnitShort(requireContext()).uppercase(Locale.getDefault())}", "", 0)
        setDuration("0.0", "DURATION", "", 0)
        setDistance("0.0 ${getUserDistanceUnitShort(requireContext())}", "DISTANCE", "", 0)
        setAvgSpeed("0.0 ${getUserSpeedUnitShort(requireContext())}", "AVG SPEED", "", 0)
        setSplit("0.0", "SPLIT ${getUserSpeedUnitShort(requireContext()).uppercase(Locale.getDefault())}", "", 0)
        setSlope("0.0", "SLOPE", "", 0)
        setGrade("0.0", "GRADE", "", 0)
        setElevation( "0.0 ${getUserAltitudeUnitShort(requireContext())}", "ALTITUDE", "", 0)
        setGpsBearing( "0.0", "G|BEARING", "", 0)
        setMagBearing( "0.0", "M|BEARING", "", 0)
        setCadence("0.0", "RPM", "", 0)
        setWind("0.0", "WIND", "", 0)
        setTemperature("0.0", "TEMP", "", 0)
        setAirPressure("0.0", "AIR P.", "", 0)
        setRelativeHumidity("0.0", "REL HUM", "", 0)
        setHeartRate("0.0", "BPM", "", 0)
        setPowerMeter("0.0", "POWER", "", 0)

        batteryLevel()
    }

    private fun setIcon(index: Int, iconId: Int) {
        when (index) {
            1 -> {
                topView.setIcon(iconId)
            }
            2 -> {
                topLeftView.setIcon(iconId)
            }
            3 -> {
                topRightView.setIcon(iconId)
            }
            4 -> {
                middleUpperLeftView.setIcon(iconId)
            }
            5 -> {
                middleUpperRightView.setIcon(iconId)
            }
            6 -> {
                middleLowerLeftView.setIcon(iconId)
            }
            7 -> {
                middleLowerRightView.setIcon(iconId)
            }
            8 -> {
                bottomLeftView.setIcon(iconId)
            }
            9 -> {
                bottomRightView.setIcon(iconId)
            }
        }
    }

    private fun setIconVisible(index: Int, visible: Boolean) {
        var visibility = View.GONE
        if (visible) {
            visibility = View.VISIBLE
        }
        when (index) {
            1 -> {
                topView.setIconVisibility(visibility)
            }
            2 -> {
                topLeftView.setIconVisibility(visibility)
            }
            3 -> {
                topRightView.setIconVisibility(visibility)
            }
            4 -> {
                middleUpperLeftView.setIconVisibility(visibility)
            }
            5 -> {
                middleUpperRightView.setIconVisibility(visibility)
            }
            6 -> {
                middleLowerLeftView.setIconVisibility(visibility)
            }
            7 -> {
                middleLowerRightView.setIconVisibility(visibility)
            }
            8 -> {
                bottomLeftView.setIconVisibility(visibility)
            }
            9 -> {
                bottomRightView.setIconVisibility(visibility)
            }
        }
    }

    private fun setExtraInfo(index: Int, label: String) {
        when (index) {
            1 -> {
                topView.extraInfo = label
            }
            2 -> {
                topLeftView.extraInfo = label
            }
            3 -> {
                topRightView.extraInfo = label
            }
            4 -> {
                middleUpperLeftView.extraInfo = label
            }
            5 -> {
                middleUpperRightView.extraInfo = label
            }
            6 -> {
                middleLowerLeftView.extraInfo = label
            }
            7 -> {
                middleLowerRightView.extraInfo = label
            }
            8 -> {
                bottomLeftView.extraInfo = label
            }
            9 -> {
                bottomRightView.extraInfo = label
            }
        }
    }

    private fun setLabel(index: Int, label: String) {
        when (index) {
            1 -> {
                topView.label = label
            }
            2 -> {
                topLeftView.label = label
            }
            3 -> {
                topRightView.label = label
            }
            4 -> {
                middleUpperLeftView.label = label
            }
            5 -> {
                middleUpperRightView.label = label
            }
            6 -> {
                middleLowerLeftView.label = label
            }
            7 -> {
                middleLowerRightView.label = label
            }
            8 -> {
                bottomLeftView.label = label
            }
            9 -> {
                bottomRightView.label = label
            }
        }
    }

    private fun setValue(index: Int, label: String) {
        when (index) {
            1 -> {
                topView.value = label
            }
            2 -> {
                topLeftView.value = label
            }
            3 -> {
                topRightView.value = label
            }
            4 -> {
                middleUpperLeftView.value = label
            }
            5 -> {
                middleUpperRightView.value = label
            }
            6 -> {
                middleLowerLeftView.value = label
            }
            7 -> {
                middleLowerRightView.value = label
            }
            8 -> {
                bottomLeftView.value = label
            }
            9 -> {
                bottomRightView.value = label
            }
        }
    }

    private fun handleData(index: Int, value: String, label: String, icon: Int) {
        setValue(index, value)
        setLabel(index, label)
        if (icon != 0) {
            setIcon(index, icon)
            setIconVisible(index, true)
        } else {
            if (icon == -1) {
                setIconVisible(index, false)
            }
        }
    }

    private fun handleData(index: Int, value: String, label: String, extraInfo: String, icon: Int) {
        setValue(index, value)
        setLabel(index, label)
        setExtraInfo(index, extraInfo)
        if (icon != 0) {
            setIcon(index, icon)
            setIconVisible(index, true)
        } else {
            if (icon == -1) {
                setIconVisible(index, false)
            }
        }
    }

    private fun setSpeed(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPEED, DISPLAY_SETTINGS_TOP)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setDuration(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DURATION, DISPLAY_SETTINGS_TOP_LEFT)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setDistance(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_DISTANCE, DISPLAY_SETTINGS_TOP_RIGHT)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setAvgSpeed(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AVG_SPEED, DISPLAY_SETTINGS_MID_UP_LEFT)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setSplit(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SPLITS, DISPLAY_SETTINGS_MID_UP_RIGHT)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setSlope(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_SLOPE, DISPLAY_SETTINGS_MID_LO_LEFT)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setGrade(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GRADE, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setHeartRate(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_HEART_RATE, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setCadence(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_CADENCE, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setPowerMeter(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_POWER_METER, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, icon)
        }
    }

    private fun setTemperature(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_TEMPERATURE, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setRelativeHumidity(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_RH, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setAirPressure(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_AP, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setWind(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_WIND, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setGpsBearing(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_GPS_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setMagBearing(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_MAG_BEARING, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, extraInfo, icon)
        }
    }

    private fun setElevation(value: String, label: String, extraInfo: String, icon: Int) {
        var index = sharedPreferences.getInt(PREFS_DISPLAY_SETTINGS_ELEVATION, DISPLAY_SETTINGS_DISABLED)
        if (index != DISPLAY_SETTINGS_DISABLED) {
            handleData(index, value, label, icon)
        }
    }

    private fun getDebugString(): String {
        var debugString = ""
        //debugString += "%.2f".format(location.accuracy)
        //autoCircumference?.let { c -> debugString += " | C%.3f".format(c) }
        //autoCircumferenceVariance?.let { v -> debugString += " | ±%.7f".format(v) }
        //viewModel.currentProgress.value?.slope?.let { s -> debugString += " | S%.3f".format(s) }
        viewModel.currentProgress.value?.slope?.takeIf { it.isFinite() }
            ?.let { s -> debugString += "S%.1f".format(s * 100f) }
        return debugString
    }

    private fun getGpsSpeed(location: Location): String {
        var speed: Double
        if (location.speed < 0.5) {
            speed = 0.0
        } else {
            speed = getUserSpeed(requireContext(), location.speed.toDouble()).toDouble()
        }
        return String.format("%.1f", speed)
    }

    private fun getGpsSpeedOld(
        location: Location,
        averageSpeed: Double?,
        alpha: Double = 1.0
    ) = when (location.speed < 0.5) {
        true -> 0.0
        else -> {
            val weight =
                when (location.hasSpeedAccuracy()) {
                    true -> (location.speedAccuracyMetersPerSecond.coerceAtMost( 10f) / 10.0 - 1).pow(8)
                    else -> 0.0
                }
            (averageSpeed
                ?: getUserSpeed(requireContext(), location.speed.toDouble()).toDouble())
                .takeIf { it.isFinite() }
                ?.let { oldSpeed ->
                    exponentialSmoothing(
                        alpha * weight,
                        getUserSpeed(
                            requireContext(),
                            location.speed.toDouble()
                        ).toDouble(),
                        oldSpeed
                    )
                }
        }
    }.let { speed ->
        String.format("%.1f", speed)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.tripId?.let { outState.putLong("tripId", it) }
        super.onSaveInstanceState(outState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
    }

    private var hasMagnetic: Boolean = false
    private var hasAccel: Boolean = false
    private var hasAirPressure: Boolean = false

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this);
    }

    override fun onResume() {
        super.onResume()
        Log.d(logTag, "Called onResume: currentState = ${viewModel.currentState}")
        if (!::sensorManager.isInitialized) {
            try {
                sensorManager = currentContext.getSystemService(SENSOR_SERVICE) as SensorManager
            } catch (e: Exception) {
            }
        }
        if (!::accelerometer.isInitialized) {
            try {
                accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
            } catch (e: Exception) {
            }
        }
        if (!::magnetometer.isInitialized) {
            try {
                magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
            } catch (e: Exception) {
            }
        }
        if (!::pressuresensor.isInitialized) {
            try {
                pressuresensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
            } catch (e: Exception) {
            }
        }

        hasMagnetic = sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        hasAccel = sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        if (!::pressuresensor.isInitialized) {
            try {
                hasAirPressure = sensorManager.registerListener(
                    this,
                    pressuresensor,
                    SensorManager.SENSOR_DELAY_NORMAL
                );
            } catch (e: Exception) {
                hasAirPressure = false
            }
        } else {
            hasAirPressure = false
        }

        view?.doOnPreDraw { hideResumeStop() }
        val colorInt: Int = currentContext.getColor(R.color.button_red)
        pauseButton.backgroundTintList = ColorStateList.valueOf(colorInt)

        try {
            args.tripId.takeIf { tripId -> tripId != -1L }?.let { tripId ->
                viewModel.tripId = tripId
            }

            when (val tripId = viewModel.tripId) {
                null -> {
                    requireActivity().startService(Intent(
                        requireContext(),
                        TripInProgressService::class.java
                    ).apply {
                        this.action = getString(R.string.action_initialize_trip_service)
                    })
                    pauseButton.setOnClickListener(startTripListener)
                    pauseButton.text = getString(R.string.start_label)
                    val colorInt: Int = currentContext.getColor(R.color.button_green)
                    pauseButton.backgroundTintList = ColorStateList.valueOf(colorInt)
                }

                else -> {
                    view?.doOnPreDraw { hidePause() }
                    Log.d(logTag, "Received trip ID argument $tripId")
                    Log.d(logTag, "Resuming trip $tripId")
                    initializeAfterTripCreated(tripId)
                    viewModel.resumeTrip(tripId, viewLifecycleOwner)
                    requireActivity().startService(
                        Intent(
                            requireContext(),
                            TripInProgressService::class.java
                        ).apply {
                            this.action = getString(R.string.action_start_trip_service)
                            this.putExtra("tripId", tripId)
                        })

                    viewLifecycleOwner.lifecycleScope.launch {
                        setTimeStateButtonState(viewModel.getCurrentTimeState(tripId))
                    }
                }
            }
        } catch (e: Exception) {
            when (e) {
                is java.lang.IllegalArgumentException,
                is java.lang.reflect.InvocationTargetException -> {
                    Log.e(logTag, "Failed to parse navigation args", e)
                    endTrip(-1)
                }

                else -> throw e
            }
        }
    }

    private fun updateClock() {
        var hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        if (!useTwentyFourHour) {
            hour = Calendar.getInstance().get(Calendar.HOUR)
            if (hour == 0) {
                hour = 12
            }
        }
        val time = String.format(
            "%d:%02d",
            hour,
            Calendar.getInstance().get(Calendar.MINUTE)
        )
        var amPm = if (Calendar.getInstance().get(Calendar.AM_PM) == 0) "AM" else "PM"
        if (useTwentyFourHour) amPm = "  "
        timeOfDayTextView.apply {
            visibility = VISIBLE
            text = "$time $amPm"
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        Log.d(logTag, "onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(logTag, "onDestroyView")
        if (isTimeTickRegistered) currentContext?.unregisterReceiver(timeTickReceiver)

        if (viewModel.tripId == null)
            requireContext().startService(Intent(
                requireContext(),
                TripInProgressService::class.java
            ).apply {
                this.action = getString(R.string.action_shutdown_trip_service)
            })
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        Log.v(logTag, event.toString())
        Log.v(logTag, "viewModel.currentState = ${viewModel.currentState}")

        return when (event?.action) {
            MotionEvent.ACTION_UP -> if (viewModel.currentState == TimeStateEnum.START || viewModel.currentState == TimeStateEnum.RESUME) {
                handleScreenTouchClick()
                v?.performClick()
                true
            } else false

            MotionEvent.ACTION_DOWN -> true
            else -> false
        }
    }

    private fun isPauseButtonHidden() = pauseButton.translationY != 0f

    private fun handleScreenTouchClick(): Boolean {
        Log.d(logTag, "handleTouchClick")
        if (isPauseButtonHidden()) {
            slidePauseUp()
            hidePauseHandler.postDelayed(hidePauseCallback, 5000)
        } else {
            hidePauseHandler.removeCallbacks(hidePauseCallback)
            hidePauseCallback.run()
        }
        return true
    }

    private fun clearTcxGpx() {
        if (::vtmMapView.isInitialized) {
            if (::startMarker.isInitialized) {
                ptsStart.remove(startMarker)
            }
            if (::endMarker.isInitialized) {
                ptsEnd.remove(endMarker)
            }

            if (::startMarkerLayer.isInitialized) {
                startMarkerLayer.removeAllItems();
            }
            if (::endMarkerLayer.isInitialized) {
                endMarkerLayer.removeAllItems();
            }
            if (::pathLayer.isInitialized) {
                pathLayer.clearPath()
            }
        }
    }

    private fun handleTcxData(latlngSegments: List<MutableList<GeoPoint>>) {
        if (latlngSegments.size >= 1 && latlngSegments.get(0).size >= 2) {
            var geoPointStart: GeoPoint = latlngSegments.get(0).get(0)
            var geoPointEnd: GeoPoint = latlngSegments.get(latlngSegments.size - 1).get(latlngSegments.get(0).size - 1)

            if (vtmMapView != null) {
                if (longitude == 0.0 || longitude == -9999.0 ||
                    latitude == 0.0 || latitude == -9999.0) {
                    longitude = geoPointStart.longitude
                    latitude = geoPointStart.latitude
                    val centerMapPosition = MapPosition()
                        .setX(MercatorProjection.longitudeToX(longitude))
                        .setY(MercatorProjection.latitudeToY(latitude))
                        .setZoomLevel(zoomLevel)

                    /* set map center position */
                    vtmMapView.map().setMapPosition(centerMapPosition)
                    vtmMapView.map().viewport().setMapPosition(centerMapPosition)
                }
            }

            val cB: Int = Color.RED
            val styleB: LineStyle = LineStyle.builder()
                .color(cB)
                .strokeWidth(4.0f)
                .cap(Paint.Cap.ROUND)
                .build()

            for (seg in 0..latlngSegments.size - 1) {
                pathLayer = PathLayer(vtmMapView.map(), styleB)
                pathLayer.setPoints(latlngSegments.get(seg))
                vtmMapView.map().layers().add(pathLayer)
            }

            /* start and stop markers */
            var `is`: InputStream? = null
            try {
                `is` = resources.openRawResource(R.raw.marker_start)
                bitmapMarkerStart = CanvasAdapter.decodeSvgBitmap(
                    `is`,
                    (48 * CanvasAdapter.getScale()).toInt(),
                    (48 * CanvasAdapter.getScale()).toInt(), 100
                )
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                IOUtils.closeQuietly(`is`)
            }

            try {
                `is` = resources.openRawResource(R.raw.marker_end)
                bitmapMarkerEnd = CanvasAdapter.decodeSvgBitmap(
                    `is`,
                    (48 * CanvasAdapter.getScale()).toInt(),
                    (48 * CanvasAdapter.getScale()).toInt(), 100
                )
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                IOUtils.closeQuietly(`is`)
            }

            startSymbol = MarkerSymbol(bitmapMarkerStart, MarkerSymbol.HotspotPlace.CENTER, false)
            endSymbol = MarkerSymbol(bitmapMarkerEnd, MarkerSymbol.HotspotPlace.CENTER, false)
            startMarkerLayer = ItemizedLayer(vtmMapView.map(), ArrayList<MarkerInterface>(), startSymbol, this)
            endMarkerLayer = ItemizedLayer(vtmMapView.map(), ArrayList<MarkerInterface>(), endSymbol, this)
            vtmMapView.map().layers().add(startMarkerLayer);
            vtmMapView.map().layers().add(endMarkerLayer);

            //ptsStart: ArrayList<MarkerInterface> = ArrayList()
            //ptsEnd: ArrayList<MarkerInterface> = ArrayList()
            var descriptionStart = "start marker"
            var descriptionEnd = "end marker"

            startMarker = MarkerItem(
                geoPointStart.latitude.toString() + "/" + geoPointStart.longitude.toString(),
                descriptionStart,
                GeoPoint(geoPointStart.latitude, geoPointStart.longitude))

            endMarker = MarkerItem(
                geoPointEnd.latitude.toString() + "/" + geoPointEnd.longitude.toString(),
                descriptionEnd,
                GeoPoint(geoPointEnd.latitude, geoPointEnd.longitude))

            ptsStart.add(startMarker)
            ptsEnd.add(endMarker)
            startMarkerLayer.addItems(ptsStart);
            endMarkerLayer.addItems(ptsEnd);
        }
    }

    private fun handleTcxFile(tcxFile: File): Boolean {
        if (tcxFile == null  || !tcxFile.exists()) {
            return false;
        }
        var mergemultisport = false
        val parser = TcxParser(currentContext, mergemultisport)
        try {
            var result = parser.parseTcxFile(tcxFile)
            if (result) {
                handleTcxData(parser.getSegmentList())
                return true
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        Toast.makeText(currentContext, "ERROR: failed to parse TCX file", Toast.LENGTH_LONG).show()
        return false
    }

    private lateinit var pathLayer: PathLayer
    private fun handleGpxFile(gpxFile: File): Boolean {
        if (gpxFile == null  || !gpxFile.exists()) {
            return false;
        }
        if (!useMapViewForTripProgress) {
            return false
        }
        val parser = GPXParser()
        try {
            val input: FileInputStream = FileInputStream(gpxFile)
            // consider using a background thread
            parsedGpx = parser.parse(input)
            Log.d(logTag, "GPX FILE PARSED")
            Log.d(logTag, "Creator  : " + parsedGpx.creator);
            Log.d(logTag, "Version  : " + parsedGpx.version);
            Log.d(logTag, "MD: DESC : " + parsedGpx.metadata.desc);
            Log.d(logTag, "MD: NAME : " + parsedGpx.metadata.name);
            Log.d(logTag, "MD: KEYW : " + parsedGpx.metadata.keywords);
            Log.d(logTag, "MD: AUTH : " + parsedGpx.metadata.author);
            Log.d(logTag, "MD: CRIT : " + parsedGpx.metadata.copyright);
            Log.d(logTag, "MD: LINK : " + parsedGpx.metadata.link.href);
            Log.d(logTag, "MD: LINK : " + parsedGpx.metadata.link.text);
            Log.d(logTag, "MD: LINK : " + parsedGpx.metadata.link.type);
            Log.d(logTag, "MD: TIME : " + parsedGpx.metadata.time);
            Log.d(logTag, "MD: LAT- : " + parsedGpx.metadata.bounds.minLat);
            Log.d(logTag, "MD: LNG- : " + parsedGpx.metadata.bounds.minLon);
            Log.d(logTag, "MD: LAT+ : " + parsedGpx.metadata.bounds.maxLat);
            Log.d(logTag, "MD: LNG+ : " + parsedGpx.metadata.bounds.maxLon);
            if (parsedGpx.routes.size != null && parsedGpx.routes.size > 0) {
                Log.d(logTag, "Routes   : " + parsedGpx.routes.size);
            }
            if (parsedGpx.routes.size != null && parsedGpx.tracks.size > 0) {
                val latlngSegments = arrayListOf<MutableList<GeoPoint>>()
                Log.d(logTag, "Tracks   : " + parsedGpx.tracks.size);
                for (item in 0..(parsedGpx.tracks.size - 1)) {
                    var trackitem = parsedGpx.tracks.get(item)
                    if (trackitem.trackSegments.size > 0) {
                        Log.d(logTag, "Track Segments : " + trackitem.trackSegments.size);
                        for (segitem in 0..(trackitem.trackSegments.size - 1)) {
                            val latLongs: MutableList<GeoPoint> = ArrayList()
                            if (trackitem.trackSegments.get(segitem).trackPoints.size > 0) {
                                Log.d(logTag, "Segment Points : " + trackitem.trackSegments.get(segitem).trackPoints.size);
                                for (pntitem in 0..(trackitem.trackSegments.get(segitem).trackPoints.size - 1)) {
                                    var point =
                                        trackitem.trackSegments.get(segitem).trackPoints.get(pntitem)
                                    if (segitem == 0 && pntitem == 0) {
                                        startLocation = GeoPoint(point.latitude, point.longitude)
                                        if (vtmMapView != null) {
                                            if (longitude == 0.0 || longitude == -9999.0 ||
                                                latitude == 0.0 || latitude == -9999.0) {
                                                longitude = point.longitude
                                                latitude = point.latitude
                                                val centerMapPosition = MapPosition()
                                                    .setX(MercatorProjection.longitudeToX(longitude))
                                                    .setY(MercatorProjection.latitudeToY(latitude))
                                                    .setZoomLevel(zoomLevel)

                                                /* set map center position */
                                                vtmMapView.map().setMapPosition(centerMapPosition)
                                                vtmMapView.map().viewport().setMapPosition(centerMapPosition)
                                            }
                                        }
                                    }
                                    if (segitem == trackitem.trackSegments.size - 1 &&
                                        pntitem == trackitem.trackSegments.get(segitem).trackPoints.size - 1) {
                                        endLocation = GeoPoint(point.latitude, point.longitude)
                                    }
                                    latLongs.add(GeoPoint(point.latitude, point.longitude))
                                }
                            }
                            latlngSegments.add(latLongs)
                        }
                    }
                }

                if (latlngSegments.size >= 1 && latlngSegments.get(0).size >= 2) {
                    var geoPointStart: GeoPoint = latlngSegments.get(0).get(0)
                    var geoPointEnd: GeoPoint = latlngSegments.get(latlngSegments.size - 1).get(latlngSegments.get(0).size - 1)

                    for (seg in 0..latlngSegments.size - 1) {
                        /* line path */
                        if (true) {
                            val cB: Int = Color.RED
                            val styleB: LineStyle = LineStyle.builder()
                                .color(cB)
                                .strokeWidth(4.0f)
                                .cap(Paint.Cap.ROUND)
                                .build()
                            pathLayer = PathLayer(vtmMapView.map(), styleB)
                            pathLayer.setPoints(latlngSegments.get(seg))
                            vtmMapView.map().layers().add(pathLayer)
                        }

                        /* stipple line path */
                        if (false) {
                            val pathLayer: PathLayer
                            val cR: Int = Color.RED
                            val styleR: LineStyle = LineStyle.builder()
                                .stippleColor(cR)
                                .stipple(24)
                                .stippleWidth(1.0f)
                                .strokeWidth(8.0f)
                                .strokeColor(cR)
                                .fixed(true)
                                .randomOffset(false)
                                .isOutline(true)
                                .build()
                            pathLayer = PathLayer(vtmMapView.map(), styleR)
                            pathLayer.setPoints(latlngSegments.get(seg))
                            vtmMapView.map().layers().add(pathLayer)
                        }
                    }

                    /* start and stop markers */
                    if (true) {
                        var `is`: InputStream? = null
                        try {
                            `is` = resources.openRawResource(R.raw.marker_start)
                            bitmapMarkerStart = CanvasAdapter.decodeSvgBitmap(
                                `is`,
                                (48 * CanvasAdapter.getScale()).toInt(),
                                (48 * CanvasAdapter.getScale()).toInt(), 100
                            )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            IOUtils.closeQuietly(`is`)
                        }

                        try {
                            `is` = resources.openRawResource(R.raw.marker_end)
                            bitmapMarkerEnd = CanvasAdapter.decodeSvgBitmap(
                                `is`,
                                (48 * CanvasAdapter.getScale()).toInt(),
                                (48 * CanvasAdapter.getScale()).toInt(), 100
                            )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            IOUtils.closeQuietly(`is`)
                        }

                        startSymbol = MarkerSymbol(bitmapMarkerStart, MarkerSymbol.HotspotPlace.CENTER, false)
                        endSymbol = MarkerSymbol(bitmapMarkerEnd, MarkerSymbol.HotspotPlace.CENTER, false)
                        startMarkerLayer = ItemizedLayer(vtmMapView.map(), ArrayList<MarkerInterface>(), startSymbol, this)
                        endMarkerLayer = ItemizedLayer(vtmMapView.map(), ArrayList<MarkerInterface>(), endSymbol, this)
                        vtmMapView.map().layers().add(startMarkerLayer);
                        vtmMapView.map().layers().add(endMarkerLayer);

                        //ptsStart: ArrayList<MarkerInterface> = ArrayList()
                        //ptsEnd: ArrayList<MarkerInterface> = ArrayList()
                        var descriptionStart = "start marker"
                        var descriptionEnd = "end marker"

                        startMarker = MarkerItem(
                            geoPointStart.latitude.toString() + "/" + geoPointStart.longitude.toString(),
                            descriptionStart,
                            GeoPoint(geoPointStart.latitude, geoPointStart.longitude))

                        endMarker = MarkerItem(
                            geoPointEnd.latitude.toString() + "/" + geoPointEnd.longitude.toString(),
                            descriptionEnd,
                            GeoPoint(geoPointEnd.latitude, geoPointEnd.longitude))

                        ptsStart.add(startMarker)
                        ptsEnd.add(endMarker)
                        startMarkerLayer.addItems(ptsStart);
                        endMarkerLayer.addItems(ptsEnd);
                    }
                }

                /*
                if (latlngSegments.size > 0) {
                    //layers = vtmMapView.getLayerManager().getLayers()
                    layers = vtmMapView.map().layers()

                    var scaleValue = (4 * vtmMapView.getModel().displayModel.getScaleFactor())
                    var colorValue = AndroidGraphicFactory.INSTANCE.createColor(Color.BLUE)
                    var paintValue = MapUtils.createPaint(colorValue, scaleValue, Style.STROKE)
                    for (ii in 0..(latlngSegments.size-1)) {
                        val latLongs = latlngSegments.get(ii)
                        val polyline: Polyline = Polyline(paintValue, AndroidGraphicFactory.INSTANCE)
                        polyline.setPoints(latLongs);
                        layers.add(polyline)
                    }
                }
                 */
            }
            if (parsedGpx.wayPoints.size != null && parsedGpx.wayPoints.size > 0) {
                Log.d(logTag, "WayPoints: " + parsedGpx.wayPoints.size);
            }

            invalidateMap()

            return true
        } catch (e: IOException) {
            // do something with this exception
            e.printStackTrace()
        } catch (e: XmlPullParserException) {
            // do something with this exception
            e.printStackTrace()
        }

        Toast.makeText(currentContext, "ERROR: failed to parse GPX file", Toast.LENGTH_LONG).show()

        return false
    }
    private fun invalidateMap() {
        vtmMapView.map().updateMap(true)
        vtmMapView.invalidate()
    }

    private fun handleMapFile(mapfile: File) {
        try {
            // Tile source
            var fis = FileInputStream(mapfile)
            val tileSource = MapFileTileSource()
            tileSource.setMapFileInputStream(fis)

            if (!::vtmMapView.isInitialized) {
                throw RuntimeException("MAP IS NOT INITIALIZED CANNOT CONTINUE")
            }

            var map = vtmMapView.map()
            // Vector layer
            val tileLayer: VectorTileLayer = map.setBaseMap(tileSource)

            // Building layer
            map.layers().add(BuildingLayer(map, tileLayer))

            // Label layer
            map.layers().add(LabelLayer(map, tileLayer))

            // Render theme
            var vtmTheme = VtmThemes.DEFAULT
            var maptheme = sharedPreferences.getString(
                currentContext.getString(R.string.preference_map_theme_key),
                "1")
            when (maptheme) {
                "1" -> {
                    vtmTheme = VtmThemes.DEFAULT
                }
                "2" -> {
                    vtmTheme = VtmThemes.NEWTRON
                }
                "3" -> {
                    vtmTheme = VtmThemes.TRONRENDER
                }
                "4" -> {
                    vtmTheme = VtmThemes.BIKER
                }
                "5" -> {
                    vtmTheme = VtmThemes.MAPZEN
                }
                "6" -> {
                    vtmTheme = VtmThemes.OSMARENDER
                }
                "7" -> {
                    vtmTheme = VtmThemes.OSMAGRAY
                }
                "8" -> {
                    vtmTheme = VtmThemes.MOTORIDER
                }
                "9" -> {
                    vtmTheme = VtmThemes.MOTORIDER_DARK
                }
            }

            mapTheme = map.setTheme(vtmTheme) as RenderTheme

            invalidateMap()

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun clearCenterPointFocus() {
        if (::vtmMapView.isInitialized) {
            if (::currentMarker.isInitialized) {
                ptsCurrent.remove(currentMarker)
            }
            if (::currentMarkerLayer.isInitialized) {
                currentMarkerLayer.removeAllItems();
            }
        }
    }

    private fun initCenterPointFocus(geoPoint: GeoPoint) {
        var `is`: InputStream? = null
        try {
            `is` = resources.openRawResource(R.raw.marker)
            bitmapMarkerCurrent = CanvasAdapter.decodeSvgBitmap(
                `is`,
                (48 * CanvasAdapter.getScale()).toInt(),
                (48 * CanvasAdapter.getScale()).toInt(), 100
            )
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            IOUtils.closeQuietly(`is`)
            currentSymbol = MarkerSymbol(bitmapMarkerCurrent, MarkerSymbol.HotspotPlace.CENTER, false)
            currentMarkerLayer = ItemizedLayer(vtmMapView.map(), ArrayList<MarkerInterface>(), currentSymbol, this)
            vtmMapView.map().layers().add(currentMarkerLayer);
            currentMarker = MarkerItem(
                geoPoint.latitude.toString() + "/" + geoPoint.longitude.toString(),
                "current Location",
                GeoPoint(geoPoint.latitude, geoPoint.longitude))
            ptsCurrent.add(currentMarker)
            currentMarkerLayer.addItems(ptsCurrent);
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastKnownLocation() {
        if (locationManager == null) {
            locationManager = currentContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }
        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l = locationManager.getLastKnownLocation(provider)
            if (l == null) {
                continue
            }
            if (bestLocation == null || l.accuracy < bestLocation!!.accuracy) {
                bestLocation = l
            }
        }
        if (bestLocation != null) {
            longitude = bestLocation.longitude
            latitude = bestLocation.latitude
            val centerMapPosition = MapPosition()
                .setX(MercatorProjection.longitudeToX(longitude))
                .setY(MercatorProjection.latitudeToY(latitude))
                .setZoomLevel(zoomLevel)

            /* set map center position */
            vtmMapView.map().setMapPosition(centerMapPosition)
            vtmMapView.map().viewport().setMapPosition(centerMapPosition)
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastKnownFusedLocation() {
        var location = fusedLocationClient!!.lastLocation!!
        if (location != null) {
            var lastlocation = location.result
            if (lastlocation != null) {
                longitude = lastlocation.longitude
                latitude = lastlocation.latitude
                val centerMapPosition = MapPosition()
                    .setX(MercatorProjection.longitudeToX(longitude))
                    .setY(MercatorProjection.latitudeToY(latitude))
                    .setZoomLevel(zoomLevel)

                /* set map center position */
                vtmMapView.map().setMapPosition(centerMapPosition)
                vtmMapView.map().viewport().setMapPosition(centerMapPosition)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun initMapView() {
        if (useMapViewForTripProgress) {
            if (mapViewFrame != null) {
                vtmMapView = org.oscim.android.MapView(currentContext)
                vtmMapView.map().events.bind(this)
                mapViewFrame.addView(vtmMapView)
                Handler().postDelayed(Runnable {
                    vtmMapView.map().updateMap(true)
                    //mapViewFrame.visibility = View.GONE
                    //mapViewFrame.visibility = View.VISIBLE
                    //vtmMapView.visibility = View.GONE
                    //vtmMapView.visibility = View.VISIBLE
                }, 5000);

                try {
                    if (fusedLocationClient == null) {
                        fusedLocationClient = LocationServices.getFusedLocationProviderClient(currentContext)
                    }
                    if (fusedLocationClient != null) {
                        getLastKnownFusedLocation()
                    } else {
                        getLastKnownLocation()
                    }
                } catch (e: Exception) {
                    getLastKnownLocation()
                }

                var mapFilename = sharedPreferences.getString(getString(R.string.preferences_map_filename_key), "")
                if (!mapFilename.isNullOrEmpty()) {
                    var mapFile = File(mapFilename)
                    if (mapFile.exists()) {
                        handleMapFile(mapFile)
                    } else {
                        Log.e(logTag, "FAILED to load map file " + mapFile.name)
                        Toast.makeText(currentContext, "FAILED to load map file " + mapFile.name, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    var alertDialogBuilder = AlertDialog.Builder(currentContext)
                    alertDialogBuilder.setTitle("Select Map File")
                    alertDialogBuilder.setMessage("No map files have been selected. Continue to select a map file?")
                    alertDialogBuilder.setPositiveButton("Continue", DialogInterface.OnClickListener {
                            dialog, which ->
                                handleNewMapFile()
                    })
                    alertDialogBuilder.setNegativeButton("Cancel", DialogInterface.OnClickListener {
                            dialog, which ->
                                Log.w(logTag, "No map file selected or loaded, user canceled")
                    })
                    alertDialogBuilder.create().show()
                }

                var tcxgpxFilename = sharedPreferences.getString(getString(R.string.preferences_tcx_gpx_track_filename_key), "")
                if (!tcxgpxFilename.isNullOrEmpty()) {
                    if (tcxgpxFilename.lowercase().contains("tcx")) {
                        var tcxFile = File(tcxgpxFilename)
                        if (tcxFile.exists()) {
                            handleTcxFile(tcxFile)
                        } else {
                            Log.e(logTag, "FAILED to load Tcx/Gpx file " + tcxFile.name)
                            Toast.makeText(currentContext, "FAILED to load Tcx/Gpx file " + tcxFile.name, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        var gpxFile = File(tcxgpxFilename)
                        if (gpxFile.exists()) {
                            handleGpxFile(gpxFile)
                        } else {
                            Log.e(logTag, "FAILED to load Tcx/Gpx file " + gpxFile.name)
                            Toast.makeText(currentContext, "FAILED to load Tcx/Gpx file " + gpxFile.name, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Log.w(logTag, "No Tcx/Gpx file selected or loaded")
                }
            }
        }
    }

    override fun onItemSingleTapUp(index: Int, item: MarkerInterface?): Boolean {
        return true
    }

    override fun onItemLongPress(index: Int, item: MarkerInterface?): Boolean {
        return true
    }

    override fun onMapEvent(event: Event?, mapPosition: MapPosition?) {
        if (event == org.oscim.map.Map.SCALE_EVENT ||
            event == org.oscim.map.Map.UPDATE_EVENT ||
            event == org.oscim.map.Map.POSITION_EVENT) {
            if (debugLog) Log.i(logTag, "onMapEven called ZL:" + mapPosition!!.zoomLevel)
            zoomLevel = mapPosition!!.zoomLevel
        }
    }

    private lateinit var gravity: FloatArray
    private lateinit var geomagnetic: FloatArray
    private var sensorAirPress: Float = 0.0f
    private var smoothedAngle: Double = 0.0
    private val smootFactor: Double = 20.0
    private var hasGeomagneticVal = false
    private var hasAirPressureVal = false
    override fun onSensorChanged(event: SensorEvent?) {

        if (event != null) {
            if (event.sensor != null) {
                if (event.values != null) {
                    if (event?.sensor!!.type == Sensor.TYPE_ACCELEROMETER) {
                        gravity = event!!.values;
                    }
                    if (event?.sensor!!.type == Sensor.TYPE_MAGNETIC_FIELD) {
                        geomagnetic = event!!.values;
                        hasGeomagneticVal = true
                    }
                    if (event?.sensor!!.type == Sensor.TYPE_PRESSURE) {
                        sensorAirPress = event?.values!!.get(0)
                        setAirPressure("${sensorAirPress.toInt()} hPa", "AIR P.", "", 0)
                    }
                }
            }
        }

        if (::gravity.isInitialized && ::geomagnetic.isInitialized &&
            gravity != null && geomagnetic != null) {

            val R = FloatArray(9)
            val I = FloatArray(9)
            val success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)
            if (success) {
                hasGeomagneticVal = true

                val orientation = FloatArray(3)
                SensorManager.getOrientation(R, orientation)

                var screenRotation = 0
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.R) {
                    screenRotation = requireActivity().resources.configuration.orientation
                } else {
                    screenRotation = requireActivity().windowManager.defaultDisplay.rotation
                }
                when (screenRotation) {
                    Surface.ROTATION_0 -> {
                        orientation[1] = -orientation[1]
                    }
                    Surface.ROTATION_90 -> {
                        orientation[0] += (Math.PI / 2f).toFloat()
                        val tmpOldPitch = orientation[1]
                        orientation[1] = -orientation[2]
                        orientation[2] = -tmpOldPitch
                    }
                    Surface.ROTATION_180 -> {
                        orientation[0] =
                            (if (orientation[0] > 0f) (orientation[0] - Math.PI) else (orientation[0] + Math.PI)).toFloat() // offset
                        orientation[2] = -orientation[2]
                    }
                    Surface.ROTATION_270 -> {
                        orientation[0] -= (Math.PI / 2).toFloat()
                        val tmpOldPitch = orientation[1]
                        orientation[1] = orientation[2]
                        orientation[2] = tmpOldPitch
                    }
                }
                var angle = Math.toDegrees(orientation[0].toDouble()) - 90
                if (angle < 0) {
                    angle += 360
                }
                if (angle > 360) {
                    angle -= 360
                }

                if (Math.abs(smoothedAngle - angle) > 4) {
                    smoothedAngle = angle
                }
                smoothedAngle += (angle - smoothedAngle) / smootFactor

                if (angle < 0) {
                    angle += 360
                }
                if (angle > 360) {
                    angle -= 360
                }

                angle -= declination
                if (angle < 0) {
                    angle += 360
                }
                if (angle > 360) {
                    angle -= 360
                }

                var bearingDirection = degreesToCardinal(angle.toFloat())

                setMagBearing(bearingDirection, "M|BEARING", "${angle.toInt()}° ", 0)
            }
        }
    }

    fun handleGpsServiceFixQuality() {
        var color: Int = currentContext.getColor(R.color.red)
        trackingImage.alpha = 0.5f
        when (fixQuality) {
            0 -> {
                color = currentContext.getColor(R.color.red)
            }
            1 -> {
                color = currentContext.getColor(R.color.green)
            }
            2 -> {
                color = currentContext.getColor(R.color.green)
            }
            3 -> {
                color = currentContext.getColor(R.color.yellow)
            }
            4 -> {
                color = currentContext.getColor(R.color.green)
            }
            5 -> {
                color = currentContext.getColor(R.color.green)
            }
            6 -> {
                color = currentContext.getColor(R.color.green)
            }
            else -> {
                color = currentContext.getColor(R.color.white)
                trackingImage.alpha = 0.9f
            }
        }
        trackingImage.imageTintList = ColorStateList.valueOf(color)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        when (accuracy) {
            SensorManager.SENSOR_STATUS_UNRELIABLE -> {
                if (sensor!!.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    compassIndicatorColor = currentContext.getColor(R.color.red)
                }
            }
            SensorManager.SENSOR_STATUS_ACCURACY_LOW -> {
                if (sensor!!.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    compassIndicatorColor = currentContext.getColor(R.color.orange)
                }
            }
            SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM -> {
                if (sensor!!.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    compassIndicatorColor = currentContext.getColor(R.color.yellow)
                }
            }
            SensorManager.SENSOR_STATUS_ACCURACY_HIGH -> {
                if (sensor!!.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    compassIndicatorColor = currentContext.getColor(R.color.green)
                }
            }
        }
        if (::compassImage.isInitialized) {
            compassImage.imageTintList = ColorStateList.valueOf(compassIndicatorColor)
            compassImage.alpha = 0.5f
        }
    }
}
