package com.draekko.cyclometer

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.draekko.cyclometer.util.getUserHeight
import com.draekko.cyclometer.util.getUserWeight
import javax.inject.Inject

class BiometricsRepository @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    val sex = sharedPreferences.getString(
        CyclometerApp.instance.getString(
            R.string.preference_key_biometrics_user_sex
        ), ""
    )
    val dob = sharedPreferences.getString(
        CyclometerApp.instance
            .getString(R.string.preference_key_biometrics_user_dob), ""
    )
    val maxHeartRate = sharedPreferences.getString(
        CyclometerApp.instance
            .getString(R.string.preference_key_biometrics_user_maxHeartRate), ""
    )
    val vo2max = sharedPreferences.getString(
        CyclometerApp.instance
            .getString(R.string.preference_key_biometrics_user_vo2max), ""
    )

    private val _restingHeartRate = MutableLiveData<String>(
        sharedPreferences.getString(
            CyclometerApp.instance
                .getString(R.string.preference_key_biometrics_user_restingHeartRate), ""
        )
    )
    var restingHeartRate: LiveData<String>
        get() {
            return _restingHeartRate
        }
        set(newValue) {
            sharedPreferences.edit {
                this.putString(
                    CyclometerApp.instance
                        .getString(R.string.preference_key_biometrics_user_restingHeartRate),
                    newValue.value
                )
            }
        }

    private val _height = MutableLiveData<Float>(getUserHeight(sharedPreferences))
    val height: LiveData<Float>
        get() {
            return _height
        }

    private val _weight = MutableLiveData<Float>(getUserWeight(sharedPreferences))
    val weight: LiveData<Float>
        get() {
            return _weight
        }
}