package com.draekko.cyclometer

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
import android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.View.SYSTEM_UI_LAYOUT_FLAGS
import android.view.WindowManager
import android.view.WindowManager.LayoutParams
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.draekko.cyclometer.events.BluetoothActionEvent
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

@AndroidEntryPoint
class PreferencesActivity : AppCompatActivity() {

    val logTag: String = this.javaClass.simpleName

    private val uiOptions = SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
            SYSTEM_UI_LAYOUT_FLAGS or
            SYSTEM_UI_FLAG_LAYOUT_STABLE or
            SYSTEM_UI_FLAG_HIDE_NAVIGATION or
            SYSTEM_UI_FLAG_FULLSCREEN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(logTag, "onCreate")

        this.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        this.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)

        if (Build.VERSION.SDK_INT < 30) {
            val decorView = window.decorView
            decorView.systemUiVisibility = uiOptions
        } else {
            val decorView = this.window.decorView
            decorView.systemUiVisibility = uiOptions
            //if (!hideSystemBars()) {
            //    val decorView = this.window.decorView
            //    decorView.systemUiVisibility = uiOptions
            //}
        }

        setContentView(R.layout.activity_preferences)
        findNavController(R.id.nav_host_fragment_preferences).setGraph(
            R.navigation.preferences_nav_graph,
            intent.extras
        )
        Log.d(logTag, "$intent")
        intent.data?.let {
            Log.d(logTag, "${intent.data!!.query}")
            Log.d(logTag, "${intent.data!!.getQueryParameter("code")}")
            if (it.getQueryParameter("scope")?.contains("activity:write") == true) {
                //start worker with code
                WorkManager.getInstance(applicationContext).enqueue(
                    OneTimeWorkRequestBuilder<StravaTokenExchangeWorker>()
                        .setInputData(workDataOf("authCode" to it.getQueryParameter("code")))
                        .build()
                )
            } else {
                AlertDialog.Builder(applicationContext).apply {
                    setTitle("Cannot upload to Strava")
                    setMessage("Please select \"Sync with Strava\" again and check the box next to \"Upload activities from Cyclometer to Strava.\"")
                }.create().show()
            }
        }

        setSupportActionBar(findViewById(R.id.preferences_toolbar))

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Deprecated("Deprecated in Java")
    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //Required for Google Sign-in
        Log.d(logTag, "onActivityResult")
        Log.d(logTag, "$data")
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(this.javaClass.simpleName, "onActivityResult: ${resultCode}")
    }

    override fun onStart() {
        super.onStart()
        Log.d(this.javaClass.simpleName, "onStart")
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        Log.d(this.javaClass.simpleName, "onStop")
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onBluetoothActionEvent(event: BluetoothActionEvent) {
        Log.d(this.javaClass.simpleName, "Show enable bluetooth dialog")
        startActivity(Intent(event.action))
    }
}
