package com.draekko.cyclometer

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import androidx.core.text.isDigitsOnly
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.draekko.cyclometer.data.UserSexEnum
import com.draekko.cyclometer.util.*
import kotlin.math.roundToInt
import kotlin.reflect.KProperty


class BiometricsViewModel constructor(
    private val sharedPreferences: SharedPreferences,
) : BaseObservable() {
    private val logTag = "BIOMETRICS_VM"

    private val keyUserSex =
        CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_sex)

    class ObservablePreference constructor(
        resId: Int,
        private val sharedPreferences: SharedPreferences,
        private val observable: BaseObservable,
    ) {
        private val prefKey = CyclometerApp.instance.getString(resId)

        operator fun getValue(
            biometricsViewModel: BiometricsViewModel,
            property: KProperty<*>,
        ) = sharedPreferences.getString(prefKey, "")

        operator fun setValue(
            biometricsViewModel: BiometricsViewModel,
            property: KProperty<*>,
            newValue: String?,
        ) {
            sharedPreferences.edit {
                this.putString(prefKey, newValue)
            }
            observable.notifyPropertyChanged(BR.maxHrHint)
            observable.notifyPropertyChanged(BR.vo2maxHint)
            observable.notifyPropertyChanged(BR.vo2maxDefined)
            observable.notifyPropertyChanged(BR.instructionText)
        }
    }

    private fun makeObservablePreference(resId: Int) =
        ObservablePreference(resId, sharedPreferences, this)

    var sex: Int
        @Bindable
        get() =
            try {
                sharedPreferences.getString(keyUserSex, "").let {
                    when (it) {
                        UserSexEnum.MALE.name -> R.id.preference_biometrics_sex_male
                        UserSexEnum.FEMALE.name -> R.id.preference_biometrics_sex_female
                        else -> -1
                    }
                }
            } catch (e: ClassCastException) {
                -1
            }
        set(newValue) {
            Log.d(logTag, "Sex $newValue")
            sharedPreferences.edit {
                this.putString(
                    keyUserSex, when (newValue) {
                        R.id.preference_biometrics_sex_male -> UserSexEnum.MALE.name
                        R.id.preference_biometrics_sex_female -> UserSexEnum.FEMALE.name
                        else -> null
                    }
                )
            }
        }


    @get:Bindable
    var dob by makeObservablePreference(R.string.preference_key_biometrics_user_dob)

    @get:Bindable
    var vo2max by makeObservablePreference(R.string.preference_key_biometrics_user_vo2max)

    @get:Bindable
    var maxHeartRate by makeObservablePreference(R.string.preference_key_biometrics_user_maxHeartRate)

    var weight
        @Bindable
        get() =
            try {
                sharedPreferences.getString(CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_weight), "")
            } catch (e: ClassCastException) {
                ""
            }
        set(newValue) {
            sharedPreferences.edit {
                this.putString(
                    CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_weight),
                    newValue
                )
            }
            notifyChange()
        }

    var height
        @Bindable
        get() =
            try {
                sharedPreferences.getString(CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_height), "")
            } catch (e: ClassCastException) {
                ""
            }
        set(newValue) {
            sharedPreferences.edit {
                this.putString(
                    CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_height),
                    newValue
                )
            }
            notifyChange()
        }

    var restingHeartRate
        @Bindable
        get() =
            try {
                sharedPreferences.getString(CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_restingHeartRate),"")
            } catch (e: ClassCastException) {
                ""
            }
        set(newValue) {
            sharedPreferences.edit {
                this.putString(
                    CyclometerApp.instance.getString(R.string.preference_key_biometrics_user_restingHeartRate),
                    newValue
                )
            }
            notifyChange()
        }

    @get:Bindable
    val heightHint: String
        get() =
            when (sharedPreferences.getString(
                CyclometerApp.instance.getString(R.string.preference_key_system_of_measurement),
                "1"
            )) {
                "1" -> "Height (inches)"
                else -> "Height (cm)"
            }

    @get:Bindable
    val weightHint: String
        get() =
            when (sharedPreferences.getString(
                CyclometerApp.instance.getString(R.string.preference_key_system_of_measurement),
                "1"
            )) {
                "1" -> "Weight (lbs)"
                else -> "Weight (kg)"
            }

    @get:Bindable
    val vo2maxHint: String
        get() = try {
            "VO2Max ${
                if (vo2max.isNullOrEmpty()) restingHeartRate?.takeIf { it.isNotBlank() && it.isDigitsOnly() }
                    ?.toInt()
                    ?.let {
                        estimateVo2Max(
                            it,
                            getUserMaxHeartRate(sharedPreferences) ?: estimateMaxHeartRate(
                                getUserAge(sharedPreferences)?.roundToInt()!!
                            )
                        )
                    }?.let {
                        "(est ${it.toInt()} mL/kg/min)"
                    } ?: "(enter resting/max HR to estimate)" else "(enter resting/max HR to estimate)"
            }"
        } catch (e: NullPointerException) {
            "VO2Max (enter resting/max HR to estimate)"
        }

    @get:Bindable
    val maxHrHint: String
        get() = "Max heart rate ${
            getUserAge(sharedPreferences)?.let {
                "(${estimateMaxHeartRate(it.roundToInt())} based on age)"
            } ?: "(or use age to estimate)"
        }"

    @get:Bindable
    val isVo2maxDefined: Boolean
        get() = getUserVo2max(sharedPreferences) != null

    @get:Bindable
    val instructionText: String
        get() = getCaloriesEstimateType(sharedPreferences)
}