package com.draekko.cyclometer

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
import android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.View.SYSTEM_UI_LAYOUT_FLAGS
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import androidx.work.BackoffPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.draekko.cyclometer.data.Trip
import com.draekko.cyclometer.util.checkSubDirs
import com.draekko.cyclometer.util.isStravaSynced
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val logTag = MainActivity::class.simpleName
    private var newRidesDisabledDialog: AlertDialog? = null
    private val viewModel: MainActivityViewModel by viewModels()

    private val PERMISSION_REQUEST_ALL_FILES_ACCESS = 0xF006
    private val REQUEST_CODE_WRITE_SETTINGS_PERMISSION = 0xE006

    private lateinit var startTripHandler: () -> Unit

    private val requestNotificationPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        for (entry in permissions.entries) {

            Log.d("NOTIFICATION_PERMISSIONS", "${entry.key} = ${entry.value}")
            if (entry.key == Manifest.permission.POST_NOTIFICATIONS) {
                if (entry.value
                ) {
                    try {
                        startTripHandler()
                    } catch (e: IllegalArgumentException) {
                        Log.d(logTag, "CANNOT HANDLE MULTIPLE TRIP START TOUCHES")
                    }
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.

                    newRidesDisabledDialog?.show()
                }
            }
        }
    }

    private val requestLocationPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        for (entry in permissions.entries) {

            Log.d("LOCATION_PERMISSIONS", "${entry.key} = ${entry.value}")
            if (entry.key == Manifest.permission.ACCESS_FINE_LOCATION) {
                if (entry.value
                ) {
                    try {
                        startTripHandler()
                    } catch (e: IllegalArgumentException) {
                        Log.d(logTag, "CANNOT HANDLE MULTIPLE TRIP START TOUCHES")
                    }
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.

                    newRidesDisabledDialog?.show()
                }
            }
        }
    }

    private val requestBtConnectPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        for (entry in permissions.entries) {

            Log.d("BT_CONNECT_PERMISSIONS", "${entry.key} = ${entry.value}")
            if (entry.key == Manifest.permission.BLUETOOTH_CONNECT) {
                if (entry.value
                ) {
                    try {
                        startTripHandler()
                    } catch (e: IllegalArgumentException) {
                        Log.d(logTag, "CANNOT HANDLE MULTIPLE TRIP START TOUCHES")
                    }
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.

                    newRidesDisabledDialog?.show()
                }
            }
        }
    }

    private val requestBtScanPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        for (entry in permissions.entries) {

            Log.d("BT_SCAN_PERMISSIONS", "${entry.key} = ${entry.value}")
            if (entry.key == Manifest.permission.BLUETOOTH_SCAN) {
                if (entry.value
                ) {
                    try {
                        startTripHandler()
                    } catch (e: IllegalArgumentException) {
                        Log.d(logTag, "CANNOT HANDLE MULTIPLE TRIP START TOUCHES")
                    }
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.

                    newRidesDisabledDialog?.show()
                }
            }
        }
    }

    private fun initializeBtConnectService() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.BLUETOOTH_CONNECT) -> {
                this.let {
                    AlertDialog.Builder(it).apply {
                        setPositiveButton(
                            "PROCEED"
                        ) { _, _ ->
                            // User clicked OK button
                            requestBtConnectPermissions.launch(
                                arrayOf(
                                    Manifest.permission.BLUETOOTH_CONNECT
                                )
                            )
                        }
                        setNegativeButton(
                            "DENY"
                        ) { _, _ ->
                            // User cancelled the dialog
                            Log.d("TRIP_SUMMARIES", "CLICKED DENY")
                            newRidesDisabledDialog?.show()
                        }
                        setTitle("Grant Bluetooth Connect Access")
                        setMessage("This app requires connection access to bluetooth devices for collecting sensor data.")
                    }.create()
                }.show()
            }

            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestBtConnectPermissions.launch(
                    arrayOf(
                        Manifest.permission.BLUETOOTH_CONNECT
                    )
                )
            }
        }
    }

    private fun initializeBtScanService() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.BLUETOOTH_SCAN) -> {
                this.let {
                    AlertDialog.Builder(it).apply {
                        setPositiveButton(
                            "PROCEED"
                        ) { _, _ ->
                            // User clicked OK button
                            requestBtScanPermissions.launch(
                                arrayOf(
                                    Manifest.permission.BLUETOOTH_SCAN
                                )
                            )
                        }
                        setNegativeButton(
                            "DENY"
                        ) { _, _ ->
                            // User cancelled the dialog
                            Log.d("TRIP_SUMMARIES", "CLICKED DENY")
                            newRidesDisabledDialog?.show()
                        }
                        setTitle("Grant Bluetooth Scan Access")
                        setMessage("Due to changes by Google this app requires scan access to find bluetooth devices.")
                    }.create()
                }.show()
            }

            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestBtScanPermissions.launch(
                    arrayOf(
                        Manifest.permission.BLUETOOTH_SCAN
                    )
                )
            }
        }
    }

    private fun initializeNotificationService() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS) -> {
                this.let {
                    AlertDialog.Builder(it).apply {
                        setPositiveButton(
                            "PROCEED"
                        ) { _, _ ->
                            // User clicked OK button
                            requestNotificationPermissions.launch(
                                arrayOf(
                                    Manifest.permission.POST_NOTIFICATIONS
                                )
                            )
                        }
                        setNegativeButton(
                            "DENY"
                        ) { _, _ ->
                            // User cancelled the dialog
                            Log.d("TRIP_SUMMARIES", "CLICKED DENY")
                            newRidesDisabledDialog?.show()
                        }
                        setTitle("Grant Push Notifications")
                        setMessage("Due to changes by Google this app requires to push notifications for services.")
                    }.create()
                }.show()
            }

            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestNotificationPermissions.launch(
                    arrayOf(
                        Manifest.permission.POST_NOTIFICATIONS
                    )
                )
            }
        }
    }

    private fun initializeLocationService() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                this.let {
                    AlertDialog.Builder(it).apply {
                        setPositiveButton(
                            "PROCEED"
                        ) { _, _ ->
                            // User clicked OK button
                            requestLocationPermissions.launch(
                                arrayOf(
                                    Manifest.permission.ACCESS_FINE_LOCATION
                                )
                            )
                        }
                        setNegativeButton(
                            "DENY"
                        ) { _, _ ->
                            // User cancelled the dialog
                            Log.d("TRIP_SUMMARIES", "CLICKED DENY")
                            newRidesDisabledDialog?.show()
                        }
                        setTitle("Grant Location Access")
                        setMessage("This app collects location data to enable the in-ride dashboard, and post-ride maps and graphs even when the app is closed or not in use.\n\nCyclometer needs access to GPS location data and background location data to calculate speed and distance traveled during your rides. Data is only collected and recorded while rides are in progress. Data collected is stored on your device for your personal use. Your data is not sent to any third parties, including the developer (unless you enable the Google Fit integration). Please select PROCEED and then grant Cyclometer access to location data.")
                    }.create()
                }.show()
            }

            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestLocationPermissions.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                )
            }
        }
    }

    private fun handleFabClick(): View.OnClickListener = View.OnClickListener {
        // TODO: Multiple touches causes fatal exception
        try {
            when (PackageManager.PERMISSION_GRANTED) {
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                -> startTripHandler()

                else -> initializeLocationService()
            }
        } catch (e: IllegalArgumentException) {
            Log.d("TRIP_SUMMARIES", "CANNOT HANDLE MULTIPLE TRIP START TOUCHES")
        }
    }

    override fun onResume() {
        super.onResume()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                return;
            }
        }

        // Check app directories exists
        checkSubDirs(this)

        Log.d(logTag, "MainActivity onResume")
        if (isStravaSynced(this)) {
            WorkManager.getInstance(this).beginUniqueWork(
                "StravaSyncTripsWorker",
                ExistingWorkPolicy.REPLACE,
                OneTimeWorkRequestBuilder<StravaSyncTripsWorker>()
                    .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 15, TimeUnit.MINUTES)
                    .build()
            ).enqueue()
        }
    }

    private fun showWhatsNewDialog() {
        getString(R.string.whats_new_message).takeIf { it.isNotEmpty() }?.let { message ->
            AlertDialog.Builder(this).apply {
                setTitle("What's new in version ${BuildConfig.VERSION_CODE}")
                setMessage(message)
                setPositiveButton(
                    "OK"
                ) { _, _ ->
                    Log.d(logTag, "What's new acknowledged")
                }
            }.create().show()
        }
    }

    private fun checkUserCurrentVersion() {
        Log.v(logTag, "checkUserCurrentVersion")
        PreferenceManager.getDefaultSharedPreferences(this)
            .getInt(getString(R.string.preference_key_user_current_version), -1)
            .let { currentVersion ->
                if (currentVersion != BuildConfig.VERSION_CODE) {
                    Log.d(logTag, "New app version detected. Version ${BuildConfig.VERSION_CODE}")
                    PreferenceManager.getDefaultSharedPreferences(this)
                        .edit {
                            putInt(
                                getString(R.string.preference_key_user_current_version),
                                BuildConfig.VERSION_CODE
                            )
                            commit()
                        }
                    //if (currentVersion < 59) {
                    //    showWhatsNewDialog()
                    //}
                }
            }
    }

    override fun onStart() {
        super.onStart()
    }

    private fun hideSystemBars(): Boolean {
        val windowInsetsController =
            ViewCompat.getWindowInsetsController(window.decorView) ?: return false
        // Configure the behavior of the hidden system bars
        windowInsetsController.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

        // Hide both the status bar and the navigation bar
        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
        return true
    }

    val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        val activityIntent =
            Intent(this, MainActivity::class.java)
        startActivity(activityIntent)
        finish()
        finishAffinity()
    }

    private val uiOptions = SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
            SYSTEM_UI_LAYOUT_FLAGS or
            SYSTEM_UI_FLAG_LAYOUT_STABLE or
            SYSTEM_UI_FLAG_HIDE_NAVIGATION or
            SYSTEM_UI_FLAG_FULLSCREEN

    private lateinit var toolbar: ActionBar
    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        super.onCreate(savedInstanceState)
        Log.d(logTag, "onCreate")
        var context: Context = this

        this.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        this.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)
        //this.window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)


        if (Build.VERSION.SDK_INT < 30) {
            val decorView = window.decorView
            decorView.systemUiVisibility = uiOptions
        } else {
            val decorView = this.window.decorView
            decorView.systemUiVisibility = uiOptions
            //if (!hideSystemBars()) {
            //    val decorView = this.window.decorView
            //    decorView.systemUiVisibility = uiOptions
            //}
        }
        try {
            toolbar = supportActionBar!!
            if (toolbar != null) {
                toolbar.hide()
            }
        } catch (e: Exception) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("ACTION REQUIRED")
                builder.setMessage("Due to changes by Google to Android since version 11 file access " +
                        "requires the user to enable the appropriate permission to continue using this " +
                        "app. Clicking YES will bring you to the Settings windows to enable file access.")

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    val uri = Uri.fromParts(
                        "package",
                        getPackageName(),
                        null
                    )

                    val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                    intent.setData(uri)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        startForResult.launch(intent)
                    } else {
                        startActivityForResult(intent, PERMISSION_REQUEST_ALL_FILES_ACCESS)
                    }
                }

                builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    Toast.makeText(this, "File access denied", Toast.LENGTH_LONG).show()
                    Handler().postDelayed(kotlinx.coroutines.Runnable {
                        finish()
                        finishAffinity()
                    } , 3500)
                }

                builder.setOnCancelListener {
                    Toast.makeText(this, "File access denied", Toast.LENGTH_LONG).show()
                    Handler().postDelayed(kotlinx.coroutines.Runnable {
                        finish()
                        finishAffinity()
                    } , 3500)
                }

                builder.show()

                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(context)) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("ACTION REQUIRED")
                builder.setMessage("Android requires the user to enable the appropriate permission to " +
                        "continue using this app. Clicking YES will bring you to the Settings windows " +
                        "to enable settings access.")

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    val uri = Uri.fromParts(
                        "package",
                        getPackageName(),
                        null
                    )
                    val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
                    intent.setData(uri)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        startForResult.launch(intent)
                    } else {
                        startActivityForResult(intent, REQUEST_CODE_WRITE_SETTINGS_PERMISSION)
                    }
                    /*
                    Intent(
                        Settings.ACTION_MANAGE_WRITE_SETTINGS,
                        Uri.parse("package:${context.packageName}")
                    ).let {
                        startActivityForResult(it, REQUEST_CODE_WRITE_SETTINGS_PERMISSION)
                    }
                     */
                }

                builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    Toast.makeText(this, "File access denied", Toast.LENGTH_LONG).show()
                    Handler().postDelayed(kotlinx.coroutines.Runnable {
                        finish()
                        finishAffinity()
                    } , 3500)
                }

                builder.setOnCancelListener {
                    Toast.makeText(this, "File access denied", Toast.LENGTH_LONG).show()
                    Handler().postDelayed(kotlinx.coroutines.Runnable {
                        finish()
                        finishAffinity()
                    } , 3500)
                }

                builder.show()

                return
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ActivityCompat.checkSelfPermission(
                    baseContext,
                    Manifest.permission.BLUETOOTH_CONNECT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                initializeBtConnectService()
            }
            if (ActivityCompat.checkSelfPermission(
                    baseContext,
                    Manifest.permission.BLUETOOTH_SCAN
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                initializeBtScanService()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ActivityCompat.checkSelfPermission(
                    baseContext,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                initializeNotificationService()
            }
        }


        title = ""
        when (intent.getStringExtra("destinationView")) {
            TripInProgressFragment.toString() -> {
                setTheme(R.style.CylometerTheme)
            }
            else -> {
                if (BuildConfig.DEBUG) {
                    var view: View? = layoutInflater.inflate(R.layout.activity_main, null)
                    setContentView(view)
                } else {
                    setContentView(R.layout.activity_main)
                }
                setTheme(R.style.CylometerTheme)
            }
        }
        setSupportActionBar(findViewById(R.id.toolbar_main))

        findViewById<BottomNavigationView>(R.id.main_activity_bottom_menu).setupWithNavController(
            findNavController(R.id.nav_host_fragment)
        )

        checkUserCurrentVersion()

        newRidesDisabledDialog = this.let {
            AlertDialog.Builder(it).apply {
                setPositiveButton(
                    "OK"
                ) { _, _ ->
                    Log.d("ALERT DIALOG", "CLICKED")
                }
                setTitle("New rides disabled")
                setMessage("Please access Cyclometer app settings and grant permission to Location data. You will not be able to start a ride until location permission is granted.\n\nCyclometer needs access to GPS location data and background location data to calculate speed and distance traveled during your rides. Data is only recorded while rides are in progress. Data collected is stored on your device for your personal use. Your data is not sent to any third parties, including the developer (unless you enable the Google Fit integration).")
            }.create()
        }

        findViewById<FloatingActionButton>(R.id.fab).apply {
            isEnabled = false
            visibility = View.INVISIBLE
            viewModel.latestTrip.observe(this@MainActivity) { trip: Trip? ->
                startTripHandler = {
                    findNavController(R.id.nav_host_fragment).navigate(
                        TripSummariesFragmentDirections.actionStartTrip(
                            trip?.takeIf { it.inProgress }?.id ?: -1
                        )
                    )
                }
                isEnabled = true
                visibility = View.VISIBLE
                setOnClickListener(handleFabClick())
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(
            window.decorView
        ) { v: View?, insets: WindowInsetsCompat? ->
            ViewCompat.onApplyWindowInsets(
                v!!, WindowInsetsCompat.CONSUMED
            )
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (requestCode == REQUEST_CODE_WRITE_SETTINGS_PERMISSION) {
            val activityIntent =
                Intent(this, MainActivity::class.java)
            startActivity(activityIntent)
            finish()
            finishAffinity()
        } else if (requestCode == PERMISSION_REQUEST_ALL_FILES_ACCESS) {
            val activityIntent =
                Intent(this, MainActivity::class.java)
            startActivity(activityIntent)
            finish()
            finishAffinity()
        } else {
            super.onActivityResult(requestCode, resultCode, resultData)
        }
    }
}
