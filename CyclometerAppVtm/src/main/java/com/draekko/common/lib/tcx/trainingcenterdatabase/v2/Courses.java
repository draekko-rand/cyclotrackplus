package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for Courses_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Courses_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CourseFolder" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}CourseFolder_t"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class Courses {

    protected CourseFolder courseFolder;
    protected Extensions extensions;

    /**
     * Gets the value of the courseFolder property.
     * 
     * @return
     *     possible object is
     *     {@link CourseFolder }
     *     
     */
    public CourseFolder getCourseFolder() {
        return courseFolder;
    }

    /**
     * Sets the value of the courseFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link CourseFolder }
     *     
     */
    public void setCourseFolder(CourseFolder value) {
        this.courseFolder = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
