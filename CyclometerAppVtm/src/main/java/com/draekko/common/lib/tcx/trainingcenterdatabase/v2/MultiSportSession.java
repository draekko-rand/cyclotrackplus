package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>Java class for MultiSportSession_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiSportSession_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FirstSport" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}FirstSport_t"/>
 *         &lt;element name="NextSport" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}NextSport_t" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class MultiSportSession {

    protected XMLGregorianCalendar id;
    protected FirstSport firstSport;
    protected List<NextSport> nextSport;
    protected String notes;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setId(XMLGregorianCalendar value) {
        this.id = value;
    }

    /**
     * Gets the value of the firstSport property.
     * 
     * @return
     *     possible object is
     *     {@link FirstSport }
     *     
     */
    public FirstSport getFirstSport() {
        return firstSport;
    }

    /**
     * Sets the value of the firstSport property.
     * 
     * @param value
     *     allowed object is
     *     {@link FirstSport }
     *     
     */
    public void setFirstSport(FirstSport value) {
        this.firstSport = value;
    }

    /**
     * Gets the value of the nextSport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nextSport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNextSport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NextSport }
     * 
     * 
     */
    public List<NextSport> getNextSport() {
        if (nextSport == null) {
            nextSport = new ArrayList<NextSport>();
        }
        return this.nextSport;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

}
