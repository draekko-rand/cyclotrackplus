package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for Sport_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Sport_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/UserProfile/v1}Token_t">
 *     &lt;enumeration value="Running"/>
 *     &lt;enumeration value="Biking"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum Sport {

    RUNNING("Running"),
    BIKING("Biking"),
    OTHER("Other");
    private final String value;

    Sport(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Sport fromValue(String v) {
        for (Sport c: Sport.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
