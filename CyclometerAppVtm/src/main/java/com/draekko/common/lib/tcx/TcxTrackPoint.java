package com.draekko.common.lib.tcx;

import java.util.HashMap;
import java.util.Map;

public class TcxTrackPoint {

    private float latitude;
    private float longitude;
    private float elevation;
    private long time;
    private float distance;
    private int cadence;
    private int hr_value;
    private Map tpx_ext;

    private String [] hashMapKeys = { "time", "longitude", "latitude", "distance", "elevation", "hr_value",  "cadence" };

    private Map<String, Object> tpx = new HashMap<>();

    public TcxTrackPoint() {
        initMap();
    }

    public void initMap() {
        tpx = new HashMap<>();
        tpx.put(hashMapKeys[0], time);
        tpx.put(hashMapKeys[1], longitude);
        tpx.put(hashMapKeys[2], latitude);
        tpx.put(hashMapKeys[3], distance);
        tpx.put(hashMapKeys[4], distance);
        tpx.put(hashMapKeys[5], elevation);
        tpx.put(hashMapKeys[6], hr_value);
        tpx.put(hashMapKeys[7], cadence);
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public void setHrValue(int hr_value) {
        this.hr_value = hr_value;
    }

    public void setCadence(int cadence) {
        this.cadence = cadence;
    }

    public void setTpxExt(Map tpx_ext) {
        this.tpx_ext = tpx_ext;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getElevation() {
        return elevation;
    }

    public long getTime() {
        return time;
    }

    public float getDistance() {
        return distance;
    }

    public int getHrValue() {
        return hr_value;
    }

    public int getCadence() {
        return cadence;
    }

    public Map getTpxExt() {
        return tpx_ext;
    }
}
