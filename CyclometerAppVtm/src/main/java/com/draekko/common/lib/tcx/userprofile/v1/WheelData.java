package com.draekko.common.lib.tcx.userprofile.v1;

import java.math.BigInteger;

/**
 * <p>Java class for WheelData_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WheelData_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SizeMillimeters" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AutoWheelSize" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class WheelData {

    protected BigInteger sizeMillimeters;
    protected Extensions extensions;
    protected boolean autoWheelSize;

    /**
     * Gets the value of the sizeMillimeters property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSizeMillimeters() {
        return sizeMillimeters;
    }

    /**
     * Sets the value of the sizeMillimeters property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSizeMillimeters(BigInteger value) {
        this.sizeMillimeters = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

    /**
     * Gets the value of the autoWheelSize property.
     * 
     */
    public boolean isAutoWheelSize() {
        return autoWheelSize;
    }

    /**
     * Sets the value of the autoWheelSize property.
     * 
     */
    public void setAutoWheelSize(boolean value) {
        this.autoWheelSize = value;
    }

}
