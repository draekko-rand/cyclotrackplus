package com.draekko.common.lib.tcx.userprofile.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for AbstractProfileActivity_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractProfileActivity_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MaximumHeartRateBpm" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}HeartRateInBeatsPerMinute_t"/>
 *         &lt;element name="GearWeightKilograms" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="HeartRateZones" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}ProfileHeartRateZone_t" maxOccurs="5" minOccurs="5"/>
 *         &lt;element name="SpeedZones" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}ProfileSpeedZone_t" maxOccurs="10" minOccurs="10"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public abstract class AbstractProfileActivity {

    protected HeartRateInBeatsPerMinute maximumHeartRateBpm;
    protected double gearWeightKilograms;
    protected List<ProfileHeartRateZone> heartRateZones;
    protected List<ProfileSpeedZone> speedZones;
    protected Extensions extensions;

    /**
     * Gets the value of the maximumHeartRateBpm property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public HeartRateInBeatsPerMinute getMaximumHeartRateBpm() {
        return maximumHeartRateBpm;
    }

    /**
     * Sets the value of the maximumHeartRateBpm property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public void setMaximumHeartRateBpm(HeartRateInBeatsPerMinute value) {
        this.maximumHeartRateBpm = value;
    }

    /**
     * Gets the value of the gearWeightKilograms property.
     * 
     */
    public double getGearWeightKilograms() {
        return gearWeightKilograms;
    }

    /**
     * Sets the value of the gearWeightKilograms property.
     * 
     */
    public void setGearWeightKilograms(double value) {
        this.gearWeightKilograms = value;
    }

    /**
     * Gets the value of the heartRateZones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the heartRateZones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHeartRateZones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProfileHeartRateZone }
     * 
     * 
     */
    public List<ProfileHeartRateZone> getHeartRateZones() {
        if (heartRateZones == null) {
            heartRateZones = new ArrayList<ProfileHeartRateZone>();
        }
        return this.heartRateZones;
    }

    /**
     * Gets the value of the speedZones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the speedZones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpeedZones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProfileSpeedZone }
     * 
     * 
     */
    public List<ProfileSpeedZone> getSpeedZones() {
        if (speedZones == null) {
            speedZones = new ArrayList<ProfileSpeedZone>();
        }
        return this.speedZones;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
