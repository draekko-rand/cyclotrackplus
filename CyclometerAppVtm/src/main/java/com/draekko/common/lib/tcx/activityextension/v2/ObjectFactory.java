package com.draekko.common.lib.tcx.activityextension.v2;

import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.garmin.xmlschemas.activityextension.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */

public class ObjectFactory {

    private final static QName _TPX_QNAME =
            new QName("http://www.garmin.com/xmlschemas/ActivityExtension/v2",
                    "TPX");
    private final static QName _LX_QNAME =
            new QName("http://www.garmin.com/xmlschemas/ActivityExtension/v2",
                    "LX");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived
     * classes for package: com.garmin.xmlschemas.activityextension.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivityLapExtension }
     * 
     */
    public ActivityLapExtension createActivityLapExtension() {
        return new ActivityLapExtension();
    }

    /**
     * Create an instance of {@link ActivityTrackpointExtension }
     * 
     */
    public ActivityTrackpointExtension createActivityTrackpointExtension() {
        return new ActivityTrackpointExtension();
    }

    /**
     * Create an instance of {@link Extensions }
     * 
     */
    public Extensions createExtensions() {
        return new Extensions();
    }

}
