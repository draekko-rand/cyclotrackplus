package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.garmin.xmlschemas.trainingcenterdatabase.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */

public class ObjectFactory {

    private final static QName _TrainingCenterDatabase_QNAME =
            new QName("http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2",
                    "TrainingCenterDatabase");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema
     * derived classes for package: com.garmin.xmlschemas.trainingcenterdatabase.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TrainingCenterDatabase }
     * 
     */
    public TrainingCenterDatabase createTrainingCenterDatabase() {
        return new TrainingCenterDatabase();
    }

    /**
     * Create an instance of {@link CustomHeartRateZone }
     * 
     */
    public CustomHeartRateZone createCustomHeartRateZone() {
        return new CustomHeartRateZone();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link NextSport }
     * 
     */
    public NextSport createNextSport() {
        return new NextSport();
    }

    /**
     * Create an instance of {@link ActivityReference }
     * 
     */
    public ActivityReference createActivityReference() {
        return new ActivityReference();
    }

    /**
     * Create an instance of {@link Course }
     * 
     */
    public Course createCourse() {
        return new Course();
    }

    /**
     * Create an instance of {@link ActivityList }
     * 
     */
    public ActivityList createActivityList() {
        return new ActivityList();
    }

    /**
     * Create an instance of {@link Distance }
     * 
     */
    public Distance createDistance() {
        return new Distance();
    }

    /**
     * Create an instance of {@link UserInitiated }
     * 
     */
    public UserInitiated createUserInitiated() {
        return new UserInitiated();
    }

    /**
     * Create an instance of {@link Device }
     * 
     */
    public Device createDevice() {
        return new Device();
    }

    /**
     * Create an instance of {@link HeartRateAsPercentOfMax }
     * 
     */
    public HeartRateAsPercentOfMax createHeartRateAsPercentOfMax() {
        return new HeartRateAsPercentOfMax();
    }

    /**
     * Create an instance of {@link Repeat }
     * 
     */
    public Repeat createRepeat() {
        return new Repeat();
    }

    /**
     * Create an instance of {@link HeartRateBelow }
     * 
     */
    public HeartRateBelow createHeartRateBelow() {
        return new HeartRateBelow();
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link None }
     * 
     */
    public None createNone() {
        return new None();
    }

    /**
     * Create an instance of {@link HeartRateAbove }
     * 
     */
    public HeartRateAbove createHeartRateAbove() {
        return new HeartRateAbove();
    }

    /**
     * Create an instance of {@link ActivityLap }
     * 
     */
    public ActivityLap createActivityLap() {
        return new ActivityLap();
    }

    /**
     * Create an instance of {@link Extensions }
     * 
     */
    public Extensions createExtensions() {
        return new Extensions();
    }

    /**
     * Create an instance of {@link CaloriesBurned }
     * 
     */
    public CaloriesBurned createCaloriesBurned() {
        return new CaloriesBurned();
    }

    /**
     * Create an instance of {@link CourseLap }
     * 
     */
    public CourseLap createCourseLap() {
        return new CourseLap();
    }

    /**
     * Create an instance of {@link MultiSportFolder }
     * 
     */
    public MultiSportFolder createMultiSportFolder() {
        return new MultiSportFolder();
    }

    /**
     * Create an instance of {@link Workouts }
     * 
     */
    public Workouts createWorkouts() {
        return new Workouts();
    }

    /**
     * Create an instance of {@link Folders }
     * 
     */
    public Folders createFolders() {
        return new Folders();
    }

    /**
     * Create an instance of {@link Week }
     * 
     */
    public Week createWeek() {
        return new Week();
    }

    /**
     * Create an instance of {@link NameKeyReference }
     * 
     */
    public NameKeyReference createNameKeyReference() {
        return new NameKeyReference();
    }

    /**
     * Create an instance of {@link CoursePoint }
     * 
     */
    public CoursePoint createCoursePoint() {
        return new CoursePoint();
    }

    /**
     * Create an instance of {@link Version }
     * 
     */
    public Version createVersion() {
        return new Version();
    }

    /**
     * Create an instance of {@link Build }
     * 
     */
    public Build createBuild() {
        return new Build();
    }

    /**
     * Create an instance of {@link MultiSportSession }
     * 
     */
    public MultiSportSession createMultiSportSession() {
        return new MultiSportSession();
    }

    /**
     * Create an instance of {@link FirstSport }
     * 
     */
    public FirstSport createFirstSport() {
        return new FirstSport();
    }

    /**
     * Create an instance of {@link PredefinedSpeedZone }
     * 
     */
    public PredefinedSpeedZone createPredefinedSpeedZone() {
        return new PredefinedSpeedZone();
    }

    /**
     * Create an instance of {@link PredefinedHeartRateZone }
     * 
     */
    public PredefinedHeartRateZone createPredefinedHeartRateZone() {
        return new PredefinedHeartRateZone();
    }

    /**
     * Create an instance of {@link Trackpoint }
     * 
     */
    public Trackpoint createTrackpoint() {
        return new Trackpoint();
    }

    /**
     * Create an instance of {@link Step }
     * 
     */
    public Step createStep() {
        return new Step();
    }

    /**
     * Create an instance of {@link HeartRate }
     * 
     */
    public HeartRate createHeartRate() {
        return new HeartRate();
    }

    /**
     * Create an instance of {@link HistoryFolder }
     * 
     */
    public HistoryFolder createHistoryFolder() {
        return new HistoryFolder();
    }

    /**
     * Create an instance of {@link HeartRateInBeatsPerMinute }
     * 
     */
    public HeartRateInBeatsPerMinute createHeartRateInBeatsPerMinute() {
        return new HeartRateInBeatsPerMinute();
    }

    /**
     * Create an instance of {@link Position }
     * 
     */
    public Position createPosition() {
        return new Position();
    }

    /**
     * Create an instance of {@link Courses }
     * 
     */
    public Courses createCourses() {
        return new Courses();
    }

    /**
     * Create an instance of {@link Plan }
     * 
     */
    public Plan createPlan() {
        return new Plan();
    }

    /**
     * Create an instance of {@link WorkoutList }
     * 
     */
    public WorkoutList createWorkoutList() {
        return new WorkoutList();
    }

    /**
     * Create an instance of {@link Speed }
     * 
     */
    public Speed createSpeed() {
        return new Speed();
    }

    /**
     * Create an instance of {@link CustomSpeedZone }
     * 
     */
    public CustomSpeedZone createCustomSpeedZone() {
        return new CustomSpeedZone();
    }

    /**
     * Create an instance of {@link QuickWorkout }
     * 
     */
    public QuickWorkout createQuickWorkout() {
        return new QuickWorkout();
    }

    /**
     * Create an instance of {@link Workout }
     * 
     */
    public Workout createWorkout() {
        return new Workout();
    }

    /**
     * Create an instance of {@link Cadence }
     * 
     */
    public Cadence createCadence() {
        return new Cadence();
    }

    /**
     * Create an instance of {@link Training }
     * 
     */
    public Training createTraining() {
        return new Training();
    }

    /**
     * Create an instance of {@link CourseList }
     * 
     */
    public CourseList createCourseList() {
        return new CourseList();
    }

    /**
     * Create an instance of {@link Application }
     * 
     */
    public Application createApplication() {
        return new Application();
    }

    /**
     * Create an instance of {@link WorkoutFolder }
     * 
     */
    public WorkoutFolder createWorkoutFolder() {
        return new WorkoutFolder();
    }

    /**
     * Create an instance of {@link Time }
     * 
     */
    public Time createTime() {
        return new Time();
    }

    /**
     * Create an instance of {@link Track }
     * 
     */
    public Track createTrack() {
        return new Track();
    }

    /**
     * Create an instance of {@link CourseFolder }
     * 
     */
    public CourseFolder createCourseFolder() {
        return new CourseFolder();
    }

}
