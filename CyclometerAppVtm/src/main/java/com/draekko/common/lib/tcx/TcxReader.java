package com.draekko.common.lib.tcx;

import android.content.Context;

import io.ticofab.androidgpxparser.parser.domain.TrackPoint;

public class TcxReader {

    private Context mContext;

    public final static String GARMIN_XML_SCHEMA = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}";
    public final static String GARMIN_XML_EXTENSIONS = "http://www.garmin.com/xmlschemas/ActivityExtension/v2}";

    public enum NullValueHandling {
        NONE(1),
        LINEAR_INTERPOLATION(2);

        public final int value;

        private NullValueHandling(int value) {
            this.value = value;
        }
    }

    public TcxReader(Context context) {
        mContext = context;
    }

    public void read(String filename, boolean gps_only, int null_valueHandling) {

    }
}
