package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for BikeData_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BikeData_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}RestrictedToken_t"/>
 *         &lt;element name="OdometerMeters" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="WeightKilograms" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="WheelSize" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}WheelData_t"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="HasCadenceSensor" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class BikeData {

    protected String name;
    protected double odometerMeters;
    protected double weightKilograms;
    protected WheelData wheelSize;
    protected Extensions extensions;
    protected boolean hasCadenceSensor;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the odometerMeters property.
     * 
     */
    public double getOdometerMeters() {
        return odometerMeters;
    }

    /**
     * Sets the value of the odometerMeters property.
     * 
     */
    public void setOdometerMeters(double value) {
        this.odometerMeters = value;
    }

    /**
     * Gets the value of the weightKilograms property.
     * 
     */
    public double getWeightKilograms() {
        return weightKilograms;
    }

    /**
     * Sets the value of the weightKilograms property.
     * 
     */
    public void setWeightKilograms(double value) {
        this.weightKilograms = value;
    }

    /**
     * Gets the value of the wheelSize property.
     * 
     * @return
     *     possible object is
     *     {@link WheelData }
     *     
     */
    public WheelData getWheelSize() {
        return wheelSize;
    }

    /**
     * Sets the value of the wheelSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link WheelData }
     *     
     */
    public void setWheelSize(WheelData value) {
        this.wheelSize = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

    /**
     * Gets the value of the hasCadenceSensor property.
     * 
     */
    public boolean isHasCadenceSensor() {
        return hasCadenceSensor;
    }

    /**
     * Sets the value of the hasCadenceSensor property.
     * 
     */
    public void setHasCadenceSensor(boolean value) {
        this.hasCadenceSensor = value;
    }

}
