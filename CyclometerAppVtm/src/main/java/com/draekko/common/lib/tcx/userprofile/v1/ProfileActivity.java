package com.draekko.common.lib.tcx.userprofile.v1;

/**
 *
 * User Activity Profile
 * 
 * <p>Java class for ProfileActivity_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileActivity_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/UserProfile/v1}AbstractProfileActivity_t">
 *       &lt;attribute name="Sport" use="required" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}Sport_t" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class ProfileActivity
    extends AbstractProfileActivity
{

    protected Sport sport;

    /**
     * Gets the value of the sport property.
     * 
     * @return
     *     possible object is
     *     {@link Sport }
     *     
     */
    public Sport getSport() {
        return sport;
    }

    /**
     * Sets the value of the sport property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sport }
     *     
     */
    public void setSport(Sport value) {
        this.sport = value;
    }

}
