package com.draekko.common.lib.tcx.activityextension.v2;

/**
 * <p>Java class for ActivityLapExtension_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityLapExtension_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AvgSpeed" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MaxBikeCadence" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}CadenceValue_t" minOccurs="0"/>
 *         &lt;element name="AvgRunCadence" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}CadenceValue_t" minOccurs="0"/>
 *         &lt;element name="MaxRunCadence" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}CadenceValue_t" minOccurs="0"/>
 *         &lt;element name="Steps" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="AvgWatts" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="MaxWatts" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class ActivityLapExtension {

    protected Double avgSpeed;
    protected Short maxBikeCadence;
    protected Short avgRunCadence;
    protected Short maxRunCadence;
    protected Integer steps;
    protected Integer avgWatts;
    protected Integer maxWatts;
    protected Extensions extensions;

    /**
     * Gets the value of the avgSpeed property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAvgSpeed() {
        return avgSpeed;
    }

    /**
     * Sets the value of the avgSpeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAvgSpeed(Double value) {
        this.avgSpeed = value;
    }

    /**
     * Gets the value of the maxBikeCadence property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getMaxBikeCadence() {
        return maxBikeCadence;
    }

    /**
     * Sets the value of the maxBikeCadence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMaxBikeCadence(Short value) {
        this.maxBikeCadence = value;
    }

    /**
     * Gets the value of the avgRunCadence property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAvgRunCadence() {
        return avgRunCadence;
    }

    /**
     * Sets the value of the avgRunCadence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAvgRunCadence(Short value) {
        this.avgRunCadence = value;
    }

    /**
     * Gets the value of the maxRunCadence property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getMaxRunCadence() {
        return maxRunCadence;
    }

    /**
     * Sets the value of the maxRunCadence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMaxRunCadence(Short value) {
        this.maxRunCadence = value;
    }

    /**
     * Gets the value of the steps property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSteps() {
        return steps;
    }

    /**
     * Sets the value of the steps property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSteps(Integer value) {
        this.steps = value;
    }

    /**
     * Gets the value of the avgWatts property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAvgWatts() {
        return avgWatts;
    }

    /**
     * Sets the value of the avgWatts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAvgWatts(Integer value) {
        this.avgWatts = value;
    }

    /**
     * Gets the value of the maxWatts property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxWatts() {
        return maxWatts;
    }

    /**
     * Sets the value of the maxWatts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxWatts(Integer value) {
        this.maxWatts = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
