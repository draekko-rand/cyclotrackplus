package com.draekko.common.lib.tcx.userprofile.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Profile with extensions for new bicycle specific data
 * 
 * <p>Java class for BikeProfileActivity_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BikeProfileActivity_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/UserProfile/v1}AbstractProfileActivity_t">
 *       &lt;sequence>
 *         &lt;element name="Bike" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}BikeData_t" maxOccurs="3" minOccurs="3"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Sport" use="required" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}Sport_t" fied="Biking" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class BikeProfileActivity
    extends AbstractProfileActivity
{

    protected List<BikeData> bike;
    protected Sport sport;

    /**
     * Gets the value of the bike property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bike property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBike().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BikeData }
     * 
     * 
     */
    public List<BikeData> getBike() {
        if (bike == null) {
            bike = new ArrayList<BikeData>();
        }
        return this.bike;
    }

    /**
     * Gets the value of the sport property.
     * 
     * @return
     *     possible object is
     *     {@link Sport }
     *     
     */
    public Sport getSport() {
        if (sport == null) {
            return Sport.BIKING;
        } else {
            return sport;
        }
    }

    /**
     * Sets the value of the sport property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sport }
     *     
     */
    public void setSport(Sport value) {
        this.sport = value;
    }

}
