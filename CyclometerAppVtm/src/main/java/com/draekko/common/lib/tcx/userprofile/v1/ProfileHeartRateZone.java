package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for ProfileHeartRateZone_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileHeartRateZone_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}HeartRateZoneNumbers_t"/>
 *         &lt;element name="ViewAs" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}HeartRateType_t"/>
 *         &lt;element name="Low" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}HeartRateInBeatsPerMinute_t"/>
 *         &lt;element name="High" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}HeartRateInBeatsPerMinute_t"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class ProfileHeartRateZone {

    protected int number;
    protected HeartRateType viewAs;
    protected HeartRateInBeatsPerMinute low;
    protected HeartRateInBeatsPerMinute high;

    /**
     * Gets the value of the number property.
     * 
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    public void setNumber(int value) {
        this.number = value;
    }

    /**
     * Gets the value of the viewAs property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateType }
     *     
     */
    public HeartRateType getViewAs() {
        return viewAs;
    }

    /**
     * Sets the value of the viewAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateType }
     *     
     */
    public void setViewAs(HeartRateType value) {
        this.viewAs = value;
    }

    /**
     * Gets the value of the low property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public HeartRateInBeatsPerMinute getLow() {
        return low;
    }

    /**
     * Sets the value of the low property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public void setLow(HeartRateInBeatsPerMinute value) {
        this.low = value;
    }

    /**
     * Gets the value of the high property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public HeartRateInBeatsPerMinute getHigh() {
        return high;
    }

    /**
     * Sets the value of the high property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateInBeatsPerMinute }
     *     
     */
    public void setHigh(HeartRateInBeatsPerMinute value) {
        this.high = value;
    }

}
