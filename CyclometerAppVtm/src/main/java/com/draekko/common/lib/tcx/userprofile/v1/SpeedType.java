package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for SpeedType_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SpeedType_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/UserProfile/v1}Token_t">
 *     &lt;enumeration value="Pace"/>
 *     &lt;enumeration value="Speed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum SpeedType {

    PACE("Pace"),
    SPEED("Speed");
    private final String value;

    SpeedType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SpeedType fromValue(String v) {
        for (SpeedType c: SpeedType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
