package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for HeartRateBelow_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeartRateBelow_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Duration_t">
 *       &lt;sequence>
 *         &lt;element name="HeartRate" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HeartRateValue_t"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class HeartRateBelow
    extends Duration
{

    protected HeartRateValue heartRate;

    /**
     * Gets the value of the heartRate property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateValue }
     *     
     */
    public HeartRateValue getHeartRate() {
        return heartRate;
    }

    /**
     * Sets the value of the heartRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateValue }
     *     
     */
    public void setHeartRate(HeartRateValue value) {
        this.heartRate = value;
    }

}
