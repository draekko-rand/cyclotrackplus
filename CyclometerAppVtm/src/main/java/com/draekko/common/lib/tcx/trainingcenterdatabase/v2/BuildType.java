package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for BuildType_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildType_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Token_t">
 *     &lt;enumeration value="Internal"/>
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="Beta"/>
 *     &lt;enumeration value="Release"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum BuildType {

    INTERNAL("Internal"),
    ALPHA("Alpha"),
    BETA("Beta"),
    RELEASE("Release");
    private final String value;

    BuildType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildType fromValue(String v) {
        for (BuildType c: BuildType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
