package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for SensorState_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SensorState_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Token_t">
 *     &lt;enumeration value="Present"/>
 *     &lt;enumeration value="Absent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum SensorState {

    PRESENT("Present"),
    ABSENT("Absent");
    private final String value;

    SensorState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SensorState fromValue(String v) {
        for (SensorState c: SensorState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
