package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for CustomHeartRateZone_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomHeartRateZone_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Zone_t">
 *       &lt;sequence>
 *         &lt;element name="Low" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HeartRateValue_t"/>
 *         &lt;element name="High" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HeartRateValue_t"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class CustomHeartRateZone
    extends Zone
{

    protected HeartRateValue low;
    protected HeartRateValue high;

    /**
     * Gets the value of the low property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateValue }
     *     
     */
    public HeartRateValue getLow() {
        return low;
    }

    /**
     * Sets the value of the low property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateValue }
     *     
     */
    public void setLow(HeartRateValue value) {
        this.low = value;
    }

    /**
     * Gets the value of the high property.
     * 
     * @return
     *     possible object is
     *     {@link HeartRateValue }
     *     
     */
    public HeartRateValue getHigh() {
        return high;
    }

    /**
     * Sets the value of the high property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeartRateValue }
     *     
     */
    public void setHigh(HeartRateValue value) {
        this.high = value;
    }

}
