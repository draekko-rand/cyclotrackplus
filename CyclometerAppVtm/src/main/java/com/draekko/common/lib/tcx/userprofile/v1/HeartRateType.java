package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for HeartRateType_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HeartRateType_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/UserProfile/v1}Token_t">
 *     &lt;enumeration value="Percent Max"/>
 *     &lt;enumeration value="Beats Per Minute"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum HeartRateType {

    PERCENT_MAX("Percent Max"),
    BEATS_PER_MINUTE("Beats Per Minute");
    private final String value;

    HeartRateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HeartRateType fromValue(String v) {
        for (HeartRateType c: HeartRateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
