package com.draekko.common.lib.tcx.userprofile.v1;

/**
 * <p>Java class for ProfileSpeedZone_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileSpeedZone_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}SpeedZoneNumbers_t"/>
 *         &lt;element name="Name" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}RestrictedToken_t" minOccurs="0"/>
 *         &lt;element name="Value" type="{http://www.garmin.com/xmlschemas/UserProfile/v1}CustomSpeedZone_t"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class ProfileSpeedZone {

    protected int number;
    protected String name;
    protected CustomSpeedZone value;

    /**
     * Gets the value of the number property.
     * 
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    public void setNumber(int value) {
        this.number = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link CustomSpeedZone }
     *     
     */
    public CustomSpeedZone getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomSpeedZone }
     *     
     */
    public void setValue(CustomSpeedZone value) {
        this.value = value;
    }

}
