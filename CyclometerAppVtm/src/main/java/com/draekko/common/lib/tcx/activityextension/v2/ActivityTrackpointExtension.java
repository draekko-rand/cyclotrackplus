package com.draekko.common.lib.tcx.activityextension.v2;

/**
 * <p>Java class for ActivityTrackpointExtension_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityTrackpointExtension_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Speed" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="RunCadence" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}CadenceValue_t" minOccurs="0"/>
 *         &lt;element name="Watts" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CadenceSensor" type="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}CadenceSensorType_t" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class ActivityTrackpointExtension {

    protected Double speed;
    protected Short runCadence;
    protected Integer watts;
    protected Extensions extensions;
    protected CadenceSensorType cadenceSensor;

    /**
     * Gets the value of the speed property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSpeed(Double value) {
        this.speed = value;
    }

    /**
     * Gets the value of the runCadence property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getRunCadence() {
        return runCadence;
    }

    /**
     * Sets the value of the runCadence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setRunCadence(Short value) {
        this.runCadence = value;
    }

    /**
     * Gets the value of the watts property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWatts() {
        return watts;
    }

    /**
     * Sets the value of the watts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWatts(Integer value) {
        this.watts = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

    /**
     * Gets the value of the cadenceSensor property.
     * 
     * @return
     *     possible object is
     *     {@link CadenceSensorType }
     *     
     */
    public CadenceSensorType getCadenceSensor() {
        return cadenceSensor;
    }

    /**
     * Sets the value of the cadenceSensor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CadenceSensorType }
     *     
     */
    public void setCadenceSensor(CadenceSensorType value) {
        this.cadenceSensor = value;
    }

}
