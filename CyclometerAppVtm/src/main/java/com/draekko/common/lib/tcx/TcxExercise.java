package com.draekko.common.lib.tcx;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Map;

public class TcxExercise {

    private ArrayList<TcxTrackPoint> trackpoints;
    private ArrayList<TcxLap> laps;
    private String activity_type;
    private int calories;
    private float hr_avg;
    private float hr_max;
    private float hr_min;
    private float duration;
    private float max_speed;
    private float avg_speed;
    private DateTime start_time;
    private DateTime end_time;
    private float cadence_avg;
    private float cadence_max;
    private float ascent;
    private float descent;
    private float distance;
    private float altitude_avg;
    private float altitude_min;
    private float altitude_max;
    private TcxAuthor author;
    private Map tpx_ext_stats;
    private Map lx_ext;

    public TcxExercise() {}

    public void setTrackpoints(ArrayList<TcxTrackPoint> trackpoints) {
        this.trackpoints = trackpoints;
    }

    public void setLaps(ArrayList<TcxLap> laps) {
        this.laps = laps;
    }

    public void setActivityType(String activity_type) {
        this.activity_type = activity_type;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setHrAvg(float hr_avg) {
        this.hr_avg = hr_avg;
    }

    public void setHrMin(float hr_min) {
        this.hr_min = hr_min;
    }

    public void setHrMax(float hr_max) {
        this.hr_max = hr_max;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setMaxSpeed(float max_speed) {
        this.max_speed = max_speed;
    }

    public void setAvgSpeed(float avg_speed) {
        this.avg_speed = avg_speed;
    }

    public void setStartTime(DateTime start_time) {
        this.start_time = start_time;
    }

    public void setEndTime(DateTime end_time) {
        this.end_time = end_time;
    }

    public void setCadenceMax(float cadence_max) {
        this.cadence_max = cadence_max;
    }

    public void setCadenceAvg(float cadence_avg) {
        this.cadence_avg = cadence_avg;
    }

    public void setAscent(float ascent) {
        this.ascent = ascent;
    }

    public void setDescent(float descent) {
        this.descent = descent;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public void setAltitudeAvg(float altitude_avg) {
        this.altitude_avg = altitude_avg;
    }

    public void setAltitudeMin(float altitude_min) {
        this.altitude_min = altitude_min;
    }

    public void setAltitudeMax(float altitude_max) {
        this.altitude_max = altitude_max;
    }

    public void setAuthor(TcxAuthor author) {
        this.author = author;
    }

    public void setTpxExtStats(Map tpx_ext_stats) {
        this.tpx_ext_stats = tpx_ext_stats;
    }

    public void setLxExt(Map lx_ext) {
        this.lx_ext = lx_ext;
    }

    public ArrayList<TcxTrackPoint> getTrackpoints() {
        return trackpoints;
    }

    public ArrayList<TcxLap> getLaps() {
        return laps;
    }

    public String getActivityType() {
        return activity_type;
    }

    public int getCalories() {
        return calories;
    }

    public float getHrAvg() {
        return hr_avg;
    }

    public float getHrMin() {
        return hr_min;
    }

    public float getHrMax() {
        return hr_max;
    }

    public float getDuration() {
        return duration;
    }

    public float getMaxSpeed() {
        return max_speed;
    }

    public float getAvgSpeed() {
        return avg_speed;
    }

    public DateTime getStartTime() {
        return start_time;
    }

    public DateTime getEndTime() {
        return end_time;
    }

    public float getCadenceMax() {
        return cadence_max;
    }

    public float getCadenceAvg() {
        return cadence_avg;
    }

    public float getAscent() {
        return ascent;
    }

    public float getDescent() {
        return descent;
    }

    public float getDistance() {
        return distance;
    }

    public float getAltitudeAvg() {
        return altitude_avg;
    }

    public float getAltitudeMin() {
        return altitude_min;
    }

    public float getAltitudeMax() {
        return altitude_max;
    }

    public TcxAuthor getAuthor() {
        return author;
    }

    public Map getTpxExtStats() {
        return tpx_ext_stats;
    }

    public Map getLxExt() {
        return lx_ext;
    }
}
