package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for Workouts_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Workouts_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Running" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}WorkoutFolder_t"/>
 *         &lt;element name="Biking" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}WorkoutFolder_t"/>
 *         &lt;element name="Other" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}WorkoutFolder_t"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class Workouts {

    protected WorkoutFolder running;
    protected WorkoutFolder biking;
    protected WorkoutFolder other;
    protected Extensions extensions;

    /**
     * Gets the value of the running property.
     * 
     * @return
     *     possible object is
     *     {@link WorkoutFolder }
     *     
     */
    public WorkoutFolder getRunning() {
        return running;
    }

    /**
     * Sets the value of the running property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkoutFolder }
     *     
     */
    public void setRunning(WorkoutFolder value) {
        this.running = value;
    }

    /**
     * Gets the value of the biking property.
     * 
     * @return
     *     possible object is
     *     {@link WorkoutFolder }
     *     
     */
    public WorkoutFolder getBiking() {
        return biking;
    }

    /**
     * Sets the value of the biking property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkoutFolder }
     *     
     */
    public void setBiking(WorkoutFolder value) {
        this.biking = value;
    }

    /**
     * Gets the value of the other property.
     * 
     * @return
     *     possible object is
     *     {@link WorkoutFolder }
     *     
     */
    public WorkoutFolder getOther() {
        return other;
    }

    /**
     * Sets the value of the other property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkoutFolder }
     *     
     */
    public void setOther(WorkoutFolder value) {
        this.other = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
