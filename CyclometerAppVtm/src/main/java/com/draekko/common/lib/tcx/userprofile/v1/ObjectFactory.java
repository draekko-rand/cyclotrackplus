package com.draekko.common.lib.tcx.userprofile.v1;

import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.garmin.xmlschemas.userprofile.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */

public class ObjectFactory {

    private final static QName _Profile_QNAME =
            new QName("http://www.garmin.com/xmlschemas/UserProfile/v1",
                    "Profile");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived
     * classes for package: com.garmin.xmlschemas.userprofile.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProfileData }
     * 
     */
    public ProfileData createProfileData() {
        return new ProfileData();
    }

    /**
     * Create an instance of {@link CustomSpeedZone }
     * 
     */
    public CustomSpeedZone createCustomSpeedZone() {
        return new CustomSpeedZone();
    }

    /**
     * Create an instance of {@link WheelData }
     * 
     */
    public WheelData createWheelData() {
        return new WheelData();
    }

    /**
     * Create an instance of {@link BikeData }
     * 
     */
    public BikeData createBikeData() {
        return new BikeData();
    }

    /**
     * Create an instance of {@link ProfileHeartRateZone }
     * 
     */
    public ProfileHeartRateZone createProfileHeartRateZone() {
        return new ProfileHeartRateZone();
    }

    /**
     * Create an instance of {@link Extensions }
     * 
     */
    public Extensions createExtensions() {
        return new Extensions();
    }

    /**
     * Create an instance of {@link ProfileActivity }
     * 
     */
    public ProfileActivity createProfileActivity() {
        return new ProfileActivity();
    }

    /**
     * Create an instance of {@link ProfileSpeedZone }
     * 
     */
    public ProfileSpeedZone createProfileSpeedZone() {
        return new ProfileSpeedZone();
    }

    /**
     * Create an instance of {@link BikeProfileActivity }
     * 
     */
    public BikeProfileActivity createBikeProfileActivity() {
        return new BikeProfileActivity();
    }

    /**
     * Create an instance of {@link HeartRateInBeatsPerMinute }
     * 
     */
    public HeartRateInBeatsPerMinute createHeartRateInBeatsPerMinute() {
        return new HeartRateInBeatsPerMinute();
    }

}
