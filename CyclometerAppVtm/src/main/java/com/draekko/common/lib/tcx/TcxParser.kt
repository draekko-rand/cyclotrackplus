package com.draekko.common.lib.tcx

import android.content.Context
import android.util.Log
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.Activity
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.ActivityList
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.CourseList
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.MultiSportSession
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.NextSport
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.Sport
import com.draekko.common.lib.tcx.trainingcenterdatabase.v2.TrainingCenterDatabase
import com.google.android.gms.maps.model.LatLng
import org.oscim.core.GeoPoint
import org.xml.sax.Attributes
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.StringReader
import java.text.DecimalFormat
import java.time.Instant
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar
import javax.xml.parsers.ParserConfigurationException
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory
import kotlin.math.roundToInt


class TcxParser(context: Context, combinedSports: Boolean) {

    private var appContext: Context
    private var combineAllSports: Boolean = false

    init {
        appContext = context
        combineAllSports = combinedSports
    }

    companion object {
        var latlngSegments = arrayListOf<MutableList<GeoPoint>>()

        val TAG_TRAININGCENTERDATABASE: String = "trainingcenterdatabase"
        val TAG_ACTIVITIES: String = "activities"
        val TAG_ACTIVITY: String = "activity"
        val TAG_MULTISPORTSESSION: String = "multisportsession"
        val TAG_ID: String = "id"
        val TAG_LAP: String = "lap"
        val TAG_TRACK: String = "track"
        val TAG_TRACKPOINT: String = "trackpoint"
        val TAG_TIME: String = "time"
        val TAG_POSITION: String = "position"
        val TAG_LATITUDEDEGREES: String = "latitudedegrees"
        val TAG_LONGITUDEDEGREES: String = "longitudedegrees"
        val TAG_COURSES: String = "courses"
        val TAG_FIRSTSPORT: String = "firstsport"
        val TAG_NEXTSPORT: String = "nextsport"

        /*
        val TAG_TOTALTIMESECONDS: String = "totaltimeseconds"
        val TAG_DISTANCEMETERS: String = "distancemeters"
        val TAG_CALORIES: String = "calories"
        val TAG_INTENSITY: String = "intensity"
        val TAG_TRIGGERMETHOD: String = "triggermethod"
        val TAG_TYPE: String = "type"
        val TAG_ALTITUDEMETERS: String = "altitudemeters"
        val TAG_SENSORSTATE: String = "sensorstate"
        val TAG_CREATOR: String = "creator"
        val TAG_NAME: String = "name"
        val TAG_UNITID: String = "unitid"
        val TAG_LANGID: String = "langid"
        val TAG_BUILD: String = "build"
        val TAG_PARTNUMBER: String = "partnumber"
        val TAG_PRODUCTID: String = "productid"
        val TAG_VERSION: String = "version"
        val TAG_VERSIONMAJOR: String = "versionmajor"
        val TAG_VERSIONMINOR: String = "versionminor"
        val TAG_BUILDMAJOR: String = "buildmajor"
        val TAG_BUILDMINOR: String = "buildminor"
        val TAG_COURSE: String = "course"
        val TAG_FOLDER: String = "folder"
        val TAG_ACTIVITYREF: String = "activityref"
        val TAG_MULTISPORTACTIVITYREF: String = "multisportactivityref"
        val TAG_WEEK: String = "week"

        val TAG_TRANSITION: String = "transition"
        val TAG_BUILDER: String = "builder"
        val TAG_NOTES: String = "notes"
        val TAG_NAMES: String = "names"
        val TAG_PLAN: String = "plan"
        val TAG_TRAINING: String = "training"
        val TAG_QUICKWORKOUTRESULTS: String = "quickworkoutresults"

        val TAG_RUNNING = "running"
        val TAG_BIKING = "biking"
        val TAG_OTHER = "other"

        val TAG_STEP = "step"
        val tag_scheduledon = "scheduledon"
        val TAG_WORKOUTNAMEREF = "workoutnameref"
        val TAG_STEPID = "stepid"
        val TAG_REPETITIONS = "repetitions"
        val TAG_CHILD = "child"
        val TAG_TARGET = "target"
        val TAG_SPEEDZONE = "speedzone"
        val TAG_HEARTRATEZONE = "heartratezone"

        val TAG_AVERAGEHEARTRATEBPM: String = "averageheartratebpm"
        val TAG_VALUE: String = "value"
        val TAG_CADENCE: String = "cadence"
        val TAG_HEARTRATEBPM: String = "heartratebpm"

        val TAG_EXTENSIONS: String = "extensions"
        val TAG_TPX: String = "tpx"
        val TAG_SPEED: String = "speed"
        val TAG_WATTS: String = "watts"

        val TAG_BEGINPOSITION: String = "beginposition"
        val TAG_ENDPOSITION: String = "endposition"
         */

        //var trainingCenterDatabase: TrainingCenterDatabase = TrainingCenterDatabase()
    }

    //fun getTrainingCenterDatabase(): TrainingCenterDatabase? {
    //    return trainingCenterDatabase
    //}

    fun getSegmentList(): List<MutableList<GeoPoint>> {
        return latlngSegments
    }

    fun parseTcxFile(file: File): Boolean {
        if (file != null) {
            try {
                val spf = SAXParserFactory.newInstance()
                val sp: SAXParser = spf.newSAXParser()
                val xr = sp.xmlReader
                val xml: HandleTcxFile = HandleTcxFile()
                xr.contentHandler = xml
                val inputStream = FileInputStream(file)
                val inputSource = InputSource()
                inputSource.encoding = "UTF-8"
                inputSource.byteStream = inputStream
                xr.parse(inputSource)
                return true
            } catch (e: ParserConfigurationException) {
                e.printStackTrace()
            } catch (e: SAXException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
        }
        return false
    }

    class HandleTcxFile: DefaultHandler() {

        private var hasMultiSport = false
        private var hasCourse = false

        private var in_trainingcenterdatabase = false

        private var in_activities = false

        private var in_firstsport = false
        private var in_nextsport = false
        private var in_nextsport_id = false

        private var in_activity = false // attribute Sport String
        private var in_activity_id = false

        private var in_lap = false // attribute StartTime String

        private var in_courses = false
        private var in_course = false
        private var in_name = false

        private var in_multisportsession = false

        private var in_id = false

        private var multisport_index = 0
        private var multisport_next_index = 0
        private var activity_index = 0

        private var in_track = false
        private var in_trackpoint = false

        private var in_tp_time = false
        private var in_tp_position = false
        private var in_tp_position_lat = false
        private var in_tp_position_lng = false
        private var in_tp_alt = false
        private var in_tp_dist = false
        private var in_tp_sensor_state = false
        private var in_tp_ext_tpx_watts = false
        private var in_tp_ext_tpx_speed = false
        private var in_tp_ext_tpx = false
        private var in_tp_extensions = false

        var segmentTrack: MutableList<GeoPoint> = ArrayList()

        var latitudeStr: String = ""
        var longitudeStr: String = ""

        @Throws(SAXException::class)
        override fun startDocument() {
        }

        @Throws(SAXException::class)
        override fun endDocument() {
        }

        override fun startElement(
            namespaceURI: String?,
            localName: String?,
            qName: String?,
            atts: Attributes?
        ) {
            if (localName.isNullOrEmpty()) {
                return;
            }
            val localStartName = localName!!.lowercase(Locale.US)

            if (localStartName == TAG_TRAININGCENTERDATABASE) {
                in_trainingcenterdatabase = true
            } else if (localStartName == TAG_ACTIVITIES) {
                in_activities = true
            } else if (localStartName == TAG_COURSES) {
                in_activities = true
            } else if (localStartName == TAG_MULTISPORTSESSION) {
                in_multisportsession = true
            } else if (localStartName == TAG_ID) {
                in_id = true
            } else if (localStartName == TAG_FIRSTSPORT) {
                in_firstsport = true
            } else if (localStartName == TAG_NEXTSPORT) {
                in_nextsport = true
            } else if (localStartName == TAG_ACTIVITY) {
                in_activity = true
            } else if (localStartName == TAG_LAP) {
                in_lap = true
            } else if (localStartName == TAG_TRACK) {
                in_track = true
                segmentTrack = ArrayList()
            } else if (localStartName == TAG_TRACKPOINT) {
                in_trackpoint = true
            } else if (localStartName == TAG_TIME) {
                if (in_track && in_trackpoint) {
                    in_tp_time = true
                }
            } else if (localStartName == TAG_POSITION) {
                in_tp_position = true
            } else if (localStartName == TAG_LATITUDEDEGREES) {
                if (in_track && in_trackpoint && in_tp_position) {
                    in_tp_position_lat = true
                }
            } else if (localStartName == TAG_LONGITUDEDEGREES) {
                if (in_track && in_trackpoint && in_tp_position) {
                    in_tp_position_lng = true
                }
            }
        }

        var index = 0
        override fun endElement(namespaceURI: String?, localName: String?, qName: String?) {
            val localEndName = localName!!.toLowerCase(Locale.US)
            if (localEndName == TAG_TRAININGCENTERDATABASE) {
                in_trainingcenterdatabase = false
            } else if (localEndName == TAG_ACTIVITIES) {
                in_activities = false
            } else if (localEndName == TAG_COURSES) {
                in_activities = false
            } else if (localEndName == TAG_MULTISPORTSESSION) {
                in_multisportsession = false
            } else if (localEndName == TAG_ID) {
                in_id = false
            } else if (localEndName == TAG_FIRSTSPORT) {
                in_firstsport = false
            } else if (localEndName == TAG_NEXTSPORT) {
                in_nextsport = false
            } else if (localEndName == TAG_ACTIVITY) {
                in_activity = false
            } else if (localEndName == TAG_LAP) {
                in_lap = false
            } else if (localEndName == TAG_TRACK) {
                in_track = false
                latlngSegments.add(segmentTrack)
            } else if (localEndName == TAG_TRACKPOINT) {
                in_trackpoint = false
            } else if (localEndName == TAG_TIME) {
                if (in_track && in_trackpoint) {
                    in_tp_time = false
                }
            } else if (localEndName == TAG_POSITION) {
                in_tp_position = false
                if (latitudeStr.isNullOrEmpty() || longitudeStr.isNullOrEmpty()) {
                    return
                }

                var latitude = latitudeStr.toDouble()
                var longitude = longitudeStr.toDouble()

                segmentTrack.add(GeoPoint(latitude, longitude))

                latitudeStr = ""
                longitudeStr = ""
            } else if (localEndName == TAG_LATITUDEDEGREES) {
                if (in_track && in_trackpoint && in_tp_position) {
                    in_tp_position_lat = false
                }
            } else if (localEndName == TAG_LONGITUDEDEGREES) {
                if (in_track && in_trackpoint && in_tp_position) {
                    in_tp_position_lng = false
                }
            }
        }

        override fun characters(buffer: CharArray?, start: Int, length: Int) {
            if (buffer == null) {
                return
            }

            var data: String = ""
            for (ch in start..length-1) {
                data += buffer[ch].toChar().toString()
            }

            if (in_track && in_trackpoint) {
                if (in_tp_position) {
                    if (in_tp_position_lat) {
                        latitudeStr += data
                    }
                    if (in_tp_position_lng) {
                        longitudeStr += data
                    }
                }
            }
        }
    }
}