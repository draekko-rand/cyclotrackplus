package com.draekko.common.lib.tcx.trackpointextension.v2;

import java.math.BigDecimal;

/**
 * 
 *     This type contains data fields that cannot
 *     be represented in track points in GPX 1.1 instances.
 *     
 * 
 * <p>Java class for TrackPointExtension_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TrackPointExtension_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="atemp" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}DegreesCelsius_t" minOccurs="0"/>
 *         &lt;element name="wtemp" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}DegreesCelsius_t" minOccurs="0"/>
 *         &lt;element name="depth" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}Meters_t" minOccurs="0"/>
 *         &lt;element name="hr" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}BeatsPerMinute_t" minOccurs="0"/>
 *         &lt;element name="cad" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}RevolutionsPerMinute_t" minOccurs="0"/>
 *         &lt;element name="speed" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}MetersPerSecond_t" minOccurs="0"/>
 *         &lt;element name="course" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}DegreesTrue_t" minOccurs="0"/>
 *         &lt;element name="bearing" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}DegreesTrue_t" minOccurs="0"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/TrackPointExtension/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class TrackPointExtension {

    protected Double atemp;
    protected Double wtemp;
    protected Double depth;
    protected Short hr;
    protected Short cad;
    protected Double speed;
    protected BigDecimal course;
    protected BigDecimal bearing;
    protected Extensions extensions;

    /**
     * Gets the value of the atemp property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAtemp() {
        return atemp;
    }

    /**
     * Sets the value of the atemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAtemp(Double value) {
        this.atemp = value;
    }

    /**
     * Gets the value of the wtemp property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getWtemp() {
        return wtemp;
    }

    /**
     * Sets the value of the wtemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setWtemp(Double value) {
        this.wtemp = value;
    }

    /**
     * Gets the value of the depth property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDepth() {
        return depth;
    }

    /**
     * Sets the value of the depth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDepth(Double value) {
        this.depth = value;
    }

    /**
     * Gets the value of the hr property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getHr() {
        return hr;
    }

    /**
     * Sets the value of the hr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setHr(Short value) {
        this.hr = value;
    }

    /**
     * Gets the value of the cad property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCad() {
        return cad;
    }

    /**
     * Sets the value of the cad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCad(Short value) {
        this.cad = value;
    }

    /**
     * Gets the value of the speed property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSpeed(Double value) {
        this.speed = value;
    }

    /**
     * Gets the value of the course property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCourse() {
        return course;
    }

    /**
     * Sets the value of the course property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCourse(BigDecimal value) {
        this.course = value;
    }

    /**
     * Gets the value of the bearing property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBearing() {
        return bearing;
    }

    /**
     * Sets the value of the bearing property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBearing(BigDecimal value) {
        this.bearing = value;
    }

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
