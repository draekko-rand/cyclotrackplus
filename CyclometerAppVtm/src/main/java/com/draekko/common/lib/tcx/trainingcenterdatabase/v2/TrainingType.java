package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for TrainingType_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TrainingType_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Token_t">
 *     &lt;enumeration value="Workout"/>
 *     &lt;enumeration value="Course"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

public enum TrainingType {

    WORKOUT("Workout"),
    COURSE("Course");
    private final String value;

    TrainingType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrainingType fromValue(String v) {
        for (TrainingType c: TrainingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
