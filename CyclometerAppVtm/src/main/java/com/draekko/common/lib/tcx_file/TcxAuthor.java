package com.draekko.common.lib.tcx_file;

public class TcxAuthor {
    private String name;
    private int version_major;
    private int version_minor;
    private int build_major;
    private int build_minor;

    public TcxAuthor() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setVersion_major(int version_major) {
        this.version_major = version_major;
    }

    public void setVersion_minor(int version_minor) {
        this.version_minor = version_minor;
    }

    public void setBuild_major(int build_major) {
        this.build_major = build_major;
    }

    public void setBuild_minor(int build_minor) {
        this.build_minor = build_minor;
    }

    public String getName() {
        return name;
    }

    public int getBuild_major() {
        return build_major;
    }

    public int getBuild_minor() {
        return build_minor;
    }

    public int getVersion_major() {
        return version_major;
    }

    public int getVersion_minor() {
        return version_minor;
    }
}
