package com.draekko.common.lib.tcx.activityextension.v2;

/**
 * <p>Java class for CadenceSensorType_t.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CadenceSensorType_t">
 *   &lt;restriction base="{http://www.garmin.com/xmlschemas/ActivityExtension/v2}Token_t">
 *     &lt;enumeration value="Footpod"/>
 *     &lt;enumeration value="Bike"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
public enum CadenceSensorType {

    FOOTPOD("Footpod"),
    BIKE("Bike");
    private final String value;

    CadenceSensorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CadenceSensorType fromValue(String v) {
        for (CadenceSensorType c: CadenceSensorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
