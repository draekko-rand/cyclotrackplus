package com.draekko.common.lib.tcx.trainingcenterdatabase.v2;

/**
 * <p>Java class for HeartRate_t complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeartRate_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Target_t">
 *       &lt;sequence>
 *         &lt;element name="HeartRateZone" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Zone_t"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

public class HeartRate
    extends Target
{

    protected Zone heartRateZone;

    /**
     * Gets the value of the heartRateZone property.
     * 
     * @return
     *     possible object is
     *     {@link Zone }
     *     
     */
    public Zone getHeartRateZone() {
        return heartRateZone;
    }

    /**
     * Sets the value of the heartRateZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Zone }
     *     
     */
    public void setHeartRateZone(Zone value) {
        this.heartRateZone = value;
    }

}
